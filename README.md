
# Uplift README

## About Uplift

Uplift is a helper library for writing provisioning and extension DLLs for Microsoft FIM. It is called Uplift because it under-girds synchronization logic, allowing a simpler functional syntax rather than traditional imperative logic.

Uplift provides utilities to aid in attribute flow and provisioning, writing log entries, sending notification emails, and looking up values in SQL and XML files.

Uplift was created by SoftwareIDM inc. and has been used as licensed software for several years in enterprise FIM deployments.
For enterprise support, contact [SoftwareIDM](http://softwareidm.com/contact).

Uplift is released on this site as open source software under the Ms-RL license (Microsoft Reciprocal License).

[Uplift Documentation](https://softwareidm.com/docs/uplift)

## Installing

* Download the zip file or clone the repository.
* Open Uplift/Uplift.sln and build the extensions.
* Add the projects to your rules extension solution, or add references to SoftwareIDM.Uplift.dll

