#FIM Sync Configuration Deployer
Function TestFilePath {
    Param (
        [Parameter(Mandatory=$True)]
                                [string]$Path,
        [Parameter(Mandatory=$True)]
                                [bool]$CreatePath
        )
    if((Test-Path -Path $Path) -eq $false -and $CreatePath)
    {
        #Path Does not exist and we should create it
        try
        {
            New-Item -ItemType directory -Path $Path
        }
        catch
        {
            Write-Host $error[0]
        }
        if((Test-Path $Path) -eq $true)
        {
            return $true
        }
        else
        {
            return $false
        }
    }
    elseif((Test-Path -Path $Path) -eq $false -and !$CreatePath)
    {
        #Path does not exist and we should not create it
        return $false
    }
    else
    {
        #Path already exists
        return $true
    }
}
Function WriteXml
{
    PARAM (
        [Parameter(Mandatory=$True)]
                                [xml]$SourceDocument,
        [Parameter(Mandatory=$True)]
                                [xml]$TargetDocument,
        [Parameter(Mandatory=$True)]
                                [string]$NodePath,
        [Parameter(Mandatory=$True)]
                                [string]$MaName
    )
    if($SourceDocument.SelectSingleNode($NodePath) -and $TargetDocument.SelectSingleNode($NodePath))
    {
        Write-Host "Replacing $NodePath for MA: $MaName"
        Try
        {
            $TargetDocument.SelectSingleNode($NodePath).InnerXML = $SourceDocument.SelectSingleNode($NodePath).InnerXML
            return $true
        }
        catch
        {
            Write-Host $error[0]
            return $false
        }
    }
    else
    {
        Write-Host "MA: $MaName does not have node $NodePath"
        return $false
    }
}
Function ConvertMaXmlToNextEnvironment
{
    PARAM ([string]$LowerEnvPath, [string]$TargetEnvPath, [string]$NewXmlDocPath, $guidMap)
    #Test if the paths we are given exist   
    if((TestFilePath $LowerEnvPath $false) -eq $false)
    {
        Write-Host "Error: Lower Environment Path Could Not be found"
        exit;
    }
    if((TestFilePath $TargetEnvPath $false) -eq $false)
    {
        Write-Host "Error: Target Environment Path Could Not be found"
        exit;
    }
    if((TestFilePath $NewXmlDocPath $true) -eq $false)
    {
        Write-Host "Error: New Document Path could not be found or created"
        exit;
    }

    Get-ChildItem $LowerEnvPath -Filter *.xml | `
    Foreach-Object {

		$devText = [IO.File]::ReadAllText($_.Fullname)
		$guidMap.Keys | % { $devText = $devText -ireplace $_, $guidMap.Item($_) }
		$DevXmlDocument = [xml]$devText

		$targetName = $_.Name
		
		if ($guidMap -ne $null) {
			$guidMap.Keys | % { $targetName = $targetName -replace $_, $guidMap.Item($_) }
		}

        $ProdFile = $TargetEnvPath + '\' + $targetname
        $NewFile = $NewXmlDocPath + '\' + $targetname

		$prodText = [IO.File]::ReadAllText($ProdFile)
		$guidMap.Keys | % { $prodText = $prodText -ireplace $_, $guidMap.Item($_) }
        
		$ProdXmlDocument = [xml]$prodText
        
		if($_.Name -ne 'mv.xml')
        {
            try
            {
                $CurrentMA = $DevXmlDocument."saved-ma-configuration"."ma-data"."name"
                Write-Host "Processing $CurrentMA"
            }
            Catch
            {
                 $error[0]
            }
            #private-configuration
            $Result = WriteXml $ProdXmlDocument `
                     $DevXmlDocument `
                     "//saved-ma-configuration//ma-data//private-configuration" `
                     $CurrentMA
            #Partition Data is not the same from env to env
            $Result = WriteXml $ProdXmlDocument `
                     $DevXmlDocument `
                     "//saved-ma-configuration//ma-data//ma-partition-data" `
                     $CurrentMA
            #Connection information for Group and user migration               
           $Result = WriteXml $ProdXmlDocument `
                     $DevXmlDocument `
                     "//saved-ma-configuration//ma-data//controller-configuration//impersonation" `
                     $CurrentMA
            #Run Profiles will not be moved from Env to Env
           $Result = WriteXml $ProdXmlDocument `
                     $DevXmlDocument `
                     "//saved-ma-configuration//ma-data//ma-run-data" `
                     $CurrentMA
           #sql ma connection information
           $Result = WriteXml $ProdXmlDocument `
                     $DevXmlDocument `
                     "//saved-ma-configuration//ma-data//connection-info" `
                     $CurrentMA
            #Fim Portal Connection Info 
           $Result = WriteXml $ProdXmlDocument `
                     $DevXmlDocument `
                     "//saved-ma-configuration//ma-data//fimma-configuration//connection-info" `
                     $CurrentMA
        }
        #Save to new file
        $DevXmlDocument.Save($NewFile)
    }
}

$devToProdGuidMap = @{
	#"<dev-guid>" = "<prod-guid"; # Miss-matched MA
}
 
$NewPath = "$pwd\CompareConfigOutput"
$DevPath = "$pwd\DEVConfig-Load"
$ProdPath = "$pwd\QAConfig-Operational"
cls
ConvertMaXmlToNextEnvironment $DevPath $ProdPath $NewPath $devToProdGuidMap