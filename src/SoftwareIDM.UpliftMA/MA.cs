﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using Microsoft.Extensions.Logging;
using Microsoft.MetadirectoryServices;
using SoftwareIDM.PanelClient.Helpers;
using SoftwareIDM.PanelModel.Helpers;
using SoftwareIDM.PanelModel.Models;
using SoftwareIDM.Uplift;

namespace SoftwareIDM.UpliftMA
{
    public class MA : 
        IMAExtensible2GetCapabilitiesEx,
        IMAExtensible2CallImport, 
        IMAExtensible2CallExport, 
        IMAExtensible2GetSchema, 
        IMAExtensible2GetParameters
    {
        public MA()
        {
            UpliftEngine.Config.Init();
        }

        public int ImportDefaultPageSize => 100;

        public int ImportMaxPageSize => 1000;

        public int ExportDefaultPageSize => 500;

        public int ExportMaxPageSize => 2000;

        readonly static Dictionary<string, SystemSchema> _schemas = new Dictionary<string, SystemSchema>();
        SystemSchema _importSchema = null;
        readonly Dictionary<string, string> _mapAttr = new Dictionary<string, string>();
        readonly Dictionary<string, string> _demapAttr = new Dictionary<string, string>();
        readonly Regex _deformat = new Regex(@"\*[a-z]\*");

        int _importPageSize = 0;
        MAFeedData _lastFeed = null;
        string _silo = null;
        string _ruleFilter = null;
        string _dnPrefix = "";
        Schema _specifiedSchema = null;

        void WriteEvent(ProgressEventArgs ev)
        {
            switch (ev.Level)
            {
                case LogLevel.Error:
                    WriteEventLog(_deformat.Replace(ev.Message, ""), System.Diagnostics.EventLogEntryType.Error, ev.EventId);
                    break;
                case LogLevel.Warning:
                    WriteEventLog(_deformat.Replace(ev.Message, ""), System.Diagnostics.EventLogEntryType.Warning, ev.EventId);
                    break;
                case LogLevel.Information:
                    WriteEventLog(_deformat.Replace(ev.Message, ""), System.Diagnostics.EventLogEntryType.Information, ev.EventId);
                    break;
                case LogLevel.Critical:
                    WriteEventLog(_deformat.Replace(ev.Message, ""), System.Diagnostics.EventLogEntryType.Warning, ev.EventId);
                    break;
            }
            
            BufferOne.Save<ProgressEventArgs>(ev, "logs", new CancellationToken()).GetAwaiter().GetResult();
        }

        internal static void WriteEventLog(string eventMessage, System.Diagnostics.EventLogEntryType type, int eventId)
        {
            if (!System.Diagnostics.EventLog.SourceExists(Config.EventSource))
                System.Diagnostics.EventLog.CreateEventSource(Config.EventSource, "Application");

            System.Diagnostics.EventLog.WriteEntry(Config.EventSource, eventMessage, type, eventId);
        }

        void SetSchema()
        {
            if (_silo != null && !_schemas.TryGetValue(_silo, out _))
            {
                try
                {
                    var schema = Rest.Get<SystemSchema>("mafeed/schema/" + _silo, new CancellationToken()).GetAwaiter().GetResult();
                    foreach (var name in schema.Attributes.Keys)
                    {
                        var aName = Regex.Replace(name, @"[^\w]", "");
                        _mapAttr[name] = aName;
                        _demapAttr[aName] = name;
                    }

                    _schemas[_silo] = schema;
                }
                catch (Exception e)
                {
                    WriteEvent(new ProgressEventArgs(LogLevel.Error, e.Message + "\r\n" + e.StackTrace, EventIds.ECMAError));
                    throw;
                }
            }
        }

        public MACapabilities GetCapabilitiesEx(KeyedCollection<string, ConfigParameter> configParameters)
        {
            _silo = configParameters["Silo"].Value.Split('(')[1].TrimEnd(')').Trim();
            SetSchema();
            var schema = _schemas[_silo];
            return new MACapabilities
            {
                ConcurrentOperation = true,
                DeltaImport = true,
                DistinguishedNameStyle = (MADistinguishedNameStyle)schema.Capabilities.DistinguishedNameStyle,
                ExportPasswordInFirstPass = schema.Capabilities.ExportPasswordInFirstPass,
                // always export full multi-value attrs if updating
                ExportType = (MAExportType)schema.Capabilities.ExportType,
                IsDNAsAnchor = false,
                NoReferenceValuesInFirstExport = schema.Capabilities.NoReferenceValuesInFirstExport,
                // Important to schedule so that export is processed before import
                ObjectConfirmation = MAObjectConfirmation.Normal,
                ObjectRename = (schema.Capabilities.Capabilities | ConnectorCapability.Rename) > 0,
                // anything more than read allows export
                SupportExport = (int)schema.Capabilities.Capabilities > (int)ConnectorCapability.Read,
                SupportHierarchy = schema.Capabilities.DistinguishedNameStyle == ConnectorDNStyle.Ldap,
                SupportImport = true,
                Normalizations = (MANormalizations)schema.Capabilities.Normalizations,
                DeleteAddAsReplace = false,
                FullExport = false,
                SupportPartitions = schema.Capabilities.SupportPartitions,
                SupportPassword = false
            };
        }

        public IList<ConfigParameterDefinition> GetConfigParameters(KeyedCollection<string, ConfigParameter> configParameters, ConfigParameterPage page)
        {
            var ret = new List<ConfigParameterDefinition>();
            try
            {
                switch (page)
                {
                    case ConfigParameterPage.Connectivity:
                        var res = Rest.Get<ListWrap<string>>("mafeed/silos", new CancellationToken()).GetAwaiter().GetResult();
                        res.Data.Sort();
                        ret.Add(ConfigParameterDefinition.CreateDropDownParameter("Silo", res.Data.Select(d => $"{d.Split('|')[0]} ({d.Split('|')[1]})").ToArray(), false));
                        break;
                    case ConfigParameterPage.Capabilities:
                        if (configParameters.Contains("Silo") && configParameters["Silo"].Value.Trim().Length > 0)
                        {
                            _silo = configParameters["Silo"].Value.Split('(')[1].TrimEnd(')').Trim();
                            SetSchema();
                            var schema = _schemas[_silo];
                            if (schema.Capabilities.DistinguishedNameStyle == ConnectorDNStyle.Generic)
                            {
                                ret.Add(ConfigParameterDefinition.CreateStringParameter("DN Prefix", ".*", "OBJECT="));
                            }

                            ret.Add(ConfigParameterDefinition.CreateTextParameter("Rule Filter"));
                        }
                        break;
                }
            }
            catch (Exception e)
            {
                WriteEvent(new ProgressEventArgs(LogLevel.Error, e.Message + "\r\n" + e.StackTrace, EventIds.ECMAError));
                throw;
            }

            return ret;
        }

        public ParameterValidationResult ValidateConfigParameters(KeyedCollection<string, ConfigParameter> configParameters, ConfigParameterPage page)
        {
            switch (page)
            {
                case ConfigParameterPage.Connectivity:
                    if (configParameters["Silo"].Value.Trim().Length == 0)
                    {
                        return new ParameterValidationResult
                        {
                            Code = ParameterValidationResultCode.Failure,
                            ErrorMessage = "Silo choice may not be blank",
                            ErrorParameter = "Silo"
                        };
                    }
                    break;
            }

            return new ParameterValidationResult { Code = ParameterValidationResultCode.Success };
        }

        public Schema GetSchema(KeyedCollection<string, ConfigParameter> configParameters)
        {
            var schema = Schema.Create();
            _silo = configParameters["Silo"].Value.Split('(')[1].TrimEnd(')').Trim();

            SetSchema();
            var sc = _schemas[_silo];
            try
            {
                foreach (var oType in sc.ObjectTypes)
                {
                    // DN style determines if anchor is locked
                    var schemaType = SchemaType.Create(oType.Name, true);
                    schemaType.Attributes.Add(SchemaAttribute.CreateAnchorAttribute("Id", AttributeType.Binary));
                    if (sc.Capabilities.PossibleDNComponentsForProvisioning != null && sc.Capabilities.PossibleDNComponentsForProvisioning.Count > 0)
                    {
                        foreach (var pn in sc.Capabilities.PossibleDNComponentsForProvisioning)
                        {
                            schemaType.PossibleDNComponentsForProvisioning.Add(pn);
                        }
                    }

                    foreach (var attr in oType.Attributes)
                    {
                        var def = sc.Attributes[attr];
                        if (def.Reference)
                        {
                            schemaType.Attributes.Add(SchemaAttribute.CreateMultiValuedAttribute(_mapAttr[def.Name], AttributeType.Reference));
                            schemaType.Attributes.Add(SchemaAttribute.CreateMultiValuedAttribute(_mapAttr[def.Name] + "String", AttributeType.String));
                            continue;
                        }

                        var valType = Kind(def.ValueType);

                        if (def.Multivalued)
                        {
                            switch (valType)
                            {
                                case PanelModel.Rules.AttributeType.Binary:
                                    schemaType.Attributes.Add(SchemaAttribute.CreateMultiValuedAttribute(_mapAttr[def.Name], AttributeType.Binary, def.ExportOnly ? AttributeOperation.ExportOnly : AttributeOperation.ImportExport));
                                    break;
                                case PanelModel.Rules.AttributeType.Long:
                                    schemaType.Attributes.Add(SchemaAttribute.CreateMultiValuedAttribute(_mapAttr[def.Name], AttributeType.Integer, def.ExportOnly ? AttributeOperation.ExportOnly : AttributeOperation.ImportExport));
                                    break;
                                default:
                                    schemaType.Attributes.Add(SchemaAttribute.CreateMultiValuedAttribute(_mapAttr[def.Name], AttributeType.String, def.ExportOnly ? AttributeOperation.ExportOnly : AttributeOperation.ImportExport));
                                    break;
                            }
                        }
                        else
                        {
                            switch (valType)
                            {
                                case PanelModel.Rules.AttributeType.Binary:
                                    schemaType.Attributes.Add(SchemaAttribute.CreateSingleValuedAttribute(_mapAttr[def.Name], AttributeType.Binary, def.ExportOnly ? AttributeOperation.ExportOnly : AttributeOperation.ImportExport));
                                    break;
                                case PanelModel.Rules.AttributeType.Boolean:
                                    schemaType.Attributes.Add(SchemaAttribute.CreateSingleValuedAttribute(_mapAttr[def.Name], AttributeType.Boolean, def.ExportOnly ? AttributeOperation.ExportOnly : AttributeOperation.ImportExport));
                                    break;
                                case PanelModel.Rules.AttributeType.Long:
                                    schemaType.Attributes.Add(SchemaAttribute.CreateSingleValuedAttribute(_mapAttr[def.Name], AttributeType.Integer, def.ExportOnly ? AttributeOperation.ExportOnly : AttributeOperation.ImportExport));
                                    break;
                                default:
                                    schemaType.Attributes.Add(SchemaAttribute.CreateSingleValuedAttribute(_mapAttr[def.Name], AttributeType.String, def.ExportOnly ? AttributeOperation.ExportOnly : AttributeOperation.ImportExport));
                                    break;
                            }
                        }
                    }

                    schema.Types.Add(schemaType);
                }
            }
            catch (Exception e)
            {
                WriteEvent(new ProgressEventArgs(LogLevel.Error, e.Message + "\r\n" + e.StackTrace, EventIds.ECMAError));
                throw;
            }

            return schema;
        }

        public OpenImportConnectionResults OpenImportConnection(KeyedCollection<string, ConfigParameter> configParameters, Schema types, OpenImportConnectionRunStep importRunStep)
        {
            _importPageSize = importRunStep.PageSize;
            _silo = configParameters["Silo"].Value.Split('(')[1].TrimEnd(')').Trim();
            _ruleFilter = configParameters["Rule Filter"].Value;
            _dnPrefix = configParameters["DN Prefix"].Value;
            _specifiedSchema = types;
            SetSchema();
            var schema = _schemas[_silo];

            _importSchema = new SystemSchema
            {
                Capabilities = schema.Capabilities,
                Provider = schema.Provider,
                System = schema.System
            };
            try
            {
                foreach (var schemaType in types.Types)
                {
                    var oType = schema.ObjectTypes.Find(o => o.Name == schemaType.Name);
                    var addO = new ObjectSchema { Name = schemaType.Name };
                    _importSchema.ObjectTypes.Add(addO);
                    foreach (var attr in schemaType.Attributes)
                    {
                        // bypass *String
                        if (!_demapAttr.TryGetValue(attr.Name, out string aName)) { continue; }

                        addO.Attributes.Add(aName);
                        _importSchema.Attributes[aName] = schema.Attributes[aName];
                    }
                }

                var lastRun = DateTime.MinValue;
                if (!string.IsNullOrEmpty(importRunStep.CustomData))
                {
                    DateTime.TryParse(importRunStep.CustomData.Split('|')[0], out lastRun);
                    lastRun = lastRun.ToUniversalTime();
                }

                var control = new MAFeedControl
                {
                    Delta = importRunStep.ImportType == OperationType.Delta,
                    Direction = FeedDirection.Import,
                    Page = 0,
                    RunId = Guid.Empty,
                    Schema = _importSchema,
                    Silo = _silo,
                    LastRun = lastRun,
                    RuleFilter = _ruleFilter
                };

                var run = Rest.Post<MAFeedData, MAFeedControl>("mafeed/import", control, new CancellationToken()).GetAwaiter().GetResult();
                _lastFeed = run;
                // run created timestamp, run Id, page, pageSize, pageCount
                return new OpenImportConnectionResults($"{run.Created.FormatISODate()}|{run.RunId}|0|0|{run.PageCount}");
            }
            catch (Exception e)
            {
                WriteEvent(new ProgressEventArgs(LogLevel.Error, e.Message + "\r\n" + e.StackTrace, EventIds.ECMAError));
                throw;
            }
        }

        public CloseImportConnectionResults CloseImportConnection(CloseImportConnectionRunStep importRunStep)
        {
            try
            {
                if (!string.IsNullOrEmpty(importRunStep.CustomData) && importRunStep.CustomData.Split('|').Length > 1 &&
                    Guid.TryParse(importRunStep.CustomData.Split('|')[1], out Guid runId))
                {
                    var control = new MAFeedControl
                    {
                        Direction = FeedDirection.Import,
                        Page = 0,
                        RunId = runId
                    };

                    Rest.Post<ApiDict<string>, MAFeedControl>("mafeed/close", control, new CancellationToken()).GetAwaiter().GetResult();
                }

                return new CloseImportConnectionResults(importRunStep.CustomData.Split('|')[0]);
            }
            catch (Exception e)
            {
                WriteEvent(new ProgressEventArgs(LogLevel.Error, e.Message + "\r\n" + e.StackTrace, EventIds.ECMAError));
                throw;
            }
        }

        public GetImportEntriesResults GetImportEntries(GetImportEntriesRunStep importRunStep)
        {
            try
            {
                var cData = importRunStep.CustomData.Split('|');
                DateTime.TryParse(cData[0], out DateTime created);
                created = created.ToUniversalTime();
                Guid.TryParse(cData[1], out Guid runId);
                int.TryParse(cData[2], out int lastPage);
                int.TryParse(cData[3], out int lastPageIndex);
                int.TryParse(cData[4], out int pageCount);

                var ret = new List<CSEntryChange>(_importPageSize);
                while (ret.Count < _importPageSize)
                {
                    // build out results
                    if (lastPageIndex < _lastFeed.Count)
                    {
                        ret.Add(BuildCS(_lastFeed.Objects[lastPageIndex]));
                        lastPageIndex++;
                    }
                    // looks like an off-by-one error because the first page is status only without data
                    else if (lastPage < pageCount)
                    {
                        // get a new page
                        lastPage++;
                        var control = new MAFeedControl
                        {
                            Delta = _lastFeed.Delta,
                            Direction = FeedDirection.Import,
                            Page = lastPage,
                            RunId = runId,
                            Schema = _importSchema,
                            Silo = _silo,
                            RuleFilter = _ruleFilter,
                            LastRun = created
                        };

                        var run = Rest.Post<MAFeedData, MAFeedControl>("mafeed/import", control, new CancellationToken()).GetAwaiter().GetResult();
                        run.Hydrate();
                        _lastFeed = run;
                    }
                    else
                    {
                        // wrap up
                        return new GetImportEntriesResults($"{created.FormatISODate()}|{runId}|{lastPage}|{lastPageIndex}|{pageCount}", false, ret);
                    }
                }

                return new GetImportEntriesResults($"{created.FormatISODate()}|{runId}|{lastPage}|{lastPageIndex}|{pageCount}", true, ret);
            }
            catch (Exception e)
            {
                WriteEvent(new ProgressEventArgs(LogLevel.Error, e.Message + "\r\n" + e.StackTrace, EventIds.ECMAError));
                throw;
            }
        }

        object ImportConvert(object val, PanelModel.Rules.AttributeType valueType, bool reference)
        {
            switch (valueType)
            {
                case PanelModel.Rules.AttributeType.Binary:
                    return val;
                case PanelModel.Rules.AttributeType.Boolean:
                    return val;
                case PanelModel.Rules.AttributeType.Long:
                    return val;
                default:
                    var ret = new PanelModel.Rules.AttributeValue(val, null).ToString();
                    if (reference) { ret = _dnPrefix + ret; }
                    return ret;
            }
        }

        CSEntryChange BuildCS(MAFeedEntry entry)
        {
            var schema = _schemas[_silo];
            switch (entry.Action)
            {
                case FeedAction.Update:
                    var up = CSEntryChange.Create();
                    up.ObjectModificationType = ObjectModificationType.Update;
                    up.ObjectType = entry.ObjectType;
                    up.DN = _dnPrefix + entry.DN;
                    up.AnchorAttributes.Add(AnchorAttribute.Create("Id", entry.Anchor.ToByteArray()));

                    foreach (var attr in entry.AddAttrs ?? new AttributeDictionary<object>())
                    {
                        var aName = _mapAttr[attr.Key];
                        var aVal = ImportConvert(attr.Value, Kind(schema.Attributes[attr.Key].ValueType), false);
                        up.AttributeChanges.Add(Microsoft.MetadirectoryServices.AttributeChange.CreateAttributeAdd(aName, aVal));
                    }

                    foreach (var attr in entry.UpdateAttrs ?? new AttributeDictionary<object>())
                    {
                        var aName = _mapAttr[attr.Key];
                        var aVal = ImportConvert(attr.Value, Kind(schema.Attributes[attr.Key].ValueType), false);
                        up.AttributeChanges.Add(Microsoft.MetadirectoryServices.AttributeChange.CreateAttributeUpdate(aName, aVal));
                    }

                    foreach (var attr in entry.DeleteAttrs ?? new AttributeDictionary<bool>())
                    {
                        var aName = _mapAttr[attr.Key];
                        up.AttributeChanges.Add(Microsoft.MetadirectoryServices.AttributeChange.CreateAttributeDelete(aName));
                    }

                    foreach (var mattr in entry.MultiAttrs ?? new AttributeDictionary<MAFeedMultiEntry>())
                    {
                        var aName = _mapAttr[mattr.Key];
                        var changes = new List<ValueChange>();
                        foreach (var val in mattr.Value.AddValues ?? new List<object>())
                        {
                            var aVal = ImportConvert(val, Kind(schema.Attributes[mattr.Key].ValueType), schema.Attributes[mattr.Key].Reference);
                            changes.Add(ValueChange.CreateValueAdd(aVal));
                        }

                        foreach (var val in mattr.Value.DeleteValues ?? new List<object>())
                        {
                            var aVal = ImportConvert(val, Kind(schema.Attributes[mattr.Key].ValueType), schema.Attributes[mattr.Key].Reference);
                            changes.Add(ValueChange.CreateValueDelete(aVal));
                        }

                        up.AttributeChanges.Add(Microsoft.MetadirectoryServices.AttributeChange.CreateAttributeUpdate(aName, changes));

                        if (schema.Attributes[mattr.Key].Reference && _specifiedSchema.Types[entry.ObjectType].Attributes.Contains(aName + "String"))
                        {
                            changes = new List<ValueChange>();
                            foreach (var val in mattr.Value.AddValues ?? new List<object>())
                            {
                                var aVal = ImportConvert(val, Kind(schema.Attributes[mattr.Key].ValueType), false);
                                changes.Add(ValueChange.CreateValueAdd(aVal));
                            }

                            foreach (var val in mattr.Value.DeleteValues ?? new List<object>())
                            {
                                var aVal = ImportConvert(val, Kind(schema.Attributes[mattr.Key].ValueType), false);
                                changes.Add(ValueChange.CreateValueDelete(aVal));
                            }

                            up.AttributeChanges.Add(Microsoft.MetadirectoryServices.AttributeChange.CreateAttributeUpdate(aName + "String", changes));
                        }
                    }

                    return up;
                case FeedAction.Delete:
                    var del = CSEntryChange.Create();
                    del.ObjectModificationType = ObjectModificationType.Delete;
                    del.ObjectType = entry.ObjectType;
                    del.DN = _dnPrefix + entry.DN;
                    del.AnchorAttributes.Add(AnchorAttribute.Create("Id", entry.Anchor.ToByteArray()));
                    return del;
                default: // Full, Add
                    var add = CSEntryChange.Create();
                    add.ObjectModificationType = ObjectModificationType.Add;
                    add.ObjectType = entry.ObjectType;
                    add.DN = _dnPrefix + entry.DN;
                    add.AnchorAttributes.Add(AnchorAttribute.Create("Id", entry.Anchor.ToByteArray()));

                    foreach (var attr in entry.FullAttrs ?? new AttributeDictionary<object>())
                    {
                        var aName = _mapAttr[attr.Key];
                        var aVal = ImportConvert(attr.Value, Kind(schema.Attributes[attr.Key].ValueType), false);
                        add.AttributeChanges.Add(Microsoft.MetadirectoryServices.AttributeChange.CreateAttributeAdd(aName, aVal));
                    }

                    foreach (var mattr in entry.MultiAttrs ?? new AttributeDictionary<MAFeedMultiEntry>())
                    {
                        var aType = Kind(schema.Attributes[mattr.Key].ValueType);
                        var aName = _mapAttr[mattr.Key];
                        var isRef = schema.Attributes[mattr.Key].Reference;
                        var vals = (from v in mattr.Value.FullValues ?? mattr.Value.AddValues ?? new List<object>()
                                    select ImportConvert(v, aType, isRef)).ToList();
                        add.AttributeChanges.Add(Microsoft.MetadirectoryServices.AttributeChange.CreateAttributeAdd(aName, vals));

                        if (schema.Attributes[mattr.Key].Reference && _specifiedSchema.Types[entry.ObjectType].Attributes.Contains(aName + "String"))
                        {
                            vals = (from v in mattr.Value.FullValues ?? mattr.Value.AddValues ?? new List<object>()
                                    select ImportConvert(v, aType, false)).ToList();

                            add.AttributeChanges.Add(Microsoft.MetadirectoryServices.AttributeChange.CreateAttributeAdd(aName + "String", vals));
                        }
                    }

                    return add;
            }
        }

        public void OpenExportConnection(KeyedCollection<string, ConfigParameter> configParameters, Schema types, OpenExportConnectionRunStep exportRunStep)
        {
            _silo = configParameters["Silo"].Value.Split('(')[1].TrimEnd(')').Trim();
            _dnPrefix = configParameters["DN Prefix"].Value;
            _specifiedSchema = types;
            _runId = Guid.NewGuid();
            SetSchema();
        }

        public void CloseExportConnection(CloseExportConnectionRunStep exportRunStep)
        {
            return;
        }

        Guid _runId;
        int _page = 1;
        public PutExportEntriesResults PutExportEntries(IList<CSEntryChange> csentries)
        {
            try
            {
                var data = new MAFeedData
                {
                    RunId = _runId,
                    Silo = _silo,
                    Created = DateTime.UtcNow,
                    Direction = FeedDirection.Export,
                    Delta = true,
                    Map = new PortableNameMap(),
                    Server = Config.Server,
                    User = Config.User,
                    Page = _page
                };

                foreach (var csChange in csentries)
                {
                    bool isFull;
                    switch (csChange.ObjectModificationType)
                    {
                        case ObjectModificationType.Add:
                            data.Write(ExportAdd(csChange), out isFull);
                            break;
                        case ObjectModificationType.Delete:
                            data.Write(ExportDelete(csChange), out isFull);
                            break;
                        default:
                            data.Write(ExportUpdate(csChange), out isFull);
                            break;
                    }

                    if (isFull)
                    {
                        data.Finish();
                        var res = Rest.Post<ApiDict<string>, MAFeedData>("mafeed/export", data, new CancellationToken()).GetAwaiter().GetResult();
                        if (res["Result"] != "Success")
                        {
                            throw new FormatException(res["Result"]);
                        }

                        _page++;
                        data = new MAFeedData
                        {
                            RunId = _runId,
                            Silo = _silo,
                            Created = DateTime.UtcNow,
                            Direction = FeedDirection.Export,
                            Delta = true,
                            Map = new PortableNameMap(),
                            Server = Config.Server,
                            User = Config.User,
                            Page = _page
                        };
                    }
                }

                data.Finish();
                if (data.Count > 0)
                {
                    var res = Rest.Post<ApiDict<string>, MAFeedData>("mafeed/export", data, new CancellationToken()).GetAwaiter().GetResult();
                    if (res["Result"] != "Success")
                    {
                        throw new FormatException(res["Result"]);
                    }

                    _page++;
                }

                return new PutExportEntriesResults();
            }
            catch (Exception e)
            {
                WriteEvent(new ProgressEventArgs(LogLevel.Error, e.Message + "\r\n" + e.StackTrace, EventIds.ECMAError));
                throw;
            }
        }

        MAFeedEntry ExportAdd(CSEntryChange cs)
        {
            var schema = _schemas[_silo];
            var ret = new MAFeedEntry
            {
                DN = cs.DN,
                ObjectType = cs.ObjectType,
                Action = FeedAction.Add,
                AddAttrs = new AttributeDictionary<object>(),
                MultiAttrs = new AttributeDictionary<MAFeedMultiEntry>()
            };

            if (!string.IsNullOrEmpty(_dnPrefix) && ret.DN.StartsWith(_dnPrefix))
            {
                ret.DN = ret.DN.Substring(_dnPrefix.Length);
            }

            // set attrs
            foreach (var attr in cs.AttributeChanges)
            {
                var aName = _demapAttr[attr.Name];
                var aSchema = schema.Attributes[aName];
                if (aSchema.Multivalued || aSchema.Reference) // includes all reference
                {
                    var multi = new MAFeedMultiEntry { IsRef = aSchema.Reference, AddValues = new List<object>() };
                    ret.MultiAttrs[aName] = multi;
                    foreach (var ch in attr.ValueChanges)
                    {
                        multi.AddValues.Add(ExportConvert(ch, Kind(aSchema.ValueType), aSchema.Reference));
                    }
                }
                else
                {
                    ret.AddAttrs[aName] = ExportConvert(attr.ValueChanges[0], Kind(aSchema.ValueType), false);
                }
            }

            return ret;
        }

        MAFeedEntry ExportUpdate(CSEntryChange cs)
        {
            var schema = _schemas[_silo];
            var ret = new MAFeedEntry
            {
                DN = cs.DN,
                Anchor = new Guid((byte[])cs.AnchorAttributes["Id"].Value),
                ObjectType = cs.ObjectType,
                Action = FeedAction.Update,
                AddAttrs = new AttributeDictionary<object>(),
                DeleteAttrs = new AttributeDictionary<bool>(),
                UpdateAttrs = new AttributeDictionary<object>(),
                MultiAttrs = new AttributeDictionary<MAFeedMultiEntry>()
            };

            if (!string.IsNullOrEmpty(_dnPrefix) && ret.DN.StartsWith(_dnPrefix))
            {
                ret.DN = ret.DN.Substring(_dnPrefix.Length);
            }

            foreach (var attr in cs.AttributeChanges)
            {
                if (attr.Name == "DN" && attr.ValueChanges.Count == 1)
                {
                    ret.UpdateAttrs.Add("DN", attr.ValueChanges[0].Value);
                    continue;
                }

                var aName = _demapAttr[attr.Name];
                var aSchema = schema.Attributes[aName];
                if (aSchema.Multivalued || aSchema.Reference)
                {
                    var multi = new MAFeedMultiEntry { IsRef = aSchema.Reference, AddValues = new List<object>() };
                    ret.MultiAttrs[aName] = multi;
                    foreach (var ch in attr.ValueChanges)
                    {
                        if (ch.ModificationType == ValueModificationType.Add)
                        {
                            multi.AddValues.Add(ExportConvert(ch, Kind(aSchema.Reference ? "String" : aSchema.ValueType), aSchema.Reference));
                        }
                        else
                        {
                            multi.DeleteValues.Add(ExportConvert(ch, Kind(aSchema.Reference ? "String" : aSchema.ValueType), aSchema.Reference));
                        }
                    }
                }
                else
                {
                    switch (attr.ModificationType)
                    {
                        case AttributeModificationType.Add:
                            ret.AddAttrs[aName] = ExportConvert(attr.ValueChanges[0], Kind(aSchema.ValueType), false);
                            break;
                        case AttributeModificationType.Delete:
                            ret.DeleteAttrs[aName] = true;
                            break;
                        case AttributeModificationType.Replace:
                            ret.DeleteAttrs[aName] = true;
                            ret.AddAttrs[aName] = ExportConvert(attr.ValueChanges.Where(v => v.ModificationType == ValueModificationType.Add).FirstOrDefault(), Kind(aSchema.ValueType), false);
                            break;
                        case AttributeModificationType.Update:
                            ret.UpdateAttrs[aName] = ExportConvert(attr.ValueChanges.Where(v => v.ModificationType == ValueModificationType.Add).FirstOrDefault(), Kind(aSchema.ValueType), false);
                            break;
                    }
                }
            }

            return ret;
        }

        MAFeedEntry ExportDelete(CSEntryChange cs)
        {
            var ret = new MAFeedEntry {
                DN = cs.DN,
                Anchor = new Guid((byte[])cs.AnchorAttributes["Id"].Value),
                ObjectType = cs.ObjectType,
                Action = FeedAction.Delete
            };


            if (!string.IsNullOrEmpty(_dnPrefix) && ret.DN.StartsWith(_dnPrefix))
            {
                ret.DN = ret.DN.Substring(_dnPrefix.Length);
            }

            return ret;
        }

        static PanelModel.Rules.AttributeType Kind(string kind)
        {
            return (PanelModel.Rules.AttributeType)Enum.Parse(typeof(PanelModel.Rules.AttributeType), kind, true);
        }

        object ExportConvert(ValueChange val, PanelModel.Rules.AttributeType aType, bool isRef)
        {
            if (isRef && val != null && !string.IsNullOrEmpty(_dnPrefix) && val.Value is string str && str.StartsWith(_dnPrefix))
            {
                return new PanelModel.Rules.AttributeValue(str.Substring(_dnPrefix.Length), null).AsType(aType);
            }

            if (val == null)
            {
                return new PanelModel.Rules.AttributeValue(null, null).AsType(aType);
            }

            return new PanelModel.Rules.AttributeValue(val.Value, null).AsType(aType);
        }
    }
}
