﻿using System;
using Microsoft.MetadirectoryServices;
using SoftwareIDM.UpliftEngine;

namespace SoftwareIDM.MVExtension
{
    public class MVExtension : IMVSynchronization
    {
        public MVExtension()
        {
            if (!MAExtension.MAExtension.ResolveHandlerSet)
            {
                AppDomain.CurrentDomain.AssemblyResolve += MAExtension.MAExtension.AssemblyResolveHandler;
                MAExtension.MAExtension.ResolveHandlerSet = true;
            }
        }

        public void Initialize()
        {
            Config.Init();
        }

        public void Provision(MVEntry mventry)
        {
            ProvisionEngine.Provision(mventry, true);
        }

        public bool ShouldDeleteFromMV(CSEntry csentry, MVEntry mventry)
        {
            throw new NotImplementedException();
        }

        public void Terminate()
        {
            Logger.Finish();
        }
    }
}
