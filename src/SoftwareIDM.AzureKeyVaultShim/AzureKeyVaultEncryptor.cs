﻿// Copyright (c) .NET Foundation. All rights reserved.
// Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.

using System;
using System.IO;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Xml.Linq;
using Azure.Security.KeyVault.Keys.Cryptography;
using Microsoft.AspNetCore.DataProtection.XmlEncryption;

namespace SoftwareIDM.AzureKeyVaultShim
{
    internal class AzureKeyVaultXmlEncryptor : IXmlEncryptor
    {
        internal static KeyWrapAlgorithm DefaultKeyEncryption = KeyWrapAlgorithm.RsaOaep;
        internal static Func<SymmetricAlgorithm> DefaultSymmetricAlgorithmFactory = Aes.Create;

        private readonly RandomNumberGenerator _randomNumberGenerator;
        private readonly IKeyVaultWrappingClient _client;

        public AzureKeyVaultXmlEncryptor(IKeyVaultWrappingClient client)
            : this(client, RandomNumberGenerator.Create())
        {
        }

        internal AzureKeyVaultXmlEncryptor(IKeyVaultWrappingClient client, RandomNumberGenerator randomNumberGenerator)
        {
            _client = client;
            _randomNumberGenerator = randomNumberGenerator;
        }

        public EncryptedXmlInfo Encrypt(XElement plaintextElement)
        {
            return EncryptAsync(plaintextElement).GetAwaiter().GetResult();
        }

        private async Task<EncryptedXmlInfo> EncryptAsync(XElement plaintextElement)
        {
            byte[] value;
            using (var memoryStream = new MemoryStream())
            {
                plaintextElement.Save(memoryStream, SaveOptions.DisableFormatting);
                value = memoryStream.ToArray();
            }

            using (var symmetricAlgorithm = DefaultSymmetricAlgorithmFactory())
            {
                var symmetricBlockSize = symmetricAlgorithm.BlockSize / 8;
                var symmetricKey = new byte[symmetricBlockSize];
                var symmetricIV = new byte[symmetricBlockSize];
                _randomNumberGenerator.GetBytes(symmetricKey);
                _randomNumberGenerator.GetBytes(symmetricIV);
                byte[] encryptedValue;
                using (var encryptor = symmetricAlgorithm.CreateEncryptor(symmetricKey, symmetricIV))
                {
                    encryptedValue = encryptor.TransformFinalBlock(value, 0, value.Length);
                }

                var wrappedKey = await _client.WrapKeyAsync(DefaultKeyEncryption, symmetricKey);

                var element = new XElement("encryptedKey",
                    new XComment(" This key is encrypted with Azure KeyVault. "),
                    new XElement("kid", _client.KeyId),
                    new XElement("key", Convert.ToBase64String(wrappedKey.EncryptedKey)),
                    new XElement("iv", Convert.ToBase64String(symmetricIV)),
                    new XElement("value", Convert.ToBase64String(encryptedValue)));

                return new EncryptedXmlInfo(element, typeof(AzureKeyVaultXmlDecryptor));
            }
        }
    }

    internal interface IKeyVaultWrappingClient
    {
        Task<UnwrapResult> UnwrapKeyAsync(KeyWrapAlgorithm algorithm, byte[] cipherText);
        Task<WrapResult> WrapKeyAsync(KeyWrapAlgorithm algorithm, byte[] cipherText);
        string KeyId { get; }
    }

    internal class KeyVaultClientWrapper : IKeyVaultWrappingClient
    {
        private readonly CryptographyClient _client;

        public string KeyId => _client.KeyId;

        public KeyVaultClientWrapper(CryptographyClient client)
        {
            _client = client;
        }

        public Task<UnwrapResult> UnwrapKeyAsync(KeyWrapAlgorithm algorithm, byte[] cipherText)
        {
            return _client.UnwrapKeyAsync(algorithm, cipherText);
        }

        public Task<WrapResult> WrapKeyAsync(KeyWrapAlgorithm algorithm, byte[] cipherText)
        {
            return _client.WrapKeyAsync(algorithm, cipherText);
        }
    }
}
