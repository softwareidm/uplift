﻿// (c) Copyright SoftwareIDM Inc. 
// This library uses the Microsoft AspNetCore dev branch implementation of AzureKeyVault Data Protection implementation to provide helpers
// for encrypting and decrypting short strings using the Azure KeyWrap API
using System;
using System.Xml.Linq;
using Azure.Security.KeyVault.Keys.Cryptography;
using Azure.Security.KeyVault.Secrets;
using Microsoft.AspNetCore.DataProtection.XmlEncryption;
using Microsoft.Extensions.Configuration;

namespace SoftwareIDM.AzureKeyVaultShim
{
    public static class ProtectionHelper
    {
        public static string Protect(string data, string protectionSid)
        {
            if (protectionSid == "SID=mine")
            {
                protectionSid = $"SID={System.Security.Principal.WindowsIdentity.GetCurrent().User.Value}";
            }

            var encryptor = new DpapiNGXmlEncryptor(protectionSid, DpapiNGProtectionDescriptorFlags.None, Microsoft.Extensions.Logging.Abstractions.NullLoggerFactory.Instance);
            var ret = encryptor.Encrypt(new XElement("key", data));
            return ret.EncryptedElement.ToString(SaveOptions.DisableFormatting);
        }

        public static string UnProtect(string data)
        {
            var decryptor = new DpapiNGXmlDecryptor();
            var ret = decryptor.Decrypt(XElement.Parse(data));
            return ret.Value;
        }
        
        static CryptographyClient _client = null;
        static SecretClient _secret = null;
        public static string Protect(string data, CryptographyClient client)
        {
            var encryptor = new AzureKeyVaultXmlEncryptor(new KeyVaultClientWrapper(client));
            var ret = encryptor.Encrypt(new XElement("key", data));
            return ret.EncryptedElement.ToString(SaveOptions.DisableFormatting);
        }

        public static string UnProtect(string data, CryptographyClient client)
        {
            var decryptor = new AzureKeyVaultXmlDecryptor(new KeyVaultClientWrapper(client));
            var ret = decryptor.Decrypt(XElement.Parse(data));
            return ret.Value;
        }

        public static SecretClient GetSecret(IConfiguration configuration)
        {
            if (_secret == null)
            {
                var credential = new Azure.Identity.DefaultAzureCredential();
                _secret = new SecretClient(new Uri(configuration["Auth:SecretVault"]), credential);
            }

            return _secret;
        }

        public static CryptographyClient GetClient(IConfiguration configuration, Uri keyIdentifier = null)
        {
            if (keyIdentifier == null)
            {
                keyIdentifier = KeyIdentifier(configuration);
            }

            if (_client == null)
            {
                var credential = new Azure.Identity.DefaultAzureCredential();
                _client = new CryptographyClient(keyIdentifier, credential);
            }

            return _client;
        }

        public static Uri KeyIdentifier(IConfiguration configuration)
        {
            return new Uri(configuration["Auth:KeyVaultIdentifier"]);
        }
    }
}
