﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using SoftwareIDM.PanelModel.Attributes;
using SoftwareIDM.PanelModel.Models;
using SoftwareIDM.PanelModel.Rules;

namespace SoftwareIDM.Uplift
{
    [ClassDoc(Name = "Rule Functions Config")]
    public class RuleFunctionsConfig : ConfigSetting
    {
        [MemberDoc("Comma separated list of Dll extensions with additional rule functions to load and make available in the App domain.", Name = "Extension Dlls")]
        [UI(UIAttrKind.Text, Size = 12, Description = true)]
        public string ExtensionDlls { get; set; }

        [JsonProperty(ItemTypeNameHandling = TypeNameHandling.Objects)]
        [UI(UIAttrKind.Strips, CSSClass = "configs")]
        [MemberDoc(Name = "Custom Rule Functions")]
        public List<RuleFunction> RuleFunctions { get; set; }

        [UI(UIAttrKind.Label, Size = 4, Header = true, NoLabel = true, Default = "Rule Functions Config")]
        public override string ConfigName { get => base.ConfigName; set => base.ConfigName = value; }

        public RuleFunctionsConfig()
        {
            RuleFunctions = new List<RuleFunction>();
        }

        public override void FromXml(XElement xml)
        {
            if (xml.Name == "ruleFunctions")
            {
                base.FromXml(xml);
                if (xml.Attributes("extensionDlls").Count() > 0)
                {
                    ExtensionDlls = xml.Attribute("extensionDlls").Value;
                }

                foreach (var rule in xml.Elements("function"))
                {
                    var add = new RuleFunction { Name = rule.Attribute("name").Value, Rule = rule.Value };
                    if (rule.Attributes("kind").Count() > 0)
                    {
                        add.Kind = rule.Attribute("kind").Value;
                    }

                    RuleFunctions.Add(add);
                }
            }
        }

        public override XElement GetXml()
        {
            var ret = new XElement(XmlHelper.SI + "ruleFunctions");
            foreach (var r in RuleFunctions)
            {
                var add = new XElement(XmlHelper.SI + "function", new XAttribute("name", r.Name), r.Rule);
                if (r.Kind != null && r.Kind != "Rule")
                {
                    add.Add(new XAttribute("kind", r.Kind));
                }

                ret.Add(add);
            }

            if (!string.IsNullOrEmpty(ExtensionDlls))
            {
                ret.Add(new XAttribute("extensionDlls", ExtensionDlls));
            }

            return AdjustXml(ret);
        }

        public override void Init() { }

        public override void Merge(ConfigSetting other)
        {
            var rules = (RuleFunctionsConfig)other;
            if (!string.IsNullOrEmpty(rules.ExtensionDlls))
            {
                if (string.IsNullOrEmpty(ExtensionDlls))
                {
                    ExtensionDlls = rules.ExtensionDlls;
                }
                else
                {
                    ExtensionDlls = string.Join(",", ExtensionDlls, rules.ExtensionDlls);
                }
            }

            if (rules.RuleFunctions != null && rules.RuleFunctions.Count > 0)
            {
                RuleFunctions.AddRange(rules.RuleFunctions);
            }
        }
    }

    [ClassDoc(RuleDoc = false, DocIgnore = true)]
    public class MARuleConfig : ConfigSetting
    {
        public Dictionary<string, List<FilterSet>> FilterRules { get; set; }

        public Dictionary<string, List<Project>> ProjectionRules { get; set; }

        public Dictionary<string, List<ScopeRule>> DeprovisionRules { get; set; }

        public Dictionary<string, List<ProvisionRule>> ProvisionRules { get; set; }

        public MARuleConfig()
        {
            FilterRules = new Dictionary<string, List<FilterSet>>();
            ProjectionRules = new Dictionary<string, List<Project>>();
            DeprovisionRules = new Dictionary<string, List<ScopeRule>>();
            ProvisionRules = new Dictionary<string, List<ProvisionRule>>();
        }

        public void Add(string ma, FilterSet filter)
        {
            if (!FilterRules.TryGetValue(ma, out List<FilterSet> filters))
            {
                filters = new List<FilterSet>();
                FilterRules[ma] = filters;
            }

            filters.Add(filter);
        }

        public void Add(string ma, ProvisionRule prov)
        {
            if (!ProvisionRules.TryGetValue(ma, out List<ProvisionRule> provs))
            {
                provs = new List<ProvisionRule>();
                ProvisionRules[ma] = provs;
            }

            provs.Add(prov);
        }

        public void Add(string ma, Project project)
        {
            if (!ProjectionRules.TryGetValue(ma, out List<Project> projects))
            {
                projects = new List<Project>();
                ProjectionRules[ma] = projects;
            }

            projects.Add(project);
        }

        public void Add(string ma, ScopeRule deprovision)
        {
            if (!DeprovisionRules.TryGetValue(ma, out List<ScopeRule> deps))
            {
                deps = new List<ScopeRule>();
                DeprovisionRules[ma] = deps;
            }

            deps.Add(deprovision);
        }

        public override void Init() { }

        public override void Merge(ConfigSetting other) { }

        public override void FromXml(XElement xml)
        {
            if (xml.Name == "maRuleConfig")
            {
                base.FromXml(xml);
                foreach (var rule in xml.Elements("filter"))
                {
                    Add(rule.Attribute("ma").Value, new FilterSet { CSObjectType = rule.Attribute("csObjectType").Value, Rule = rule.Value });
                }

                foreach (var rule in xml.Elements("project"))
                {
                    Add(rule.Attribute("ma").Value, new Project { ExtensionRule = rule.Value, CSObjectType = rule.Attribute("csObjectType").Value });
                }

                foreach (var rule in xml.Elements("deprovision"))
                {
                    var add = new ScopeRule { CSObjectType = rule.Attribute("csObjectType").Value, Action = rule.Attribute("action").Value };
                    if (rule.Attributes("scope").Count() > 0)
                    {
                        add.Scope = rule.Attribute("scope").Value;
                    }

                    if (rule.Elements("set").Count() > 0)
                    {
                        add.AttributeRules = (from s in rule.Elements("set")
                                              select new SetRule { Attribute = s.Attribute("name").Value, Rule = s.Value }).ToList();
                    }

                    Add(rule.Attribute("ma").Value, add);
                }

                foreach (var rule in xml.Elements("provision"))
                {
                    var add = new ProvisionRule(rule);
                    Add(rule.Attribute("ma").Value, add);
                }
            }
        }

        public override XElement GetXml()
        {
            var ret = new XElement(XmlHelper.SI + "maRuleConfig");
            foreach (var set in FilterRules)
            {
                foreach (var rule in set.Value)
                {
                    if (!String.IsNullOrEmpty(rule.Rule))
                    {
                        ret.Add(new XElement(XmlHelper.SI + "filter", new XAttribute("ma", set.Key), new XAttribute("csObjectType", rule.CSObjectType), rule.Rule));
                    }
                }
            }

            foreach (var set in ProjectionRules)
            {
                foreach (var rule in set.Value)
                {
                    if (!String.IsNullOrEmpty(rule.ExtensionRule))
                    {
                        ret.Add(new XElement(XmlHelper.SI + "project", new XAttribute("ma", set.Key), new XAttribute("csObjectType", rule.CSObjectType), rule.ExtensionRule));
                    }
                }
            }

            foreach (var set in DeprovisionRules)
            {
                foreach (var rule in set.Value)
                {
                    if (!String.IsNullOrEmpty(rule.Action))
                    {
                        var add = new XElement(XmlHelper.SI + "deprovision", new XAttribute("csObjectType", rule.CSObjectType), new XAttribute("action", rule.Action));
                        add.Add(new XAttribute("ma", set.Key));
                        if (!String.IsNullOrEmpty(rule.Scope))
                        {
                            add.Add(new XAttribute("scope", rule.Scope));
                        }

                        if (rule.AttributeRules != null && rule.AttributeRules.Count > 0)
                        {
                            foreach (var s in rule.AttributeRules)
                            {
                                add.Add(new XElement("set", new XAttribute("name", s.Attribute), s.Rule));
                            }
                        }

                        ret.Add(add);
                    }
                }
            }

            foreach (var set in ProvisionRules)
            {
                foreach (var rule in set.Value)
                {
                    var add = rule.GetXml();
                    add.Add(new XAttribute("ma", set.Key));
                    ret.Add(add);
                }
            }

            return AdjustXml(ret);
        }
    }

    [ClassDoc(Name = "Declarative provisioning rule")]
    public class ProvisionRule
    {
        [UI(UIAttrKind.Text, Size = 4, Required = true, Unique = true, Header = true, NoLabel = true)]
        public string Name { get; set; }

        [UI(UIAttrKind.Select, Size = 3, Required = true, Header = true, NoLabel = true, Choices = "js:helpers.uplift.mvObjectSelect")]
        [MemberDoc(Name = "MV Object Type")]
        public string MVObjectType { get; set; }

        [UI(UIAttrKind.Select, Size = 6, Required = true, Choices = "js:helpers.uplift.csObjectSelect")]
        [MemberDoc(Name = "CS Object Type")]
        public string CSObjectType { get; set; }

        [MemberDoc(Name = "Object Class", Description = "Optional comma-separated object class list for provisioning with an auxiliary type hierarchy (e.g. generic LDAP MA).")]
        [UI(UIAttrKind.Text, Size = 6, Description = true)]
        public string ObjectClass { get; set; }

        [UI(UIAttrKind.Text, Size = 6, Required = true, Description = true)]
        [MemberDoc(Name = "Connector Count", Description = "Number of connectors the rule applies to. May be a number, or an operator and a number, e.g. 0 or \">= 1\"")]
        public string Connector { get; set; }

        [UI(UIAttrKind.LongText, Description = true, RuleHelp = true, RuleObject = "SoftwareIDM.Uplift.IEntryWrapper, SoftwareIDM.Uplift", RuleWarnOnly = true, CSSClass = "input-monospace")]
        [MemberDoc("Condition rule to scope whether or not the rule should be applied")]
        public string Condition { get; set; }

        Rule _conditionRule = null;
        [BsonIgnore, JsonIgnore]
        public Rule ConditionRule
        {
            get
            {
                if (_conditionRule == null && !String.IsNullOrEmpty(Condition))
                {
                    _conditionRule = Rule.Build(Condition);
                }

                return _conditionRule;
            }
        }

        [MemberDoc(Name = "Attribute Rules")]
        [JsonProperty(ItemTypeNameHandling = TypeNameHandling.Objects)]
        [UI(UIAttrKind.Strips, CSSClass = "flow-rule")]
        public List<SetRule> AttributeRules { get; set; }

        [MemberDoc(Name = "Retry Rules")]
        [JsonProperty(ItemTypeNameHandling = TypeNameHandling.Objects)]
        [UI(UIAttrKind.Strips, CSSClass = "flow-rule")]
        public List<SetRule> RetryRules { get; set; }

        [UI(UIAttrKind.Bool, Size = 3, Description = true)]
        [MemberDoc("If checked rule will deprovision instead of rename or add.", Name = "Deprovision")]
        public bool Deprovision { get; set; }

        [UI(UIAttrKind.Bool, Size = 3, Description = true)]
        [MemberDoc("Commit a new connector using ExchangeUtils helper. All required attributes must be set.", Name = "Use Exchange Utils")]
        public bool IsExchange { get; set; }

        [UI(UIAttrKind.Bool, Size = 3, Description = true)]
        [MemberDoc("Commit a new connector even if there is already a connector in the MA.", Name = "Force Add")]
        public bool ForceAdd { get; set; }

        [UI(UIAttrKind.Bool, Size = 3, Description = true)]
        [MemberDoc("Invoke the rule even when App Setting ProvisionEnabled is false.", Name = "Always Enable")]
        public bool AlwaysEnable { get; set; }

        public ProvisionRule()
        {
            AttributeRules = new List<SetRule>();
            RetryRules = new List<SetRule>();
        }

        public ProvisionRule(XElement xml)
        {
            Name = xml.Attribute("name").Value;
            MVObjectType = xml.Attribute("mvObjectType").Value;
            CSObjectType = xml.Attributes("csObjectType").Count() > 0 ? xml.Attribute("csObjectType").Value : "";
            ObjectClass = xml.Attributes("objectClass").Count() > 0 ? xml.Attribute("objectClass").Value : null;
            Connector = xml.Attribute("connector").Value;
            Condition = xml.Attributes("condition").Count() > 0 ? xml.Attribute("condition").Value : "";
            Deprovision = xml.Attributes("deprovision").Count() > 0 ? (xml.Attribute("deprovision").Value.ToLower() == "true") : false;
            AlwaysEnable = xml.Attributes("alwaysEnable").Count() > 0 ? (xml.Attribute("alwaysEnable").Value.ToLower() == "true") : false;
            ForceAdd = xml.Attributes("forceAdd").Count() > 0 ? (xml.Attribute("forceAdd").Value.ToLower() == "true") : false;
            IsExchange = xml.Attributes("exchange").Count() > 0 ? (xml.Attribute("exchange").Value.ToLower() == "true") : false;
            AttributeRules = new List<SetRule>();
            RetryRules = new List<SetRule>();

            AttributeRules.AddRange(from s in xml.Elements("set")
                                    select new SetRule { Attribute = s.Attribute("name").Value, Rule = s.Value });

            RetryRules.AddRange(from s in xml.Elements("retry")
                                select new SetRule { Attribute = s.Attribute("name").Value, Rule = s.Value });
        }

        public XElement GetXml()
        {
            var ret = new XElement(XmlHelper.SI + "provision",
                new XAttribute("name", Name),
                new XAttribute("mvObjectType", MVObjectType),
                new XAttribute("connector", Connector)
            );

            if (!String.IsNullOrEmpty(CSObjectType)) { ret.Add(new XAttribute("csObjectType", CSObjectType)); }
            if (!String.IsNullOrEmpty(ObjectClass))
            {
                ret.Add(new XAttribute("objectClass", ObjectClass));
            }

            if (!String.IsNullOrEmpty(Condition)) { ret.Add(new XAttribute("condition", Condition)); }
            if (Deprovision) { ret.Add(new XAttribute("deprovision", "true")); }
            if (AlwaysEnable) { ret.Add(new XAttribute("alwaysEnable", "true")); }
            if (ForceAdd) { ret.Add(new XAttribute("forceAdd", "true")); }
            if (IsExchange) { ret.Add(new XAttribute("exchange", "true")); }
            foreach (var r in AttributeRules)
            {
                ret.Add(new XElement(XmlHelper.SI + "set", new XAttribute("name", r.Attribute), r.Rule));
            }

            foreach (var r in RetryRules)
            {
                ret.Add(new XElement(XmlHelper.SI + "retry", new XAttribute("name", r.Attribute), r.Rule));
            }

            return ret;
        }
    }

    [ClassDoc(Name = "Set Rule")]
    public class SetRule
    {
        [UI(UIAttrKind.Select, Size = 3, Required = true, Header = true, NoLabel = true, Unique = true, Choices = "js:helpers.uplift.csProvAttributeSelect")]
        public string Attribute { get; set; }

        [UI(UIAttrKind.LongText, Size = 7, Required = true, Header = true, NoLabel = true, RuleHelp = true, RuleWarnOnly = true, RuleObject = "SoftwareIDM.Uplift.IEntryWrapper, SoftwareIDM.Uplift", CSSClass = "input-monospace")]
        public string Rule { get; set; }

        Rule _rule = null;
        [JsonIgnore]
        [BsonIgnore]
        public Rule RuleInst
        {
            get
            {
                if (_rule == null)
                {
                    _rule = PanelModel.Rules.Rule.Build(Rule);
                }

                return _rule;
            }
        }
    }
}
