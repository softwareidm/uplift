﻿using System;
using System.Collections.Generic;
using System.Reflection;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using SoftwareIDM.PanelModel.Attributes;
using SoftwareIDM.PanelModel.Models;

namespace SoftwareIDM.Uplift
{
    public class ModelTypes : IKnownTypes
    {
        public IEnumerable<Type> Types()
        {
            foreach (var type in Assembly.GetExecutingAssembly().ExportedTypes)
            {
                if (type.GetCustomAttribute<ClassDoc>() != null)
                {
                    yield return type;
                }
            }
        }
        public Dictionary<string, Type> Resources => new Dictionary<string, Type>
        {
            { "UpliftSettings", typeof(UpliftSettings) }
        };
    }

    [BsonDiscriminator("UpliftSettings", Required = true)]
    [ClassDoc("Settings for Uplift MIM configuration", Name = "Uplift Settings")]
    public class UpliftSettings : JsonSettings
    {
        [MemberDoc("List of provider connections", Name = "Uplift Configurations")]
        [JsonProperty(ItemTypeNameHandling = TypeNameHandling.Objects)]
        [UI(Kind = UIAttrKind.Strips, CSSClass = "uplift", NoLabel = true, Download = "js:helpers.uplift.upliftDownload", WarnRemove = true)]
        public List<UpliftConfig> Data { get; set; }

        public UpliftSettings()
        {
            Data = new List<UpliftConfig>();
        }

        public bool Merge(UpliftSettings other)
        {
            bool merge = false;
            // TODO: add logic for merging based on config upload or not
            if (other != null && other.Data != null && other.Data.Count > 0)
            {
                if (other.Data.Count > 1)
                {
                    Data = other.Data;
                    return false;
                }

                foreach (var dat in other.Data)
                {
                    bool found = false;
                    for (int i = 0; i < Data.Count; i++)
                    {
                        if (Data[i].SyncProvider == dat.SyncProvider)
                        {
                            Data[i] = dat;
                            found = true;
                            break;
                        }
                    }

                    if (!found)
                    {
                        Data.Add(dat);
                        merge = true;
                    }
                }
            }

            return merge;
        }
    }

    [ClassDoc("Configuration for a single MIM or FIM environment", Name = "Uplift Configuration")]
    public class UpliftConfig
    {
        [MemberDoc("MS Sync Environment", Name = "Sync Provider")]
        [UI(UIAttrKind.Select, Required = true, Header = true, NoLabel = true, Size = 4, Choices = "js:helpers.sync.providerSelect")]
        public string SyncProvider { get; set; }
        
        [JsonProperty(ItemTypeNameHandling = TypeNameHandling.Objects)]
        [UI(UIAttrKind.Strips, CSSClass = "configs", Duplicate = true)]
        [MemberDoc("General settings configuration, including app settings, advanced flow rule functions, and provisioning logic.")]
        public List<ConfigSetting> Settings { get; set; }
        
        public SyncConfig Config { get; set; }

        [JsonProperty(ItemTypeNameHandling = TypeNameHandling.Objects)]
        [UI(UIAttrKind.Strips, CSSClass = "configs", Duplicate = true, Description = true)]
        [MemberDoc("Per-MA configuration including object lifecyle and export attribute flow", Name = "Management Agents")]
        public List<MARules> Rules { get; set; }
        
        [MemberDoc("Metaverse Extension name", Name = "Metaverse Extension")]
        [UI(UIAttrKind.Text, Size = 5)]
        public string MVExtensionName { get; set; }

        [UI(UIAttrKind.Bool, Size = 3)]
        [MemberDoc("Run Extension code in another process", Name = "Separate Process")]
        public bool HighProtect { get; set; }

        [MemberDoc("Provisioning State", Name = "Provision")]
        [UI(UIAttrKind.Select, Required = true, Size = 3, Choices = "scripted:Scripted|sync-rule:Portal Rule|both:Both|none:None")]
        public string Provision { get; set; }

        [JsonProperty(ItemTypeNameHandling = TypeNameHandling.Objects)]
        [UI(UIAttrKind.Strips, CSSClass = "configs", Duplicate = true, Description = true)]
        [MemberDoc("Logic for importing from all MAs to metaverse, including flow precedence.", Name = "Metaverse Import Flow")]
        public List<ObjectFlows> ImportFlow { get; set; }


        public UpliftConfig()
        {
            Settings = new List<ConfigSetting>();
            Rules = new List<MARules>();
            ImportFlow = new List<ObjectFlows>();
        }
    }

    [ClassDoc("MA specific uplift rules for filter, join, projection, and export flow", Name = "MA Rules")]
    public class MARules
    {
        [UI(UIAttrKind.Select, Required = true, Size = 3, Header = true, NoLabel = true, Choices = "js:helpers.sync.maSelect")]
        [MemberDoc("MA this batch of rules applies to")]
        public string MA { get; set; }

        [UI(UIAttrKind.Text, Size = 6, Description = true)]
        [MemberDoc("Name of dll extension to use for this MA. If MA is MIM Portal type, this value will be ignored.", Name = "MA Extension Name")]
        public string MAExtensionName { get; set; }

        [UI(UIAttrKind.Bool, Size = 4)]
        [MemberDoc("Run Extension code in another process", Name = "Run in Separate Process")]
        public bool HighProtect { get; set; }
        
        [JsonProperty(ItemTypeNameHandling = TypeNameHandling.Objects)]
        [UI(UIAttrKind.Strips, CSSClass = "flow-rule", Duplicate = true)]
        [MemberDoc("Provisioning rules for MA", Name = "Provision Rules")]
        public List<ProvisionRule> ProvisionRules { get; set; }

        [JsonProperty(ItemTypeNameHandling = TypeNameHandling.Objects)]
        [UI(UIAttrKind.Strips, CSSClass = "flow-rule", Duplicate = true, Description = true)]
        [MemberDoc("Export flow rules. Implement simple and advanced flow rules here. If a multi-line rule is given, the rule body will be copied to Uplift Config settings for download.", Name = "Export Flow Rules")]
        public List<ExportObjectFlows> ExportFlow { get; set; }

        [JsonProperty(ItemTypeNameHandling = TypeNameHandling.Objects)]
        [UI(UIAttrKind.Strips, CSSClass = "flow-rule", Duplicate = true, Description = true)]
        [MemberDoc("Scoped extension filter for disconnect rules. The filter rule applies if the scope condition is blank or true. If the rule returns true the object is filtered.", Name = "Filter Rules")]
        public List<FilterSet> Filter { get; set; }

        [JsonProperty(ItemTypeNameHandling = TypeNameHandling.Objects)]
        [UI(UIAttrKind.Strips, CSSClass = "flow-rule", Duplicate = true, Description = true)]
        [MemberDoc("Extension join rules. The rule is used to produce a join-value in the context of the CS wrapper if the scope condition is blank or true.", Name = "Join Rules")]
        public List<JoinRule> Join { get; set; }

        [JsonProperty(ItemTypeNameHandling = TypeNameHandling.Objects)]
        [UI(UIAttrKind.Strips, CSSClass = "flow-rule", Duplicate = true, Description = true)]
        [MemberDoc("Extension projection rules. The rule should return either null/false, or the object type to project.", Name = "Projection Rules")]
        public List<Project> Projection { get; set; }

        [UI(UIAttrKind.Select, Description = true, Choices = "Disconnect:Make Disconnector|ExplicitDisconnect:Make Explicit Disconnector|Delete:Stage Delete|Extension:Determine with Extension")]
        [MemberDoc("Deprovisioning action to take when disconnected or MV is deleted. If MA is MIM Portal type, a value of Extension will be ignored.", Name = "Deprovision Action")]
        public string DeprovisionMode { get; set; }

        [JsonProperty(ItemTypeNameHandling = TypeNameHandling.Objects)]
        [UI(UIAttrKind.Strips, CSSClass = "flow-rule", Duplicate = true, Description = true)]
        [MemberDoc("Extension deprovisioning rules. The rule should return Disconnect, Delete, or ExplicitDisconnect.", Name = "Deprovision Rules")]
        public List<ScopeRule> Deprovision { get; set; }
        

        public MARules()
        {
            ProvisionRules = new List<ProvisionRule>();
            ExportFlow = new List<ExportObjectFlows>();
            Filter = new List<FilterSet>();
            Join = new List<JoinRule>();
            Projection = new List<Project>();
            Deprovision = new List<ScopeRule>();
        }
    }

    [ClassDoc(Name = "Join Rule")]
    public class JoinRule
    {
        [UI(UIAttrKind.Select, Size = 3, Required = true, Header = true, NoLabel = true, Choices = "js:helpers.uplift.csObjectSelect")]
        [MemberDoc(Name = "CS Object Type")]
        public string CSObjectType { get; set; }

        [JsonProperty(ItemTypeNameHandling = TypeNameHandling.Objects)]
        [UI(UIAttrKind.Strips, CSSClass = "flow-rule", Duplicate = true)]
        [MemberDoc(Name = "Join Criteria")]
        public List<JoinCriterion> JoinCriteria { get; set; }

        public JoinRule()
        {
            JoinCriteria = new List<JoinCriterion>();
        }
    }

    [ClassDoc(Name = "Join Criterion")]
    public class JoinCriterion
    {
        [UI(UIAttrKind.Select, Size = 3, Required = true, Header = true, NoLabel = true, Choices = "js:helpers.uplift.mvObjectSelect")]
        [MemberDoc(Name = "MV Object Type")]
        public string MVObjectType { get; set; }

        [JsonProperty(ItemTypeNameHandling = TypeNameHandling.Objects)]
        [UI(UIAttrKind.Strips, CSSClass = "flow-rule")]
        public List<JoinMapping> Mappings { get; set; }

        [UI(UIAttrKind.LongText, Size = 12, RuleHelp = true, RuleObject = "SoftwareIDM.Uplift.IEntryWrapper, SoftwareIDM.Uplift", RuleWarnOnly = true, CSSClass = "input-monospace")]
        [MemberDoc(Name = "Join Resolution Rule")]
        public string ResolutionRule { get; set; }

        [UI(Kind = UIAttrKind.Hidden, Default = "js:helpers.getGuid")]
        public Guid Id { get; set; }

        [UI(UIAttrKind.Hidden, Header = true, OnBind = "js:helpers.uplift.portalJoin")]
        public string PortalMapping { get; set; }

        public JoinCriterion()
        {
            Mappings = new List<JoinMapping>();
        }
    }

    [ClassDoc(Name = "Join Mapping")]
    public class JoinMapping
    {
        [UI(UIAttrKind.Select, Required = true, Size = 6, Header = true, NoLabel = true, Choices = "js:helpers.uplift.csAttributesSelect", Unique = true)]
        [MemberDoc(Name = "CS Attribute(s)")]
        public List<string> CSAttribute { get; set; }

        [UI(UIAttrKind.Select, Required = true, Size = 4, Header = true, NoLabel = true, Choices = "js:helpers.uplift.mvAttributeSelect", Unique = true)]
        [MemberDoc(Name = "MV Attribute")]
        public string MVAttribute { get; set; }

        [UI(UIAttrKind.LongText, Size = 12, RuleHelp = true, RuleObject = "SoftwareIDM.Uplift.IEntryWrapper, SoftwareIDM.Uplift", RuleWarnOnly = true, CSSClass = "input-monospace")]
        public string Rule { get; set; }

        public JoinMapping() { }
    }

    [ClassDoc(Name = "Projection Rule")]
    public class Project
    {
        [UI(UIAttrKind.Select, Size = 4, Required = true, Header = true, NoLabel = true, Choices = "js:helpers.uplift.csObjectSelect")]
        [MemberDoc(Name = "CS Object Type")]
        public string CSObjectType { get; set; }

        [UI(UIAttrKind.Select, Size = 3, Header = true, NoLabel = true, Choices = "js:helpers.uplift.mvObjectSelect")]
        [MemberDoc(Name = "MV Object Type")]
        public string MVObjectType { get; set; }

        [UI(UIAttrKind.Select, Required = true, Size = 3, Header = true, NoLabel = true, Choices = "declared:Declared|scripted:Extension", OnBind = "js:helpers.uplift.projectType")]
        [MemberDoc(Name = "Rule Type")]
        public string RuleType { get; set; }

        [UI(Kind = UIAttrKind.Hidden, Default = "js:helpers.getGuid")]
        public Guid Id { get; set; }
        
        [UI(UIAttrKind.LongText, Size = 12, Description = true, RuleHelp = true, RuleObject = "SoftwareIDM.Uplift.IEntryWrapper, SoftwareIDM.Uplift", RuleWarnOnly = true, CSSClass = "input-monospace")]
        [MemberDoc("If Projection Type is Extension, this rule is evaluated by SoftwareIDM.MAExtension. Return a string with the the name of the object type to project, or return false/null to skip projection.", Name = "Extension Rule")]
        public string ExtensionRule { get; set; }
    }

    [ClassDoc(Name = "Filter Set")]
    public class FilterSet
    {
        [UI(UIAttrKind.Select, Size = 4, Required = true, Header = true, NoLabel = true, Unique = true, Choices = "js:helpers.uplift.csObjectSelect")]
        [MemberDoc(Name = "CS Object Type")]
        public string CSObjectType { get; set; }

        [UI(UIAttrKind.Select, Size = 4, Required = true, Header = true, NoLabel = true, Choices = "declared:Declared|scripted:Extension")]
        [MemberDoc(Name = "Filter Type")]
        public string FilterType { get; set; }

        [JsonProperty(ItemTypeNameHandling = TypeNameHandling.Objects)]
        [UI(UIAttrKind.Strips, CSSClass = "filter-rule", Duplicate = true)]
        public List<FilterRule> Filters { get; set; }

        [UI(UIAttrKind.LongText, Description = true, Size = 12, RuleHelp = true, RuleObject = "SoftwareIDM.Uplift.IEntryWrapper, SoftwareIDM.Uplift", RuleWarnOnly = true, OnBind = "js:helpers.uplift.filterType", CSSClass = "input-monospace")]
        [MemberDoc("If Filter Type is Extension, this expression is evaluated by SoftwareIDM.MAExtension. Returning true will result in the object becoming a filtered disconnector.", Name = "Extension Rule")]
        public string Rule { get; set; }

        [UI(Kind = UIAttrKind.Hidden, Default = "js:helpers.getGuid")]
        public Guid Id { get; set; }

        public FilterSet()
        {
            Filters = new List<FilterRule>();
        }
    }

    [BsonIgnoreExtraElements]
    [ClassDoc(Name = "Filter Rule")]
    public class FilterRule
    {
        [JsonProperty(ItemTypeNameHandling = TypeNameHandling.Objects)]
        [UI(UIAttrKind.Strips, CSSClass = "filter-condition", Duplicate = true)]
        public List<FilterCondition> Conditions { get; set; }

        [UI(Kind = UIAttrKind.Hidden, Default = "js:helpers.getGuid")]
        public Guid Id { get; set; }
    }

    [ClassDoc(Name = "Filter Condition")]
    public class FilterCondition
    {
        [UI(UIAttrKind.Select, Required = true, Size = 3, Header = true, NoLabel = true, Choices = "js:helpers.uplift.csAttributeSelect")]
        public string Attribute { get; set; }

        [UI(UIAttrKind.Select, Required = true, Size = 3, Header = true, NoLabel = true,
            Choices = "equality:Equals|inequality:Not Equals|present:Is Present|not-present:Not Present|substring-start:Starts With|" +
            "not-substring-start:Not Starts With|substring-end:Ends With|not-substring-end:Not Ends With|substring-any:Contains|not-substring-any:Not Contains")]
        public string Operator { get; set; }

        [UI(UIAttrKind.Text, Size = 4, Header = true, NoLabel = true)]
        public string Value { get; set; }
    }

    [ClassDoc(Name = "Export Object Flows")]
    public class ExportObjectFlows
    {
        [UI(UIAttrKind.Select, Size = 3, Required = true, Header = true, NoLabel = true, Choices = "js:helpers.uplift.mvObjectSelect")]
        [MemberDoc(Name = "MV Object Type")]
        public string MVObjectType { get; set; }

        [UI(UIAttrKind.Select, Size = 3, Required = true, Header = true, NoLabel = true, Choices = "js:helpers.uplift.csObjectSelect")]
        [MemberDoc(Name = "CS Object Type")]
        public string CSObjectType { get; set; }
        
        [JsonProperty(ItemTypeNameHandling = TypeNameHandling.Objects)]
        [UI(UIAttrKind.Strips, CSSClass = "flow-rule", Duplicate = true)]
        [MemberDoc(Name = "Flow Rules")]
        public List<ExportFlowRule> FlowRules { get; set; }

        public ExportObjectFlows()
        {
            FlowRules = new List<ExportFlowRule>();
        }
    }
    
    [ClassDoc(Name = "Export Flow Rule")]
    public class ExportFlowRule
    {
        [UI(UIAttrKind.Select, Required = true, Size = 7, Header = true, NoLabel = true, Choices = "js:helpers.uplift.mvAttributesSelect", OnBind = "js:helpers.uplift.ruleTypePortal")]
        [MemberDoc(Name = "MV Attribute(s)")]
        public List<string> Attributes { get; set; }

        [UI(UIAttrKind.Select, Required = true, Size = 3, Header = true, NoLabel = true, Choices = "js:helpers.uplift.csAttributeSelect", Unique = true)]
        [MemberDoc(Name = "CS Attribute")]
        public string Attribute { get; set; }

        [MemberDoc(Name = "Rule Type")]
        [JsonConverter(typeof(Newtonsoft.Json.Converters.StringEnumConverter))]
        [UI(UIAttrKind.Select, Size = 3, Required = true, Choices = "Direct|Extension|Constant|Portal", Default = "Direct", OnBind = "js:helpers.uplift.ruleTypeEnforce")]
        public RuleTypeEnum RuleType { get; set; }

        [UI(UIAttrKind.LongText, Size = 12, RuleHelp = true, RuleObject = "SoftwareIDM.Uplift.IEntryWrapper, SoftwareIDM.Uplift", RuleWarnOnly = true, CSSClass = "input-monospace")]
        public string Rule { get; set; }

        [UI(Kind = UIAttrKind.Hidden, Default = "js:helpers.getGuid")]
        public Guid Id { get; set; }

        [UI(UIAttrKind.Bool, Default = "false")]
        [MemberDoc(Name = "Allow Null")]
        public bool AllowNull { get; set; }
    }

    [ClassDoc("Scoped rule for expressing sync lifecycle logic", Name = "Deprovision Rule")]
    public class ScopeRule
    {
        [UI(UIAttrKind.Select, Required = true, Size = 3, Header = true, NoLabel = true, Choices = "js:helpers.uplift.csObjectSelect")]
        [MemberDoc(Name = "CS Object Type")]
        public string CSObjectType { get; set; }

        [UI(UIAttrKind.Text, Size = 12, Description = true, RuleHelp = true, RuleObject = "SoftwareIDM.Uplift.IEntryWrapper, SoftwareIDM.Uplift", RuleWarnOnly = true, CSSClass = "input-monospace")]
        [MemberDoc("Optional scoping rule is evaluated by SoftwareIDM.MAExtension to only apply the Action Rule to the right objects. Return true to process the action.", Name = "Rule Scope")]
        public string Scope { get; set; }

        [UI(UIAttrKind.Select, Size = 6, Description = true, Choices = "Disconnect|Delete|ExplicitDisconnect")]
        [MemberDoc("Deprovision action to take", Name = "Action")]
        public string Action { get; set; }

        [MemberDoc(Name = "Attribute Rules")]
        [JsonProperty(ItemTypeNameHandling = TypeNameHandling.Objects)]
        [UI(UIAttrKind.Strips, CSSClass = "flow-rule")]
        public List<SetRule> AttributeRules { get; set; } = new List<SetRule>();
    }

    [ClassDoc("Import flows for an object", Name = "Object Flows")]
    public class ObjectFlows
    {
        [UI(UIAttrKind.Select, Size = 4, Required = true, NoLabel = true, Header = true, Choices = "js:helpers.uplift.mvObjectSelect", Unique = true)]
        [MemberDoc(Name = "MV Object Type")]
        public string MVObjectType { get; set; }

        [JsonProperty(ItemTypeNameHandling = TypeNameHandling.Objects)]
        [UI(UIAttrKind.Strips, CSSClass = "flow-rule", Duplicate = true)]
        [MemberDoc(Name = "Metaverse Import Flows")]
        public List<ImportFlowSet> Flows { get; set; }

        public ObjectFlows()
        {
            Flows = new List<ImportFlowSet>();
        }
    }

    [ClassDoc("Import flow rules for an attribute", Name = "Import Flow Set")]
    public class ImportFlowSet
    {
        [UI(UIAttrKind.Select, Size = 4, Required = true, NoLabel = true, Header = true, Choices = "js:helpers.uplift.mvAttributeSelect", Unique = true)]
        [MemberDoc(Name = "Attribute")]
        public string Attribute { get; set; }

        [UI(UIAttrKind.Select, Size = 3, Required = true, Header = true, Choices = "equal:Equal|manual:Manual|ranked:Ranked")]
        [MemberDoc(Name = "Preced.")]
        public string PrecedenceType { get; set; }

        [JsonProperty(ItemTypeNameHandling = TypeNameHandling.Objects)]
        [UI(UIAttrKind.Strips, CSSClass = "flow-rule", Duplicate = true)]
        [MemberDoc(Name = "Import Rule(s)")]
        public List<ImportFlowRule> FlowRules { get; set; }

        public ImportFlowSet()
        {
            FlowRules = new List<ImportFlowRule>();
        }
    }

    public enum RuleTypeEnum
    {
        Direct,
        Extension,
        Constant,
        Portal,
        ADSync
    }

    [ClassDoc(Name = "Import Flow Rule")]
    public class ImportFlowRule
    {
        [UI(UIAttrKind.Select, Required = true, Size = 3, Header = true, NoLabel = true, Unique = true, Choices = "js:helpers.sync.maSelect")]
        [MemberDoc("MA this rule applies to")]
        public string MA { get; set; }

        [UI(UIAttrKind.Select, Size = 3, Required = true, Header = true, NoLabel = true, Choices = "js:helpers.uplift.csObjectSelect")]
        [MemberDoc(Name = "CS Object Type")]
        public string CSObjectType { get; set; }

        [MemberDoc(Name = "Rule Type")]
        [JsonConverter(typeof(Newtonsoft.Json.Converters.StringEnumConverter))]
        [UI(UIAttrKind.Select, Size = 3, Required = true, Header = true, NoLabel = true, Default = "Direct", Choices = "Direct|Extension|Constant|Portal", OnBind = "js:helpers.uplift.ruleTypeEnforce")]
        public RuleTypeEnum RuleType { get; set; }

        [UI(Kind = UIAttrKind.Hidden, Default = "js:helpers.getGuid")]
        public Guid Id { get; set; }

        [UI(UIAttrKind.Select, Required = true, Size = 12, Choices = "js:helpers.uplift.csAttributesSelect")]
        [MemberDoc(Name = "CS Attribute(s)")]
        public List<string> Attributes { get; set; }

        [UI(UIAttrKind.Text, Size = 12, RuleHelp = true, RuleObject = "SoftwareIDM.Uplift.IEntryWrapper, SoftwareIDM.Uplift", RuleWarnOnly = true, CSSClass = "input-monospace")]
        public string Rule { get; set; }
    }

    [ClassDoc("Sync configuration files", Name = "Sync Config")]
    public class SyncConfig
    {
        public List<string> Files { get; set; }

        public SyncConfig()
        {
            Files = new List<string>();
        }

    }
}
