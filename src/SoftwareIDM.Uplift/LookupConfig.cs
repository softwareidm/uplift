﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;
using Newtonsoft.Json;
using SoftwareIDM.PanelModel.Attributes;
using SoftwareIDM.PanelModel.Models;
using SoftwareIDM.PanelModel.Rules;

namespace SoftwareIDM.Uplift
{
    [ClassDoc(Name = "Lookup Config")]
    public class LookupsConfig : ConfigSetting
    {
        Dictionary<string, Lookup> _data;

        [JsonProperty(ItemTypeNameHandling = TypeNameHandling.Objects)]
        [UI(Kind = UIAttrKind.Strips, CSSClass = "uplift", Duplicate = true)]
        public List<LookupSet> Lookups { get; set; }


        [UI(UIAttrKind.Label, Size = 4, Header = true, NoLabel = true, Default = "Looup Config")]
        public override string ConfigName { get => base.ConfigName; set => base.ConfigName = value; }

        public LookupsConfig()
        {
            _data = new Dictionary<string, Lookup>();
            Lookups = new List<LookupSet>();
        }

        public string Find(string name, params string[] search)
        {
            var lookup = _data[name.ToLower()];
            return lookup.Find(0, search);
        }

        public override void Init()
        {
            foreach (var look in Lookups)
            {
                _data[look.Name.ToLower()] = new Lookup(look);
            }
        }

        public override void Merge(ConfigSetting other)
        {
            var looks = (LookupsConfig)other;
            if (looks.Lookups != null && looks.Lookups.Count > 0)
            {
                foreach (var look in looks.Lookups)
                {
                    _data[look.Name.ToLower()] = new Lookup(look);
                }
            }
        }

        public override void FromXml(XElement xml)
        {
            if (xml.Name == "lookups")
            {
                base.FromXml(xml);
                foreach (var look in xml.Elements("lookup"))
                {
                    var add = new LookupSet { Name = look.Attribute("name").Value };
                    foreach (var entry in look.Elements("add"))
                    {
                        var le = new LookupEntry();
                        foreach(var attr in entry.Attributes())
                        {
                            switch (attr.Name.LocalName)
                            {
                                case "key":
                                    le.Key = attr.Value;
                                    break;
                                case "value":
                                    le.Value = attr.Value;
                                    break;
                                default:
                                    le.ExtraAttributes.Add(new AppSettingEntry { Key = attr.Name.LocalName, Value = attr.Value });
                                    break;
                            }
                        }

                        add.Entries.Add(le);
                    }
                    
                    Lookups.Add(add);
                }
            }
        }

        public override XElement GetXml()
        {
            return AdjustXml(new XElement(XmlHelper.SI + "lookups",
                from l in Lookups
                select new XElement(XmlHelper.SI + "lookup", new XAttribute("name", l.Name),
                    from a in l.Entries
                    select new XElement(XmlHelper.SI + "add", new XAttribute("key", a.Key), new XAttribute("value", a.Value), from e in a.ExtraAttributes
                                                                                                                              select new XAttribute(e.Key, e.Value))
                )
            ));
        }
    }

    [ClassDoc(Name = "Lookup Set")]
    public class LookupSet
    {
        [UI(UIAttrKind.Text, Size = 3, Unique = true, Required = true, Header = true, NoLabel = true)]
        public string Name { get; set; }

        [JsonProperty(ItemTypeNameHandling = TypeNameHandling.Objects)]
        [UI(Kind = UIAttrKind.Strips, CSSClass = "uplift", Duplicate = true)]
        public List<LookupEntry> Entries { get; set; }

        public LookupSet()
        {
            Entries = new List<LookupEntry>();
        }
    }

    [ClassDoc(Name = "Lookup Entry")]
    public class LookupEntry
    {
        [UI(UIAttrKind.Text, Size = 5, Unique = true, Required = true, Header = true, NoLabel = true)]
        public string Key { get; set; }

        [UI(UIAttrKind.Text, Size = 5, Required = true, Header = true, NoLabel = true)]
        public string Value { get; set; }

        [JsonProperty(ItemTypeNameHandling = TypeNameHandling.Objects)]
        [UI(Kind = UIAttrKind.Strips, CSSClass = "uplift")]
        [MemberDoc(Name = "Extra Attributes")]
        public List<AppSettingEntry> ExtraAttributes { get; set; }

        public LookupEntry()
        {
            ExtraAttributes = new List<AppSettingEntry>();
        }
    }

    public class Lookup
    {
        Dictionary<string, Lookup> _data = null;
        string _value;
        Dictionary<string, string> _values = null;
        bool _leaf;
        readonly bool _hasStar = false;
        Lookup _star = null;
        readonly int _depth = 0;

        public string Name { get; set; }

        private Lookup() { }

        void Extend(Lookup lookup)
        {
            if (lookup._data != null)
            {
                foreach (var pair in lookup._data)
                {
                    _data[pair.Key] = pair.Value;
                }
            }
        }

        string Find(string retAttr, int level, params string[] search)
        {
            string ret;
            if (search[level] != null && _data.TryGetValue(search[level].ToLower(), out Lookup look))
            {
                if (look._leaf)
                {
                    if (retAttr == null)
                    {
                        return look._value;
                    }

                    look._values.TryGetValue(retAttr.ToLower(), out ret);
                    return ret;
                }

                ret = look.Find(retAttr, level + 1, search);
                if (ret != null)
                {
                    return ret;
                }
            }

            if (_hasStar)
            {
                if (_star._leaf)
                {
                    if (retAttr == null)
                    {
                        return _star._value;
                    }

                    _star._values.TryGetValue(retAttr.ToLower(), out ret);
                    return ret;
                }

                return _star.Find(retAttr, level + 1, search);
            }

            return null;
        }

        public Lookup(LookupSet look, int level = 0)
        {
            _data = new Dictionary<string, Lookup>();
            foreach (var add in look.Entries)
            {
                var split = Regex.Split(add.Key, @"(?<!\\)-", RegexOptions.Singleline);
                _depth = split.Length;
                var keys = Regex.Split(split[level], @"(?<!\\),", RegexOptions.Singleline);
                foreach (var key in keys)
                {
                    var copy = key.Replace("\\-", "-").Replace("\\,", ",").Trim().ToLower();
                    Lookup newLookup = null;
                    if (split.Length == level + 1)
                    {
                        // Leaf
                        newLookup = new Lookup { _leaf = true, _value = add.Value };
                        foreach (var attr in add.ExtraAttributes)
                        {
                            if (newLookup._values == null) { newLookup._values = new Dictionary<string, string>(); }
                            newLookup._values[attr.Key.ToLower()] = attr.Value;
                        }
                    }
                    else if (level + 1 < split.Length)
                    {
                        // Nest
                        newLookup = new Lookup(new LookupSet { Entries = new List<LookupEntry> { add } }, level + 1);
                    }

                    if (_data.TryGetValue(copy, out Lookup existLookup))
                    {
                        existLookup.Extend(newLookup);
                    }
                    else
                    {
                        _data[copy] = newLookup;
                        if (copy == "*")
                        {
                            _hasStar = true;
                            _star = _data[copy];
                        }
                    }
                }
            }
        }

        public string Find(int level = 0, params string[] search)
        {
            if (search.Length > _depth)
            {
                var copy = new string[search.Length - 1];
                var retAttr = search[0];
                for (int i = 1; i < search.Length; i++)
                {
                    copy[i - 1] = search[i];
                }

                return Find(retAttr, level, copy);
            }

            return Find(null, level, search);
        }
    }
}
