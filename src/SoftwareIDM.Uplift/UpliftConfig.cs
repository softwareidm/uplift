﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using Newtonsoft.Json;
using MongoDB.Bson.Serialization.Attributes;
using SoftwareIDM.PanelModel.Attributes;
using SoftwareIDM.PanelModel.Models;
using SoftwareIDM.PanelModel.Rules;

namespace SoftwareIDM.Uplift
{
    [ClassDoc(Name = "Uplift configuration settings")]
    public abstract class ConfigSetting
    {
        /// <summary>
        /// Called right after deserialization, opportunity to initialize dictionaries and static variables
        /// </summary>
        public abstract void Init();
        
        /// <summary>
        /// De-serialize from a namespace-stripped XElement
        /// </summary>
        public virtual void FromXml(XElement xml)
        {
            if (xml.Attributes("environment").Count() > 0)
            {
                EnvironmentRule = xml.Attribute("environment").Value;
            }
        }

        /// <summary>
        /// Produce a serialized XElement (with namespaces)
        /// </summary>
        public abstract XElement GetXml();
        
        protected XElement AdjustXml(XElement xml)
        {
            if (!string.IsNullOrEmpty(EnvironmentRule))
            {
                xml.Add(new XAttribute("environment", EnvironmentRule));
            }

            return xml;
        }

        /// <summary>
        /// Merge gets called after Init and gets passed other settings of the same type. Merge always gets called on the first item in the list so that later items can override.
        /// </summary>
        public abstract void Merge(ConfigSetting other);

        [UI(UIAttrKind.Label, Size = 4, Header = true, NoLabel = true)]
        public virtual string ConfigName { get { return ((ClassDoc)this.GetType().GetCustomAttributes(typeof(ClassDoc), true).First()).Name; } set { } }

        [UI(UIAttrKind.Text, Size = 6, Header = true, NoLabel = true, RuleHelp = true, Description = true)]
        [MemberDoc("Scoping rule to restrict settings to a given environment. e.g. Environment(\"MachineName\") == \"server01\".", Name = "Environment Scoping Rule")]
        public string EnvironmentRule { get; set; }
    }
    
    [ClassDoc(Name = "App Settings Collection")]
    public class AppSettingsConfig : ConfigSetting
    {
        readonly Dictionary<string, string> _appSettings;

        public string this[string key]
        {
            get
            {
                _appSettings.TryGetValue(key, out string ret);
                return ret;
            }
        }

        public bool ContainsKey(string key)
        {
            return _appSettings.ContainsKey(key);
        }

        [JsonProperty(ItemTypeNameHandling = TypeNameHandling.Objects)]
        [UI(UIAttrKind.Strips, CSSClass = "configs")]
        public List<AppSettingEntry> AppSettings { get; set; }

        [UI(UIAttrKind.Label, Size = 4, Header = true, NoLabel = true, Default = "App Settings Collection")]
        public override string ConfigName { get => base.ConfigName; set => base.ConfigName = value; }

        public AppSettingsConfig()
        {
            _appSettings = new Dictionary<string, string>();
            AppSettings = new List<AppSettingEntry>();
        }

        public override void Init()
        {
            if (AppSettings != null && AppSettings.Count > 0)
            {
                foreach (var e in AppSettings)
                {
                    _appSettings[e.Key] = e.Value;
                }
            }
        }

        public override void Merge(ConfigSetting other)
        {
            var merge = (AppSettingsConfig)other;
            foreach (var pair in merge._appSettings)
            {
                _appSettings[pair.Key] = pair.Value;
            }
        }

        public override void FromXml(XElement xml)
        {
            if (xml.Name == "appSettings")
            {
                base.FromXml(xml);
                foreach (var app in xml.Elements("add"))
                {
                    _appSettings[app.Attribute("key").Value] = app.Attribute("value").Value;
                    AppSettings.Add(new AppSettingEntry { Key = app.Attribute("key").Value, Value = app.Attribute("value").Value });
                }
            }
        }

        public override XElement GetXml()
        {
            return AdjustXml(new XElement(XmlHelper.SI + "appSettings",
                from a in AppSettings
                select new XElement(XmlHelper.SI + "add", new XAttribute("key", a.Key), new XAttribute("value", a.Value))
            ));
        }
    }

    [ClassDoc(Name = "App Settings Entry")]
    public class AppSettingEntry
    {
        [UI(UIAttrKind.Text, Size = 3, Unique = true, Required = true, Header = true, NoLabel = true)]
        public string Key { get; set; }

        [UI(UIAttrKind.Text, Size = 7, Header = true, NoLabel = true)]
        public string Value { get; set; }
    }
    
    [ClassDoc(Name = "Connection String")]
    public class ConnectionConfig
    {
        [UI(UIAttrKind.Text, Size = 3, Unique = true, Required = true, Header = true, NoLabel = true)]
        public string Name { get; set; }

        [UI(UIAttrKind.Text, Size = 7, Header = true, NoLabel = true)]
        [MemberDoc(Name = "Connection String")]
        public string ConnectionString { get; set; }
    }

    [ClassDoc(Name = "Connection Strings Collection")]
    public class ConnectionStringsConfig : ConfigSetting
    {
        readonly Dictionary<string, ConnectionConfig> _connectionStrings;

        public ConnectionConfig this[string key]
        {
            get
            {
                if (_connectionStrings.TryGetValue(key, out ConnectionConfig conf))
                {
                    return conf;
                }

                return null;
            }
        }

        [JsonProperty(ItemTypeNameHandling = TypeNameHandling.Objects)]
        [UI(UIAttrKind.Strips, CSSClass = "configs")]
        [MemberDoc(Name = "Connection Strings")]
        public List<ConnectionConfig> ConnectionStrings { get; set; }


        [UI(UIAttrKind.Label, Size = 4, Header = true, NoLabel = true, Default = "Connection Strings Collection")]
        public override string ConfigName { get => base.ConfigName; set => base.ConfigName = value; }

        public ConnectionStringsConfig()
        {
            _connectionStrings = new Dictionary<string, ConnectionConfig>();
            ConnectionStrings = new List<ConnectionConfig>();
        }

        public override void Init()
        {
            if (ConnectionStrings != null && ConnectionStrings.Count > 0)
            {
                foreach (var conn in ConnectionStrings)
                {
                    _connectionStrings[conn.Name] = conn;
                }
            }
        }

        public override void Merge(ConfigSetting other)
        {
            var conn = (ConnectionStringsConfig)other;
            foreach (var pair in conn._connectionStrings)
            {
                _connectionStrings[pair.Key] = pair.Value;
            }
        }

        public override void FromXml(XElement xml)
        {
            if (xml.Name == "connectionStrings")
            {
                base.FromXml(xml);
                foreach (var conn in xml.Elements("add"))
                {
                    var add = new ConnectionConfig { Name = conn.Attribute("name").Value, ConnectionString = conn.Attribute("connectionString").Value };
                    _connectionStrings[add.Name] = add;
                    ConnectionStrings.Add(add);
                }
            }
        }

        public override XElement GetXml()
        {
            return AdjustXml(new XElement(XmlHelper.SI + "connectionStrings",
                from cs in ConnectionStrings
                select new XElement(XmlHelper.SI + "add", new XAttribute("name", cs.Name), new XAttribute("connectionString", cs.ConnectionString))
            ));
        }
    }

    [ClassDoc(Name = "Deprovision Config")]
    public class DeprovisionConfig : ConfigSetting
    {
        [JsonProperty(ItemTypeNameHandling = TypeNameHandling.Objects)]
        [UI(UIAttrKind.Strips, CSSClass = "configs")]
        [MemberDoc(Name = "Deprovision Rules")]
        public List<DeprovisionRule> Rules { get; set; }
        
        [UI(UIAttrKind.Label, Size = 4, Header = true, NoLabel = true, Default = "Deprovision Config")]
        public override string ConfigName { get => base.ConfigName; set => base.ConfigName = value; }

        public DeprovisionConfig()
        {
            Rules = new List<DeprovisionRule>();
        }

        public override void Init()
        {

        }

        public override void Merge(ConfigSetting other)
        {
            var dep = (DeprovisionConfig)other;
            Rules.AddRange(dep.Rules);
        }

        public override void FromXml(XElement xml)
        {
            if (xml.Name == "deprovisionConfig")
            {
                base.FromXml(xml);
                foreach (var rule in xml.Elements("rule"))
                {
                    var add = new DeprovisionRule
                    {
                        Condition = rule.Attribute("condition").Value,
                        MVObjectType = rule.Attribute("mvObjectType").Value
                    };
                    if (rule.Elements("ma").Count() > 0)
                    {
                        add.MAs = new List<string>();
                        add.MAs.AddRange(from m in rule.Elements("ma")
                                         select m.Value);
                    }

                    Rules.Add(add);
                }
            }
        }

        public override XElement GetXml()
        {
            var ret = AdjustXml(new XElement(XmlHelper.SI + "deprovisionConfig"));
            foreach (var rule in Rules)
            {
                var add = new XElement(XmlHelper.SI + "rule", new XAttribute("condition", rule.Condition), new XAttribute("mvObjectType", rule.MVObjectType));
                if (rule.MAs != null)
                {
                    foreach(var ma in rule.MAs)
                    {
                        add.Add(new XElement(XmlHelper.SI + "ma", ma));
                    }
                }

                ret.Add(add);
            }

            return ret;
        }
    }

    [ClassDoc("Rule to deprovision multiple MAs together", Name = "Deprovision Rule")]
    public class DeprovisionRule
    {

        [UI(UIAttrKind.Select, Size = 3, Required = true, Header = true, NoLabel = true, Choices = "js:helpers.uplift.mvObjectSelect")]
        [MemberDoc(Name = "MV Object Type")]
        public string MVObjectType { get; set; }

        [UI(UIAttrKind.Select, Size = 6, Header = true, NoLabel = true, Choices = "js:helpers.uplift.deprovisionMAsSelect")]
        [MemberDoc("MA this batch of rules applies to")]
        public List<string> MAs { get; set; }

        [UI(UIAttrKind.LongText, Description = true, RuleHelp = true, RuleObject = "SoftwareIDM.Uplift.IEntryWrapper, SoftwareIDM.Uplift", RuleWarnOnly = true, CSSClass = "input-monospace")]
        [MemberDoc("Condition rule to scope whether or not the rule should be applied")]
        public string Condition { get; set; }

        Rule _conditionRule = null;
        [BsonIgnore, JsonIgnore]
        public Rule ConditionRule
        {
            get
            {
                if (_conditionRule == null && !String.IsNullOrEmpty(Condition))
                {
                    _conditionRule = Rule.Build(Condition);
                }

                return _conditionRule;
            }
        }
    }
}
