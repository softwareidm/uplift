﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using SoftwareIDM.PanelModel.Attributes;
using SoftwareIDM.PanelModel.Rules;

namespace SoftwareIDM.Uplift
{
    [ClassDoc("Wrapper for sync object accessors", Name = "Entry Wrapper")]
    public interface IEntryWrapper
    {
        AttributeValue this[string key] { get; set; }

        void SetOther(IEntryWrapper other);

        object Raw { get; }
    }

    [ClassDoc("Schema extension for sync engine", Name = "Sync Schema Attribute")]
    [BsonDiscriminator("sa")]
    public class SyncSchemaAttr : PanelModel.Models.SchemaAttr
    {
        [BsonIgnoreIfDefault]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public bool Immutable { get; set; }

        [BsonIgnoreIfDefault]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public bool Indexed { get; set; }
    }
}
