﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using SoftwareIDM.PanelModel.Attributes;
using SoftwareIDM.PanelModel.Models;

namespace SoftwareIDM.Uplift
{
    [ClassDoc("Controller class for MA Feed operations", Name = "MA Feed Control")]
    public class MAFeedControl
    {
        [BsonElement("rid")]
        public Guid RunId { get; set; }

        [BsonElement("p")]
        public int Page { get; set; }
        
        [BsonElement("fd")]
        public FeedDirection Direction { get; set; }

        [BsonElement("d")]
        public bool Delta { get; set; }

        [BsonElement("lr")]
        public DateTime LastRun { get; set; }

        [BsonElement("s")]
        public string Silo { get; set; }

        [BsonElement("rf")]
        public string RuleFilter { get; set; }

        [BsonElement("sc")]
        public SystemSchema Schema { get; set; }
    }

    [ClassDoc("Object data for MA import and export", Name = "MA Feed Data")]
    public class MAFeedData
    {
        [MemberDoc(Ignore = true)]
        public ObjectId Id { get; set; }

        [BsonElement("si")]
        public string Silo { get; set; }

        [BsonElement("u")]
        public string User { get; set; }

        [BsonElement("s")]
        public string Server { get; set; }

        [BsonElement("rid")]
        public Guid RunId { get; set; }

        [BsonElement("p")]
        public int Page { get; set; }

        [BsonElement("pc")]
        public int PageCount { get; set; }

        [BsonElement("fd")]
        public FeedDirection Direction { get; set; }

        [BsonElement("c")]
        public DateTime Created { get; set; }

        [BsonElement("co")]
        public int Count { get; set; } = 0;

        [BsonElement("d")]
        public byte[] Data { get; set; }

        [BsonElement("ha")]
        public bool HasAdds { get; set; }

        [BsonElement("de")]
        [BsonIgnoreIfDefault]
        public bool Delta { get; set; }

        [BsonElement("m")]
        public PortableNameMap Map { get; set; } = new PortableNameMap();

        readonly MemoryStream _buffer = new MemoryStream();
        public void Write(MAFeedEntry record, out bool isFull)
        {
            Count++;
            isFull = false;
            var write = record.ToBson(new PortableAttributeMapper(Map));
            _buffer.Write(BitConverter.GetBytes(write.Length), 0, 4);
            _buffer.Write(write, 0, write.Length);
            if (_buffer.Length > 1024 * 1024 * 4)
            {
                isFull = true;
            }
        }

        public void Finish()
        {
            _buffer.Position = 0;
            using (var compressed = new MemoryStream())
            {
                using (var gzip = new GZipStream(compressed, CompressionMode.Compress))
                {
                    _buffer.CopyTo(gzip);
                }

                Data = compressed.ToArray();
            }

            _buffer.Dispose();
        }

        public void Hydrate()
        {
            Objects = new List<MAFeedEntry>();
            var mapper = new PortableAttributeMapper(Map);
            using (var stream = new MemoryStream(Data))
            {
                using (var gzip = new GZipStream(stream, CompressionMode.Decompress))
                {
                    using (var result = new MemoryStream())
                    {
                        gzip.CopyTo(result);
                        result.Position = 0;
                        while (result.Position < result.Length)
                        {
                            var buffer = new byte[4];
                            result.Read(buffer, 0, 4);
                            var len = BitConverter.ToInt32(buffer, 0);
                            buffer = new byte[len];
                            result.Read(buffer, 0, len);
                            Objects.Add(buffer.FromBson<MAFeedEntry>(mapper));
                        }
                    }
                }
            }
        }

        [BsonIgnore]
        public List<MAFeedEntry> Objects { get; set; }
    }

    public enum FeedAction
    {
        Full,
        Add,
        Update,
        Delete,
        Restore
    }

    public enum FeedDirection
    {
        Import,
        Export
    }

    [ClassDoc("MA Feed Entry class for returning object data to Uplift MA")]
    public class MAFeedEntry
    {
        [BsonElement("an")]
        public Guid Anchor { get; set; }

        [BsonElement("dn")]
        public string DN { get; set; }

        [BsonElement("ot")]
        public string ObjectType { get; set; }

        [BsonElement("a")]
        public FeedAction Action { get; set; }

        [BsonElement("fa")]
        [BsonIgnoreIfNull]
        public AttributeDictionary<object> FullAttrs { get; set; }

        [BsonElement("aa")]
        [BsonIgnoreIfNull]
        public AttributeDictionary<object> AddAttrs { get; set; }

        [BsonElement("ua")]
        [BsonIgnoreIfNull]
        public AttributeDictionary<object> UpdateAttrs { get; set; }

        [BsonElement("da")]
        [BsonIgnoreIfNull]
        public AttributeDictionary<bool> DeleteAttrs { get; set; }

        [BsonElement("ma")]
        [BsonIgnoreIfNull]
        public AttributeDictionary<MAFeedMultiEntry> MultiAttrs { get; set; }
    }

    [ClassDoc("MA Feed Entry class for returning object data to Uplift MA")]
    public class MAFeedMultiEntry
    {
        [BsonElement("r")]
        [BsonIgnoreIfDefault]
        public bool IsRef { get; set; }

        [BsonElement("f")]
        [BsonIgnoreIfNull]
        public List<object> FullValues { get; set; }

        [BsonElement("d")]
        [BsonIgnoreIfNull]
        public List<object> DeleteValues { get; set; }

        [BsonElement("a")]
        [BsonIgnoreIfNull]
        public List<object> AddValues { get; set; }
    }

    [BsonIgnoreExtraElements]
    [ClassDoc("Workflow step to invoke a MIM Test fixture", Name = "MA Feed Export Step")]
    public class MAFeedExport
    {
        public BsonObjectId Id { get; set; }

        [BsonElement("ts")]
        public DateTime TimeStamp { get; set; }

        [BsonElement("n")]
        public string Name { get; set; }

        [BsonElement("ps")]
        public List<string> PreferredServices { get; set; }

        [BsonElement("di")]
        public bool Dispatched { get; set; }

        [BsonElement("co")]
        public bool Completed { get; set; }

        [BsonElement("p")]
        public Guid Provider { get; set; }

        [BsonElement("s")]
        public string Silo { get; set; }

        [BsonElement("u")]
        public string User { get; set; }

        [BsonElement("e")]
        public MAFeedData Export { get; set; }

        public const byte EXPORTADD = 25;
        public const byte EXPORTUPDATE = 26;
        public const byte EXPORTRENAME = 27;
        public const byte EXPORTDELETE = 28;
        public const byte EXPORTFAIL = 30;
    }

    public interface IProviderWriteback : IDisposable
    {
        void Init(Provider provider);

        Task Write(MAFeedData data, HistoryRecord history, List<ErrorDetail> errorDetails, System.Threading.CancellationToken cts);
    }
}
