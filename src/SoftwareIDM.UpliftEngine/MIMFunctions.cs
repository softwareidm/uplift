﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using Microsoft.MetadirectoryServices;
using SoftwareIDM.PanelModel.Attributes;
using SoftwareIDM.PanelModel.Models;
using SoftwareIDM.PanelModel.Rules;
using SoftwareIDM.Uplift;

namespace SoftwareIDM.UpliftEngine
{
    public class ModelTypes : IKnownTypes
    {
        public IEnumerable<Type> Types()
        {
            foreach (var type in Assembly.GetExecutingAssembly().ExportedTypes)
            {
                if (type.GetCustomAttribute<ClassDoc>() != null)
                {
                    yield return type;
                }
            }
        }
        public Dictionary<string, Type> Resources => null;
    }

    [ClassDoc("Functions for MIM attribute flow rules", Name = "MIM Uplift Functions")]
    public class CommonMIMFunctions : IRuleFunctions
    {
        public const string SKIPFLOW = "<SKIP-FLOW>";

        public object CallingContext { get; set; }

        [MemberDoc("Skip setting the current attribute.")]
        public AttributeValue Skip()
        {
            return new AttributeValue(SKIPFLOW, CallingContext);
        }

        [MemberDoc("Mark a multi-value attribute value behavior to append values.")]
        public AttributeValue MultiAppend(AttributeValue value)
        {
            // init to empty list instead of null so we don't flow a null into an existing list
            object list = new AppendList<string>();
            var ret = new AttributeValue(list, CallingContext);
            if (value.Value != null && value.Value is System.Collections.IEnumerable)
            {
                bool first = true;
                foreach (object val in (System.Collections.IEnumerable)value.Value)
                {
                    if (val == null) { continue; }
                    if (first)
                    {
                        first = false;
                        var t = typeof(AppendList<>).MakeGenericType(val.GetType());
                        list = Activator.CreateInstance(t);
                        ret = new AttributeValue(list, CallingContext);
                    }

                    list.GetType().GetMethod("Add").Invoke(list, new object[] { val });
                }
            }

            return ret;
        }

        [MemberDoc("Throw an UnexpectedDataException with the given message and args.")]
        public static AttributeValue UnexpectedDataException(AttributeValue message, params AttributeValue[] args)
        {
            if (!message.AsBoolean)
            {
                throw new UnexpectedDataException();
            }

            throw new UnexpectedDataException(string.Format(message.ToString(), args));
        }

        [MemberDoc("Throw a DeclineMappingException with the given message and args.")]
        public static AttributeValue DeclineMappingException(AttributeValue message, params AttributeValue[] args)
        {
            if (!message.AsBoolean)
            {
                throw new DeclineMappingException();
            }

            throw new DeclineMappingException(String.Format(message.ToString(), args));
        }

        [MemberDoc("Trigger a debugger launch event. Due to lazy binding, the argument will be evaluated after the attachment opportunity.")]
        public static AttributeValue Break(AttributeValue val)
        {
            System.Diagnostics.Debugger.Launch();
            return val;
        }

        [MemberDoc("Look up the connector count of the given Metaverse object.")]
        public AttributeValue ConnectorCount(AttributeValue mv, AttributeValue maName)
        {
            var mve = (MVEntry)((MVWrapper)mv.Value).Raw;
            return new AttributeValue(mve.ConnectedMAs[maName.ToString()].Connectors.Count, CallingContext);
        }


        [MemberDoc("Evaluates a template string and replaes values in square braces with rule expressions.")]
        public AttributeValue ApplyTemplate(AttributeValue template, AttributeValue context)
        {
            var str = template.ToString();
            if (str != null)
            {
                if (str.StartsWith("[") && str.EndsWith("]"))
                {
                    return RuleEngine.GetValue(str.Substring(1, str.Length - 2), context.Value, CallingContext);
                }

                var ret = Regex.Replace(str, @"(?<!\\)\[(.*?)(?<!\\)]", delegate (Match m)
                {
                    var rule = m.Groups[1].Value;
                    var val = RuleEngine.GetValue(rule, context.Value, CallingContext).ToString();
                    if (String.IsNullOrEmpty(val))
                    {
                        return "";
                    }

                    return val;
                }, RegexOptions.Singleline);

                return new AttributeValue(ret, CallingContext);
            }

            return new AttributeValue(null, CallingContext);
        }

        [MemberDoc("Return a value from a Lookup config.")]
        public AttributeValue Lookup(AttributeValue name, params AttributeValue[] search)
        {
            return new AttributeValue(Config.Get<LookupsConfig>().Find(name.ToString(), (from s in search
                                                                                         select s.ToString()).ToArray()), CallingContext);
        }

        [MemberDoc("Return a value from a Lookup config with a default override.")]
        public AttributeValue LookupWithDefault(AttributeValue name, AttributeValue defaultValue, params AttributeValue[] search)
        {
            var ret = Lookup(name, search);
            return ret.Value == null ? defaultValue : ret;
        }

        [MemberDoc("Return the value of an Uplift appSettings key")]
        public AttributeValue AppSettings(AttributeValue name)
        {
            return new AttributeValue(Config.AppSettings[name.ToString()], CallingContext);
        }

        [MemberDoc("Return the value of an Uplift connectionStrings key")]
        public AttributeValue ConnectionStrings(AttributeValue name)
        {
            return new AttributeValue(Config.ConnectionStrings[name.ToString()].ConnectionString, CallingContext);
        }

        [MemberDoc("Lookup the value of a particular environment variable.")]
        public AttributeValue Environment(AttributeValue name)
        {
            return new AttributeValue(System.Environment.GetEnvironmentVariable(name.ToString()), CallingContext);
        }

        [MemberDoc("Lookup the server domain name.")]
        public AttributeValue ServerDomain()
        {
            return new AttributeValue(System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().DomainName, CallingContext);
        }

        [MemberDoc("Log a message to Identity Panel service feed and return a value. Level may be \"Trace\", \"Information\", \"Warning\", or \"Error\".")]
        public static AttributeValue Log(AttributeValue returnValue, AttributeValue message, AttributeValue level)
        {
            Logger.WriteEntry(message.ToString(), (Microsoft.Extensions.Logging.LogLevel)Enum.Parse(typeof(Microsoft.Extensions.Logging.LogLevel), level.ToString(), true));
            return returnValue;
        }
        
        [MemberDoc("Helper function for producing change comparison value for unmanagedAccount object")]
        public AttributeValue UnManaged(AttributeValue joinAttribute, params AttributeValue[] args)
        {
            if (joinAttribute.AsBoolean)
            {
                return Skip();
            }

            var join = new List<string> { "Join" };
            foreach (var val in args)
            {
                if (val.AsBoolean)
                {
                    join.Add(val.ToString());
                }
            }

            return new AttributeValue(string.Join("|", join.ToArray()), CallingContext);
        }

        public static Dictionary<string, bool> UnmanagedChanged = new Dictionary<string, bool>();

        [MemberDoc("Helper function for detecting unmanaged join value changed")]
        public AttributeValue UnManagedChanged(AttributeValue joinAttribute, params AttributeValue[] args)
        {
            if (joinAttribute.AsBoolean)
            {
                var join = new List<string> { "Join" };
                foreach (var val in args)
                {
                    if (val.AsBoolean)
                    {
                        join.Add(val.ToString());
                    }
                }

                var test = String.Join("|", join.ToArray());
                if (test != joinAttribute.ToString() && joinAttribute.CallingContext != null && joinAttribute.CallingContext is MVWrapper)
                {
                    var mv = (MVWrapper)joinAttribute.CallingContext;
                    var cs = (CSWrapper)mv.CS;
                    UnmanagedChanged[cs.DN] = true;
                }

                return new AttributeValue(test != joinAttribute.ToString(), CallingContext);
            }

            return Skip();
        }

        public AttributeValue UnManagedHasChanged(AttributeValue dn)
        {
            return new AttributeValue(UnmanagedChanged.ContainsKey(dn.ToString()), CallingContext);
        }
    }

}
