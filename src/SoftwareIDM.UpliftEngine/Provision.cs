﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.MetadirectoryServices;
using SoftwareIDM.PanelModel.Rules;
using SoftwareIDM.Uplift;

namespace SoftwareIDM.UpliftEngine
{

    public static class ProvisionEngine
    {
        static bool? enableProvision;

        static readonly Regex reInteger = new Regex(@"(-?\d+)");
        static readonly Regex reComparison = new Regex(@"(>=|<=|==|>|<)");
        static bool TestConnector(long connectorCount, ProvisionRule maRule)
        {
            if (!string.IsNullOrEmpty(maRule.Connector))
            {
                var num = int.Parse(reInteger.Match(maRule.Connector).Groups[1].Value);
                if (reComparison.IsMatch(maRule.Connector))
                {
                    var compare = reComparison.Match(maRule.Connector).Groups[1].Value;

                    switch (compare)
                    {
                        case ">=":
                            return connectorCount >= num;
                        case "<=":
                            return connectorCount <= num;
                        case "==":
                            return connectorCount == num;
                        case ">":
                            return connectorCount > num;
                        case "<":
                            return connectorCount < num;
                    }
                }
                else
                {
                    return num == connectorCount;
                }
            }

            return false;
        }

        static bool TestRule(MVEntry mventry, ProvisionRule maRule, string maName, bool log)
        {
            try
            {
                if (!mventry.ObjectType.Equals(maRule.MVObjectType, StringComparison.OrdinalIgnoreCase))
                {
                    return false;
                }

                if (!enableProvision.HasValue)
                {
                    enableProvision = Config.AppSettings == null || Config.AppSettings["EnableProvision"] != "false";
                }

                if (!enableProvision.Value && !maRule.AlwaysEnable)
                {
                    return false;
                }

                var connectedMA = mventry.ConnectedMAs[maName];
                var connectorCount = (long)connectedMA.Connectors.Count;
                if (!TestConnector(connectorCount, maRule))
                {
                    return false;
                }

                if (!string.IsNullOrEmpty(maRule.Condition))
                {
                    var mvw = (MVWrapper)mventry;
                    bool passCondition = false;
                    var conns = connectedMA.Connectors;
                    if (conns.Count > 0)
                    {
                        for (int i = 0; i < conns.Count; i++)
                        {
                            mvw.SetOther((CSWrapper)conns.ByIndex[i]);
                            if (RuleEngine.GetValue(maRule.ConditionRule, mvw, mvw).AsBoolean)
                            {
                                passCondition = true;
                            }
                        }
                    }
                    else
                    {
                        passCondition = RuleEngine.GetValue(maRule.ConditionRule, mvw, mvw).AsBoolean;
                    }

                    if (!passCondition)
                    {
                        return false;
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                if (log)
                {
                    Logger.WriteEntry($"*r*Error testing provision conditions on {mventry.ObjectID}:*r*\r\nFor {maRule.MVObjectType}, Connector count {maRule.Connector}\r\n{EscapeRule(maRule.Condition)}\r\n {e.Message}\r\n{e.StackTrace}", LogLevel.Error);
                }
                throw;
            }
        }

        static string EscapeRule(string rule)
        {
            if (!string.IsNullOrEmpty(rule))
            {
                return rule.Replace("{", "{{").Replace("}", "}}");
            }

            return rule ?? "null";
        }

        static List<ProvisionChange> Stage(MVEntry mventry, ProvisionRule maRule, string maName, bool log)
        {
            var connectedMA = mventry.ConnectedMAs[maName];
            var connectorCount = (long)connectedMA.Connectors.Count;
            var mv = (MVWrapper)mventry;
            var ret = new List<ProvisionChange>();
            if (connectorCount > 0)
            {
                for (int i = 0; i < connectorCount; i++)
                {
                    var update = new ProvisionChange { Provision = new ProvisionWrapper(connectedMA), RuleName = maRule.Name };
                    update.CSEntry = connectedMA.Connectors.ByIndex[i];
                    update.DN = update.CSEntry.DN.ToString();
                    update.MA = connectedMA;
                    ret.Add(update);
                }
            }

            if (maRule.ForceAdd || connectorCount == 0)
            {
                var add = new ProvisionChange { Change = ChangeType.Start, Provision = new ProvisionWrapper(connectedMA), RuleName = maRule.Name };
                add.Provision.Changed = true;
                add.Provision.StartConnector = true;
                add.MA = connectedMA;
                ret.Add(add);
            }

            for (int i = 0; i < ret.Count; i++)
            {
                var wrap = ret[i];
                // on a force add we skip existing connectors
                if (maRule.ForceAdd && !wrap.Provision.StartConnector)
                {
                    continue;
                }

                if (maRule.IsExchange)
                {
                    wrap.Provision.ExchangeUtils = true;
                }

                if (maRule.Deprovision)
                {
                    wrap.Change = ChangeType.Deprovision;
                    wrap.Provision.Changed = true;
                    wrap.Provision.Deprovision = true;
                    continue;
                }

                wrap.Provision.SetOther(mv);
                mv.SetOther(wrap.Provision);
                if (wrap.CSEntry != null)
                {
                    wrap.Provision.SetCS(wrap.CSEntry);
                }

                // do actual rule now
                if (!String.IsNullOrEmpty(maRule.CSObjectType))
                {
                    if (Regex.IsMatch(maRule.CSObjectType, @"^[a-z]+$", RegexOptions.IgnoreCase))
                    {
                        wrap.Provision.Set("objecttype", new AttributeValue(maRule.CSObjectType, mv));
                    }
                    else
                    {
                        wrap.Provision.Set("objecttype", RuleEngine.GetValue(maRule.CSObjectType, mv, mv));
                    }
                }

                if (maRule.ObjectClass != null)
                {
                    wrap.Provision.Set("objectclass", new AttributeValue(maRule.ObjectClass.Split(',').ToList(), mv));
                }

                foreach (var aRule in maRule.AttributeRules)
                {
                    AttributeValue candidate;
                    try
                    {
                        candidate = RuleEngine.GetValue(aRule.RuleInst, mv, mv);
                    }
                    catch (Exception e)
                    {
                        if (e is UnexpectedDataException || e is DeclineMappingException) { throw; }
                        if (log)
                        {
                            Logger.WriteEntry($"*r*Error calculating value for {aRule.Attribute} on {mventry.ObjectID}:*r*\r\nFor {EscapeRule(maRule.Condition)}\r\n {e.Message}\r\n{e.StackTrace}", LogLevel.Error);
                        }

                        throw;
                    }

                    if (candidate.ToString() == CommonMIMFunctions.SKIPFLOW) { continue; }

                    if (!wrap.Provision.Get(aRule.Attribute).Equals(candidate))
                    {
                        wrap.Provision.Set(aRule.Attribute, candidate);
                        wrap.Provision.Changed = true;
                        if (aRule.Attribute.Equals("dn", StringComparison.OrdinalIgnoreCase))
                        {
                            if (wrap.Change == ChangeType.None)
                            {
                                if (wrap.CSEntry != null && !wrap.CSEntry.DN.ToString().Equals(candidate.ToString(), StringComparison.OrdinalIgnoreCase))
                                {
                                    wrap.Change = ChangeType.Rename;
                                }
                            }
                            else if (wrap.Change == ChangeType.Start)
                            {
                                wrap.DN = candidate.ToString();
                            }
                        }
                    }
                }

                wrap.Retries = maRule.RetryRules;
            }

            return ret;
        }

        public static List<ProvisionChange> Provision(MVEntry mventry, bool log)
        {
            var config = Config.Get<MARuleConfig>();
            var ret = new List<ProvisionChange>();
            // ma -> accumlated changes
            var stage = new List<ProvisionChange>();
            var deprovision = new Dictionary<string, bool>(StringComparer.OrdinalIgnoreCase);
            foreach (var pair in config.ProvisionRules)
            {
                foreach (var maRule in pair.Value)
                {
                    if (!TestRule(mventry, maRule, pair.Key, log))
                    {
                        continue;
                    }

                    foreach (var change in Stage(mventry, maRule, pair.Key, log))
                    {
                        if (change.Change == ChangeType.None) { continue; }
                        stage.Add(change);
                        if (change.Change == ChangeType.Deprovision)
                        {
                            deprovision[change.MA.Name + change.CSEntry.DN.ToString()] = true;
                        }
                    }
                }
            }

            var conf = Config.Get<DeprovisionConfig>();
            if (conf != null)
            {
                foreach (var rule in conf.Rules)
                {
                    if (!rule.MVObjectType.Equals(mventry.ObjectType, StringComparison.OrdinalIgnoreCase))
                    {
                        continue;
                    }

                    var mv = (MVWrapper)mventry;
                    var val = RuleEngine.GetValue(rule.ConditionRule, mv, mv);
                    if (!val.AsBoolean)
                    {
                        continue;
                    }

                    if (rule.MAs == null || rule.MAs.Contains("all", StringComparer.OrdinalIgnoreCase))
                    {
                        stage.Add(new ProvisionChange { Change = ChangeType.Deprovision });
                        deprovision["*"] = true;
                        continue;
                    }

                    foreach (var ma in rule.MAs)
                    {
                        var m = mventry.ConnectedMAs[ma];
                        if (m.Connectors.Count > 0)
                        {
                            foreach (var cs in m.Connectors)
                            {
                                stage.Add(new ProvisionChange { Change = ChangeType.Deprovision, CSEntry = cs, MA = m, DN = cs.DN.ToString() });
                                deprovision[m.Name + cs.DN.ToString()] = true;
                            }
                        }
                    }
                }
            }


            // apply accumulated changes
            try
            {
                foreach (var set in stage)
                {
                    switch (set.Change)
                    {
                        case ChangeType.Deprovision:
                            if (set.Provision != null && set.Provision.Raw != null && set.Provision.Raw is Dictionary<string, AttributeValue> attributes)
                            {
                                var csw = (CSWrapper)set.CSEntry;
                                foreach (var pair in attributes)
                                {
                                    csw.Set(pair.Key, pair.Value);
                                }
                            }

                            if (set.CSEntry == null)
                            {
                                mventry.ConnectedMAs.DeprovisionAll();
                                ret.Add(set);
                                return ret;
                            }

                            set.CSEntry.Deprovision();
                            ret.Add(set);
                            break;
                        case ChangeType.Rename:
                            if (deprovision.ContainsKey(set.MA.Name + set.DN.ToString()) || deprovision.ContainsKey("*")) { break; }
                            if (!set.CSEntry.DN.ToString().Equals(set.Provision.Get("DN").ToString(), StringComparison.OrdinalIgnoreCase))
                            {
                                try
                                {
                                    ((CSWrapper)set.CSEntry).Set("DN", set.Provision.Get("DN"));
                                    ret.Add(set);
                                }
                                catch (ObjectAlreadyExistsException)
                                {
                                    if (set.Retries != null && set.Retries.Count > 0)
                                    {
                                        AttributeValue newDN = null;
                                        foreach (var aRule in set.Retries)
                                        {
                                            if (aRule.Attribute.ToLower() != "dn") { continue; }
                                            var mv = (MVWrapper)mventry;
                                            mv.SetOther(set.Provision);
                                            set.Provision.SetOther(mv);
                                            newDN = RuleEngine.GetValue(aRule.RuleInst, mv, mv);
                                        }

                                        if (newDN != null && newDN.Value != null)
                                        {
                                            ((CSWrapper)set.CSEntry).Set("DN", newDN);
                                            ret.Add(set);
                                        }
                                    }
                                    else
                                    {
                                        throw;
                                    }
                                }
                            }
                            break;
                        case ChangeType.Start:
                            if (set.Provision.ExchangeUtils)
                            {
                                string alias = null;
                                string dn = set.Provision.Get("DN").ToString();
                                string homeMDB = set.Provision.Get("homeMDB").ToString();
                                string cn = AD.GetCN(dn);
                                string ou = AD.GetContainer(dn);
                                if (set.Provision["alias"].AsBoolean)
                                {
                                    alias = set.Provision["alias"].ToString();
                                }
                                else if (set.Provision["mailnickname"].AsBoolean)
                                {
                                    alias = set.Provision["mailnickname"].ToString();
                                }

                                set.CSEntry = ExchangeUtils.CreateMailbox(set.MA, set.MA.EscapeDNComponent(cn).Concat(ou), alias, homeMDB);
                            }
                            else
                            {
                                if (set.Provision.Get("objectclass").AsBoolean)
                                {
                                    var ocs = ((List<string>)set.Provision["objectClass"].Value).ToArray();
                                    var oc = Utils.ValueCollection(ocs);
                                    set.CSEntry = set.MA.Connectors.StartNewConnector(set.Provision.Get("objecttype").ToString(), oc);
                                }
                                else
                                {
                                    set.CSEntry = set.MA.Connectors.StartNewConnector(set.Provision.Get("objecttype").ToString());
                                }
                            }

                            var omit = new List<string> { "objecttype", "objectclass" };
                            if (set.Provision.ExchangeUtils)
                            {
                                omit.AddRange(new string[] { "dn", "alias", "mailnickname", "homemdb" });
                            }

                            var cs = (CSWrapper)set.CSEntry;
                            bool skip = false;
                            foreach (var pair in ((Dictionary<string, AttributeValue>)set.Provision.Raw))
                            {
                                if (omit.Contains(pair.Key))
                                {
                                    continue;
                                }

                                // allow skipping of provision in a provision scenario
                                if (pair.Value.ToString() == CommonMIMFunctions.SKIPFLOW)
                                {
                                    skip = true;
                                }
                                else
                                {
                                    cs.Set(pair.Key, pair.Value);
                                }
                            }

                            try
                            {
                                if (!skip)
                                {
                                    set.CSEntry.CommitNewConnector();
                                    ret.Add(set);
                                }
                            }
                            catch (ObjectAlreadyExistsException)
                            {
                                if (set.Retries != null && set.Retries.Count > 0)
                                {
                                    skip = false;
                                    foreach (var aRule in set.Retries)
                                    {
                                        var mv = (MVWrapper)mventry;
                                        mv.SetOther(set.Provision);
                                        set.Provision.SetOther(mv);
                                        try
                                        {
                                            var val = RuleEngine.GetValue(aRule.RuleInst, mv, mv);
                                            // allow skipping of provision in a retry scenario
                                            if (val.ToString() == CommonMIMFunctions.SKIPFLOW)
                                            {
                                                skip = true;
                                            }
                                            else
                                            {
                                                cs.Set(aRule.Attribute, val);
                                            }
                                        }
                                        catch
                                        {
                                            if (log)
                                            {
                                                Logger.WriteEntry($"*r*Error calculating retry value for {mventry.ObjectID}:*r*\r\n {aRule.Attribute} to {aRule.Rule}", LogLevel.Error);
                                            }
                                            throw;
                                        }
                                    }

                                    if (!skip)
                                    {
                                        set.CSEntry.CommitNewConnector();
                                        ret.Add(set);
                                    }
                                }
                                else
                                {
                                    throw;
                                }
                            }

                            break;
                    }
                }
            }
            catch (Exception e)
            {
                if (log)
                {
                    Logger.WriteEntry($"*r*Error applying changes to {mventry.ObjectID}:*r*\r\n {e.Message}\r\n{e.StackTrace}", LogLevel.Error);
                }

                throw;
            }

            return ret;
        }
    }

    public class ProvisionChange
    {
        public ChangeType Change { get; set; }

        public ProvisionWrapper Provision { get; set; }

        internal List<SetRule> Retries { get; set; }

        internal CSEntry CSEntry { get; set; }

        internal ConnectedMA MA { get; set; }

        internal string DN { get; set; }

        internal string RuleName { get; set; }
    }

    public enum ChangeType
    {
        None,
        Deprovision,
        Start,
        Rename
    }
}
