﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using SoftwareIDM.Uplift;
using Microsoft.MetadirectoryServices;
using System.Xml.Linq;
using SoftwareIDM.PanelModel.Rules;
using SoftwareIDM.PanelModel.Models;
using System.Windows.Forms.VisualStyles;
using MongoDB.Bson.Serialization.Conventions;

namespace SoftwareIDM.UpliftEngine
{
    public static class Config
    {
        static Dictionary<string, ConfigSetting> _data;
        public static Dictionary<string, ConfigSetting> Data
        {
            get
            {
                if (_data == null)
                {
                    _data = new Dictionary<string, ConfigSetting>();
                    Init();
                }

                return _data;
            }
        }

        static List<Type> _types = null;
        static RuleCache _ruleCache;
        static RuleFunctions _rFuncs;

        public static void Init()
        {
            Init(Utils.ExtensionsDirectory);
        }

        public static void Finish()
        {
            Logger.Finish();
            if (_rFuncs != null)
            {
                _rFuncs.Dispose();
            }
        }

        public static void Init(string extensionsDir, bool reset=false)
        {
            if (_types != null && !reset)
            {
                return;
            }

            // load the file
            // FromXml on each one, then Merge from the first to the last, then Init on the final
            _types = new List<Type>();
            _ruleCache = new RuleCache();
            _data = new Dictionary<string, ConfigSetting>();
            RuleEngine.RuleCache = o => _ruleCache;

            var xml = XElement.Load(Path.Combine(extensionsDir, "App.config"));
            var foundAssem = new Dictionary<string, bool>();

            var locate = @"C:\Program Files\SoftwareIDM\PanelTools\config.json";
            if (xml.Attributes("jsonConfig").Count() > 0)
            {
                locate = xml.Attribute("jsonConfig").Value;
            }

            if (xml.Attributes("launchDebugger").Count() > 0 && bool.Parse(xml.Attribute("launchDebugger").Value))
            {
                System.Diagnostics.Debugger.Launch();
            }

            if (locate.EndsWith("config.json"))
            {
                locate = new FileInfo(locate).Directory.FullName;
            }

            var dir = new DirectoryInfo(extensionsDir);
            foreach (var dll in dir.GetFiles("*.dll"))
            {
                if (dll.FullName.Contains("SoftwareIDM"))
                {
                    System.Reflection.Assembly.LoadFile(dll.FullName);
                }
            }

            //For Mongo BSON ignore exra elements instead of throwing error
            var pack = new ConventionPack
            {
                new IgnoreExtraElementsConvention(true)
            };
            ConventionRegistry.Register("Switch to Ignore Elements", pack, (ty) => true);

            foreach (var assem in from a in AppDomain.CurrentDomain.GetAssemblies()
                                  where a.FullName.Contains("SoftwareIDM.")
                                  select a)
            {
                foundAssem[assem.FullName.Split(',')[0].Trim()] = true;
                _types.AddRange(from c in assem.ExportedTypes
                                where typeof(ConfigSetting).IsAssignableFrom(c)
                                select c);
                foreach (var loaderType in (from c in assem.ExportedTypes
                                            where c.IsClass && typeof(IKnownTypes).IsAssignableFrom(c)
                                            select c))
                {
                    if (loaderType == null) { continue; }
                    var loader = (IKnownTypes)Activator.CreateInstance(loaderType);
                    foreach (var t in loader.Types())
                    {
                        if (t.ContainsGenericParameters || t.IsInterface)
                        {
                            continue;
                        }

                        MongoDB.Bson.Serialization.BsonClassMap.LookupClassMap(t);
                    }
                }
            }
            
            PanelModel.Helpers.Conf.Set(locate);
            PanelModel.Helpers.Conf.Reset();
            Rest.StartSession(new System.Threading.CancellationToken(), true).GetAwaiter().GetResult();
            
            // make sure panel client is in the app domain
            var server = ClientConfig.Server;

            var all = new List<ConfigSetting>();
            xml = StripNamespaces(xml);
            foreach (var el in xml.Elements())
            {
                var type = _types.Find(m => m.Name.ToLower().StartsWith(el.Name.LocalName.ToLower()));
                var inst = (ConfigSetting)Activator.CreateInstance(type);
                inst.FromXml(el);
                if (!string.IsNullOrEmpty(inst.EnvironmentRule))
                {
                    var val = RuleEngine.GetValue(inst.EnvironmentRule, null, null);
                    if (!val.AsBoolean) { continue; }
                }

                inst.Init();
                if (_data.TryGetValue(type.Name, out ConfigSetting exist))
                {
                    exist.Merge(inst);
                }
                else
                {
                    _data[type.Name] = inst;
                }
            }

            // load other dlls that might have rule functions we need
            var funcs = Get<RuleFunctionsConfig>();
            if (funcs != null)
            {
                var extend = Get<RuleFunctionsConfig>().ExtensionDlls;
                if (!string.IsNullOrEmpty(extend))
                {
                    foreach (var file in extend.Split(','))
                    {
                        var path = Path.Combine(extensionsDir, file.Trim());
                        var panelPath = locate.Replace("config.json", file);
                        if (File.Exists(path))
                        {
                            // load into app domain
                            var assem = System.Reflection.Assembly.LoadFrom(path);
                        }
                        else if (File.Exists(panelPath))
                        {
                            var assem = System.Reflection.Assembly.LoadFrom(path);
                        }
                    }

                    RuleEngine.ResetCache();
                }

                foreach (var rule in funcs.RuleFunctions)
                {
                    if (_rFuncs == null)
                    {
                        _rFuncs = new RuleFunctions();
                    }

                    _rFuncs.SetCaller(rule);
                    _ruleCache.RegisterRule(rule);
                }
            }

            var settings = Util.Settings<Schedules>(new System.Threading.CancellationToken()).GetAwaiter().GetResult();
            if (settings.RuleFunctions != null && settings.RuleFunctions.Count > 0)
            {
                foreach (var rule in settings.RuleFunctions)
                {
                    _rFuncs.SetCaller(rule);
                    _ruleCache.RegisterRule(rule);
                }
            }
        }

        public static T Get<T>() where T : ConfigSetting
        {
            Data.TryGetValue(typeof(T).Name, out ConfigSetting ret);
            return (T)ret ?? null;
        }

        public static ConnectionStringsConfig ConnectionStrings
        {
            get
            {
                return Get<ConnectionStringsConfig>();
            }
        }

        public static AppSettingsConfig AppSettings
        {
            get
            {
                return Get<AppSettingsConfig>();
            }
        }

        public static LookupsConfig Lookups
        {
            get
            {
                return Get<LookupsConfig>();
            }
        }

        // http://stackoverflow.com/questions/1145659/ignore-namespaces-in-linq-to-xml
        public static XElement StripNamespaces(XElement rootElement)
        {
            foreach (var element in rootElement.DescendantsAndSelf())
            {
                // update element name if a namespace is available
                if (element.Name.Namespace != XNamespace.None)
                {
                    element.Name = XNamespace.None.GetName(element.Name.LocalName);
                }

                // check if the element contains attributes with defined namespaces (ignore xml and empty namespaces)
                bool hasDefinedNamespaces = element.Attributes().Any(attribute => attribute.IsNamespaceDeclaration ||
                        (attribute.Name.Namespace != XNamespace.None && attribute.Name.Namespace != XNamespace.Xml));

                if (hasDefinedNamespaces)
                {
                    // ignore attributes with a namespace declaration
                    // strip namespace from attributes with defined namespaces, ignore xml / empty namespaces
                    // xml namespace is ignored to retain the space preserve attribute
                    var attributes = element.Attributes()
                                            .Where(attribute => !attribute.IsNamespaceDeclaration)
                                            .Select(attribute =>
                                                (attribute.Name.Namespace != XNamespace.None && attribute.Name.Namespace != XNamespace.Xml) ?
                                                    new XAttribute(XNamespace.None.GetName(attribute.Name.LocalName), attribute.Value) :
                                                    attribute
                                            );

                    // replace with attributes result
                    element.ReplaceAttributes(attributes);
                }
            }
            return rootElement;
        }
    }
}
