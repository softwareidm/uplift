﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.MetadirectoryServices;
using SoftwareIDM.PanelModel.Rules;
using SoftwareIDM.Uplift;

namespace SoftwareIDM.UpliftEngine
{
    public class AppendList<T> : List<T>
    {
        public AppendList() : base() { }
        public AppendList(int capacity) : base(capacity) { }
        public AppendList(IEnumerable<T> collection) : base(collection) { }
    }

    public static class MultiHelper
    {
        public static List<byte[]> FromBinMulti(Attrib multi)
        {
            if (multi.IsPresent)
            {
                return new List<byte[]>(multi.Values.ToByteArrays());
            }

            return new List<byte[]>();
        }

        public static List<long> FromIntMulti(Attrib multi)
        {
            if (multi.IsPresent)
            {
                return new List<long>(multi.Values.ToIntegerArray());
            }

            return new List<long>();
        }

        public static List<string> FromStringMulti(Attrib multi)
        {
            if (multi.IsPresent)
            {
                return new List<string>(multi.Values.ToStringArray());
            }

            return new List<string>();
        }

        public static void ToByteMulti(Attrib multi, AttributeValue value)
        {
            var merge = new List<byte[]>();
            if (value.Value is AppendList<byte[]> && multi.IsPresent)
            {
                merge.AddRange(multi.Values.ToByteArrays());
            }

            if (value.Value != null)
            {
                if (value.DataType == PanelModel.Rules.AttributeType.Other)
                {
                    foreach (object val in (System.Collections.IEnumerable)value.Value)
                    {
                        if (val != null)
                        {
                            merge.Add((byte[])CoerceType.SafeCoerce(PanelModel.Rules.AttributeType.Binary, val));
                        }
                    }
                }
                else
                {
                    merge.Add((byte[])value.AsType(PanelModel.Rules.AttributeType.Binary));
                }
            }

            if (merge.Count > 0)
            {
                multi.Values = Utils.ValueCollection(merge.ToArray());
            }
            else if (multi.IsPresent)
            {
                multi.Delete();
            }
        }

        public static void ToIntMulti(Attrib multi, AttributeValue value)
        {
            var merge = new List<long>();
            if (value.Value is AppendList<long> && multi.IsPresent)
            {
                merge.AddRange(multi.Values.ToIntegerArray());
            }

            if (value.Value != null)
            {
                if (value.DataType == PanelModel.Rules.AttributeType.Other)
                {
                    foreach (object val in (System.Collections.IEnumerable)value.Value)
                    {
                        if (val != null)
                        {
                            merge.Add((long)CoerceType.SafeCoerce(PanelModel.Rules.AttributeType.Long, val));
                        }
                    }
                }
                else
                {
                    merge.Add((long)value.AsType(PanelModel.Rules.AttributeType.Long));
                }
            }

            if (merge.Count > 0)
            {
                multi.Values = Utils.ValueCollection(merge.ToArray());
            }
            else if (multi.IsPresent)
            {
                multi.Delete();
            }
        }

        public static void ToStringMulti(Attrib multi, AttributeValue value)
        {
            var merge = new List<string>();
            if (value.Value is AppendList<string> && multi.IsPresent)
            {
                merge.AddRange(multi.Values.ToStringArray());
            }

            if (value.Value != null)
            {
                if (value.DataType == PanelModel.Rules.AttributeType.Other)
                {
                    foreach (object val in (System.Collections.IEnumerable)value.Value)
                    {
                        if (val != null)
                        {
                            merge.Add((string)CoerceType.SafeCoerce(PanelModel.Rules.AttributeType.String, val));
                        }
                    }
                }
                else
                {
                    merge.Add((string)value.AsType(PanelModel.Rules.AttributeType.String));
                }
            }

            if (merge.Count > 0)
            {
                multi.Values = Utils.ValueCollection(merge.ToArray());
            }
            else if (multi.IsPresent)
            {
                multi.Delete();
            }
        }

        public static void ToRefMulti(Attrib multi, AttributeValue value, ManagementAgent ma)
        {
            var merge = new List<string>();
            if (value.Value is AppendList<string> && multi.IsPresent)
            {
                merge.AddRange(multi.Values.ToStringArray());
            }

            if (value.Value != null)
            {
                if (value.DataType == PanelModel.Rules.AttributeType.Other)
                {
                    foreach (object val in (System.Collections.IEnumerable)value.Value)
                    {
                        if (val != null)
                        {
                            merge.Add((string)CoerceType.SafeCoerce(PanelModel.Rules.AttributeType.String, val));
                        }
                    }
                }
                else
                {
                    merge.Add((string)value.AsType(PanelModel.Rules.AttributeType.String));
                }
            }

            if (merge.Count > 0)
            {
                multi.Values = Utils.ValueCollection(Array.Empty<string>());
                for (int i = 0; i < merge.Count; i++)
                {
                    multi.Values.Add(ma.CreateDN(merge[i]));
                }
            }
            else if (multi.IsPresent)
            {
                multi.Delete();
            }
        }
    }

    public class ProvisionWrapper : IEntryWrapper
    {
        public ProvisionWrapper() { }

        public AttributeValue this[string key]
        {
            get
            {
                return Get(key);
            }

            set
            {
                Set(key, value);
            }
        }

        readonly Dictionary<string, AttributeValue> _stage = new Dictionary<string, AttributeValue>();
        CSWrapper _cs = null;
        readonly ManagementAgent _ma;

        public void SetCS(CSEntry cs)
        {
            _cs = cs;
        }

        public bool Changed { get; set; }

        public bool Deprovision { get; set; }

        public bool StartConnector { get; set; }

        public bool ExchangeUtils { get; set; }

        public string MAName { get { return _ma.Name; } }

        public IEntryWrapper MV { get; set; }

        // Allow ObjectRecord rules to work the same on Wrappers
        public IEntryWrapper Attributes { get { return this; } }

        public object Raw { get { return _stage; } }

        public void SetOther(IEntryWrapper other)
        {
            MV = other;
        }

        public ProvisionWrapper(ManagementAgent ma)
        {
            _ma = ma;
        }

        public AttributeValue Get(string field)
        {
            if (_stage.TryGetValue(field.ToLower(), out AttributeValue ret))
            {
                return ret;
            }

            if (_cs != null)
            {
                return _cs.Get(field.ToLower());
            }

            return new AttributeValue(null, this);
        }

        public void Set(string field, AttributeValue value)
        {
            _stage[field.ToLower()] = value;
        }
    }

    public class CSWrapper : IEntryWrapper
    {
        public AttributeValue this[string key]
        {
            get
            {
                return Get(key);
            }

            set
            {
                Set(key, value);
            }
        }

        public object Raw { get { return _cs; } }

        public string MAName { get { return _cs.MA.Name; } }

        public string RDN { get { return _cs.RDN; } }

        public string DN { get { return _cs.DN.ToString(); } }

        public string ObjectType { get { return _cs.ObjectType.ToString(); } }

        public AttributeValue Get(string field)
        {
            if (field.ToLower() == "dn")
            {
                try
                {
                    return new AttributeValue(_cs.DN.ToString(), this);
                }
                catch (InvalidOperationException) { } // DN not yet 
            }
            else if (field.ToLower() == "rdn")
            {
                return new AttributeValue(_cs.RDN, this);
            }
            else if (field.ToLower() == "objecttype")
            {
                return new AttributeValue(_cs.ObjectType, this);
            }

            if (_cs[field].IsPresent)
            {
                if (_cs[field].IsMultivalued)
                {
                    switch (_cs[field].DataType)
                    {
                        case Microsoft.MetadirectoryServices.AttributeType.Binary:
                            return new AttributeValue(MultiHelper.FromBinMulti(_cs[field]), this);
                        case Microsoft.MetadirectoryServices.AttributeType.Integer:
                            return new AttributeValue(MultiHelper.FromIntMulti(_cs[field]), this);
                        default:
                            return new AttributeValue(MultiHelper.FromStringMulti(_cs[field]), this);
                    }
                }

                switch (_cs[field].DataType)
                {
                    case Microsoft.MetadirectoryServices.AttributeType.Binary:
                        return new AttributeValue(_cs[field].BinaryValue, this);
                    case Microsoft.MetadirectoryServices.AttributeType.Boolean:
                        return new AttributeValue(_cs[field].BooleanValue, this);
                    case Microsoft.MetadirectoryServices.AttributeType.Integer:
                        return new AttributeValue(_cs[field].IntegerValue, this);
                    case Microsoft.MetadirectoryServices.AttributeType.Reference:
                        return new AttributeValue(_cs[field].ReferenceValue.ToString(), this);
                    default:
                        return new AttributeValue(_cs[field].Value, this);
                }
            }

            return new AttributeValue(null, this);
        }

        ReferenceValue MakeRef(AttributeValue value)
        {
            return _cs.MA.CreateDN(value.ToString());
        }

        public void Set(string field, AttributeValue value)
        {
            if (value.ToString() == CommonMIMFunctions.SKIPFLOW) { return; }

            //cdata
            if (field.ToLower() == "dn")
            {
                _cs.DN = MakeRef(value);
                return;
            }

            if (_cs[field].IsMultivalued)
            {
                switch (_cs[field].DataType)
                {
                    case Microsoft.MetadirectoryServices.AttributeType.Binary:
                        MultiHelper.ToByteMulti(_cs[field], value);
                        return;
                    case Microsoft.MetadirectoryServices.AttributeType.Integer:
                        MultiHelper.ToIntMulti(_cs[field], value);
                        return;
                    case Microsoft.MetadirectoryServices.AttributeType.Reference:
                        MultiHelper.ToRefMulti(_cs[field], value, _cs.MA);
                        return;
                    default: // Microsoft.MetadirectoryServices.AttributeType.String:
                        MultiHelper.ToStringMulti(_cs[field], value);
                        return;
                }
            }
            else
            {
                if (value.Value == null)
                {
                    _cs[field].Delete();
                }
                else
                {
                    switch (_cs[field].DataType)
                    {
                        case Microsoft.MetadirectoryServices.AttributeType.Binary:
                            _cs[field].BinaryValue = (byte[])value.AsType(PanelModel.Rules.AttributeType.Binary);
                            break;
                        case Microsoft.MetadirectoryServices.AttributeType.Boolean:
                            _cs[field].BooleanValue = value.AsBoolean;
                            break;
                        case Microsoft.MetadirectoryServices.AttributeType.Integer:
                            _cs[field].IntegerValue = (long)value.AsType(PanelModel.Rules.AttributeType.Long);
                            break;
                        case Microsoft.MetadirectoryServices.AttributeType.Reference:
                            if (value.Value is ReferenceValue)
                            {
                                _cs[field].ReferenceValue = (ReferenceValue)value.Value;
                            }
                            else
                            {
                                _cs[field].ReferenceValue = MakeRef(value);
                            }
                            break;
                        default:
                            _cs[field].Value = value.ToString();
                            break;
                    }
                }
            }
        }

        public IEntryWrapper MV { get { return _other; } }
        public IEntryWrapper MVEntry { get { return _other; } }

        public IEntryWrapper CS { get { return this; } }

        // Allow ObjectRecord rules to work the same on Wrappers
        public IEntryWrapper Attributes { get { return this; } }

        IEntryWrapper _other;

        readonly CSEntry _cs;

        public void SetOther(IEntryWrapper other)
        {
            _other = other;
        }

        public CSWrapper() { }

        public CSWrapper(CSEntry cs)
        {
            _cs = cs;
        }

        public static implicit operator CSWrapper(CSEntry csentry)
        {
            return new CSWrapper(csentry);
        }

        public static implicit operator CSEntry(CSWrapper cs)
        {
            return cs._cs;
        }

        public CSWrapper(CSEntry csentry, IEntryWrapper other) : this(csentry)
        {
            _other = other;
        }
    }

    public class MVContributingMA
    {
        readonly MVEntry _mv;
        public MVContributingMA(MVEntry mv)
        {
            _mv = mv;
        }

        public string this[string key]
        {
            get
            {
                if (_mv[key].IsPresent)
                {
                    return _mv[key].LastContributingMA.Name;
                }

                return null;
            }
        }
    }

    public class MVContributingTime
    {
        readonly MVEntry _mv;
        public MVContributingTime(MVEntry mv)
        {
            _mv = mv;
        }

        public DateTime this[string key]
        {
            get
            {
                if (_mv[key].IsPresent)
                {
                    return _mv[key].LastContributionTime;
                }

                return DateTime.MinValue;
            }
        }
    }

    public class MVWrapper : IEntryWrapper
    {
        public AttributeValue this[string key]
        {
            get
            {
                return Get(key);
            }

            set
            {
                Set(key, value);
            }
        }

        public object Raw { get { return _mv; } }

        MVContributingMA _cma = null;
        MVContributingTime _cmt = null;

        public MVContributingMA LastContributingMA
        {
            get
            {
                if (_cma == null)
                {
                    _cma = new MVContributingMA(_mv);
                }

                return _cma;
            }
        }

        public MVContributingTime LastContributionTime
        {
            get
            {
                if (_cmt == null)
                {
                    _cmt = new MVContributingTime(_mv);
                }

                return _cmt;
            }
        }

        public AttributeValue Get(string field)
        {
            if (field.Equals("LastContributionTime", StringComparison.OrdinalIgnoreCase))
            {
                return new AttributeValue(LastContributionTime, this);
            }
            else if (field.Equals("LastContributingMA", StringComparison.OrdinalIgnoreCase))
            {
                return new AttributeValue(LastContributingMA, this);
            } else if (field.Equals("ObjectId", StringComparison.OrdinalIgnoreCase))
            {
                try
                {
                    return new AttributeValue(_mv.ObjectID, this);
                }
                catch (InvalidOperationException) { } // DN not yet 
            }
            else if (field.Equals("ObjectType", StringComparison.Ordinal))
            {
                return new AttributeValue(_mv.ObjectType, this);
            }

            if (_mv[field].IsPresent)
            {
                var ret = new AttributeValue(null, this);

                if (_mv[field].IsMultivalued)
                {
                    switch (_mv[field].DataType)
                    {
                        case Microsoft.MetadirectoryServices.AttributeType.Binary:
                            ret.Value = MultiHelper.FromBinMulti(_mv[field]);
                            break;
                        case Microsoft.MetadirectoryServices.AttributeType.Integer:
                            ret.Value = MultiHelper.FromIntMulti(_mv[field]);
                            break;
                        default:
                            ret.Value = MultiHelper.FromStringMulti(_mv[field]);
                            break;
                    }

                    return ret;
                }

                switch (_mv[field].DataType)
                {
                    case Microsoft.MetadirectoryServices.AttributeType.Binary:
                        ret.Value = _mv[field].BinaryValue;
                        break;
                    case Microsoft.MetadirectoryServices.AttributeType.Boolean:
                        ret.Value = _mv[field].BooleanValue;
                        break;
                    case Microsoft.MetadirectoryServices.AttributeType.Integer:
                        ret.Value = _mv[field].IntegerValue;
                        break;
                    case Microsoft.MetadirectoryServices.AttributeType.Reference:
                        throw new ArgumentException("Cannot read a reference value off a metaverse object");
                    default:
                        ret.Value = _mv[field].Value;
                        break;
                }

                ret.Bind();
                return ret;
            }

            return new AttributeValue(null, this);
        }

        public void Set(string field, AttributeValue value)
        {
            if (value.ToString() == CommonMIMFunctions.SKIPFLOW) { return; }

            if (_mv[field].IsMultivalued)
            {
                switch (_mv[field].DataType)
                {
                    case Microsoft.MetadirectoryServices.AttributeType.Binary:
                        MultiHelper.ToByteMulti(_mv[field], value);
                        return;
                    case Microsoft.MetadirectoryServices.AttributeType.Integer:
                        MultiHelper.ToIntMulti(_mv[field], value);
                        return;
                    default: // Microsoft.MetadirectoryServices.AttributeType.String:
                        MultiHelper.ToStringMulti(_mv[field], value);
                        return;
                }
            }
            else
            {
                if (value.Value == null)
                {
                    _mv[field].Delete();
                }
                else
                {
                    switch (_mv[field].DataType)
                    {
                        case Microsoft.MetadirectoryServices.AttributeType.Binary:
                            _mv[field].BinaryValue = (byte[])value.AsType(PanelModel.Rules.AttributeType.Binary);
                            break;
                        case Microsoft.MetadirectoryServices.AttributeType.Boolean:
                            _mv[field].BooleanValue = value.AsBoolean;
                            break;
                        case Microsoft.MetadirectoryServices.AttributeType.Integer:
                            _mv[field].IntegerValue = (long)value.AsType(PanelModel.Rules.AttributeType.Long);
                            break;
                        case Microsoft.MetadirectoryServices.AttributeType.Reference:
                            throw new ArgumentException("Cannot set a reference value on a metaverse object");
                        default:
                            _mv[field].Value = value.ToString();
                            break;
                    }
                }
            }
        }

        public IEntryWrapper CS { get; private set; }
        public IEntryWrapper CSEntry { get { return CS; } }
        public IEntryWrapper MV { get { return this; } }
        // Allow ObjectRecord rules to work the same on Wrappers
        public IEntryWrapper Attributes { get { return this; } }
        public IEntryWrapper Source { get { return this; } }
        public ConnectedMAWrapper ConnectedMAs { get; } = null;

        readonly MVEntry _mv;

        public void SetOther(IEntryWrapper other)
        {
            CS = other;
        }

        public MVWrapper() { }

        public MVWrapper(MVEntry mv)
        {
            _mv = mv;
            ConnectedMAs = new ConnectedMAWrapper(mv);
        }

        public static implicit operator MVWrapper(MVEntry mventry)
        {
            return new MVWrapper(mventry);
        }

        public static implicit operator MVEntry(MVWrapper mv)
        {
            return mv._mv;
        }

        public MVWrapper(MVEntry mventry, IEntryWrapper other) : this(mventry)
        {
            CS = other;
        }
    }

    public class ConnectedMAWrapper
    {
        readonly MVEntry _mv;
        public ConnectedMAWrapper(MVEntry mv)
        {
            _mv = mv;
        }

        public List<CSWrapper> this[string key]
        {
            get
            {
                return Get(key);
            }
        }

        public List<CSWrapper> Get(string maName)
        {
            var ret = new List<CSWrapper>();
            var conns = _mv.ConnectedMAs[maName].Connectors;
            for (int i = 0; i < conns.Count; i++)
            {
                ret.Add(conns.ByIndex[i]);
            }

            return ret;
        }
    }
}
