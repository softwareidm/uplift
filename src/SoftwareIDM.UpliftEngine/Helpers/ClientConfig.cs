﻿using SoftwareIDM.PanelModel.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace SoftwareIDM.UpliftEngine
{
    /// <summary>
    /// Helper for strongly typed access to config.json values
    /// </summary>
    public static class ClientConfig
    {
        public static string Product { get { return Conf.Configuration["Application:Product"]; } }

        public static string TenantId
        {
            get
            {
                return Conf.Configuration["Auth:TenantId"];
            }
        }

        public static string EventSource { get { return "SoftwareIDM Panel Service"; } }

        public static string ClientId { get { return Conf.Configuration["AzureAd:ClientId"]; } }

        // don't protect or only one user can use this
        public static string APIKey { get { return Conf.Configuration["Auth:APIKey"]; } }

        public static string ResourceId { get { return Conf.Configuration["AzureAd:ResourceId"]; } }

        public static string AadInstance { get { return Conf.Configuration["AzureAd:AadInstance"]; } }

        public static string PanelHost { get { return Conf.Configuration["Application:Host"]; } }

        public static string HashesDirectory { get { return Conf.Configuration["Application:HashesDir"]; } }

        public static string Server { get { return System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().HostName; } }

        public static bool Gzip { get { return Conf.Configuration["Rest:Gzip"] == null || (Conf.Configuration["Rest:Gzip"] ?? "false").Equals("true", StringComparison.OrdinalIgnoreCase); } }


        public static int RestTimeout
        {
            get
            {
                if (Conf.Configuration["Rest:TimeoutSeconds"] != null)
                {
                    return int.Parse(Conf.Configuration["Rest:TimeoutSeconds"]);
                }

                return 120;
            }
        }

        public static string User
        {
            get
            {
                if (Conf.Configuration["Auth:AppUser"] != null)
                {
                    return $"{TenantId}|{Conf.Configuration["Auth:AppUser"]}|{WindowsIdentity.GetCurrent().Name}";
                }

                return $"{TenantId}|{Server}|{WindowsIdentity.GetCurrent().Name}";
            }
        }
    }
}
