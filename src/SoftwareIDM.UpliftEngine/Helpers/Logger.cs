﻿using System;
using System.Threading;
using Microsoft.Extensions.Logging;
using SoftwareIDM.PanelModel.Helpers;

namespace SoftwareIDM.UpliftEngine
{
    public static class Logger
    {
        public static void WriteEntry(string message, LogLevel level)
        {
            try
            {
                Buffer(new ProgressEventArgs(level, message, 1));
            }
            catch { }
        }

        static DateTime LastPost = DateTime.MinValue;
        static readonly object writeLock = new object();
        static BsonBuffer<ProgressEventArgs> buffer = null;

        static void Buffer(ProgressEventArgs e)
        {
            if (buffer == null)
            {
                Init();
            }

            lock (writeLock)
            {
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
                buffer.Write(e, new CancellationToken()).GetAwaiter().GetResult();
                if (buffer.Count > 0 && LastPost.AddSeconds(10) < DateTime.UtcNow)
                {
                    buffer.Flush(new CancellationToken()).GetAwaiter().GetResult();
                    LastPost = DateTime.UtcNow;
                }
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
            }
        }

        static void Init()
        {
            if (buffer == null)
            {
                buffer = new BsonBuffer<ProgressEventArgs>("logs");
            }
        }

        public static void Finish()
        {
            lock (writeLock)
            {
                if (buffer != null)
                {
                    buffer.Flush(new CancellationToken()).GetAwaiter().GetResult();
                    buffer.Dispose();
                    buffer = null;
                }
            }
        }
    }
}
