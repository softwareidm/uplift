﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using SoftwareIDM.PanelModel.Attributes;
using SoftwareIDM.PanelModel.Models;
using SoftwareIDM.PanelModel.Rules;

namespace SoftwareIDM.UpliftEngine
{
    [ClassDoc("Value lookup functions forward and reverse mapping of special values.", Name = "Value Lookup")]
    public class SpecialValueFunctions : IRuleFunctions
    {
        public object CallingContext { get; set; }

        static Dictionary<string, Dictionary<string, string>> _specialReverse;

        Dictionary<string, Dictionary<string, string>> SpecialValuesReverse
        {
            get
            {
                if (_specialReverse == null)
                {
                    _specialReverse = new Dictionary<string, Dictionary<string, string>>();
                }

                return _specialReverse;
            }
        }

        [MemberDoc("Returns all entries of a special value collection as a Dictionary<string, object>.")]
        public AttributeValue SpecialDict(AttributeValue name)
        {
            var find = name.ToString();
            if (RuleEngine.SpecialValues.TryGetValue(find, out ISpecialValues spec))
            {
                return new AttributeValue(spec.Values(CallingContext), CallingContext);
            }

            return new AttributeValue(null, CallingContext);
        }

        [MemberDoc("Does a reverse lookup of a special value.")]
        public AttributeValue ReverseSpecial(AttributeValue value, AttributeValue name)
        {
            var find = name.ToString();
            if (!SpecialValuesReverse.TryGetValue(find, out Dictionary<string, string> special))
            {
                if (RuleEngine.SpecialValues.TryGetValue(find, out ISpecialValues spec))
                {
                    var add = new Dictionary<string, string>();
                    foreach (var pair in spec.Values(CallingContext))
                    {
                        if (pair.Value == null) { continue; }
                        add[pair.Value.ToString()] = pair.Key;
                    }

                    SpecialValuesReverse[find] = add;
                    special = add;
                }
                else
                {
                    return new AttributeValue(null, CallingContext);
                }
            }

            special.TryGetValue(value.ToString(), out string lookup);
            return new AttributeValue(lookup, CallingContext);
        }

        [MemberDoc("Encrypts a string value and returns a string in form \"ec:<value>\". Makes an API call requiring JsonSettings Write permission.")]
        public AttributeValue Encipher(AttributeValue value)
        {
            var val = value.ToString();
            if (!string.IsNullOrEmpty(val) && !val.StartsWith("ec:"))
            {
                var rPwd = Rest.Post<ApiDict<string>, ApiDict<string>>(
                    "settings/action/encipher",
                    new ApiDict<string> { { "Value", val } },
                    new System.Threading.CancellationToken()
                ).GetAwaiter().GetResult();

                val = rPwd["Value"];
                return new AttributeValue(val, CallingContext);
            }

            throw new Exception("Unable to encrypt value");
        }

        [MemberDoc("Decrypts a string value in form \"ec:<value>\" and returns the clear text. Makes an API call requiring JsonSettings Execute permission.")]
        public AttributeValue Decipher(AttributeValue value)
        {
            var val = value.ToString();
            if (!string.IsNullOrEmpty(val) && val.StartsWith("ec:"))
            {
                var rPwd = Rest.Post<ApiDict<string>, ApiDict<string>>(
                    "settings/action/decipher",
                    new ApiDict<string> { { "Value", val } },
                    new System.Threading.CancellationToken()
                ).GetAwaiter().GetResult();

                val = rPwd["Value"];

                return new AttributeValue(val, CallingContext);
            }

            throw new Exception("Unable to decrypt value");
        }
    }

    [ClassDoc("Special values for search indices")]
    public class SilosSpecial : ISpecialValues
    {
        public string Name
        {
            get { return "Identity Silo"; }
        }

        public Dictionary<string, object> Values(object callingContext)
        {
            var indices = Rest.Get<ListWrap<IndexOptions>>("search/indices", new System.Threading.CancellationToken()).GetAwaiter().GetResult();
            var ret = new Dictionary<string, object>();
            foreach (var index in indices.Data)
            {
                ret[index.Name] = index.Index;
            }

            return ret;
        }
    }

    [ClassDoc("Special values for environment variables")]
    public class EnvironmentSpecial : ISpecialValues
    {
        public string Name
        {
            get { return "Environment"; }
        }

        public Dictionary<string, object> Values(object callingContext)
        {
            var set = Util.Settings<Schedules>(new System.Threading.CancellationToken()).GetAwaiter().GetResult();
            var ret = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
            foreach (var env in set.EnvironmentVariables ?? new List<EnvironmentRoot>())
            {
                ret[env.Name] = env.Value;
            }

            return ret;
        }
    }

    public class IndexOptions
    {
        public string Name { get; set; }

        public string Index { get; set; }

        public List<string> Fields { get; set; }

        public List<string> MultiFields { get; set; }

        public List<string> ReferenceFields { get; set; }

        public List<string> ObjectTypes { get; set; }

        public IndexOptions()
        {
            Fields = new List<string>();
            MultiFields = new List<string>();
            ReferenceFields = new List<string>();
            ObjectTypes = new List<string>();
        }
    }

    [ClassDoc("Special values for AD objects", Name = "AD")]
    public class ADSpecial : ISpecialValues
    {
        public string Name { get { return "AD"; } }

        // userAccountControl constants
        public const long SCRIPT = 0x0001;
        public const long ACCOUNTDISABLE = 0x0002;
        public const long HOMEDIR_REQUIRED = 0x0008;
        public const long LOCKOUT = 0x0010;
        public const long PASSWD_NOTREQD = 0x0020;
        public const long PASSWD_CANT_CHANGE = 0x0040;
        public const long ENCRYPTED_TEXT_PWD_ALLOWED = 0x0080;
        public const long TEMP_DUPLICATE_ACCOUNT = 0x0100;
        public const long NORMAL_ACCOUNT = 0x0200;
        public const long INTERDOMAIN_TRUST_ACCOUNT = 0x0800;
        public const long WORKSTATION_TRUST_ACCOUNT = 0x1000;
        public const long SERVER_TRUST_ACCOUNT = 0x2000;
        public const long DONT_EXPIRE_PASSWORD = 0x10000;
        public const long MNS_LOGON_ACCOUNT = 0x20000;
        public const long SMARTCARD_REQUIRED = 0x40000;
        public const long TRUSTED_FOR_DELEGATION = 0x80000;
        public const long NOT_DELEGATED = 0x100000;
        public const long USE_DES_KEY_ONLY = 0x200000;
        public const long DONT_REQ_PREAUTH = 0x400000;
        public const long PASSWORD_EXPIRED = 0x800000;
        public const long TRUSTED_TO_AUTH_FOR_DELEGATION = 0x1000000;
        public const long PARTIAL_SECRETS_ACCOUNT = 0x04000000;

        // groupType constants
        public const long GLOBAL_DIST_GRP = 2;
        public const long DOMAINLOCAL_DIST_GRP = 4;
        public const long UNIVERSAL_DIST_GRP = 8;

        public const long GLOBAL_SEC_GRP = -2147483646;
        public const long DOMAINLOCAL_SEC_GRP = -2147483644;
        public const long UNIVERSAL_SEC_GRP = -2147483640;

        public const long ACCOUNT_EXPIRES_MAX = 0x7FFFFFFFFFFFFFFF;
        public const long ACCOUNT_EXPIRES_MIN = 0x0;

        public static Regex DNParse => new Regex(@"^(.*?),(?=OU=|DC=|CN=)(.*)$");

        public Dictionary<string, object> Values(object callingContext)
        {
            var ret = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
            var fields = GetType().GetFields();
            foreach (var info in fields)
            {
                if (info.IsLiteral)
                {
                    ret[info.Name] = info.GetValue(null);
                }
            }

            return ret;
        }

        public static string GetCN(string dn)
        {
            if (DNParse.IsMatch(dn))
            {
                return DNParse.Match(dn).Groups[1].Value;
            }

            return null;
        }

        public static string GetContainer(string dn)
        {
            if (DNParse.IsMatch(dn))
            {
                return DNParse.Match(dn).Groups[2].Value;
            }

            return null;
        }
    }
}
