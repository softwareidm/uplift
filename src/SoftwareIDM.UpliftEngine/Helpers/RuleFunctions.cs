﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management.Automation;
using System.Management.Automation.Runspaces;
using System.Text.RegularExpressions;
using SoftwareIDM.PanelModel.Rules;

namespace SoftwareIDM.UpliftEngine
{
    public sealed class RuleFunctions : IDisposable
    {
        public void Dispose()
        {
            _runspace.Dispose();
        }

        Runspace _runspace = null;

        PowerShell Get()
        {
            if (_runspace == null)
            {
                _runspace = RunspaceFactory.CreateRunspace(InitialSessionState.CreateDefault());
                _runspace.Open();
            }

            var ps = PowerShell.Create();
            ps.Runspace = _runspace;
            return ps;
        }

        static Dictionary<string, Rule> GetArgs(string name, List<Rule> parameters, Dictionary<string, Rule> namedParameters, out string paramsName, out List<Rule> paramsList)
        {
            var reName = Regex.Match(name, @"(\w+)\((.*)\)");
            name = reName.Groups[1].Value;
            var argStr = reName.Groups[2].Value;
            var mParams = (from s in argStr.Split(',')
                           where s.Trim().Length > 0
                           select s.Trim()).ToArray();

            var args = new Dictionary<string, Rule>(StringComparer.OrdinalIgnoreCase);
            paramsList = null;
            paramsName = null;
            var lastList = new List<Rule>();
            var lastName = "";

            int positionalArg = 0;
            for (int i = 0; i < mParams.Length; i++)
            {
                if (i < mParams.Length - 1 || !(mParams[i].EndsWith("[]") || mParams[i].EndsWith("{}")))
                {
                    if (namedParameters.ContainsKey(mParams[i]))
                    {
                        args[mParams[i]] = namedParameters[mParams[i]];
                    }
                    else if (parameters.Count > positionalArg)
                    {
                        args[mParams[i]] = parameters[positionalArg];
                        positionalArg++;
                    }
                    else
                    {
                        args[mParams[i]] = Rule.Build("null");
                    }
                }
                else if (mParams[i].EndsWith("[]"))
                {
                    lastName = mParams[i].Replace("[]", "");
                    paramsList = lastList;
                    paramsName = lastName;
                    while (positionalArg < parameters.Count)
                    {
                        lastList.Add(parameters[positionalArg]);
                        positionalArg++;
                    }

                }
                else if (mParams[i].EndsWith("{}"))
                {
                    foreach (var key in namedParameters.Keys)
                    {
                        args[key] = namedParameters[key];
                    }
                }
            }

            return args;
        }

        Func<List<Rule>, Dictionary<string, Rule>, Func<object, object, AttributeValue>> GetPSCaller(string ruleName, string script)
        {
            if (script.EndsWith(".ps1") && System.IO.File.Exists(script))
            {
                script = System.IO.File.ReadAllText(script);
            }

            return (parameters, namedParameters) =>
            {
                var args = GetArgs(ruleName, parameters, namedParameters, out string pName, out List<Rule> pRules);
                return (context, callingContext) =>
                {
                    var powershell = Get();
                    powershell.AddScript(script);
                    foreach (var arg in args)
                    {
                        var val = arg.Value.BindValue(context, callingContext).Value;
                        powershell.AddParameter(arg.Key, val);
                    }

                    if (pName != null)
                    {
                        powershell.AddParameter(pName, (from r in pRules
                                                        select r.BindValue(context, callingContext).Value).ToArray());
                    }

                    var stdOut = new List<object>();
                    var stdErr = new List<string>();
                    powershell.Streams.Error.DataAdded += (o, e) =>
                    {
                        PSDataCollection<ErrorRecord> errorStream = (PSDataCollection<ErrorRecord>)o;
                        var msg = errorStream[e.Index].ToString();
                        stdErr.Add(msg);
                    };
                    var output = powershell.Invoke();
                    if (stdErr.Count > 0)
                    {

                        Logger.WriteEntry($"Error on script {ruleName}: " + string.Join("\r\n", stdErr), Microsoft.Extensions.Logging.LogLevel.Error);
                    }

                    var ret = output.Count > 0 ? output[output.Count - 1].BaseObject : null;

                    return new AttributeValue(ret, callingContext);
                };
            };
        }

        Func<List<Rule>, Dictionary<string, Rule>, Func<object, object, AttributeValue>> GetRuleCaller(string ruleName, Rule rule)
        {
            return (parameters, namedParameters) =>
            {
                var args = GetArgs(ruleName, parameters, namedParameters, out string pName, out List<Rule> pRules);
                return (context, callingContext) =>
                {
                    var newContext = new Dictionary<string, object>();
                    foreach (var arg in args)
                    {
                        try
                        {
                            var val = arg.Value.BindValue(context, callingContext).Value;
                            newContext[arg.Key] = val;
                        }
                        catch (Exception e)
                        {
                            throw new ArgumentException($"Unable to bind argument {arg.Key} for rule {ruleName}", e);
                        }

                    }

                    if (pName != null)
                    {
                        newContext[pName] = (from r in pRules
                                             select r.BindValue(context, callingContext).Value).ToList();
                    }

                    return RuleEngine.GetValue(rule, newContext, callingContext);
                };
            };
        }

        public void SetCaller(RuleFunction rule)
        {
            if (rule.Kind != null && rule.Kind.Equals("PowerShell", StringComparison.OrdinalIgnoreCase))
            {
                rule.Implementation = GetPSCaller(rule.Name, rule.Rule);
            }
            else
            {
                rule.Implementation = GetRuleCaller(rule.Name, Rule.Build(rule.Rule));
            }
        }
    }
}
