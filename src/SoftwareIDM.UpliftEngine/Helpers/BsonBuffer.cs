﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MongoDB.Bson;
using SoftwareIDM.PanelModel.Helpers;
using SoftwareIDM.PanelModel.Models;

namespace SoftwareIDM.UpliftEngine
{
    public sealed class BsonBuffer<T> : IDisposable where T : class
    {
        public event ProgressEvent Saving = delegate { };
        public event ProgressEvent Saved = delegate { };
        public event ProgressEvent Error = delegate { };

        public const int MAXSIZE = 1024 * 1024 * 3;

        public long ByteCount { get { return Buffer.Length; } }
        public long Count { get; set; }
        public long MaxCount { get; set; }
        public string PostUrl { get; set; }

        MemoryStream Buffer { get; set; }

        public BsonBuffer(string postUrl)
        {
            PostUrl = postUrl;
            Buffer = new MemoryStream();
            Count = 0;
        }

        public BsonBuffer() { }

        /// <summary>
        /// Write objects to the Bson buffer
        /// </summary>
        public async Task Write(T write, CancellationToken cts)
        {
            if (write == null)
            {
                return;
            }

            var add = write.ToBson();
            Count++;
            lock (writeLock)
            {
                Buffer.Write(BitConverter.GetBytes(add.Length), 0, 4);
                Buffer.Write(add, 0, add.Length);
            }

            if ((ByteCount > MAXSIZE && ByteCount > 0) || (Count > MaxCount && MaxCount > 0))
            {
                await Flush(cts);
            }
        }

        public async Task Write(byte[] data, CancellationToken cts)
        {
            Count++;
            lock (writeLock)
            {
                Buffer.Write(BitConverter.GetBytes(data.Length), 0, 4);
                Buffer.Write(data, 0, data.Length);
            }

            if ((ByteCount > MAXSIZE && ByteCount > 0) || (Count > MaxCount && MaxCount > 0))
            {
                await Flush(cts);
            }
        }

        public void UnbindEvent()
        {
            Saving = delegate { };
            Saved = delegate { };
        }

        readonly object writeLock = new object();
        readonly object saveLock = new object();

        /// <summary>
        /// Persists Bson data
        /// </summary>
        Task Save(CancellationToken cts)
        {

            byte[] payload;
            long count;
            using (var data = new MemoryStream())
            {
                lock (writeLock)
                {
                    data.Write(BitConverter.GetBytes(Count), 0, 8);
                    Buffer.Position = 0;
                    Buffer.CopyTo(data);
                    Buffer = new MemoryStream();
                    count = Count;
                    Count = 0;
                    payload = data.ToArray();
                }
            }

            try
            {
                Saving.Invoke(this, new ProgressEventArgs(LogLevel.Trace, $"Saving {count} Records of type {typeof(T).Name}", 1));
                DateTime dt;
                lock (saveLock)
                {
                    dt = DateTime.UtcNow; // use datetime instead of util so we don't just get 00 when running demo generator
                    Rest.Post(PostUrl, payload, cts).GetAwaiter().GetResult();
                }

                Saved.Invoke(this, new ProgressEventArgs(LogLevel.Trace, $"{typeof(T).Name} Saved in {DateTime.UtcNow - dt}", 1));
            }
            catch (Exception e)
            {
                Error.Invoke(this, new ProgressEventArgs(LogLevel.Error, $"Error Saving: {e.Message}", 1));
            }

            return Task.FromResult(0);
        }

        /// <summary>
        /// Flush the buffer by saving to the Rest API
        /// </summary>
        public async Task Flush(CancellationToken cts)
        {
            if (Count > 0)
            {
                await Save(cts);
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (Buffer != null)
                    {
                        Buffer.Dispose();
                    }
                }

                Buffer = null;
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}
