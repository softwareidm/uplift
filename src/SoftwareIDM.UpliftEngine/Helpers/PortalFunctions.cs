﻿using System;
using System.Collections.Generic;
using System.IO;
using SoftwareIDM.PanelModel.Attributes;
using SoftwareIDM.PanelModel.Rules;

namespace SoftwareIDM.UpliftEngine
{
    [ClassDoc("Functions for working with the Portal Provider")]
    public class PortalFunctions : IRuleFunctions
    {
        public object CallingContext { get; set; }

        [MemberDoc("Returns contents of the given file as a text string")]
        public AttributeValue FileText(AttributeValue fileName)
        {
            if (!File.Exists(fileName.ToString()))
            {
                return new AttributeValue(null, CallingContext);
            }

            return new AttributeValue(File.ReadAllText(fileName.ToString()), CallingContext);
        }

        [MemberDoc("Converts a datetime object into a format suitable for the Portal API (same as SQL server), equivalent to DateTimeFormat(dateTime, \"yyyy-MM-dd HH:mm:ss.fff\"")]
        public AttributeValue PortalTime(AttributeValue dateTime)
        {
            if (dateTime.AsBoolean)
            {
                return new AttributeValue(dateTime.ToDateTime().ToString("yyyy-MM-dd HH:mm:ss.fff"), CallingContext);
            }

            return new AttributeValue(null, CallingContext);
        }

        [MemberDoc("Accepts a map of dictionary parameters, returns the one where the key name matches the name of the local machine.")]
        public AttributeValue ServerEnv(AttributeValue _default, Dictionary<string, AttributeValue> map)
        {
            var server = Environment.MachineName.ToUpper();
            if (map != null && map.ContainsKey(server))
            {
                return map[server];
            }

            return _default;
        }

        [MemberDoc("Accepts an XPath filter and returns the Portal Object ID.")]
        public AttributeValue PortalResolve(AttributeValue query, AttributeValue system)
        {
            throw new NotImplementedException();
        }
    }
}
