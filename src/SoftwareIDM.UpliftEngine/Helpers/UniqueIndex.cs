﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using SoftwareIDM.PanelModel.Attributes;
using SoftwareIDM.PanelModel.Models;

namespace SoftwareIDM.UpliftEngine
{
    [ClassDoc("Unique names index entry", Name = "Unique Index", RuleDoc = true)]
    public class UniqueIndex
    {
        [MemberDoc("Auto-generated Id")]
        public ObjectId Id { get; set; }

        [MemberDoc("Id of unique index defining collection")]
        [BsonElement("d")]
        public Guid Definition { get; set; }

        [MemberDoc("Identity Object unique name was sourced from")]
        [BsonElement("o")]
        public Guid ObjectId { get; set; }

        [MemberDoc("List of key values identifying ownership of value. A match on any key value assumes ownership of the unique value")]
        [BsonElement("k")]
        public List<string> Keys { get; set; } = new List<string>();

        [MemberDoc("List of claimed unique values")]
        [BsonElement("v")]
        public List<string> Values { get; set; } = new List<string>();

        [MemberDoc("Lookup root for values. Should be a substring of the non-unique portion of the claimed values.")]
        [BsonElement("r")]
        public List<string> Roots { get; set; } = new List<string>();

        [BsonElement("ex")]
        [BsonIgnoreIfNull]
        public DateTime? Expires { get; set; }

        [BsonElement("dt")]
        [BsonIgnoreIfNull]
        public DateTime? Deleted { get; set; }
    }

    [ClassDoc("Context object for iteration rule on MakeUnique rule function", Name = "MakeUnique Context", RuleDoc = true)]
    public class UniqueContext
    {
        [MemberDoc("Returns an empty string on the first iteration \"\", then the index as a string on subsequent iterations")]
        public string IterationName => Iteration == 0 ? "" : Iteration.ToString(); // "", "1", "2"

        [MemberDoc("Iteration count")]
        public int Iteration { get; set; }

        [MemberDoc("Context object associated with the MakeUnique call")]
        public object ParentContext { get; set; }

        [MemberDoc("Alias for ParentContext, allows rule consistency with list function context helpers")]
        public object Parent { get { return ParentContext; } set { ParentContext = value; } }

        [MemberDoc("Root value for matching unique values")]
        public string Root { get; set; }

        [MemberDoc("Key value for asserting unique ownership")]
        public string Key { get; set; }
    }
}
