﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Azure;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using SoftwareIDM.PanelModel.Models;

namespace SoftwareIDM.UpliftEngine
{
    public static class Util
    {
        static BsonDocument json;
        static ConcurrentDictionary<string, JsonSettings> settings;

        static async Task LoadSettings(CancellationToken cts)
        {
            if (settings == null)
            {
                settings = new ConcurrentDictionary<string, JsonSettings>();
                if (json == null) { json = new BsonDocument(); }
                Rest.StartSession(new System.Threading.CancellationToken(), true).GetAwaiter().GetResult();
                var j = await Rest.Get<BsonDocument>("settings", cts);
                foreach (var pair in j)
                {
                    json[pair.Name] = pair.Value;
                }
            }
        }

        /// <summary>
        /// Retrieves the JsonSettings of type T either from the server or from cache. The cache goes stale every 5 minutes.
        /// </summary>
        public static async Task<T> Settings<T>(CancellationToken cts) where T : JsonSettings
        {
            await LoadSettings(cts);

            if (json == null)
            {
                throw new Exception("Unable to retrieve settings, verify connectivity and permissions.");
            }

            var name = typeof(T).Name;

            if (settings.ContainsKey(name))
            {
                return (T)settings[name];
            }
            else
            {
                if (json.Contains(name) && json[name] != null)
                {
                    var ret = BsonSerializer.Deserialize<T>((BsonDocument)json[name]);
                    settings[name] = ret ?? throw new Exception($"Unable to deserialize {name} from {json[name].ToJson()}");
                    return ret;
                }

                throw new ArgumentException($"Unable to find setting {name}");
            }
        }
    }
}