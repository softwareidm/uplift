﻿using System;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SoftwareIDM.PanelModel.Helpers;
using SoftwareIDM.PanelModel.Models;

namespace SoftwareIDM.UpliftEngine
{
    public static class Rest
    {
        public static HttpWebRequest BuildRequest(string action)
        {
            return BuildRequest(action, "api");
        }

        static void AddCredential(HttpWebRequest request)
        {
            request.Headers.Add("X-Api-Key", ClientConfig.APIKey);
            if (!string.IsNullOrEmpty(Conf.Configuration["Auth:AppUser"]))
            {
                request.Headers.Add("X-ServiceName", Conf.Configuration["Auth:AppUser"]);
            }
            else
            {
                request.Headers.Add("X-ServiceName", ClientConfig.Server);
            }

            if (Conf.Auth == AuthType.Windows)
            {
                if (string.IsNullOrEmpty(Conf.Configuration["Auth:User"]))
                {
                    request.UseDefaultCredentials = true;
                }
                else
                {
                    var user = Conf.Configuration["Auth:User"];
                    var pwd = Conf.Protected("Auth:Password");

                    if (user.IndexOf('\\') > -1)
                    {
                        request.Credentials = new NetworkCredential(user.Split('\\')[1], pwd, user.Split('\\')[0]);
                    }
                    else
                    {
                        request.Credentials = new NetworkCredential(user, pwd);
                    }
                }

                return;
            }

            request.CookieContainer = new CookieContainer();
            if (LoginCookies != null)
            {
                foreach (Cookie cookie in LoginCookies)
                {
                    if (cookie.Expired)
                    {
                        FormLogin();
                    }
                }

                request.CookieContainer.Add(LoginCookies);
            }
        }

        /// <summary>
        /// BuildRequest is normally used internally by Rest helper, but could be used if you need control over the raw HttpWebRequest
        /// </summary>
        /// <param name="action">URL to post to, minus the host and /api part</param>
        /// <param name="api">Optional override for api portion of URL</param>
        /// <param name="cts"></param>
        /// <returns>Returns a task, because we might need to await an authentication process to get a credentialed request.</returns>
        public static HttpWebRequest BuildRequest(string action, string api)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            ServicePointManager.DefaultConnectionLimit = ushort.MaxValue;
            string url;
            if (!string.IsNullOrEmpty(api))
            {
                url = $"{ClientConfig.PanelHost.TrimEnd('/')}/{api}/{action}";
            }
            else
            {
                url = $"{ClientConfig.PanelHost.TrimEnd('/')}/{action}";
            }

            var request = (HttpWebRequest)WebRequest.Create(url);
            request.ServicePoint.ConnectionLimit = ushort.MaxValue;
            AddCredential(request);
            request.Headers.Add("X-Version", System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString());
            if (Conf.Configuration["Application:OverrideNow"] != null)
            {
                request.Headers.Add("X-Set-Now", DateTime.UtcNow.FormatISODate());
            }

            return request;
        }

        static CookieCollection LoginCookies { get; set; }

        static void NewScramLogin(bool neverInteractive)
        {
            string pass = Conf.Configuration["Auth:Password"] ?? "";
            if (neverInteractive && pass.Length == 0)
            {
                throw new Exception($"Unable to establish logon context without interactive shell. No working saved login for {ClientConfig.User}");
            }

            Console.WriteLine($@"
To Create or Reset Application Password:
1. Go to {ClientConfig.PanelHost.TrimEnd('/')}/settings/tools
2. Enter server name: {ClientConfig.Server}
3. Enter domain and account name: {System.Security.Principal.WindowsIdentity.GetCurrent().Name}
4. Press <Create or Reset Application Password>

If this context does not have a shell, e.g. for setting up with a group managed service account,
you may place the password in ClientConfig.json Auth:Password

Enter Password>");
            var json = JObject.Parse(File.ReadAllText(Conf.ConfigPath));
            if (pass.Length == 0)
            {
                ConsoleKeyInfo key;
                do
                {
                    key = Console.ReadKey(true);

                    if (key.Key != ConsoleKey.Backspace && key.Key != ConsoleKey.Enter)
                    {
                        pass += key.KeyChar;
                        Console.Write("*");
                    }
                    else
                    {
                        if (key.Key == ConsoleKey.Backspace && pass.Length > 0)
                        {
                            pass = pass.Substring(0, pass.Length - 1);
                            Console.Write("\b \b");
                        }
                    }
                }

                // Stops Receving Keys Once Enter is Pressed
                while (key.Key != ConsoleKey.Enter);
            }
            else
            {
                ((JObject)json["Auth"]).Remove("Password");
            }

            pass = pass.Trim();
            json["Auth"]["Users"][ClientConfig.User] = Conf.Encrypt(pass, true);
            File.WriteAllText(Conf.ConfigPath, json.ToString(Formatting.Indented));
            Conf.Reset();
        }

        static void FormLogin()
        {
            var client = Conf.Configuration["Auth:AppUser"] != null ?
                new ScramClient(ClientConfig.User, Conf.Protected("Auth:" + Conf.Configuration["Auth:AppUser"] + "Password")) :
                new ScramClient(ClientConfig.User, Conf.Protected("Auth:Users:" + ClientConfig.User));
            LoginCookies = client.Authenticate();
        }

        /// <summary>
        /// Start sessions is called by PanelTools or PanelService on application startup to handle agent registration and login as needed.
        /// </summary>
        public static async Task StartSession(CancellationToken cts, bool neverInteractive = false)
        {
            if (Conf.Auth == AuthType.Azure)
            {
                if (Conf.Configuration["Auth:AppUser"] != null)
                {
                    FormLogin();
                }
                else
                {
                    if (Conf.Configuration["Auth:Users:" + ClientConfig.User] == null)
                    {
                        NewScramLogin(neverInteractive);
                    }

                    try
                    {
                        FormLogin();
                    }
                    catch
                    {
                        NewScramLogin(neverInteractive);
                        FormLogin();
                    }
                }
            }

            await Post<ApiDict<string>, ApiDict<string>>(
                "tenant/action/register",
                new ApiDict<string>() { { "User", ClientConfig.User } },
                cts
            );
        }

        /// <summary>
        /// Fetch data from the web application, uses timeout from config file, the BSON response is deserialized to type T.
        /// </summary>
        public static async Task<T> Get<T>(string action, CancellationToken cts) where T : class
        {
            return await Get<T>(action, ClientConfig.RestTimeout, cts);
        }

        /// <summary>
        /// Fetch data with a timeout override
        /// </summary>
        public static async Task<T> Get<T>(string action, int timeoutSeconds, CancellationToken cts) where T : class
        {
            int backoff = 100;

            Tuple<T, bool> ret = null;
            for (int i = 0; i < 4; i++)
            {
                ret = await DoGet<T>(action, timeoutSeconds, cts);
                if (ret.Item2) { break; }
                else
                {
                    await Task.Delay(backoff);
                    backoff *= 2;
                }
            }

            return ret.Item1;
        }

        static async Task<Tuple<T, bool>> DoGet<T>(string action, int timeoutSeconds, CancellationToken cts) where T : class
        {
            var request = BuildRequest(action);
            request.Method = "GET";

            request.ContentType = "application/bson";
            request.Accept = "application/bson";
            request.MediaType = "application/bson";
            request.Timeout = 1000 * timeoutSeconds;
            if (ClientConfig.Gzip)
            {
                request.Headers.Set(HttpRequestHeader.AcceptEncoding, "gzip");
            }

            return await GetBsonResponse<T>(request, cts);
        }

        public static string ApiVersion { get; private set; } = null;
        public static bool? VersionMatched { get; private set; } = null;
        static async Task<Tuple<T, bool>> GetBsonResponse<T>(HttpWebRequest request, CancellationToken cts) where T : class
        {
            HttpWebResponse response = null;
            try
            {
                try
                {
                    response = (HttpWebResponse)(await request.GetResponseAsync());
                }
                catch (WebException e)
                {
                    if (e.Status == WebExceptionStatus.ConnectFailure)
                    {
                        return Tuple.Create(default(T), false);
                    }

                    throw;
                }

                if (!VersionMatched.HasValue && response.Headers["X-Version"] != null)
                {
                    VersionMatched = response.Headers["X-Version"] == System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
                    ApiVersion = response.Headers["X-Version"];
                }

                if (LoginCookies != null && response.Cookies != null)
                {
                    LoginCookies.Add(response.Cookies);
                }

                if (response.StatusCode == HttpStatusCode.NoContent)
                {
                    return Tuple.Create(default(T), true);
                }

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    using (var stream = response.GetResponseStream())
                    {
                        using (var memoryStream = new MemoryStream())
                        {
                            using (var resp = response.GetResponseStream())
                            {
                                if (response.Headers[HttpResponseHeader.ContentEncoding] == "gzip")
                                {
                                    using (var gzip = new GZipStream(resp, CompressionMode.Decompress))
                                    {
                                        await gzip.CopyToAsync(memoryStream, 4096, cts);
                                    }
                                }
                                else
                                {
                                    await resp.CopyToAsync(memoryStream, 4096, cts);
                                }
                            }
                            if (typeof(T).GetInterface("ICustomSerializer") != null)
                            {
                                var inst = Activator.CreateInstance<T>();
                                if (inst is ICustomSerializer iser && iser.SupportedMediaTypes.Contains("application/bson"))
                                {
                                    memoryStream.Position = 0;
                                    iser.ReadFromStream(memoryStream, "application/bson");
                                    return Tuple.Create(inst, true);
                                }
                            }

                            var res = memoryStream.ToArray();
                            if (res.Length == 0)
                            {
                                return Tuple.Create(default(T), true);
                            }

                            if (res is T)
                            {
                                return Tuple.Create(res as T, true);
                            }

                            var n = typeof(T).Name;
                            try
                            {
                                return Tuple.Create(BsonSerializer.Deserialize<T>(res), true);
                            }
                            catch (FormatException e)
                            {
                                throw new FormatException($"Unable to read response from {request.RequestUri}: {e.Message}", e);
                            }
                        }
                    }
                }

                throw new IOException($"Http Error, response {response.StatusCode}");
            }
            finally
            {
                if (response != null)
                {
                    response.Dispose();
                }
            }
        }

        /// <summary>
        /// Post a .net object to the web server where a response is either not expected (204), or the caller doesn't need the response.
        /// </summary>
        public static async Task Post<D>(string action, D data, CancellationToken cts) where D : class
        {
            await Post<object, D>(action, data, cts);
        }

        /// <summary>
        /// Post a .net object of type D, and deserialize the response as an object of type T
        /// </summary>
        public static async Task<T> Post<T, D>(string action, D data, CancellationToken cts) where T : class
        {
            return await Post<T, D>(action, data, ClientConfig.RestTimeout, cts);
        }

        /// <summary>
        /// Post data with timeout override
        /// </summary>
        public static async Task<T> Post<T, D>(string action, D data, int timeoutSeconds, CancellationToken cts) where T : class
        {
            int backoff = 100;

            Tuple<T, bool> ret = null;
            for (int i = 0; i < 4; i++)
            {
                ret = await DoPost<T, D>(action, data, timeoutSeconds, cts);
                if (ret.Item2) { break; }
                else
                {
                    await Task.Delay(backoff);
                    backoff *= 2;
                }
            }

            return ret.Item1;
        }

        static async Task<Tuple<T, bool>> DoPost<T, D>(string action, D data, int timeoutSeconds, CancellationToken cts) where T : class
        {
            var request = BuildRequest(action);
            request.Method = "POST";

            request.ContentType = "application/bson";
            request.Accept = "application/bson";
            request.MediaType = "application/bson";
            request.Timeout = 1000 * timeoutSeconds;
            if (ClientConfig.Gzip)
            {
                request.Headers.Set(HttpRequestHeader.AcceptEncoding, "gzip");
            }

            if (data != null)
            {
                try
                {
                    byte[] write;
                    if (data is byte[])
                    {
                        write = data as byte[];
                    }
                    else if (data is ICustomSerializer cd)
                    {
                        using (var mem = new MemoryStream())
                        {
                            await cd.WriteToStream(mem, "application/bson");
                            write = mem.ToArray();
                        }
                    }
                    else
                    {
                        write = data.ToBson();
                    }

                    if (write.Length > 1000 * 100 && ClientConfig.Gzip)
                    {
                        request.Headers.Set(HttpRequestHeader.ContentEncoding, "gzip");

                        using (var gzip = new GZipStream(request.GetRequestStream(), CompressionMode.Compress))
                        {
                            gzip.Write(write, 0, write.Length);
                            gzip.Flush();
                        }
                    }
                    else
                    {
                        using (var req = request.GetRequestStream())
                        {
                            using (var writer = new BinaryWriter(req))
                            {
                                writer.Write(write);
                                writer.Flush();
                            }

                            req.Flush();
                        }
                    }
                }
                catch (WebException e)
                {
                    if (e.Status == WebExceptionStatus.ConnectFailure)
                    {
                        return Tuple.Create(default(T), false);
                    }

                    throw;
                }
            }

            return await GetBsonResponse<T>(request, cts);
        }
    }
}