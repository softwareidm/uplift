﻿using System;
using System.Collections.Concurrent;
using SoftwareIDM.PanelModel.Attributes;
using SoftwareIDM.PanelModel.Rules;

namespace SoftwareIDM.UpliftEngine
{
    class RuleAggregateTally
    {
        public string Context { get; set; }
        public long CountIsTrue { get; set; }
        public double Min { get; set; }
        public double Max { get; set; }
        public double Sum { get; set; }

        // see http://www.johndcook.com/standard_deviation.html

        public long N { get; set; }
        public double OldM { get; set; }
        public double NewM { get; set; }
        public double OldS { get; set; }
        public double NewS { get; set; }
        public DateTime DeleteAfter { get; set; }
        public TimeSpan ExpiresAfter { get; set; }

        public void Tally(AttributeValue expression, DateTime utcNow)
        {
            DeleteAfter = utcNow + ExpiresAfter;
            if (expression.AsBoolean)
            {
                CountIsTrue++;
            }

            var exprD = (double)expression.AsType(AttributeType.Double);
            Sum += exprD;
            if (exprD < Min)
            {
                Min = exprD;
            }

            if (exprD > Max)
            {
                Max = exprD;
            }

            N++;

            if (N == 1)
            {
                OldM = NewM = exprD;
                OldS = 0.0;
            }
            else
            {
                NewM = OldM + (exprD - OldM) / N;
                NewS = OldS + (exprD - OldM) * (exprD - NewM);

                // set up for next iteration
                OldM = NewM;
                OldS = NewS;
            }
        }

        public double Mean { get { return N > 0 ? NewM : 0.0; } }

        public double Variance { get { return ((N > 1) ? NewS / (N - 1) : 0.0); } }

        public double StdDev { get { return Math.Sqrt(Variance); } }
    }

    [ClassDoc("Perform aggregate calculations", "Aggregation Functions")]
    public class AggregateFunctions
    {
        public object CallingContext { get; set; }

        static ConcurrentDictionary<string, RuleAggregateTally> tallies;

        ConcurrentDictionary<string, RuleAggregateTally> RequestTallies
        {
            get
            {
                if (tallies == null)
                {
                    tallies = new ConcurrentDictionary<string, RuleAggregateTally>();
                }

                return tallies;
            }
        }

        RuleAggregateTally Tally(string name, AttributeValue expression, AttributeValue expiresAfter)
        {
            if (!expiresAfter.AsBoolean)
            {
                expiresAfter = new AttributeValue(TimeSpan.FromHours(1.0), CallingContext);
            }

            if (RequestTallies.ContainsKey(name))
            {
                if (RequestTallies[name].DeleteAfter < DateTime.UtcNow)
                {
                    RequestTallies.TryRemove(name, out var _);
                }
            }

            RuleAggregateTally w;
            if (!RequestTallies.ContainsKey(name))
            {
                w = new RuleAggregateTally { Context = name, ExpiresAfter = (TimeSpan)expiresAfter.AsType(AttributeType.TimeSpan) };
                w.DeleteAfter = DateTime.UtcNow.Add(w.ExpiresAfter);
                RequestTallies[name] = w;
            }
            else
            {
                w = RequestTallies[name];
            }

            w.Tally(expression, DateTime.UtcNow);
            return w;
        }

        [MemberDoc(@"Count takes a true/false rule expression, and adds it to a tally based on the context. <br />
If no calls to Count are made with same context for more than expiresAfter duration, the tally is removed.<br />e.g. Count(context.Deleted, context.DeleteSource, Minutes(30))<br />If expiresAfter is null, the default value is one hour.")]
        public AttributeValue Count(AttributeValue expression, AttributeValue name, AttributeValue expiresAfter)
        {
            var ag = Tally(name.ToString(), expression, expiresAfter);
            return new AttributeValue(ag.CountIsTrue, CallingContext);
        }

        [MemberDoc(@"Min tallies the numeric value of the expression in the given context, and returns the current minimum of the tally.")]
        public AttributeValue Min(AttributeValue expression, AttributeValue name, AttributeValue expiresAfter)
        {
            var ag = Tally(name.ToString(), expression, expiresAfter);
            return new AttributeValue(ag.Min, CallingContext);
        }

        [MemberDoc(@"Max tallies the numeric value of the expression in the given context, and returns the current maximum of the tally.")]
        public AttributeValue Max(AttributeValue expression, AttributeValue name, AttributeValue expiresAfter)
        {
            var ag = Tally(name.ToString(), expression, expiresAfter);
            return new AttributeValue(ag.Max, CallingContext);
        }

        [MemberDoc(@"Sums the numeric value of the expression in the given context, and returns the current sum of the aggregate")]
        public AttributeValue SumAg(AttributeValue expression, AttributeValue name, AttributeValue expiresAfter)
        {
            var ag = Tally(name.ToString(), expression, expiresAfter);
            return new AttributeValue(ag.Sum, CallingContext);
        }

        [MemberDoc(@"Mean tallies the numeric value of the expression in the given context, and returns the current mean of the tally.")]
        public AttributeValue Mean(AttributeValue expression, AttributeValue name, AttributeValue expiresAfter)
        {
            var ag = Tally(name.ToString(), expression, expiresAfter);
            return new AttributeValue(ag.Mean, CallingContext);
        }

        [MemberDoc(@"Mean tallies the numeric value of the expression in the given context, and returns the current standard deviation of the tally.")]
        public AttributeValue StdDev(AttributeValue expression, AttributeValue name, AttributeValue expiresAfter)
        {
            var ag = Tally(name.ToString(), expression, expiresAfter);
            return new AttributeValue(ag.StdDev, CallingContext);
        }

        [MemberDoc(@"Tracks a particular context and only returns true the first time the expression is true.")]
        public AttributeValue Once(AttributeValue expression, AttributeValue name, AttributeValue expiresAfter)
        {
            if (expression.AsBoolean)
            {
                if (!expiresAfter.AsBoolean)
                {
                    expiresAfter = new AttributeValue(TimeSpan.FromHours(1.0), CallingContext);
                }

                var cont = name.ToString() + "_Once";
                RuleAggregateTally w;

                if (RequestTallies.ContainsKey(cont) &&
                    RequestTallies[cont].DeleteAfter < DateTime.UtcNow)
                {
                    RequestTallies.TryRemove(cont, out var _);
                }

                if (!RequestTallies.ContainsKey(cont))
                {
                    w = new RuleAggregateTally { Context = cont, ExpiresAfter = (TimeSpan)expiresAfter.AsType(AttributeType.TimeSpan) };
                    w.DeleteAfter = DateTime.UtcNow.Add(w.ExpiresAfter);
                    RequestTallies[cont] = w;
                    return new AttributeValue(true, CallingContext);
                }
            }

            return new AttributeValue(false, CallingContext);
        }
    }
}
