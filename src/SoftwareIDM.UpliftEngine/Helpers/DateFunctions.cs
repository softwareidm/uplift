﻿using System;
using SoftwareIDM.PanelModel.Attributes;
using SoftwareIDM.PanelModel.Rules;

namespace SoftwareIDM.UpliftEngine
{
    [ClassDoc("Perform operations and transformations on dates", "Date Functions")]
    public class CommonDateFunctions : IRuleFunctions
    {
        public object CallingContext { get; set; }

        [MemberDoc("Converts dateTime to a string using the provided format. <br />See <a href=\"http://msdn.microsoft.com/en-us/library/8kb3ddd4\" target=\"_blank\">.NET Custom Date and Time Format Strings</a>")]
        public AttributeValue DateTimeFormat(AttributeValue dateTime, AttributeValue format)
        {
            try
            {
                return new AttributeValue(dateTime.ToDateTime().ToString(format.ToString()), CallingContext);
            }
            catch (FormatException)
            {
                return new AttributeValue(null, CallingContext);
            }
        }

        [MemberDoc("Converts TimeSpan to a string using the provided format. <br />See <a href=\"https://msdn.microsoft.com/en-us/library/ee372287(v=vs.110).aspx\" target =\"_blank\">.NET Custom Date and Time Format Strings</a>")]
        public AttributeValue TimeSpanFormat(AttributeValue time, AttributeValue format)
        {
            try
            {
                return new AttributeValue(time.ToTimeSpan().ToString(format.ToString()), CallingContext);
            }
            catch (FormatException)
            {
                return new AttributeValue(null, CallingContext);
            }
        }

        [MemberDoc("Converts dateTime to a file time long.")]
        public AttributeValue ToFileTime(AttributeValue dateTime)
        {
            try
            {
                return new AttributeValue(((DateTime)dateTime.AsType(AttributeType.DateTime)).ToFileTime(), CallingContext);
            }
            catch (FormatException)
            {
                return new AttributeValue(null, CallingContext);
            }
        }

        [MemberDoc("Converts dateTime to the format used by the FIM Portal.")]
        public AttributeValue ToPortalTime(AttributeValue dateTime)
        {
            if (!string.IsNullOrEmpty(dateTime.ToString()))
            {
                return new AttributeValue(((DateTime)dateTime.AsType(AttributeType.DateTime)).ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ss.000"), CallingContext);
            }

            return new AttributeValue(null, CallingContext);
        }

        [MemberDoc("Converts time from a long to a dateTime using the FromFileTime method.")]
        public AttributeValue FromFileTime(AttributeValue time)
        {
            var val = time.ToLong();
            if (val == Int64.MaxValue || val > DateTime.MaxValue.Ticks)
            {
                return new AttributeValue(DateTime.MaxValue, CallingContext);
            }

            return new AttributeValue(DateTime.FromFileTime(val), CallingContext);
        }

        [MemberDoc("Returns DateTime.Now")]
        public AttributeValue Now()
        {
            return new AttributeValue(DateTime.Now, CallingContext);
        }

        [MemberDoc("Returns DateTime.UtcNow")]
        public AttributeValue UtcNow()
        {
            return new AttributeValue(DateTime.UtcNow, CallingContext);
        }

        [MemberDoc("Returns DateTime.Now.Date")]
        public AttributeValue Today()
        {
            return new AttributeValue(DateTime.Today, CallingContext);
        }

        [MemberDoc("Returns DateTime.UtcNow.Date")]
        public AttributeValue UtcToday()
        {
            return new AttributeValue(DateTime.UtcNow.Date, CallingContext);
        }

        [MemberDoc("Parses add as a positive or negative TimeSpan, and adds it to dateTime.")]
        public AttributeValue AddTime(AttributeValue dateTime, AttributeValue add)
        {
            if (dateTime.Value != null && add.Value != null)
            {
                return new AttributeValue(dateTime.ToDateTime().Add(add.ToTimeSpan()), CallingContext);
            }

            return new AttributeValue(null, CallingContext);
        }

        [MemberDoc("Parses add as a positive or negative TimeSpan, and subtracts it from dateTime.")]
        public AttributeValue SubtractTime(AttributeValue dateTime, AttributeValue subtract)
        {
            if (dateTime.Value != null && subtract.Value != null)
            {
                return new AttributeValue(dateTime.ToDateTime().Add(subtract.ToTimeSpan().Negate()), CallingContext);
            }

            return new AttributeValue(null, CallingContext);
        }

        [MemberDoc("Subtracts one datetime from another and returns the result.")]
        public AttributeValue SubtractDate(AttributeValue dateTime, AttributeValue other)
        {
            if (dateTime.Value != null && other.Value != null)
            {
                return new AttributeValue(dateTime.ToDateTime() - other.ToDateTime(), CallingContext);
            }

            return new AttributeValue(null, CallingContext);
        }

        [MemberDoc("Shorthand for SubtractTime(Now(), time).")]
        public AttributeValue Ago(AttributeValue time)
        {
            if (time.Value == null)
            {
                return new AttributeValue(null, CallingContext);
            }

            return new AttributeValue(DateTime.Now.Add(time.ToTimeSpan().Negate()), CallingContext);
        }

        [MemberDoc("Shorthand for SubtractTime(UtcNow(), time).")]
        public AttributeValue AgoUtc(AttributeValue time)
        {
            if (time.Value == null)
            {
                return new AttributeValue(null, CallingContext);
            }

            return new AttributeValue(DateTime.UtcNow.Add(time.ToTimeSpan().Negate()), CallingContext);
        }

        [MemberDoc("Shorthand for AddTime(Now(), time).")]
        public AttributeValue Until(AttributeValue time)
        {
            if (time.Value == null)
            {
                return new AttributeValue(null, CallingContext);
            }

            return new AttributeValue(DateTime.Now.Add(time.ToTimeSpan()), CallingContext);
        }

        [MemberDoc("Shorthand for AddTime(UtcNow(), time).")]
        public AttributeValue UntilUtc(AttributeValue time)
        {
            if (time.Value == null)
            {
                return new AttributeValue(null, CallingContext);
            }

            return new AttributeValue(DateTime.UtcNow.Add(time.ToTimeSpan()), CallingContext);
        }

        [MemberDoc("Accepts a number of months to return as a TimeSpan relative to the current date (or relativeDate if specified). Only accepts integer values")]
        public AttributeValue Months(AttributeValue numberOfDays, AttributeValue relativeDate)
        {
            var context = DateTime.UtcNow.Date;
            if (relativeDate != null && relativeDate.Value != null && relativeDate.ToDateTime() != DateTime.MinValue)
            {
                context = relativeDate.ToDateTime();
            }

            var months = context.AddMonths((int)numberOfDays.ToLong());
            var duration = months - context;
            return new AttributeValue(duration, CallingContext);
        }

        [MemberDoc("Accepts a number of weeks to return as a TimeSpan.")]
        public AttributeValue Weeks(AttributeValue numberOfWeeks)
        {
            return new AttributeValue(TimeSpan.FromDays(7.0 * (double)numberOfWeeks.AsType(AttributeType.Double)), CallingContext);
        }

        [MemberDoc("Accepts a number of days to return as a TimeSpan.")]
        public AttributeValue Days(AttributeValue numberOfDays)
        {
            return new AttributeValue(TimeSpan.FromDays((double)numberOfDays.AsType(AttributeType.Double)), CallingContext);
        }

        [MemberDoc("Accepts a number of hours to return as a TimeSpan.")]
        public AttributeValue Hours(AttributeValue numberOfHours)
        {
            return new AttributeValue(TimeSpan.FromHours((double)numberOfHours.AsType(AttributeType.Double)), CallingContext);
        }

        [MemberDoc("Accepts a number of minutes to return as a TimeSpan.")]
        public AttributeValue Minutes(AttributeValue numberOfMinutes)
        {
            return new AttributeValue(TimeSpan.FromMinutes((double)numberOfMinutes.AsType(AttributeType.Double)), CallingContext);
        }

        [MemberDoc("Accepts a number of seconds to return as a TimeSpan.")]
        public AttributeValue Seconds(AttributeValue numberOfSeconds)
        {
            return new AttributeValue(TimeSpan.FromSeconds((double)numberOfSeconds.AsType(AttributeType.Double)), CallingContext);
        }

        [MemberDoc("Returns a timespan with UTC offset of the given datetime value")]
        public AttributeValue GetUtcOffset(AttributeValue value)
        {
            return new AttributeValue(TimeZoneInfo.Utc.GetUtcOffset(value.ToDateTime()), CallingContext);
        }

        [MemberDoc("Given a .NET timezone name, For possible values run PowerShell: [System.TimeZoneInfo]::GetSystemTimeZones() | select-object -Property Id. Optional date parameter returns the offset at that time.")]
        public AttributeValue GetTimeZoneOffset(AttributeValue timezoneName, AttributeValue datetime)
        {
            try
            {
                var info = TimeZoneInfo.FindSystemTimeZoneById(timezoneName.ToString());
                if (datetime.AsBoolean) { return new AttributeValue(info.GetUtcOffset(datetime.ToDateTime()), CallingContext); }
                return new AttributeValue(info.GetUtcOffset(DateTime.Now), CallingContext);
            }
            catch { }

            return new AttributeValue(null, CallingContext);
        }
    }
}
