﻿using System;
using System.Linq;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using Newtonsoft.Json;
using SoftwareIDM.PanelModel.Models;

namespace SoftwareIDM.UpliftEngine
{
    public enum ScramAlgorithm
    {
        SHA1,
        SHA256,
        SHA512
    }

    class ScramClient
    {
        readonly string _user;
        readonly string _pwd;
        readonly RandomNumberGenerator _rng = RandomNumberGenerator.Create();
        readonly string _nonce;
        readonly ScramAlgorithm _algorithm;
        readonly Func<byte[], byte[], byte[]> _hmac;
        readonly Func<byte[], byte[]> _sha;
        readonly int _hashLength = 20;

        public ScramClient(string user, string pwd, ScramAlgorithm alg = ScramAlgorithm.SHA512)
        {
            _user = user.Replace(",", "");
            _pwd = pwd;
            _algorithm = alg;
            switch (alg)
            {
                case ScramAlgorithm.SHA1:
                    _hashLength = 20;
                    _hmac = (a, b) =>
                    {
                        using (var h = new HMACSHA1(a))
                        {
                            return h.ComputeHash(b);
                        }
                    };
                    _sha = (a) =>
                    {
                        using (var h = SHA1.Create())
                        {
                            return h.ComputeHash(a);
                        }
                    };
                    break;
                case ScramAlgorithm.SHA256:
                    _hashLength = 32;
                    _hmac = (a, b) =>
                    {
                        using (var h = new HMACSHA256(a))
                        {
                            return h.ComputeHash(b);
                        }
                    };
                    _sha = (a) =>
                    {
                        using (var h = SHA256.Create())
                        {
                            return h.ComputeHash(a);
                        }
                    };
                    break;
                case ScramAlgorithm.SHA512:
                    _hashLength = 64;
                    _hmac = (a, b) =>
                    {
                        using (var h = new HMACSHA512(a))
                        {
                            return h.ComputeHash(b);
                        }
                    };
                    _sha = (a) =>
                    {
                        using (var h = SHA512.Create())
                        {
                            return h.ComputeHash(a);
                        }
                    };
                    break;
            }

            var nonce = new byte[_hashLength];
            _rng.GetBytes(nonce);
            _nonce = Convert.ToBase64String(nonce);
        }

        // https://tools.ietf.org/html/rfc5802#section-3
        public CookieCollection Authenticate()
        {
            var clientFirstMsg = $"n,,n={_user},r={_nonce}";
            var clientFirstMsgBare = $"n={_user},r={_nonce}";
            var serverFirstMsg = SendFirst(clientFirstMsg, out CookieCollection affinity);
            if (!ValidateServerFirst(serverFirstMsg, out string clientServerNonce, out byte[] salt, out int iteration))
            {
                throw new System.Security.Authentication.AuthenticationException("Unable to validate server message, login failed");
            }

            var saltedPwd = Hi(_pwd.Normalize(NormalizationForm.FormKC), salt, iteration);
            var clientKey = HMAC(saltedPwd, "Client Key");
            var storedKey = H(clientKey);

            var channelBinding = "c=" + Convert.ToBase64String(Encoding.UTF8.GetBytes("n,,"));
            var clientFinalWithoutProof = $"{channelBinding},{clientServerNonce}";
            var authMessage = $"{clientFirstMsgBare},{serverFirstMsg},{clientFinalWithoutProof}";
            var clientSignature = HMAC(storedKey, authMessage);
            var clientProof = Xor(clientKey, clientSignature);

            var clientFinalMsg = $"{clientFinalWithoutProof},p={Convert.ToBase64String(clientProof)}";

            var serverKey = HMAC(saltedPwd, "Server Key");
            var serverSignature = HMAC(serverKey, authMessage);

            var serverFinalMsg = SendFinal(clientFinalMsg, affinity, out CookieCollection ret);
            if (!ValidateServerFinal(serverFinalMsg, serverSignature))
            {
                throw new System.Security.Authentication.AuthenticationException("Unable to validate server message, login failed");
            }

            return ret;
        }

        // https://tools.ietf.org/html/rfc5802#section-2.2
        byte[] Hi(string normalizedPwd, byte[] salt, int iteration)
        {
            var pwd = Encoding.UTF8.GetBytes(normalizedPwd);
            var ustart = new byte[salt.Length + 4];
            salt.CopyTo(ustart, 0);
            new byte[] { 1, 0, 0, 0 }.CopyTo(ustart, salt.Length);

            byte[] ui = HMAC(pwd, ustart);
            var xor = ui;
            for (int i = 1; i < iteration; i++)
            {
                ui = HMAC(pwd, ui);
                xor = Xor(xor, ui);
            }

            return xor;
        }


        byte[] HMAC(byte[] pwd, string message)
        {
            return HMAC(pwd, Encoding.UTF8.GetBytes(message));
        }

        byte[] HMAC(byte[] pwd, byte[] message)
        {
            return _hmac(pwd, message);
        }

        byte[] H(byte[] clientKey) => _sha(clientKey);

        static byte[] Xor(byte[] a, byte[] b)
        {
            if (a == null || b == null || a.Length == 0 || b.Length == 0 || a.Length != b.Length)
            {
                throw new ArgumentException("Invalid XOR arguments");
            }

            byte[] ret = new byte[a.Length];
            for (int i = 0; i < a.Length; ++i)
            {
                ret[i] = (byte)(a[i] ^ b[i]);
            }

            return ret;
        }

        static bool ValidateServerFirst(string message, out string serverNonce, out byte[] salt, out int serverIteration)
        {
            salt = null;
            serverNonce = null;
            serverIteration = -1;
            if (string.IsNullOrEmpty(message)) { return false; }
            var split = message.Split(',');
            if (split.Length < 3) { return false; }
            serverNonce = split[0];
            salt = Convert.FromBase64String(split[1].Substring(2));
            if (!int.TryParse(split[2].Substring(2), out serverIteration)) { return false; }

            return true;
        }

        static bool ValidateServerFinal(string message, byte[] signature)
        {
            if (string.IsNullOrEmpty(message)) { return false; }
            var split = message.Split(new char[] { '=' }, 2);
            if (split.Length < 2) { return false; }
            if (split[0] != "v") { return false; }
            var serverSignature = Convert.FromBase64String(split[1]);
            return signature.SequenceEqual(serverSignature);
        }

        string SendFirst(string msg, out CookieCollection affinity)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            var url = $"{ClientConfig.PanelHost.TrimEnd('/')}/account/scramfirst";
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.CookieContainer = new CookieContainer();
            request.Method = "POST";
            request.ContentType = "application/json; charset=utf-8";
            request.MediaType = "application/json";
            request.Headers.Add("X-Api-Key", ClientConfig.APIKey);
            affinity = null;

            var post = JsonConvert.SerializeObject(new ApiDict<string>()
            {
                { "Message", msg },
                { "Algorithm", _algorithm.ToString().ToUpper() }
            });

            using (var writer = new StreamWriter(request.GetRequestStream()))
            {
                writer.Write(post);
            }

            var response = (HttpWebResponse)request.GetResponse();
            ApiDict<string> data;
            using (var stream = new StreamReader(response.GetResponseStream()))
            {
                data = JsonConvert.DeserializeObject<ApiDict<string>>(stream.ReadToEnd());
            }

            if (data.TryGetValue("Response", out string result))
            {
                if (response.Cookies != null && response.Cookies.Count > 0)
                {
                    affinity = response.Cookies;
                }

                return result;
            }
            else if (data.TryGetValue("Error", out string err))
            {
                throw new Exception(err);
            }
            else
            {
                throw new Exception("Login Failed");
            }
        }

        string SendFinal(string msg, CookieCollection affinity, out CookieCollection cookies)
        {
            var url = $"{ClientConfig.PanelHost.TrimEnd('/')}/account/scramfinal";
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.CookieContainer = new CookieContainer();
            request.Method = "POST";
            request.ContentType = "application/json; charset=utf-8";
            request.MediaType = "application/json";
            request.Headers.Add("X-Api-Key", ClientConfig.APIKey);
            if (affinity != null && affinity.Count > 0)
            {
                request.CookieContainer.Add(affinity);
            }

            var post = JsonConvert.SerializeObject(new ApiDict<string>()
            {
                { "Message", msg },
                { "Algorithm", _algorithm.ToString().ToUpper() }
            });

            using (var writer = new StreamWriter(request.GetRequestStream()))
            {
                writer.Write(post);
            }

            var response = (HttpWebResponse)request.GetResponse();
            ApiDict<string> data;
            using (var stream = new StreamReader(response.GetResponseStream()))
            {
                data = JsonConvert.DeserializeObject<ApiDict<string>>(stream.ReadToEnd());
            }

            if (data.TryGetValue("Response", out string result))
            {
                cookies = response.Cookies;
                return result;
            }
            else if (data.TryGetValue("Error", out string err))
            {
                throw new Exception(err);
            }
            else
            {
                throw new Exception("Login Failed");
            }
        }
    }
}
