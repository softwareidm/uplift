﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using SoftwareIDM.PanelModel.Attributes;
using SoftwareIDM.PanelModel.Rules;

namespace SoftwareIDM.UpliftEngine
{
    [ClassDoc("Rule functions for working with SQL Provider")]
    public class SqlFunctions : IRuleFunctions
    {
        public object CallingContext { get; set; }

        [MemberDoc("Executes the given SQL Scalar query against the specified system")]
        public AttributeValue SqlScalar(AttributeValue query, AttributeValue connectionString, Dictionary<string, AttributeValue> parameters)
        {
            var ret = SqlHelper.Scalar(
                connectionString.ToString(),
                query.ToString(),
                (from p in parameters
                 select new SqlParameter(p.Key.StartsWith("@") ? p.Key : "@" + p.Key, p.Value.Value)).ToArray()
            );

            if (ret == DBNull.Value)
            {
                ret = null;
            }

            return new AttributeValue(ret, CallingContext);
        }
    }
    static class SqlHelper
    {
        public static object Scalar(string connectionString, string query, params SqlParameter[] parameters)
        {
            using (var conn = new SqlConnection(connectionString))
            {
                conn.Open();
                using (var sCmd = new SqlCommand(query, conn))
                {
                    sCmd.Parameters.AddRange(parameters);
                    return sCmd.ExecuteScalar();
                }
            }
        }

        public static IEnumerable<DbDataReader> Query(string connectionString, string query, params SqlParameter[] parameters)
        {
            using (var conn = new SqlConnection(connectionString))
            {
                conn.Open();
                var cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddRange(parameters);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    yield return reader;
                }

                reader.Close();
                cmd.Dispose();
            }
        }

        public static void NonQuery(string connectionString, string query, params SqlParameter[] parameters)
        {
            using (var conn = new SqlConnection(connectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand(query, conn))
                {
                    cmd.Parameters.AddRange(parameters);
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}
