﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using MongoDB.Bson;
using SoftwareIDM.PanelModel.Attributes;
using SoftwareIDM.PanelModel.Models;
using SoftwareIDM.PanelModel.Rules;

namespace SoftwareIDM.UpliftEngine
{
    [ClassDoc("Helper functions for checking and creating unique naming values", "Uniqueness Functions")]
    public class UniquenessFunctions : IRuleFunctions
    {
        public object CallingContext { get; set; }

        [MemberDoc(@"Returns true or false whether the testValue is unique in the named uniqueness index.<br/>
indexName should match one of the defined unique indices in provider settings.<br/>
ownerKey should be a value corresponding to a key in the unique index. A match on both key value and test value will return true, as that test value is considered to own the unique value.")]
        public AttributeValue IsUnique(AttributeValue indexName, AttributeValue ownerKey, AttributeValue testValue)
        {
            var parameters = new ApiDict<string>
            {
                { "indexName", indexName.ToString() },
                { "ownerKey", ownerKey.ToString() },
                { "testValue", testValue.ToString() }
            };

            var res = Rest.Post<ApiDict<bool>, ApiDict<string>>("uplift/action/isunique", parameters, new CancellationToken()).GetAwaiter().GetResult();
            if (res == null)
            {
                throw new ArgumentException($"Unable to read unique index {indexName}");
            }

            return new AttributeValue(res["Result"], CallingContext);
        }

        [MemberDoc(@"Returns a unique naming value after checking and iterating against a unique index.<br/>
indexName should match one of the defined unique indices in provider settings.<br/>
ownerKey should be a value corresponding to a key in the unique index. A match on key value and test value will allow resuse of a value associated with that key.<br/>
rootValue is a search value to find all potential matches/collisions for a name. It should be a canonical non-unique form for the value.<br/>
uniqueRule is a rule to generate a new unique name using a UniqueContext object.<br/>
altContext (opt.) specify parent context to use in iteration, useful when defining make unique rules inside custom functions where a parameter might be desired for evaluation context.")]
        public AttributeValue MakeUnique(AttributeValue indexName, AttributeValue ownerKey, AttributeValue rootValue, AttributeValue uniqueRule, AttributeValue altContext)
        {
            var parameters = new ApiDict<string>
            {
                { "indexName", indexName.ToString() },
                { "rootValue", rootValue.ToString() }
            };

            var res = Rest.Post<ListWrap<UniqueIndex>, ApiDict<string>>("uplift/action/unique", parameters, new CancellationToken()).GetAwaiter().GetResult();
            if (res == null)
            {
                throw new ArgumentException($"Unable to read unique index {indexName}");
            }

            var hits = new Dictionary<string, UniqueIndex>(StringComparer.OrdinalIgnoreCase);
            foreach (var unique in res.Data)
            {
                foreach (var val in unique.Values)
                {
                    hits[val] = unique;
                }
            }

            var makeRule = Rule.Build(uniqueRule.Rule.ToString());
            var ruleContext = new UniqueContext
            {
                ParentContext = (altContext != null && altContext.Value != null) ? altContext.Value : uniqueRule.Context,
                Iteration = 0,
                Key = ownerKey.ToString(),
                Root = rootValue.ToString()
            };

            for (int i = 0; i < res.Count; i++)
            {
                ruleContext.Iteration = i;
                var testVal = RuleEngine.GetValue(makeRule, ruleContext, CallingContext);
                if (testVal.Value == null) { continue; }
                if (!hits.TryGetValue(testVal.ToString(), out UniqueIndex hit) || !string.IsNullOrEmpty(ownerKey.ToString()) && hit.Keys.Contains(ownerKey.ToString(), StringComparer.OrdinalIgnoreCase))
                {
                    // Query value directly to check if it really is a unique value and we didn't mess up our root
                    if (IsUnique(indexName, ownerKey, testVal).AsBoolean)
                    {
                        // reserve values
                        var save = new UniqueIndex
                        {
                            Id = ObjectId.GenerateNewId(),
                            Keys = new List<string> { ownerKey.ToString() },
                            Roots = new List<string> { rootValue.ToString() },
                            Values = new List<string> { testVal.ToString() }
                        };

                        Rest.Post("uplift/action/reserveunique?indexName=" + Uri.EscapeDataString(indexName.ToString()), save, new CancellationToken()).GetAwaiter().GetResult();
                        return testVal;
                    }
                }
            }

            return new AttributeValue(null, CallingContext);
        }

        [MemberDoc("Lookup an object record by Id and Silo, returns the full object. Returns null if the object does not exist. Excludes deleted objects unless specified")]
        public AttributeValue ResolveId(AttributeValue id, AttributeValue silo, AttributeValue includeDeleted)
        {
            var url = $"objects/{id.ToGuid()}";
            if (!string.IsNullOrEmpty(silo.ToString()))
            {
                url += $"?silo={Uri.EscapeDataString(silo.ToString())}";
            }

            try
            {
                var obj = Rest.Get<ObjectRecord>(url, new CancellationToken()).GetAwaiter().GetResult();
                if (obj != null && obj.Deleted.HasValue && !includeDeleted.AsBoolean)
                {
                    return new AttributeValue(null, CallingContext);
                }

                return new AttributeValue(obj, CallingContext);
            }
            catch
            {
                return new AttributeValue(null, CallingContext);
            }
        }

        [MemberDoc("Lookup an arbitrary Identity Silo object by silo, attribute, and value. Will only return a value if a single object matches.")]
        public AttributeValue LookupObject(AttributeValue silo, AttributeValue attribute, AttributeValue value, AttributeValue includeDeleted)
        {
            var filter = new List<string>
            {
                "Silo eq string:" + silo.ToString(),
                $"Attributes.{attribute} eq {value.DataType}:{value.ToString().Replace(" ", "\\ ")}"
            };

            if (!includeDeleted.AsBoolean)
            {
                filter.Add("Deleted null");
            }
            
            var url = $"objects?filter={Uri.EscapeDataString(string.Join(" ", filter))}&limit=1";
            var objs = Rest.Get<ListWrap<ObjectRecord>>(url, new CancellationToken()).GetAwaiter().GetResult();
            if (objs != null && objs.Count == 1)
            {
                return new AttributeValue(objs.Data[0], CallingContext);
            }

            return new AttributeValue(null, CallingContext);
        }
    }
}
