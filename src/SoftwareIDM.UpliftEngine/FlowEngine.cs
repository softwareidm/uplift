﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.MetadirectoryServices;
using SoftwareIDM.PanelModel.Rules;
using SoftwareIDM.Uplift;

namespace SoftwareIDM.UpliftEngine
{
    public static class FlowEngine
    {
        readonly static Dictionary<string, Rule> _cache = new Dictionary<string, Rule>();

        /// <summary>
        /// Handle Rule processing.
        /// </summary>
        /// <param name="rule">
        /// String with rule logic. 
        /// Rule takes the form Rule->target.
        /// </param>
        /// <param name="source">CSEntry or MVEntry inside an EntryWrapper</param>
        /// <param name="target">MVEntry or CSEntry inside an EntryWrapper</param>
        /// <returns>Whether the rule made a change.</returns>
        public static bool Run(string rule, IEntryWrapper source, IEntryWrapper target)
        {
            source.SetOther(target);
            target.SetOther(source);

            var match = Regex.Match(rule, "^(.+)->(.+)$", RegexOptions.Singleline);
            var r = match.Groups[1].Value;
            if (!_cache.TryGetValue(r, out Rule ruleInst))
            {
                ruleInst = Rule.Build(r);
                _cache[r] = ruleInst;
            }

            var val = ruleInst.BindValue(source, source.Raw);
            var field = match.Groups[2].Value.Trim();
            if (val.ToString() == CommonMIMFunctions.SKIPFLOW)
            {
                return false;
            }

            if (target[field].Equals(val))
            {
                return false;
            }

            target[field] = val;
            return true;
        }
    }
}
