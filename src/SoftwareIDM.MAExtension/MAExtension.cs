﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Linq;
using Microsoft.MetadirectoryServices;
using SoftwareIDM.PanelModel.Rules;
using SoftwareIDM.Uplift;
using SoftwareIDM.UpliftEngine;

namespace SoftwareIDM.MAExtension
{
    public class MAExtension : IMASynchronization
    {
        #region assemblyResolve
        public static bool ResolveHandlerSet = false;
        static string panelDir = null;
        public static Assembly AssemblyResolveHandler(object sender, ResolveEventArgs args)
        {
            var name = args.Name;
            var index = name.IndexOf(',');
            if (index > 0)
            {
                name = name.Substring(0, index);
            }

            var probe = Path.Combine(Utils.ExtensionsDirectory, name + ".dll");
            if (File.Exists(probe))
            {
                return Assembly.LoadFrom(probe);
            }

            return Probe(name, args.Name, panelDir);
        }

        static Assembly Probe(string name, string fullName, string startLocation)
        {
            Assembly assembly = null;
            foreach (var loc in new string[] {
                "lib",
                new FileInfo(startLocation).Directory.FullName,
            })
            {
                var path = loc;
                if (!Path.IsPathRooted(path))
                {
                    path = Path.Combine(new FileInfo(startLocation).Directory.FullName, path);
                }

                var info = new DirectoryInfo(path);
                var probe = Path.Combine(info.FullName, name + ".dll");
                if (File.Exists(probe))
                {
                    assembly = Assembly.LoadFrom(probe);
                    break;
                }
            }

            if (assembly == null && name.ToLower().EndsWith(".resources"))
            {
                return null;
            }

            // last resort
            if (assembly == null)
            {
                return Gac(name, fullName);
            }

            return assembly;
        }

        static Assembly Gac(string name, string fullName)
        {
            var version = "";
            var token = "";
            Assembly assembly = null;

            if (fullName.Contains("Version=") && fullName.Contains("PublicKeyToken="))
            {
                version = System.Text.RegularExpressions.Regex.Match(fullName, @"Version=(\d+\.\d+\.\d+)\.\d+").Groups[1].Value;
                token = System.Text.RegularExpressions.Regex.Match(fullName, @"PublicKeyToken=(\w+)").Groups[1].Value;
            }

            // look in GAC
            var framework = System.Runtime.InteropServices.RuntimeEnvironment.GetRuntimeDirectory();
            var msnet = new DirectoryInfo(framework).Parent.Parent;
            var msil = new DirectoryInfo(Path.Combine(msnet.FullName, "assembly", "GAC_MSIL"));

            var folder = msil.GetDirectories(name, SearchOption.TopDirectoryOnly);
            if (folder.Length == 1)
            {
                var dir = folder[0];
                foreach (var subdir in dir.GetDirectories())
                {
                    if (subdir.Name.Contains(version) && subdir.Name.Contains(token))
                    {
                        assembly = Assembly.LoadFrom(subdir.GetFiles("*.dll", SearchOption.TopDirectoryOnly).First().FullName);
                    }
                }
            }

            if (assembly == null && name.ToLower().EndsWith(".resources"))
            {
                return Gac(name.Substring(0, name.Length - ".resources".Length), fullName);
            }

            return assembly;
        }

        #endregion

        public MAExtension()
        {
            if (!ResolveHandlerSet)
            {
                AppDomain.CurrentDomain.AssemblyResolve += AssemblyResolveHandler;
                ResolveHandlerSet = true;
            }
        }

        public DeprovisionAction Deprovision(CSEntry csentry)
        {
            string rule = "";
            try
            {
                var get = Config.Get<MARuleConfig>();
                if (get != null && get.DeprovisionRules.TryGetValue(csentry.MA.Name, out List<ScopeRule> actions))
                {
                    foreach (var act in actions)
                    {
                        rule = "";
                        if (!act.CSObjectType.Equals(csentry.ObjectType, StringComparison.OrdinalIgnoreCase))
                        {
                            continue;
                        }

                        rule = act.Scope;
                        var cs = (CSWrapper)csentry;
                        if (!string.IsNullOrEmpty(act.Scope) && !RuleEngine.GetValue(act.Scope, cs, cs).AsBoolean)
                        {
                            continue;
                        }

                        rule = act.Action;
                        if (act.AttributeRules != null && act.AttributeRules.Count > 0)
                        {
                            foreach (var set in act.AttributeRules)
                            {
                                var val = RuleEngine.GetValue(set.RuleInst, cs, cs);
                                cs.Set(set.Attribute, val);
                            }
                        }

                        return (DeprovisionAction)Enum.Parse(typeof(DeprovisionAction), act.Action, true);
                    }
                }

                return DeprovisionAction.Disconnect;
            }
            catch (Exception e)
            {
                Logger.WriteEntry($"*r*Deprovision Error on {csentry.MA.Name}:*r* {rule}\r\n{csentry.DN}\r\n {e.Message}\r\n{e.StackTrace}", Microsoft.Extensions.Logging.LogLevel.Error);
                throw;
            }
        }

        public bool FilterForDisconnection(CSEntry csentry)
        {
            string rule = "";
            try
            {
                var get = Config.Get<MARuleConfig>();
                if (get != null && get.FilterRules.TryGetValue(csentry.MA.Name, out List<FilterSet> actions))
                {
                    foreach (var act in actions)
                    {
                        rule = "";
                        if (!act.CSObjectType.Equals(csentry.ObjectType, StringComparison.OrdinalIgnoreCase))
                        {
                            continue;
                        }

                        var cs = (CSWrapper)csentry;
                        rule = act.Rule;
                        if (!string.IsNullOrEmpty(act.Rule) && RuleEngine.GetValue(act.Rule, cs, cs).AsBoolean)
                        {
                            return true;
                        }
                    }
                }

                return false;
            }
            catch (Exception e)
            {
                Logger.WriteEntry($"*r*Filter Error on {csentry.MA.Name}:*r* {rule}\r\n{csentry.DN}\r\n {e.Message}\r\n{e.StackTrace}", Microsoft.Extensions.Logging.LogLevel.Error);
                throw;
            }
        }

        public void MapAttributesForJoin(string FlowRuleName, CSEntry csentry, ref ValueCollection values)
        {
            try
            {
                var cs = (CSWrapper)csentry;
                var val = RuleEngine.GetValue(FlowRuleName, cs, cs);
                if (val.Value != null)
                {
                    if (val.DataType == PanelModel.Rules.AttributeType.Other && val is System.Collections.IEnumerable enumC)
                    {
                        foreach (var valE in enumC)
                        {
                            if (valE == null) { continue; }
                            values.Add(valE.ToString());
                        }
                    }
                    else
                    {
                        values.Add(val.ToString());
                    }
                }
            }
            catch (Exception e)
            {
                Logger.WriteEntry($"*r*Join Rule Error on {csentry.MA.Name}:*r* {FlowRuleName}\r\n{csentry.DN}\r\n {e.Message}\r\n{e.StackTrace}", Microsoft.Extensions.Logging.LogLevel.Error);
                throw;
            }
        }

        public bool ResolveJoinSearch(string joinCriteriaName, CSEntry csentry, MVEntry[] rgmventry, out int imventry, ref string MVObjectType)
        {
            try
            {
                imventry = -1;
                var cs = (CSWrapper)csentry;
                for (int i = 0; i < rgmventry.Length; i++)
                {
                    imventry = i;
                    var mv = (MVWrapper)rgmventry[i];
                    mv.SetOther(cs);
                    cs.SetOther(mv);
                    if (RuleEngine.GetValue(joinCriteriaName, mv, mv).AsBoolean)
                    {
                        return true;
                    }
                }

                return false;
            }
            catch (Exception e)
            {
                Logger.WriteEntry($"*r*Resolve Join Error on {csentry.MA.Name}:*r* {joinCriteriaName}\r\n{csentry.DN}\r\n {e.Message}\r\n{e.StackTrace}", Microsoft.Extensions.Logging.LogLevel.Error);
                throw;
            }
        }

        public bool ShouldProjectToMV(CSEntry csentry, out string MVObjectType)
        {
            MVObjectType = "";
            var get = Config.Get<MARuleConfig>();
            if (get != null && get.ProjectionRules.TryGetValue(csentry.MA.Name, out List<Project> actions))
            {
                foreach (var act in actions)
                {
                    if (!act.CSObjectType.Equals(csentry.ObjectType, StringComparison.OrdinalIgnoreCase))
                    {
                        continue;
                    }

                    var cs = (CSWrapper)csentry;
                    if (!string.IsNullOrEmpty(act.ExtensionRule))
                    {
                        var val = RuleEngine.GetValue(act.ExtensionRule, cs, cs);
                        if (val.AsBoolean)
                        {
                            MVObjectType = val.ToString();
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        public void Initialize()
        {
            Config.Init();
        }

        public void MapAttributesForExport(string FlowRuleName, MVEntry mventry, CSEntry csentry)
        {
            try
            {
                FlowEngine.Run(FlowRuleName, (MVWrapper)mventry, (CSWrapper)csentry);
            }
            catch (Exception e)
            {
                Logger.WriteEntry($"*r*Export Error on {csentry.MA.Name}:*r* {FlowRuleName}\r\n{csentry.DN}\r\n{e.Message}\r\n{e.StackTrace}", Microsoft.Extensions.Logging.LogLevel.Error);
                throw;
            }
        }

        public void MapAttributesForImport(string FlowRuleName, CSEntry csentry, MVEntry mventry)
        {
            try
            {
                FlowEngine.Run(FlowRuleName, (CSWrapper)csentry, (MVWrapper)mventry);
            }
            catch (Exception e)
            {
                Logger.WriteEntry($"*r*Import Error on {csentry.MA.Name}:*r* {FlowRuleName}\r\n{(mventry["displayName"].IsPresent ? mventry["displayName"].Value : "")}\r\n{e.Message}\r\n{e.StackTrace}", Microsoft.Extensions.Logging.LogLevel.Error);
                throw;
            }
        }

        public void Terminate()
        {
            Logger.Finish();
        }
    }
}
