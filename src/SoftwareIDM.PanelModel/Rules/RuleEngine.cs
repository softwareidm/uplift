﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Reflection;
using SoftwareIDM.PanelModel.Attributes;

namespace SoftwareIDM.PanelModel.Rules
{
    /// <summary>
    /// Static helper class for instantiating and evaluating rules
    /// </summary>
    public static class RuleEngine
    {

        static void Handle(Type type, Dictionary<string, MethodInfo> functions, ConcurrentDictionary<string, RuleFunctionDoc> funcDoc)
        {
            var doc = new RuleFunctionDoc
            {
                Name = type.Name,
                Description = ""
            };

            foreach (var attr in type.GetCustomAttributes())
            {
                if (attr is ClassDoc)
                {
                    doc.Name = ((ClassDoc)attr).Name;
                    doc.Description = ((ClassDoc)attr).Description;
                    break;
                }
            }

            foreach (var m in type.GetMethods())
            {
                if (m.IsPrivate)
                {
                    continue;
                }

                var name = m.Name;
                var ps = new List<string>();
                foreach (var p in m.GetParameters())
                {
                    if (p.ParameterType.IsArray)
                    {
                        ps.Add(p.Name + "[]");
                    }
                    else if (p.ParameterType.Name.StartsWith("Dictionary"))
                    {
                        ps.Add(p.Name + "{}");
                    }
                    else
                    {
                        ps.Add(p.Name);
                    }
                }

                name = name + "(" + string.Join(", ", ps) + ")";

                foreach (var attr in m.GetCustomAttributes())
                {
                    if (attr is MemberDoc)
                    {
                        doc.FunctionDoc[name] = ((MemberDoc)attr).Description;
                        break;
                    }
                }

                functions[m.Name] = m;
            }

            funcDoc[type.Name] = doc;
        }

        static readonly object lockRule = new object();
        static void LoadRuleMethods()
        {
            lock (lockRule)
            {
                functions = new Dictionary<string, MethodInfo>(StringComparer.OrdinalIgnoreCase);
                funcDoc = new ConcurrentDictionary<string, RuleFunctionDoc>(StringComparer.OrdinalIgnoreCase);
                specialVals = new ConcurrentDictionary<string, ISpecialValues>(StringComparer.OrdinalIgnoreCase);
                contextTypes = new ConcurrentDictionary<string, Type>(StringComparer.OrdinalIgnoreCase);

                foreach (var assem in from a in AppDomain.CurrentDomain.GetAssemblies()
                                      where a.FullName.Contains("SoftwareIDM")
                                      select a)
                {
                    foreach (var type in assem.ExportedTypes)
                    {
                        if (type.GetCustomAttribute(typeof(ClassDoc), true) != null)
                        {
                            if (type.GetInterface("IRuleFunctions") != null && type.IsClass)
                            {
                                Handle(type, functions, funcDoc);
                            }

                            if (type.GetInterface("ISpecialValues") != null && type.IsClass)
                            {
                                var inst = (ISpecialValues)Activator.CreateInstance(type);
                                specialVals[inst.Name] = inst;
                            }

                            var attr = (ClassDoc)type.GetCustomAttribute(typeof(ClassDoc), true);
                            if (attr.RuleDoc)
                            {
                                contextTypes[type.FullName] = type;
                            }
                        }
                    }
                }
            }
        }

        public static void ResetCache()
        {
            LoadRuleMethods();
        }

        static ConcurrentDictionary<string, Type> contextTypes;
        public static ConcurrentDictionary<string, Type> ContextTypes
        {
            get
            {
                if (contextTypes == null || contextTypes.Count == 0)
                {
                    LoadRuleMethods();
                }

                return contextTypes;
            }
        }

        static ConcurrentDictionary<string, RuleFunctionDoc> funcDoc;
        public static ConcurrentDictionary<string, RuleFunctionDoc> FunctionDocs
        {
            get
            {
                if (funcDoc == null || funcDoc.Count == 0)
                {
                    LoadRuleMethods();
                }

                return funcDoc;
            }
        }

        static ConcurrentDictionary<string, ISpecialValues> specialVals;
        public static ConcurrentDictionary<string, ISpecialValues> SpecialValues
        {
            get
            {
                if (specialVals == null || specialVals.Count == 0)
                {
                    LoadRuleMethods();
                }

                return specialVals;
            }
        }

        static Dictionary<string, MethodInfo> functions;
        public static Dictionary<string, MethodInfo> Functions
        {
            get
            {
                if (functions == null || functions.Count == 0)
                {
                    LoadRuleMethods();
                }

                return functions;
            }
        }

        public static Func<object, RuleCache> RuleCache { get; set; }

        public static AttributeValue GetValue(Rule ruleInst, object context, object callingContext)
        {
            AttributeValue ret;
            ret = ruleInst.GetValue(context, callingContext);
            ret.Bind();
            return ret;
        }

        public static bool Test(Rule ruleInst, object context, object callingContext)
        {
            return GetValue(ruleInst, context, callingContext).AsBoolean;
        }

        public static AttributeValue GetValue(string rule, object context, object callingContext)
        {
            AttributeValue ret;
            Rule ruleInst;
            if (RuleCache != null)
            {
                var ruleCache = RuleCache(callingContext);
                var typeContext = rule + (context == null ? "" : context.GetType().Name);

                if (!ruleCache.Cache.TryGetValue(typeContext, out ruleInst))
                {
                    ruleInst = Rule.Build(rule);
                    ruleCache.Cache[typeContext] = ruleInst;
                }
            }
            else
            {
                ruleInst = Rule.Build(rule);
            }
            
            ret = ruleInst.GetValue(context, callingContext);
            ret.Bind();
            return ret;
        }
        
        public static bool Test(string rule, object context, object callingContext)
        {
            return GetValue(rule, context, callingContext).AsBoolean;
        }
    }

    public class RuleFunctionDoc
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public Dictionary<string, string> FunctionDoc { get; set; }

        public RuleFunctionDoc()
        {
            FunctionDoc = new Dictionary<string, string>();
        }
    }

    /// <summary>
    /// Wrapper class for run-location/instance specific cache of parsed rules and runable custom functions
    /// </summary>
    public class RuleCache
    {
        public ConcurrentDictionary<string, Rule> Cache { get; } = new ConcurrentDictionary<string, Rule>();

        public Dictionary<string, RuleFunction> Functions { get; } = new Dictionary<string, RuleFunction>();

        public void RegisterRule(RuleFunction ruleFunc)
        {
            var re1 = System.Text.RegularExpressions.Regex.Match(ruleFunc.Name, @"^(\w+)\(.*\)$");
            if (re1 != null)
            {
                Functions[re1.Groups[1].Value] = ruleFunc;
            }
        }
    }
}