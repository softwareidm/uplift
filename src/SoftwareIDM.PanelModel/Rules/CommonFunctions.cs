﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using System.Xml.XPath;
using Newtonsoft.Json.Linq;
using PhoneNumbers;
using SoftwareIDM.PanelModel.Attributes;

namespace SoftwareIDM.PanelModel.Rules
{
    public static class SafeSubString
    {

        /// <summary>
        /// SafeSubstring is a helper proxy for string.Substring that performs bounds checking to avoid
        /// index errors. The returned substring may be "", or may be shorter than the length parameter.
        /// SafeSubstring is cannot be used directly by Rules. See Mid.
        /// </summary>
        public static string SafeSubstring(this string str, int start, int length)
        {
            if (str == null || start < 0 || start > str.Length)
            {
                return "";
            }

            int max = str.Length - start;
            if (length <= 0)
            {
                length = max;
            }

            if (max < length)
            {
                length = max;
            }
            return str.Substring(start, length);
        }
    }

    [ClassDoc(
        Name = "Boolean Functions",
        Description = "Perform boolean logic operations")]
    public class CommonBoolFunctions : IRuleFunctions
    {
        public object CallingContext { get; set; }

        [MemberDoc("Returns Parameters[0] &amp;&amp; Parameters[1] .... <br />Uses javascript like boolean coercion with shortcircuit evaluation")]
        public AttributeValue And(AttributeValue[] Parameters)
        {
            bool ret = true;
            foreach (var p in CommonNumFunctions.ConvertArg(Parameters, CallingContext))
            {
                ret = ret && p.AsBoolean;
                if (!ret)
                {
                    return new AttributeValue(false, CallingContext);
                }
            }

            return new AttributeValue(true, CallingContext);
        }

        [MemberDoc("Returns null if the value argument is not true.")]
        public AttributeValue NullIfFalse(AttributeValue value)
        {
            if (!value.AsBoolean) { return new AttributeValue(null, CallingContext); }

            return value;
        }
        
        [MemberDoc("Indicates if byte[] or string is null or empty, <br />or if a number is 0 or a boolean is false.")]
        public AttributeValue IsNullOrEmpty(AttributeValue val)
        {
            bool ret;
            switch (val.DataType)
            {
                case AttributeType.Binary:
                    ret = val.Value == null || ((byte[])val.Value).Length == 0;
                    break;
                case AttributeType.Boolean:
                    ret = !(bool)val.Value;
                    break;
                case AttributeType.Double:
                    ret = (double)val.Value == 0;
                    break;
                case AttributeType.Long:
                    ret = (long)val.Value == 0;
                    break;
                default:
                    ret = string.IsNullOrEmpty(val.ToString());
                    break;
            };
            return new AttributeValue(ret, CallingContext);
        }

        [MemberDoc("Returns Parameters[0] || Parameters[1] .... <br />Uses javascript like boolean coercion with shortcircuit evaluation")]
        public AttributeValue Or(AttributeValue[] Parameters)
        {
            bool ret = false;
            foreach (var p in CommonNumFunctions.ConvertArg(Parameters, CallingContext))
            {
                ret = ret || p.AsBoolean;
                if (ret)
                {
                    return new AttributeValue(true, CallingContext);
                }
            }

            return new AttributeValue(false, CallingContext);
        }

        [MemberDoc("Returns !Value, Uses javascript like boolean coercion")]
        public AttributeValue Not(AttributeValue Value)
        {
            return new AttributeValue(!Value.AsBoolean, CallingContext);
        }

        [MemberDoc("If (Condition) use True else use False")]
        public AttributeValue If(AttributeValue Condition, AttributeValue True, AttributeValue False)
        {
            if (Condition.AsBoolean)
            {
                return True;
            }

            return False;
        }

        [MemberDoc("Switch(Condition, Value, Condition2, Value2...) Alternates condition and values rules, returns the first value rule after a true condition rule. If no values match, returns null or the last value, depending on if an even or odd number of arguments were supplied.")]
        public AttributeValue Switch(params AttributeValue[] rules)
        {
            if (rules.Length % 2 > 0)
            {
                for (int i = 0; i < rules.Length - 1; i += 2)
                {
                    if (rules[i].AsBoolean)
                    {
                        return rules[i + 1];
                    }
                }

                return rules[rules.Length - 1];
            }

            for (int i = 0; i < rules.Length; i += 2)
            {
                if (rules[i].AsBoolean)
                {
                    return rules[i + 1];
                }
            }

            return new AttributeValue(null, CallingContext);
        }
    }

    [ClassDoc("Perform operations and transformations on numeric values", "Numeric Functions")]
    public class CommonNumFunctions : IRuleFunctions
    {
        public object CallingContext { get; set; }

        // Numeric Functions
        [MemberDoc("Returns Int64.MaxValue")]
        public AttributeValue MaxLong()
        {
            return new AttributeValue(long.MaxValue, CallingContext);
        }

        [MemberDoc("Returns Int64.MinValue")]
        public AttributeValue MinLong()
        {
            return new AttributeValue(long.MinValue, CallingContext);
        }

        [MemberDoc("Returns Int32.MaxValue")]
        public AttributeValue MaxInt()
        {
            return new AttributeValue(int.MaxValue, CallingContext);
        }

        [MemberDoc("Returns Int32.MinValue")]
        public AttributeValue MinInt()
        {
            return new AttributeValue(int.MinValue, CallingContext);
        }

        [MemberDoc("Returns Int16.MaxValue")]
        public AttributeValue MaxShort()
        {
            return new AttributeValue(short.MaxValue, CallingContext);
        }

        [MemberDoc("Returns Int16.MinValue")]
        public AttributeValue MinShort()
        {
            return new AttributeValue(short.MinValue, CallingContext);
        }

        [MemberDoc("Returns bitwise and of parameters, casts to long: mask &amp; flag")]
        public AttributeValue BitAnd(AttributeValue mask, AttributeValue flag)
        {
            return new AttributeValue((long)flag.AsType(AttributeType.Long) & (long)mask.AsType(AttributeType.Long), CallingContext);
        }


        [MemberDoc("Returns bitwise and of parameters, casts to long: mask | flag")]
        public AttributeValue BitOr(AttributeValue mask, AttributeValue flag)
        {
            return new AttributeValue((long)flag.AsType(AttributeType.Long) | (long)mask.AsType(AttributeType.Long), CallingContext);
        }

        [MemberDoc("Returns bitwise not flag, casts to long: ~flag")]
        public AttributeValue BitNot(AttributeValue flag)
        {
            return new AttributeValue(~(long)flag.AsType(AttributeType.Long), CallingContext);
        }

        [MemberDoc("Numeric flag values to set. Use ADFlag function or special value to get values. eg. special.AD.NORMAL_ACCOUNT")]
        public AttributeValue SetBits(AttributeValue value, params AttributeValue[] flags)
        {
            long curr = value.ToLong();
            foreach (var val in flags)
            {
                curr |= val.ToLong();
            }

            return new AttributeValue(curr, CallingContext);
        }

        [MemberDoc("Numeric flag values to unset. Use ADFlag function or special value to get valuse. eg. special.AD.NORMAL_ACCOUNT")]
        public AttributeValue UnsetBits(AttributeValue value, params AttributeValue[] flags)
        {
            long curr = value.ToLong();
            foreach (var val in flags)
            {
                curr &= ~val.ToLong();
            }
            
            return new AttributeValue(curr, CallingContext);
        }

        static readonly Random rand = new Random();
        [MemberDoc("Returns random number such that start &lt;= rand &lt; end")]
        public AttributeValue RandomNum(AttributeValue start, AttributeValue end)
        {
            return new AttributeValue(rand.Next((int)(long)start.AsType(AttributeType.Long), (int)(long)end.AsType(AttributeType.Long)), CallingContext);
        }

        [MemberDoc("Returns a new random Guid")]
        public AttributeValue GenerateGuid()
        {
            return new AttributeValue(Guid.NewGuid(), CallingContext);
        }

        internal static IEnumerable<AttributeValue> ConvertArg(AttributeValue[] values, object callingContext)
        {
            if (values != null && values.Length == 1 && values[0] != null && values[0].DataType == AttributeType.Other && (values[0].Value is IEnumerable))
            {
                var vals = values[0].Value as IEnumerable;
                foreach (var v in vals)
                {
                    if (v is AttributeValue av) { yield return av; }
                    else { yield return new AttributeValue(v, callingContext); }
                }
            }
            else
            {
                foreach (var v in values)
                {
                    yield return v;
                }
            }
        }

        [MemberDoc("Returns sum of values, casts to long")]
        public AttributeValue Sum(AttributeValue[] values)
        {
            long sum = 0;
            foreach (var v in ConvertArg(values, CallingContext))
            {
                sum += (long)v.AsType(AttributeType.Long);
            }

            return new AttributeValue(sum, CallingContext);
        }

        [MemberDoc("Returns values multiplied together, casts to long")]
        public AttributeValue Multiply(AttributeValue[] values)
        {
            long res = 1;
            foreach (var v in ConvertArg(values, CallingContext))
            {
                res *= (long)v.AsType(AttributeType.Long);
            }

            return new AttributeValue(res, CallingContext);
        }

        [MemberDoc("Returns value % mod, casts both values to long")]
        public AttributeValue Modulus(AttributeValue value, AttributeValue mod)
        {
            return new AttributeValue((long)value.AsType(AttributeType.Long) % (long)mod.AsType(AttributeType.Long), CallingContext);
        }

        [MemberDoc("Returns numerator divided by denominator, casts to long")]
        public AttributeValue Divide(AttributeValue numerator, AttributeValue denominator)
        {
            if (denominator.Value == null || denominator.ToLong() == 0)
            {
                return new AttributeValue(null, CallingContext);
            }

            return new AttributeValue(numerator.ToLong() / denominator.ToLong(), CallingContext);
        }

        [MemberDoc("Returns absolute value of number")]
        public AttributeValue Abs(AttributeValue value)
        {
            return new AttributeValue(Math.Abs(value.ToLong()), CallingContext);
        }

        [MemberDoc("Returns absolute value of number")]
        public AttributeValue AbsDouble(AttributeValue value)
        {
            return new AttributeValue(Math.Abs(value.ToDouble()), CallingContext);
        }

        [MemberDoc("Returns sum of values, casts to double")]
        public AttributeValue SumDouble(AttributeValue[] values)
        {
            double sum = 0;
            foreach (var v in ConvertArg(values, CallingContext))
            {
                sum += (double)v.AsType(AttributeType.Double);
            }

            return new AttributeValue(sum, CallingContext);
        }

        [MemberDoc("Returns values multiplied together, casts to double")]
        public AttributeValue MultiplyDouble(AttributeValue[] values)
        {
            double res = 1;
            foreach (var v in ConvertArg(values, CallingContext))
            {
                res *= (double)v.AsType(AttributeType.Double);
            }

            return new AttributeValue(res, CallingContext);
        }

        [MemberDoc("Returns value % mod, casts both values to double")]
        public AttributeValue ModulusDouble(AttributeValue value, AttributeValue mod)
        {
            return new AttributeValue((double)value.AsType(AttributeType.Double) % (double)mod.AsType(AttributeType.Double), CallingContext);
        }

        [MemberDoc("Returns numerator divided by denominator, casts to double")]
        public AttributeValue DivideDouble(AttributeValue numerator, AttributeValue denominator)
        {
            if (denominator.Value == null || denominator.ToDouble() == 0)
            {
                return new AttributeValue(null, CallingContext);
            }

            return new AttributeValue(numerator.ToDouble() / denominator.ToDouble(), CallingContext);
        }

        [MemberDoc("Returns the byte count of an azure file size. e.g., given \"90 GB (96,636,764,160 bytes)\" will return long 96636764160")]
        public AttributeValue ParseBytesSize(AttributeValue fileSizeString)
        {
            var ret = new AttributeValue(null, CallingContext);

            var test = fileSizeString.ToString();
            var split = test.Split('(');
            if (split.Length > 1)
            {
                test = Regex.Replace(split[1], @"\D", "");
                if (test.Length > 0)
                {
                    ret = new AttributeValue(long.Parse(test), CallingContext);
                }
            }

            return ret;
        }

        // based on https://stackoverflow.com/questions/2510434/format-bytes-to-kilobytes-megabytes-gigabytes
        [MemberDoc("Accepts a long and formats it as a human readable bytes string. e.g. FormatBytes(96636764160, 0, true) will return \"90 GiB\"")]
        public AttributeValue FormatBytes(AttributeValue bytes, AttributeValue precision, AttributeValue useBase2)
        {
            var units = useBase2.AsBoolean ?
                new string[] { "B", "KiB", "MiB", "GiB", "TiB", "PiB" } :
                new string[] { "B", "KB", "MB", "GB", "TB", "PB" };
            var log = useBase2.AsBoolean ? 1024 : 1000;
            var val = (double)bytes.ToLong();
            var prec = (int)precision.ToLong();

            var pow = Math.Floor((val > 0 ? Math.Log(val) : 0) / Math.Log(log)); 
            pow = Math.Min(pow, units.Length - 1);
            val /= Math.Pow(log, pow);

            var ret = $"{Math.Round(val, prec)} {units[(int)pow]}";
            return new AttributeValue(ret, CallingContext);
        }

        [MemberDoc("Casts the value as double and converts to string using .NET format string. <br />See <a href=\"https://docs.microsoft.com/en-us/dotnet/standard/base-types/custom-numeric-format-strings\" target=\"_blank\">.NET Custom Numeric Format Strings</a>")]
        public AttributeValue FormatDouble(AttributeValue value, AttributeValue formatString)
        {
            try
            {
                return new AttributeValue(((double)value.AsType(AttributeType.Double)).ToString(formatString.ToString()), CallingContext);
            }
            catch (FormatException)
            {
                return new AttributeValue(null, CallingContext);
            }
        }

        [MemberDoc("Returns a numeric bytes value multiplied 1024 to the given power (\"KB\", \"MB\", \"GB\", \"TB\") e.g. To convert 25 MB to bytes: BytesMultiply(25, \"MB\")")]
        public AttributeValue BytesMultiply(AttributeValue bytesCount, AttributeValue powerString)
        {
            var val = (long)bytesCount.AsType(AttributeType.Long);
            switch (powerString.ToString())
            {
                case "KB":
                    val *= 1024;
                    break;
                case "MB":
                    val = val * 1024 * 1024;
                    break;
                case "GB":
                    val = val * 1024 * 1024 * 1024;
                    break;
                case "TB":
                    val = val * 1024 * 1024 * 1024 * 1024;
                    break;
            }

            return new AttributeValue(val, CallingContext);
        }
    }

    [ClassDoc("Perform explicit type conversion to control comparisons.", "Type Coercion Functions")]
    public class TypeCoercionFunctions : IRuleFunctions
    {
        public object CallingContext { get; set; }

        [MemberDoc("Converts values to be intrinsically String in underlying type.")]
        public AttributeValue CoerceString(AttributeValue val)
        {
            return new AttributeValue(val.ToString(), CallingContext);
        }

        [MemberDoc("Converts values to be intrinsically Guid in underlying type.")]
        public AttributeValue CoerceGuid(AttributeValue val)
        {
            return new AttributeValue(val.AsType(AttributeType.Guid), CallingContext);
        }

        [MemberDoc("Converts values to be intrinsically Bson ObjectId in underlying type.")]
        public AttributeValue CoerceObjectId(AttributeValue val)
        {
            return new AttributeValue(val.AsType(AttributeType.ObjectId), CallingContext);
        }

        [MemberDoc("Converts values to be intrinsically byte[] in underlying type.")]
        public AttributeValue CoerceBinary(AttributeValue val)
        {
            return new AttributeValue(val.AsType(AttributeType.Binary), CallingContext);
        }

        [MemberDoc("Converts values to be intrinsically int64 in underlying type.")]
        public AttributeValue CoerceLong(AttributeValue val)
        {
            return new AttributeValue(val.AsType(AttributeType.Long), CallingContext);
        }

        [MemberDoc("Converts values to be intrinsically double in underlying type.")]
        public AttributeValue CoerceDouble(AttributeValue val)
        {
            return new AttributeValue(val.AsType(AttributeType.Double), CallingContext);
        }

        [MemberDoc("Converts values to be intrinsically boolean in underlying type.")]
        public AttributeValue CoerceBool(AttributeValue val)
        {
            return new AttributeValue(val.AsBoolean, CallingContext);
        }

        [MemberDoc("Converts values to be intrinsically timespan in underlying type.")]
        public AttributeValue CoerceTimeSpan(AttributeValue val)
        {
            return new AttributeValue(val.AsType(AttributeType.TimeSpan), CallingContext);
        }

        [MemberDoc("Converts values to be intrinsically datetime in underlying type.<br/>" +
            "Optional format parameter can be applied if value being converted is a string in a known format. e.g. ddMMyyyy<br/>" +
            "Optional style parameter can be applied to parsing see: <a target=\"blank\" href=\"https://docs.microsoft.com/en-us/dotnet/api/system.globalization.datetimestyles?view=net-5.0\">DateTime Styles</a>")]
        public AttributeValue CoerceDateTime(AttributeValue val, AttributeValue format, AttributeValue style)
        {
            var ds = DateTimeStyles.None;
            var provider = CultureInfo.InvariantCulture;
            if (!string.IsNullOrEmpty(style.ToString()) && Enum.TryParse<DateTimeStyles>(style.ToString(), true, out var s))
            {
                ds = s;
            }

            if (!string.IsNullOrEmpty(format.ToString()))
            {
                if (DateTime.TryParseExact(val.ToString(), format.ToString(), provider, ds, out var result))
                {
                    return new AttributeValue(result, CallingContext);
                }
                else
                {
                    return new AttributeValue(DateTime.MinValue, CallingContext);
                }
            }

            if (!string.IsNullOrEmpty(style.ToString()))
            {
                if (DateTime.TryParse(val.ToString(), provider, ds, out var result))
                {
                    return new AttributeValue(result, CallingContext);
                }
                else
                {
                    return new AttributeValue(DateTime.MinValue, CallingContext);
                }
            }

            return new AttributeValue(val.AsType(AttributeType.DateTime), CallingContext);
        }

        [MemberDoc("Returns instance type of attribute value. If fullname is specified returns a fully qualified type name")]
        public AttributeValue TypeName(AttributeValue val, AttributeValue fullName)
        {
            if (fullName.AsBoolean)
            {
                return new AttributeValue(Helpers.TypeHelper.TypeName((val.Value ?? new object()).GetType()), CallingContext);
            }

            return new AttributeValue((val.Value ?? new object()).GetType().Name, CallingContext);
        }

        public AttributeValue NewGuid()
        {
            return new AttributeValue(Guid.NewGuid(), CallingContext);
        }

        [MemberDoc("Generate a random type guid from a hashable parameter (string, byte[] or Guid) value by using a Guid as a namespace value")]
        public AttributeValue NamedGuid(AttributeValue namespaceGuid, AttributeValue parameter)
        {
            Guid ret;
            if (parameter.DataType == AttributeType.Guid)
            {
                ret = Helpers.NamedGuid.Create(namespaceGuid.ToGuid(), parameter.ToGuid());
            }
            else if (parameter.DataType == AttributeType.Binary)
            {
                ret = Helpers.NamedGuid.Create(namespaceGuid.ToGuid(), parameter.ToBinary());
            }
            else
            {
                ret = Helpers.NamedGuid.Create(namespaceGuid.ToGuid(), parameter.ToString());
            }

            return new AttributeValue(ret, CallingContext);
        }
    }

    [ClassDoc("Perform operations and transformations on string values", "String Functions")]
    public partial class CommonStringFunctions : IRuleFunctions
    {
        public object CallingContext { get; set; }

        [MemberDoc("Returns true/false whether input matches regex pattern.")]
        public AttributeValue IsRegexMatch(AttributeValue input, AttributeValue pattern)
        {
            if (!input.AsBoolean)
            {
                return new AttributeValue(null, CallingContext);
            }

            return new AttributeValue(Regex.IsMatch(input.ToString(), pattern.ToString()), CallingContext);
        }


        [MemberDoc("Returns match text for input in pattern. <br />For full match, group should be 0. For a list of matched values set findAll to true")]
        public AttributeValue RegexMatch(AttributeValue input, AttributeValue pattern, AttributeValue group, AttributeValue findAll, AttributeValue regexOptions)
        {
            if (!input.AsBoolean)
            {
                return new AttributeValue(null, CallingContext);
            }

            string gr = null;
            int gri = -1;
            if (group != null && group.Value != null)
            {
                if (Regex.IsMatch(group.ToString(), @"^\d+$"))
                {
                    gri = (int)(long)group.AsType(AttributeType.Long);
                }
                else
                {
                    gr = group.ToString();
                }
            }

            var opts = RegexOptions.None;
            if (regexOptions.AsBoolean)
            {
                foreach (var opt in regexOptions.ToString().Split(','))
                {
                    opts |= (RegexOptions)Enum.Parse(typeof(RegexOptions), opt);
                }
            }

            if (findAll != null && findAll.AsBoolean)
            {
                var matches = Regex.Matches(input.ToString(), pattern.ToString(), opts);
                if (matches == null || matches.Count == 0)
                {
                    return new AttributeValue(null, CallingContext);
                }
                var ret = new List<string>();
                foreach (Match m in matches)
                {
                    if (gr != null)
                    {
                        ret.Add(m.Groups[gr].Value);
                    }
                    else if (gri >= 0)
                    {
                        ret.Add(m.Groups[gri].Value);
                    }
                    else
                    {
                        ret.Add(m.Value);
                    }
                }

                return new AttributeValue(ret, CallingContext);
            }

            var match = Regex.Match(input.ToString(), pattern.ToString(), opts);
            if (match == null)
            {
                return new AttributeValue(null, CallingContext);
            }

            if (gr != null)
            {
                return new AttributeValue(match.Groups[gr].Value, CallingContext);
            }
            else if (gri >= 0)
            {
                return new AttributeValue(match.Groups[(int)(long)group.AsType(AttributeType.Long)].Value, CallingContext);
            }
            else
            {
                return new AttributeValue(match.Value, CallingContext);
            }
        }
        
        [MemberDoc("Performs regex string replacement using input, pattern and replacement")]
        public AttributeValue RegexReplace(AttributeValue input, AttributeValue pattern, AttributeValue replacement, AttributeValue regexOptions)
        {
            if (input.Value != null && pattern.Value != null && replacement.Value != null)
            {
                var opts = RegexOptions.None;
                if (regexOptions.AsBoolean)
                {
                    foreach (var opt in regexOptions.ToString().Split(','))
                    {
                        opts |= (RegexOptions)Enum.Parse(typeof(RegexOptions), opt);
                    }
                }

                return new AttributeValue(Regex.Replace(input.ToString(), pattern.ToString(), replacement.ToString(), opts), CallingContext);
            }

            return new AttributeValue(null, CallingContext);
        }

        [MemberDoc("Converts an object to an indented JSON string")]
        public AttributeValue Json(AttributeValue value)
        {
            return new AttributeValue(value.AsType(AttributeType.Json), CallingContext);
        }

        [MemberDoc("Converts a JSON string to a JToken")]
        public AttributeValue ParseJson(AttributeValue value)
        {
            return new AttributeValue(JToken.Parse(value.ToString()), CallingContext);
        }

        [MemberDoc("Converts an XML string to an XElement")]
        public AttributeValue ParseXml(AttributeValue value)
        {
            return new AttributeValue(XElement.Parse(value.ToString()), CallingContext);
        }

        [MemberDoc("Accepts an Xml Element and an XPath string. Evaluates the XPath query")]
        public AttributeValue XPathEvaluate(AttributeValue element, AttributeValue xpath)
        {
            if (element == null || element.Value == null || !(element.Value is XNode xml))
            {
                return new AttributeValue(null, CallingContext);
            }

            var ret = xml.XPathEvaluate(xpath.ToString());
            if (ret is IEnumerable enumC && !(ret is string))
            {
                var l = new List<object>();
                foreach (var o in enumC)
                {
                    l.Add(o);
                }

                ret = l;
            }

            return new AttributeValue(ret, CallingContext);
        }

        [MemberDoc("Interpret a string value as a rule expression to apply to a context object.<br/>NOTE: Eval(rule, context) should only be used with trusted rule expressions.")]
        public AttributeValue Eval(AttributeValue rule, AttributeValue context)
        {
            var ruleS = rule.ToString();
            return RuleEngine.GetValue(ruleS, context.Value, CallingContext);
        }

        [MemberDoc("Escape data parameter for URI query")]
        public AttributeValue UriEscape(AttributeValue value)
        {
            if(value.Value == null) { return new AttributeValue(null, CallingContext); }
            return new AttributeValue(Uri.EscapeDataString(value.ToString()), CallingContext);
        }

        [MemberDoc("Encode text data to escape HTML entities")]
        public AttributeValue HtmlEncode(AttributeValue value)
        {
            if (value.Value == null) { return new AttributeValue(null, CallingContext); }
            return new AttributeValue(System.Net.WebUtility.HtmlEncode(value.ToString()), CallingContext);
        }

        [MemberDoc("Concatenates values[] with separator in between each")]
        public AttributeValue Join(AttributeValue separator, AttributeValue[] values)
        {
            if (values.Length == 1 && values[0].DataType == AttributeType.Other && 
                values[0].Value is IEnumerable enumC)
            {
                return new AttributeValue(string.Join(separator.ToString(), (from object v in enumC
                                                                             where v != null
                                                                             select v.ToString()).ToArray()), CallingContext);
            }

            return new AttributeValue(string.Join(separator.ToString(),
                    (from v in values
                     where !string.IsNullOrEmpty(v.ToString())
                     select v.ToString()).ToArray()), CallingContext);
        }

        [MemberDoc("Split a string on a separator. If separator is longer than 1 character or optional useRegex parameter is supplied, it will be treated as a regex split")]
        public AttributeValue Split(AttributeValue value, AttributeValue separator, AttributeValue useRegex)
        {
            if (value.Value == null || separator.Value == null) { return new AttributeValue(null, CallingContext); }
            if (useRegex.AsBoolean || separator.ToString().Length > 1)
            {
                return new AttributeValue(Regex.Split(value.ToString(), separator.ToString()).ToList(), CallingContext);
            }
            else
            {
                return new AttributeValue(value.ToString().Split(separator.ToString()[0]).ToList(), CallingContext);
            }
        }

        [MemberDoc("Returns the first entry in values[] that isn't null")]
        public AttributeValue FirstNotNull(params AttributeValue[] values)
        {
            foreach (var val in values)
            {
                if (val.Value != null)
                {
                    return val;
                }
            }

            return new AttributeValue(null, CallingContext);
        }

        [MemberDoc("Convert string into a URL slug: ASCII, all lowercase, with hyphens.")]
        public AttributeValue Slugify(AttributeValue str)
        {
            if (str == null || str.Value == null)
            {
                return new AttributeValue(null, CallingContext);
            }

            return new AttributeValue(Helpers.Url.Slugify(str.ToString()), CallingContext);
        }

        readonly static Dictionary<char, string> diaRepl = new Dictionary<char, string>
        {
            { 'ä', "ae" },
            { 'ö', "oe" },
            { 'ü', "ue" },
            { 'Ä', "Ae" },
            { 'Ö', "Oe" },
            { 'Ü', "Ue" },
            { 'ł', "l" },
            { 'ß', "ss" },
            { 'ı', "i" }
        };

        [MemberDoc("Remove diacritic marks from a string")]
        public AttributeValue RemoveDiacritics(AttributeValue str)
        {
            if (str == null || str.Value == null)
            {
                return new AttributeValue(null, CallingContext);
            }

            var normalizedString = Regex.Replace(str.ToString(), string.Join("|", diaRepl.Keys), (m) => diaRepl[m.Value[0]]).Normalize(NormalizationForm.FormD);
            var stringBuilder = new StringBuilder();

            foreach (var c in normalizedString)
            {
                var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
                if (unicodeCategory != UnicodeCategory.NonSpacingMark)
                {
                    stringBuilder.Append(c);
                }
            }

            return new AttributeValue(stringBuilder.ToString().Normalize(NormalizationForm.FormC), CallingContext);
        }

        [MemberDoc("Returns the first count characters from str. <br />If count &gt; str.Length, returns str.")]
        public AttributeValue Left(AttributeValue str, AttributeValue count)
        {
            return new AttributeValue(str.ToString().SafeSubstring(0, (int)(long)count.AsType(AttributeType.Long)), CallingContext);
        }

        [MemberDoc("Returns the last count characters from str. <br />If count &gt; str.Length, returns str.")]
        public AttributeValue Right(AttributeValue str, AttributeValue count)
        {
            if (str.ToString() == null) { return str; }
            if (str.ToString().Length <= count.ToLong())
            {
                return str;
            }

            return new AttributeValue(str.ToString().SafeSubstring(
                    str.ToString().Length - (int)count.ToLong(),
                    (int)count.ToLong()), CallingContext);
        }

        [MemberDoc("Returns characters from str, starting at index start, and returning count characters.")]
        public AttributeValue Mid(AttributeValue str, AttributeValue start, AttributeValue count)
        {
            if (count == null)
            {
                count = new AttributeValue(0, CallingContext);
            }

            return new AttributeValue(
                str.ToString().SafeSubstring(
                    (int)(long)start.AsType(AttributeType.Long),
                    (int)(long)count.AsType(AttributeType.Long)
                ), CallingContext);
        }

        [MemberDoc("Adds padCharacter to the beginning of str, until it is at least count long")]
        public AttributeValue LeftPad(AttributeValue str, AttributeValue count, AttributeValue padCharacter)
        {
            return string.IsNullOrEmpty(str.ToString()) ?
                str :
                new AttributeValue(str.ToString().PadLeft((int)(long)count.AsType(AttributeType.Long), padCharacter.ToString()[0]), CallingContext);
        }

        [MemberDoc("Adds padCharacter to the end of str, until it is at least count long")]
        public AttributeValue RightPad(AttributeValue str, AttributeValue count, AttributeValue padCharacter)
        {
            return string.IsNullOrEmpty(str.ToString()) ?
                str :
                new AttributeValue(str.ToString().PadRight((int)(long)count.AsType(AttributeType.Long), padCharacter.ToString()[0]), CallingContext);
        }

        [MemberDoc("Replaces all instances of values. Replacements is a params array alternating oldValue and newValue, allowing multiple replacements in a single operation.")]
        public AttributeValue ReplaceString(AttributeValue str, AttributeValue[] replacements)
        {
            if (string.IsNullOrEmpty(str.ToString())) { return str; }

            var strings = new List<string>();
            if (replacements.Length == 1 && replacements[0].DataType == AttributeType.Other &&
                replacements[0].Value is IEnumerable enumC)
            {
                // ToString() on an AttributeValue can return a null
                strings.AddRange(from object v in enumC
                                 select (v ?? "").ToString() ?? "");
            }
            else
            {
                strings.AddRange(from r in replacements
                                 select r.ToString() ?? "");
            }

            var ret = str.ToString();
            for (int i = 0; i < strings.Count - 1; i += 2)
            {
                ret = ret.Replace(strings[i], strings[i + 1]);
            }

            return new AttributeValue(ret, CallingContext);
        }

        char[] GetChars(AttributeValue[] chars)
        {
            if (chars.Length == 1 && !string.IsNullOrEmpty(chars[0].ToString()))
            {
                return chars[0].ToString().ToCharArray();
            }
            else
            {
                return (from c in chars
                        where !string.IsNullOrEmpty(c.ToString())
                        select c.ToString()[0]).ToArray();
            }
        }

        [MemberDoc("Trims whitespace from the beginning of str. If chars is supplied, it populates an array of characters to trim")]
        public AttributeValue LTrim(AttributeValue str, AttributeValue[] chars)
        {
            if (chars != null && chars.Length > 0)
            {
                return string.IsNullOrEmpty(str.ToString()) ?
                str : new AttributeValue(str.ToString().TrimStart(GetChars(chars)), CallingContext);
            }

            return string.IsNullOrEmpty(str.ToString()) ?
                str : new AttributeValue(str.ToString().TrimStart(), CallingContext);
        }


        [MemberDoc("Trims whitespace from the end of str")]
        public AttributeValue RTrim(AttributeValue str, AttributeValue[] chars)
        {
            if (chars != null && chars.Length > 0)
            {
                return string.IsNullOrEmpty(str.ToString()) ?
                str : new AttributeValue(str.ToString().TrimEnd(GetChars(chars)), CallingContext);
            }

            return string.IsNullOrEmpty(str.ToString()) ?
                str : new AttributeValue(str.ToString().TrimEnd(), CallingContext);
        }

        [MemberDoc("Trims whitespace from either end of str")]
        public AttributeValue Trim(AttributeValue str, AttributeValue[] chars)
        {
            if (chars != null && chars.Length > 0)
            {
                return string.IsNullOrEmpty(str.ToString()) ?
                str : new AttributeValue(str.ToString().Trim(GetChars(chars)), CallingContext);
            }

            return string.IsNullOrEmpty(str.ToString()) ?
                str : new AttributeValue(str.ToString().Trim(), CallingContext);
        }

        [MemberDoc("Converts str to title case")]
        public AttributeValue ProperCase(AttributeValue str)
        {
            if (!string.IsNullOrEmpty(str.ToString()))
            {
                var ti = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo;
                return new AttributeValue(ti.ToTitleCase(str.ToString().ToLower()), CallingContext);
            }

            return str;
        }

        [MemberDoc(@"Get variations on names. Use keyword parameters to enable the following modes:
<ul>
  <li>GetNicknames - List of common nickname variations (predominantly western names)</li>
  <li>SplitWords - Split name on word boundaries</li>
  <li>DoubleLetters - Try doubling each letter in the name (one-by-one)</li>
  <li>RemovePunctuation - Remove punctuation characters</li>
  <li>HyphenateWords - Split words then join with hyphens</li>
  <li>ReverseLettersIE - Find letters ei or ie and reverse them</li>
</ul>")]
        public AttributeValue NameVariations(AttributeValue name, Dictionary<string, AttributeValue> varySettings)
        {
            var arg = name.ToString();
            if (!string.IsNullOrEmpty(arg))
            {
                var setting = new VaryNameSettings();
                if (varySettings.TryGetValue("GetNicknames", out AttributeValue nick) && nick.AsBoolean)
                {
                    setting.GetNicknames = true;
                }
                if (varySettings.TryGetValue("SplitWords", out AttributeValue split) && split.AsBoolean)
                {
                    setting.SplitWords = true;
                }
                if (varySettings.TryGetValue("DoubleLetters", out AttributeValue dbl) && dbl.AsBoolean)
                {
                    setting.DoubleLetters = true;
                }
                if (varySettings.TryGetValue("RemovePunctuation", out AttributeValue punc) && punc.AsBoolean)
                {
                    setting.RemovePunctuation = true;
                }
                if (varySettings.TryGetValue("HyphenateWords", out AttributeValue hyp) && hyp.AsBoolean)
                {
                    setting.HyphenateWords = true;
                }
                if (varySettings.TryGetValue("ReverseLettersIE", out AttributeValue ie) && ie.AsBoolean)
                {
                    setting.ReverseLettersIE = true;
                }

                var names = Names.GetAll(arg, setting).ToList();
                names.Sort();
                return new AttributeValue(names, CallingContext);
            }

            return new AttributeValue(new List<string>(), CallingContext);
        }

        [MemberDoc("Converts str to lower case")]
        public AttributeValue LowerCase(AttributeValue str)
        {
            return string.IsNullOrEmpty(str.ToString()) ?
                str : new AttributeValue(str.ToString().ToLower(), CallingContext);
        }

        [MemberDoc("Converts str to upper case")]
        public AttributeValue UpperCase(AttributeValue str)
        {
            return string.IsNullOrEmpty(str.ToString()) ?
                str : new AttributeValue(str.ToString().ToUpper(), CallingContext);
        }

        [MemberDoc("Indicates if str ends with compare.")]
        public AttributeValue EndsWith(AttributeValue str, AttributeValue compare)
        {
            return str.ToString() == null ?
                new AttributeValue(false, CallingContext) :
                new AttributeValue(str.ToString().EndsWith(compare.ToString()), CallingContext);
        }

        [MemberDoc("Indicates if str starts with compare.")]
        public AttributeValue StartsWith(AttributeValue str, AttributeValue compare)
        {
            return str.ToString() == null ?
                new AttributeValue(false, CallingContext) :
                new AttributeValue(str.ToString().StartsWith(compare.ToString()), CallingContext);
        }

        [MemberDoc("Indicates if str contains compare.")]
        public AttributeValue Contains(AttributeValue str, AttributeValue compare)
        {
            if (str.DataType == AttributeType.Other && str.Value is IEnumerable enumC)
            {
                foreach (object v in enumC)
                {
                    if (new AttributeValue(v, CallingContext).Equals(compare))
                    {
                        return new AttributeValue(true, CallingContext);
                    }
                }

                return new AttributeValue(false, CallingContext);
            }

            if (str.Value == null || compare.Value == null)
            {
                return new AttributeValue(false, CallingContext);
            }

            return new AttributeValue(str.ToString().Contains(compare.ToString()), CallingContext);
        }

        [MemberDoc("Returns a simple null value.")]
        public AttributeValue Null()
        {
            return new AttributeValue(null, CallingContext);
        }
        
        [MemberDoc("Converts an object to a string - deprecated in favor of CoerceString")]
        public AttributeValue Stringify(AttributeValue value)
        {
            return new AttributeValue(value.AsType(AttributeType.String), CallingContext);
        }

        [MemberDoc("Generates a random string of the given length and character set. Character set can be \"base16\" or a list of characters: e.g. \"abcdefghjkmnopqrstuvwxyzABCDEFGHJKMNPQRSTUVWXZY23456789$@-_*\"")]
        public AttributeValue RandomString(AttributeValue length, AttributeValue charSet)
        {
            if (charSet.ToString() == "base16")
            {
                return new AttributeValue(RandStr((int)length.ToLong(), "0123456789ABCDEF"), CallingContext);
            }
            else
            {
                return new AttributeValue(RandStr((int)length.ToLong(), charSet.ToString()), CallingContext);
            }
        }

        static readonly Random rand = new Random();
        static string RandStr(int length, string charSet)
        {
            var ret = new StringBuilder();
            for (int i = 0; i < length; i++)
            {
                ret.Append(charSet[rand.Next(charSet.Length)]);
            }

            return ret.ToString();
        }

        [MemberDoc(@"Formats a phone number according to the standard specified in format. If the number cannot be formatted, the original value is returned<br/>
phoneNumber: The value to be formatted as a phone number. If it includes an international code, e.g. +44, this will override the countryCode parameter<br/>
format: NATIONAL: (555) 321-1212, INTERNATIONAL: +1 555 321 1212, E164: +15553211212, RFC3966: tel:+15553211212<br/>
countryCode: e.g. US or GB
")]
        public AttributeValue FormatPhone(AttributeValue phoneNumber, AttributeValue format, AttributeValue countryCode)
        {
            var ret = FormatPhoneStrict(phoneNumber, format, countryCode, new AttributeValue(false, CallingContext));
            if (!ret.AsBoolean) { return phoneNumber; }
            return ret;
        }

        [MemberDoc(@"Same behavior as FormatPhone, but returns null or throws an exception if the phone or format is invalid")]
        public AttributeValue FormatPhoneStrict(AttributeValue phoneNumber, AttributeValue format, AttributeValue countryCode, AttributeValue throwIfInvalid)
        {
            try
            {
                if (!countryCode.AsBoolean || !Regex.IsMatch(countryCode.ToString(), "^([A-Z]{2})|001"))
                {
                    throw new ArgumentException("Invalid country code format");
                }

                var phone = PhoneNumberUtil.GetInstance().Parse(phoneNumber.ToString(), countryCode.ToString());
                
                if (!PhoneNumberUtil.GetInstance().IsValidNumber(phone))
                {
                    throw new ArgumentException("Phone format is invalid");
                }

                var f = (PhoneNumberFormat)Enum.Parse(typeof(PhoneNumberFormat), format.ToString().Replace(".", ""), true);
                return new AttributeValue(PhoneNumberUtil.GetInstance().Format(phone, f), CallingContext);
            }
            catch
            {
                if (throwIfInvalid.AsBoolean)
                {
                    throw;
                }

                return new AttributeValue(null, CallingContext);
            }
        }

        [MemberDoc(@"Compute Levenshtein distance between two strings. If either string is null it's treated as empty. maxLength will truncate strings before processing")]
        public AttributeValue EditDistance(AttributeValue str1, AttributeValue str2, AttributeValue maxLength)
        {
            var s1 = str1.ToString() ?? "";
            var s2 = str2.ToString() ?? "";
            if (maxLength.Value != null && maxLength.ToLong() > 0)
            {
                var l = maxLength.ToLong();
                s1 = s1.Length > l ? s1.Substring(0, (int)l) : s1;
                s2 = s2.Length > l ? s2.Substring(0, (int)l) : s2;
            }

            return new AttributeValue(LevenshteinDistance(s1, s2), CallingContext);
        }

        static long LevenshteinDistance(string s, string t)
        {
            int n = s.Length;
            int m = t.Length;
            int[,] d = new int[n + 1, m + 1];

            // Step 1
            if (n == 0)
            {
                return m;
            }

            if (m == 0)
            {
                return n;
            }

            // Step 2
            for (int i = 0; i <= n; d[i, 0] = i++)
            {
            }

            for (int j = 0; j <= m; d[0, j] = j++)
            {
            }

            // Step 3
            for (int i = 1; i <= n; i++)
            {
                //Step 4
                for (int j = 1; j <= m; j++)
                {
                    // Step 5
                    int cost = (t[j - 1] == s[i - 1]) ? 0 : 1;

                    // Step 6
                    d[i, j] = Math.Min(
                        Math.Min(d[i - 1, j] + 1, d[i, j - 1] + 1),
                        d[i - 1, j - 1] + cost);
                }
            }
            // Step 7
            return d[n, m];
        }
    }

    [ClassDoc("List and enumerable operations", "List Functions")]
    public class CommonListFunctions : IRuleFunctions
    {
        public object CallingContext { get; set; }

        [MemberDoc("Evaluates each rule and returns a list containing the values.")]
        public AttributeValue List(params AttributeValue[] rules)
        {
            var ret = new List<object>();
            ret.AddRange(from r in rules
                         select RuleEngine.GetValue(r.Rule.ToString(), r.Context, CallingContext).Value);
            return new AttributeValue(ret, CallingContext);
        }

        [MemberDoc("Evaluates each key=value and returns a dictionary containing the evaluated values.")]
        public AttributeValue Dict(Dictionary<string, AttributeValue> data)
        {
            var ret = new Dictionary<string, object>();
            foreach(var pair in data)
            {
                ret[pair.Key] = pair.Value.Value;
            }

            return new AttributeValue(ret, CallingContext);
        }

        [MemberDoc(@"Executes the rule against each value in a collection, and returns an the Or of the values.<br/>
If parentContext is defined and not null, instead of the rule getting just the value in the collection it gets a structure in the form { Parent: parentContext, Child: collection item}")]
        public AttributeValue MapOr(AttributeValue collection, AttributeValue rule, AttributeValue parentContext)
        {
            if (collection.DataType == AttributeType.Other && collection.Value is IEnumerable enumC)
            {
                foreach (object v in enumC)
                {
                    var test = v;
                    if (parentContext.Value != null)
                    {
                        test = new ParentChildContext { Parent = parentContext.Value, Child = v };
                    }

                    if (RuleEngine.Test(rule.Rule.ToString(), test, CallingContext))
                    {
                        return new AttributeValue(true, CallingContext);
                    }
                }
            }

            return new AttributeValue(null, CallingContext);
        }

        [MemberDoc(@"Executes the rule against each value in a collection, and returns an the And of the values.<br/>
If parentContext is defined and not null, instead of the rule getting just the value in the collection it gets a structure in the form { Parent: parentContext, Child: collection item}")]
        public AttributeValue MapAnd(AttributeValue collection, AttributeValue rule, AttributeValue parentContext)
        {
            if (collection.DataType == AttributeType.Other && collection.Value is IEnumerable enumC)
            {
                foreach (object v in enumC)
                {
                    var test = v;
                    if (parentContext.Value != null)
                    {
                        test = new ParentChildContext { Parent = parentContext.Value, Child = v };
                    }

                    if (!RuleEngine.Test(rule.Rule.ToString(), test, CallingContext))
                    {
                        return new AttributeValue(false, CallingContext);
                    }
                }

                return new AttributeValue(true, CallingContext);
            }

            return new AttributeValue(null, CallingContext);
        }

        [MemberDoc(@"Executes the rule against each value in a collection, and returns the results as collection.<br/>
If parentContext is defined and not null, instead of the rule getting just the value in the collection it gets a structure in the form { Parent: parentContext, Child: collection item}")]
        public AttributeValue Map(AttributeValue collection, AttributeValue rule, AttributeValue parentContext)
        {
            if (collection.DataType == AttributeType.Other && collection.Value is IEnumerable enumC)
            {
                List<object> ret = new List<object>();
                foreach (object v in enumC)
                {
                    var test = v;
                    if (parentContext.Value != null)
                    {
                        test = new ParentChildContext { Parent = parentContext.Value, Child = v };
                    }

                    ret.Add(RuleEngine.GetValue(rule.Rule.ToString(), test, CallingContext).Value);
                }

                return new AttributeValue(ret, CallingContext);
            }

            return new AttributeValue(null, CallingContext);
        }

        class ReduceContext
        {
            public object Accumulator { get; set; }

            public object Current { get; set; }

            public long Index { get; set; }
        }

        [MemberDoc(@"Executes the rule against each value in a collection to reduce it to a single value. 
On each iteration the context object has three properties: 
Accumulator is the working total, Current is the current value, Index is the index in the list. 
If initial value is null, accumulator starts as the first item in the list, and current is the second.")]
        public AttributeValue Reduce(AttributeValue collection, AttributeValue rule, AttributeValue initial)
        {
            if (collection.DataType == AttributeType.Other && collection.Value is IEnumerable enumC)
            {
                var work = new ReduceContext { };
                long iteration = 0;
                foreach (object v in enumC)
                {
                    work.Index = iteration;
                    if (iteration == 0)
                    {
                        if (initial.Value == null)
                        {
                            work.Accumulator = v;
                            iteration++;
                            continue;
                        }
                        else
                        {
                            work.Accumulator = initial.Value;
                        }
                    }

                    work.Current = v;
                    work.Accumulator = RuleEngine.GetValue(rule.Rule.ToString(), work, CallingContext).Value;
                    iteration++;
                }

                return new AttributeValue(work.Accumulator, CallingContext);
            }

            return new AttributeValue(null, CallingContext);
        }

        [MemberDoc("Takes a lists of lists and de-nests it by one level. e.g. Flatten([\"abc\", [1, 2, 3, [1, 2]], [\"a\", \"b\"]) becomes [\"abc\", 1, 2, 3, [1, 2], \"a\", \"b\"]. If multiple lists are passed they will be merged into a single list")]
        public AttributeValue Flatten(params AttributeValue[] collections)
        {
            AttributeValue collection;
            if (collections.Length == 1 && collections[0].DataType == AttributeType.Other)
            {
                collection = collections[0];
            }
            else
            {
                collection = new AttributeValue(collections, CallingContext);
            }

            if (collection.DataType == AttributeType.Other && collection.Value is IEnumerable enumC)
            {
                List<object> ret = new List<object>();
                foreach (object v in enumC)
                {
                    if (v == null)
                    {
                        ret.Add(v);
                        continue;
                    }

                    if (new AttributeValue(v, null).DataType == AttributeType.Other && v is IEnumerable ev)
                    {
                        foreach (object vs in ev)
                        {
                            ret.Add(vs);
                        }
                    }
                    else
                    {
                        ret.Add(v);
                    }
                }

                return new AttributeValue(ret, CallingContext);
            }

            return new AttributeValue(null, CallingContext);
        }

        [MemberDoc(@"Returns a new collection with the distinct values produced by the rule.<br/>
If parentContext is defined and not null, instead of the rule getting just the value in the collection it gets a structure in the form { Parent: parentContext, Child: collection item}")]
        public AttributeValue Distinct(AttributeValue collection, AttributeValue rule, AttributeValue parentContext)
        {
            if (collection.DataType == AttributeType.Other && collection.Value is IEnumerable enumC)
            {
                var ret = new List<object>();
                var seen = new HashSet<AttributeValue>();
                foreach (object v in enumC)
                {
                    var test = v;
                    if (parentContext.Value != null)
                    {
                        test = new ParentChildContext { Parent = parentContext.Value, Child = v };
                    }

                    var dist = RuleEngine.GetValue(rule.Rule.ToString(), test, CallingContext);
                    if (seen.Add(dist))
                    {
                        ret.Add(dist.Value);
                    }
                }

                return new AttributeValue(ret, CallingContext);
            }

            return new AttributeValue(null, CallingContext);
        }

        [MemberDoc("Sorts the collection by the values produced by the rule.")]
        public AttributeValue SortBy(AttributeValue collection, AttributeValue sortRule)
        {
            if (collection.DataType == AttributeType.Other && collection.Value is IEnumerable enumC)
            {
                var ret = new List<Tuple<object, AttributeValue>>();
                foreach (object v in enumC)
                {
                    ret.Add(Tuple.Create(v, RuleEngine.GetValue(sortRule.Rule.ToString(), v, CallingContext)));
                }

                ret.Sort((a, b) => a.Item2.CompareTo(b.Item2));

                return new AttributeValue((from v in ret
                                            select v.Item1).ToList(), CallingContext);
            }

            return new AttributeValue(null, CallingContext);
        }

        class ParentChildContext
        {
            public object Parent { get; set; }
            public object Child { get; set; }
        }

        [MemberDoc(@"Executes the rule against each value in a collection, and returns a new collection with the original objects for which the rule was true.<br/>
If parentContext is defined and not null, instead of the rule getting just the value in the collection it gets a structure in the form { Parent: parentContext, Child: collection item}")]
        public AttributeValue Filter(AttributeValue collection, AttributeValue rule, AttributeValue parentContext)
        {
            if (collection.DataType == AttributeType.Other && collection.Value is IEnumerable enumC)
            {
                List<object> ret = new List<object>();
                foreach (object v in enumC)
                {
                    var test = v;
                    if (parentContext.Value != null)
                    {
                        test = new ParentChildContext { Parent = parentContext.Value, Child = v };
                    }

                    if (RuleEngine.Test(rule.Rule.ToString(), test, CallingContext))
                    {
                        ret.Add(v);
                    };
                }

                return new AttributeValue(ret, CallingContext);
            }

            return new AttributeValue(null, CallingContext);
        }

        [MemberDoc(@"Executes the rule against each value in a collection, and returns the first result for which the rule is true. If no match is found, returns null.<br/>
If parentContext is defined and not null, instead of the rule getting just the value in the collection it gets a structure in the form { Parent: parentContext, Child: collection item}")]
        public AttributeValue Find(AttributeValue collection, AttributeValue rule, AttributeValue parentContext)
        {
            if (collection.DataType == AttributeType.Other && collection.Value is IEnumerable enumC)
            {
                foreach (object v in enumC)
                {
                    var test = v;
                    if (parentContext.Value != null)
                    {
                        test = new ParentChildContext { Parent = parentContext.Value, Child = v };
                    }

                    if (RuleEngine.Test(rule.Rule.ToString(), test, CallingContext))
                    {
                        return new AttributeValue(v, CallingContext);
                    }
                }
            }

            return new AttributeValue(null, CallingContext);
        }

        [MemberDoc("Returns a range from a collection. The result may be smaller than the length paramater depending on the size of the source collection.")]
        public AttributeValue Range(AttributeValue collection, AttributeValue startIndex, AttributeValue length)
        {
            if (collection.DataType == AttributeType.Other && collection.Value is IEnumerable enumC)
            {
                long si = (long)startIndex.AsType(AttributeType.Long);
                long le = (long)length.AsType(AttributeType.Long);

                long index = 0;
                List<object> ret = new List<object>();
                foreach (object v in enumC)
                {
                    index++;
                    if (index <= si)
                    {
                        continue;
                    }

                    ret.Add(v);
                    if (le > 0 && ret.Count >= le) { break; }
                }

                return new AttributeValue(ret, CallingContext);
            }

            return new AttributeValue(null, CallingContext);
        }

        [MemberDoc("Returns the first object in list, or null if the parameter is not indexable")]
        public AttributeValue First(AttributeValue array)
        {
            switch (array.DataType)
            {
                case AttributeType.String:
                    var s = array.ToString();
                    if (!string.IsNullOrEmpty(s))
                    {
                        return new AttributeValue(s[0].ToString(), CallingContext);
                    }

                    break;
                case AttributeType.Binary:
                    var b = (byte[])array.AsType(AttributeType.Binary);
                    if (b != null && b.Length > 0)
                    {
                        return new AttributeValue(b[0], CallingContext);
                    }

                    break;
                case AttributeType.Other:
                    var obj = array.Value;
                    if (obj != null)
                    {
                        var type = obj.GetType();
                        if ((type.GetProperty("Length") != null && (int)type.GetProperty("Length").GetValue(obj) > 0) ||
                            (type.GetProperty("Count") != null && (int)type.GetProperty("Count").GetValue(obj) > 0))
                        {
                            try
                            {
                                if (type.GetProperty("Item") != null)
                                {
                                    return new AttributeValue(type.GetProperty("Item").GetValue(obj, new object[] { 0 }), CallingContext);
                                }
                            }
                            catch (AmbiguousMatchException)
                            {
                                PropertyInfo item = null;
                                var items = type.GetProperties(BindingFlags.Public | BindingFlags.Instance);
                                foreach (var it in items)
                                {
                                    if (it.Name == "Item" && it.GetIndexParameters()[0].ParameterType != typeof(object))
                                    {
                                        item = it;
                                        break;
                                    }
                                }

                                if (item != null)
                                {
                                    return new AttributeValue(item.GetValue(obj, new object[] { 0 }), CallingContext);
                                }
                            }
                        }
                    }

                    break;
            }

            return new AttributeValue(null, CallingContext);
        }

        [MemberDoc("Returns the last object in array, or null if the parameter is not indexable")]
        public AttributeValue Last(AttributeValue array)
        {
            switch (array.DataType)
            {
                case AttributeType.String:
                    var s = array.ToString();
                    if (!string.IsNullOrEmpty(s))
                    {
                        return new AttributeValue(s[s.Length - 1].ToString(), CallingContext);
                    }

                    break;
                case AttributeType.Binary:
                    var b = (byte[])array.AsType(AttributeType.Binary);
                    if (b != null && b.Length > 0)
                    {
                        return new AttributeValue(b[b.Length - 1], CallingContext);
                    }

                    break;
                case AttributeType.Other:
                    var obj = array.Value;
                    if (obj != null)
                    {
                        var type = obj.GetType();
                        int l = -1;
                        if (type.GetProperty("Length") != null)
                        {
                            l = (int)type.GetProperty("Length").GetValue(obj);
                        }
                        if (type.GetProperty("Count") != null)
                        {
                            l = (int)type.GetProperty("Count").GetValue(obj);
                        }

                        if (l > 0)
                        {
                            try
                            {
                                return new AttributeValue(type.GetProperty("Item").GetValue(obj, new object[] { l - 1 }), CallingContext);
                            }
                            catch (AmbiguousMatchException)
                            {
                                PropertyInfo item = null;
                                var items = type.GetProperties(BindingFlags.Public | BindingFlags.Instance);
                                foreach (var it in items)
                                {
                                    if (it.Name == "Item" && it.GetIndexParameters()[0].ParameterType != typeof(object))
                                    {
                                        item = it;
                                        break;
                                    }
                                }

                                if (item != null)
                                {
                                    return new AttributeValue(item.GetValue(obj, new object[] { l - 1 }), CallingContext);
                                }
                            }
                        }
                    }

                    break;
            }

            return new AttributeValue(null, CallingContext);
        }

        [MemberDoc("Applies the string value of name as a field lookup to the dictionary property. Used for looking up dynamically determined field names.")]
        public AttributeValue Key(AttributeValue dictionary, AttributeValue name)
        {
            return RuleEngine.GetValue("context." + name.ToString(), dictionary.Value, CallingContext);
        }

        [MemberDoc("Performs a set union of two lists using the default equality comparer. If one of the arguments is not a list, the first argument is returned.")]
        public AttributeValue Union(AttributeValue collection, AttributeValue otherCollection)
        {
            if (collection.DataType == AttributeType.Other && collection.Value is IEnumerable enumC &&
                otherCollection.DataType == AttributeType.Other && otherCollection.Value is IEnumerable enumO)
            {
                var wrap = from object o in enumC select o;
                var wrap2 = from object o in enumO select o;

                var ret = wrap.Union(wrap2).ToList();
                return new AttributeValue(ret, CallingContext);
            }

            return collection;
        }

        [MemberDoc("Returns the set intersect of two lists using the default equality comparer. If one of the arguments is not a list, an empty list is returned.")]
        public AttributeValue Intersect(AttributeValue collection, AttributeValue otherCollection)
        {
            if (collection.DataType == AttributeType.Other && collection.Value is IEnumerable enumC &&
                otherCollection.DataType == AttributeType.Other && otherCollection.Value is IEnumerable enumO)
            {
                var wrap = from object o in enumC select o;
                var wrap2 = from object o in enumO select o;

                var ret = wrap.Intersect(wrap2).ToList();
                return new AttributeValue(ret, CallingContext);
            }

            return new AttributeValue(new List<object>(), CallingContext);
        }

        [MemberDoc("Performs a set except of two lists using the default equality comparer. If one of the arguments is not a list, the first argument is returned.")]
        public AttributeValue Except(AttributeValue collection, AttributeValue otherCollection)
        {
            if (collection.DataType == AttributeType.Other && collection.Value is IEnumerable enumC &&
                otherCollection.DataType == AttributeType.Other && otherCollection.Value is IEnumerable enumO)
            {
                var wrap = from object o in enumC select o;
                var wrap2 = from object o in enumO select o;

                var ret = wrap.Except(wrap2).ToList();
                return new AttributeValue(ret, CallingContext);
            }

            return collection;
        }
    }

    [ClassDoc("Lookup and manipulate values and constants for Active Directory", "Active Directory Functions")]
    public class CommonADFunctions : IRuleFunctions
    {
        public object CallingContext { get; set; }


        // Active Directory
        [MemberDoc("Interprets a byte[] as an AD objectSid, and converts it to the S-... string format")]
        public AttributeValue ConvertSidToString(AttributeValue sid)
        {
            if (sid.Value == null) { return new AttributeValue(null, CallingContext); }
            return new AttributeValue(new System.Security.Principal.SecurityIdentifier((byte[])sid.AsType(AttributeType.Binary), 0).ToString(), CallingContext);
        }

        [MemberDoc("Reads a list of proxy addresses and adds new items to the list. If the adds includes a primary SMTP, the current primary will be made secondary.")]
        public AttributeValue ProxyInclude(AttributeValue currentProxyAddresses, AttributeValue[] adds)
        {
            var map = new Dictionary<string, bool>(StringComparer.OrdinalIgnoreCase);
            var primary = "";
            if (currentProxyAddresses.Value != null)
            {
                if (currentProxyAddresses.DataType == AttributeType.Other && currentProxyAddresses.Value is IEnumerable)
                {
                    foreach (var obj in (IEnumerable)currentProxyAddresses.Value)
                    {
                        if (obj == null) { continue; }
                        var val = obj.ToString();
                        if (string.IsNullOrEmpty(val)) { continue; }
                        if (val.StartsWith("SMTP:"))
                        {
                            primary = val;
                        }
                        else
                        {
                            map[val] = true;
                        }
                    }
                }
                else if (currentProxyAddresses.DataType == AttributeType.String)
                {
                    var val = currentProxyAddresses.ToString();
                    if (val.StartsWith("SMTP:"))
                    {
                        primary = val;
                    }
                    else
                    {
                        map[val] = true;
                    }
                }
            }

            List<string> vals;
            if (adds.Length == 1 && adds[0].DataType == AttributeType.Other &&
                adds[0].Value is IEnumerable enumC)
            {
                vals = (from object v in enumC
                        where v != null
                        select v.ToString()).Where(v => !string.IsNullOrEmpty(v)).ToList();
            }
            else
            {
                vals = (from v in adds
                        where v.Value != null
                        select v.ToString()).Where(v => !string.IsNullOrEmpty(v)).ToList();
            }
            
            foreach (var add in vals)
            {
                if (add.StartsWith("SMTP:"))
                {
                    if (primary.Length > 0)
                    {
                        map[primary.Replace("SMTP:", "smtp:")] = true;
                    }

                    primary = add;
                    var exist = map.Keys.FirstOrDefault(k => k.Equals(add, StringComparison.OrdinalIgnoreCase));
                    if (exist != null)
                    {
                        map.Remove(exist);
                    }
                }
                else
                {
                    map[add] = true;
                }
            }

            var ret = map.Keys.ToList();
            if (primary.Length > 0)
            {
                ret.Insert(0, primary);
            }

            return new AttributeValue(ret, CallingContext);
        }

        [MemberDoc("Interprets a string as an AD objectSid, and converts it to the byte[] format")]
        public AttributeValue ConvertStringToSid(AttributeValue sidString)
        {
            if (sidString.Value == null) { return new AttributeValue(null, CallingContext); }
            var sid = new System.Security.Principal.SecurityIdentifier(sidString.ToString());
            var ret = new byte[sid.BinaryLength];
            sid.GetBinaryForm(ret, 0);
            return new AttributeValue(ret, CallingContext);
        }

        [MemberDoc("Given a group type and scope, returns the AD numeric groupType. <br />Type should be Distribution, Security or MailEnabledSecurity. Scope should be DomainLocal, Global, or Universal.")]
        public AttributeValue ADGroupType(AttributeValue scope, AttributeValue type)
        {
            switch (type.ToString())
            {
                case "Distribution":
                    switch (scope.ToString().ToLower().Replace(" ", ""))
                    {
                        case "domainlocal":
                            return new AttributeValue(AD.DOMAINLOCAL_DIST_GRP, CallingContext);
                        case "global":
                            return new AttributeValue(AD.GLOBAL_DIST_GRP, CallingContext);
                        default: // "Universal":
                            return new AttributeValue(AD.UNIVERSAL_DIST_GRP, CallingContext);
                    }
                default: // Security, MailEnabledSecurity
                    switch (scope.ToString().ToLower())
                    {
                        case "domainlocal":
                            return new AttributeValue(AD.DOMAINLOCAL_SEC_GRP, CallingContext);
                        case "global":
                            return new AttributeValue(AD.GLOBAL_SEC_GRP, CallingContext);
                        default: // "Universal":
                            return new AttributeValue(AD.UNIVERSAL_SEC_GRP, CallingContext);
                    }
            }
        }

        [MemberDoc("Returns the container portion of an AD or AD LDS DN")]
        public AttributeValue ParseOU(AttributeValue dn)
        {
            if (dn != null && !string.IsNullOrEmpty(dn.ToString()))
            {
                return new AttributeValue(AD.GetContainer(dn.ToString()), CallingContext);
            }

            return new AttributeValue(null, CallingContext);
        }

        [MemberDoc("Returns the Domain (DC=...) portion of an AD or AD LDS DN")]
        public AttributeValue ParseDomain(AttributeValue dn)
        {
            if (dn != null && !string.IsNullOrEmpty(dn.ToString()))
            {
                return new AttributeValue(AD.GetDC(dn.ToString()), CallingContext);
            }

            return new AttributeValue(null, CallingContext);
        }

        [MemberDoc("Converts the DC= chain of a DN to DNS style name. e.g. CN=user,DC=fabrikam,DC=com becomes fabrikam.com")]
        public AttributeValue ParseDomainName(AttributeValue dn)
        {
            if (dn != null && !string.IsNullOrEmpty(dn.ToString()))
            {
                var dc = AD.GetDC(dn.ToString());
                if (dc != null)
                {
                    var split = Regex.Split(dc, @"(?<!\\),");
                    var ret = string.Join(".", (from s in split
                                                select s.Replace("DC=", "")).ToArray());
                    return new AttributeValue(ret, CallingContext);
                }

                return new AttributeValue(AD.GetDC(dn.ToString()), CallingContext);
            }

            return new AttributeValue(null, CallingContext);
        }

        [MemberDoc("Returns the CN portion of an AD or AD LDS DN, including CN=")]
        public AttributeValue ParseCN(AttributeValue dn)
        {
            if (dn != null && !string.IsNullOrEmpty(dn.ToString()))
            {
                return new AttributeValue(AD.GetCN(dn.ToString()), CallingContext);
            }

            return new AttributeValue(null, CallingContext);
        }

        [MemberDoc("Given a groupType numeric value, returns \"Global\", \"DomainLocal\" or \"Universal\".")]
        public AttributeValue GroupScope(AttributeValue groupType)
        {
            var gt = (long)groupType.AsType(AttributeType.Long);
            return new AttributeValue((gt & 2) == 2 ? "Global" : ((gt & 4) == 4 ? "DomainLocal" : "Universal"), CallingContext);
        }


        [MemberDoc("Given a groupType numeric value, returns \"Distribution\" or \"Security\".")]
        public AttributeValue GroupType(AttributeValue groupType)
        {
            var gt = (long)groupType.AsType(AttributeType.Long);
            return new AttributeValue(gt > 0 ? "Distribution" : "Security", CallingContext);
        }

        public const string ADFlagDoc =
@"Looks up the constant used for different AD userAccountControl flags and groupType values.";

        [MemberDoc(ADFlagDoc)]
        public AttributeValue ADFlag(AttributeValue flag)
        {
            var f = flag.ToString();
            if (!string.IsNullOrEmpty(f) && typeof(AD).GetField(f) != null)
            {
                return new AttributeValue(typeof(AD).GetField(f).GetValue(null), CallingContext);
            }

            return new AttributeValue(null, CallingContext);
        }

        [MemberDoc("Converts a user account control value into a comma separated list of flags")]
        public AttributeValue ADFlags(AttributeValue userAccountControl)
        {
            var ret = new List<string>();
            var uac = (long)userAccountControl.AsType(AttributeType.Long);
            foreach (var pair in AD.UacFlags)
            {
                if ((uac & pair.Value) > 0)
                {
                    ret.Add(pair.Key);
                }
            }

            return new AttributeValue(string.Join(", ", ret), CallingContext);
        }
    }

    public static class AD
    {
        // userAccountControl constants
        public const long SCRIPT = 0x0001;
        public const long ACCOUNTDISABLE = 0x0002;
        public const long HOMEDIR_REQUIRED = 0x0008;
        public const long LOCKOUT = 0x0010;
        public const long PASSWD_NOTREQD = 0x0020;
        public const long PASSWD_CANT_CHANGE = 0x0040;
        public const long ENCRYPTED_TEXT_PWD_ALLOWED = 0x0080;
        public const long TEMP_DUPLICATE_ACCOUNT = 0x0100;
        public const long NORMAL_ACCOUNT = 0x0200;
        public const long INTERDOMAIN_TRUST_ACCOUNT = 0x0800;
        public const long WORKSTATION_TRUST_ACCOUNT = 0x1000;
        public const long SERVER_TRUST_ACCOUNT = 0x2000;
        public const long DONT_EXPIRE_PASSWORD = 0x10000;
        public const long MNS_LOGON_ACCOUNT = 0x20000;
        public const long SMARTCARD_REQUIRED = 0x40000;
        public const long TRUSTED_FOR_DELEGATION = 0x80000;
        public const long NOT_DELEGATED = 0x100000;
        public const long USE_DES_KEY_ONLY = 0x200000;
        public const long DONT_REQ_PREAUTH = 0x400000;
        public const long PASSWORD_EXPIRED = 0x800000;
        public const long TRUSTED_TO_AUTH_FOR_DELEGATION = 0x1000000;
        public const long PARTIAL_SECRETS_ACCOUNT = 0x04000000;

        public static Dictionary<string, long> UacFlags = new Dictionary<string, long>
        {
            { "SCRIPT", 0x0001 },
            { "ACCOUNTDISABLE", 0x0002 },
            { "HOMEDIR_REQUIRED", 0x0008 },
            { "LOCKOUT", 0x0010 },
            { "PASSWD_NOTREQD", 0x0020 },
            { "PASSWD_CANT_CHANGE", 0x0040 },
            { "ENCRYPTED_TEXT_PWD_ALLOWED", 0x0080 },
            { "TEMP_DUPLICATE_ACCOUNT", 0x0100 },
            { "NORMAL_ACCOUNT", 0x0200 },
            { "INTERDOMAIN_TRUST_ACCOUNT", 0x0800 },
            { "WORKSTATION_TRUST_ACCOUNT", 0x1000 },
            { "SERVER_TRUST_ACCOUNT", 0x2000 },
            { "DONT_EXPIRE_PASSWORD", 0x10000 },
            { "MNS_LOGON_ACCOUNT", 0x20000 },
            { "SMARTCARD_REQUIRED", 0x40000 },
            { "TRUSTED_FOR_DELEGATION", 0x80000 },
            { "NOT_DELEGATED", 0x100000 },
            { "USE_DES_KEY_ONLY", 0x200000 },
            { "DONT_REQ_PREAUTH", 0x400000 },
            { "PASSWORD_EXPIRED", 0x800000 },
            { "TRUSTED_TO_AUTH_FOR_DELEGATION", 0x1000000 },
            { "PARTIAL_SECRETS_ACCOUNT", 0x04000000 }
        };

        // groupType constants
        public const long GLOBAL_DIST_GRP = 2;
        public const long DOMAINLOCAL_DIST_GRP = 4;
        public const long UNIVERSAL_DIST_GRP = 8;

        public const long GLOBAL_SEC_GRP = -2147483646;
        public const long DOMAINLOCAL_SEC_GRP = -2147483644;
        public const long UNIVERSAL_SEC_GRP = -2147483640;

        public static Regex DNParse = new Regex(@"^(.*?),(?=OU=|DC=|CN=)(.*)$");
        public static Regex DCParse = new Regex(@"^(.*?),(?=DC=)(.*)$");

        public static string GetCN(string dn)
        {
            if (DNParse.IsMatch(dn))
            {
                return DNParse.Match(dn).Groups[1].Value;
            }

            return null;
        }
        public static string GetDC(string dn)
        {
            if (DCParse.IsMatch(dn))
            {
                return DCParse.Match(dn).Groups[2].Value;
            }

            return null;
        }

        public static string GetContainer(string dn)
        {
            if (DNParse.IsMatch(dn))
            {
                return DNParse.Match(dn).Groups[2].Value;
            }

            return null;
        }
    }
}
