﻿using System;
using System.Linq.Expressions;
using System.Reflection;

namespace SoftwareIDM.PanelModel.Rules
{
    public static class RuleLambdas
    {
        public static Func<object, object, object> BuildGetAccessor(MethodInfo method)
        {
            var obj = Expression.Parameter(typeof(object), "o");
            var context = Expression.Parameter(typeof(object), "c");

            Expression<Func<object, object, object>> expr =
                Expression.Lambda<Func<object, object, object>>(
                    Expression.Convert(
                        Expression.Call(
                            Expression.Convert(obj, method.DeclaringType),
                            method),
                        typeof(object)),
                    obj, context);

            return expr.Compile();
        }

        public static Func<object, object, object> BuildIndexAccessor(MethodInfo method)
        {
            var obj = Expression.Parameter(typeof(object), "o");
            var index = Expression.Parameter(typeof(object), "i");

            Expression<Func<object, object, object>> expr =
                Expression.Lambda<Func<object, object, object>>(
                    Expression.Convert(
                        Expression.Call(
                            Expression.Convert(obj, method.DeclaringType),
                            method,
                            Expression.Convert(index, method.GetParameters()[0].ParameterType)),
                        typeof(object)),
                    obj, index);

            return expr.Compile();
        }

        public static Func<IRuleFunctions, AttributeValue> Build0(MethodInfo method)
        {
            var obj = Expression.Parameter(typeof(IRuleFunctions), "o");
            if (method.ReturnType == typeof(object))
            {
                Expression<Func<IRuleFunctions, object>> exprObj =
                Expression.Lambda<Func<IRuleFunctions, object>>(
                    Expression.Convert(
                        Expression.Call(
                            Expression.Convert(obj, method.DeclaringType),
                            method),
                        typeof(object)),
                    obj);
                var func = exprObj.Compile();
                return (f) =>
                {
                    var r = func(f);
                    return r is AttributeValue ? (AttributeValue)r : new AttributeValue(r, f.CallingContext);
                };
            }

            Expression<Func<IRuleFunctions, AttributeValue>> expr =
                Expression.Lambda<Func<IRuleFunctions, AttributeValue>>(
                    Expression.Convert(
                        Expression.Call(
                            Expression.Convert(obj, method.DeclaringType),
                            method),
                        typeof(AttributeValue)),
                    obj);

            return expr.Compile();
        }

        public static Func<IRuleFunctions, TLast, AttributeValue> Build1<TLast>(MethodInfo method)
        {
            var obj = Expression.Parameter(typeof(IRuleFunctions), "o");
            var mParams = method.GetParameters();
            var p0 = Expression.Parameter(mParams[0].ParameterType, "o0");
            if (method.ReturnType == typeof(object))
            {
                Expression<Func<IRuleFunctions, TLast, object>> exprObj =
                Expression.Lambda<Func<IRuleFunctions, TLast, object>>(
                    Expression.Convert(
                        Expression.Call(
                            Expression.Convert(obj, method.DeclaringType),
                            method,
                            Expression.Convert(p0, method.GetParameters()[0].ParameterType)),
                        typeof(object)),
                    obj, p0);
                var func = exprObj.Compile();
                return (f, p) =>
                {
                    var r = func(f, p);
                    return r is AttributeValue ? (AttributeValue)r : new AttributeValue(r, f.CallingContext);
                };
            }

            Expression<Func<IRuleFunctions, TLast, AttributeValue>> expr =
                Expression.Lambda<Func<IRuleFunctions, TLast, AttributeValue>>(
                    Expression.Convert(
                        Expression.Call(
                            Expression.Convert(obj, method.DeclaringType),
                            method,
                            Expression.Convert(p0, method.GetParameters()[0].ParameterType)),
                        typeof(AttributeValue)),
                    obj, p0);

            return expr.Compile();
        }

        public static Func<IRuleFunctions, AttributeValue, TLast, AttributeValue> Build2<TLast>(MethodInfo method)
        {
            var obj = Expression.Parameter(typeof(IRuleFunctions), "o");
            var mParams = method.GetParameters();
            var p0 = Expression.Parameter(mParams[0].ParameterType, "o0");
            var p1 = Expression.Parameter(mParams[1].ParameterType, "o1");
            if (method.ReturnType == typeof(object))
            {
                Expression<Func<IRuleFunctions, AttributeValue, TLast, object>> exprObj =
                Expression.Lambda<Func<IRuleFunctions, AttributeValue, TLast, object>>(
                    Expression.Convert(
                        Expression.Call(
                            Expression.Convert(obj, method.DeclaringType),
                            method,
                            Expression.Convert(p0, mParams[0].ParameterType),
                            Expression.Convert(p1, mParams[1].ParameterType)),
                        typeof(object)),
                    obj, p0, p1);
                var func = exprObj.Compile();
                return (f, p, pb) =>
                {
                    var r = func(f, p, pb);
                    return r is AttributeValue ? (AttributeValue)r : new AttributeValue(r, f.CallingContext);
                };
            }

            Expression<Func<IRuleFunctions, AttributeValue, TLast, AttributeValue>> expr =
                Expression.Lambda<Func<IRuleFunctions, AttributeValue, TLast, AttributeValue>>(
                    Expression.Convert(
                        Expression.Call(
                            Expression.Convert(obj, method.DeclaringType),
                            method,
                            Expression.Convert(p0, mParams[0].ParameterType),
                            Expression.Convert(p1, mParams[1].ParameterType)),
                        typeof(AttributeValue)),
                    obj, p0, p1);

            return expr.Compile();
        }

        public static Func<IRuleFunctions, AttributeValue, AttributeValue, TLast, AttributeValue> Build3<TLast>(MethodInfo method)
        {
            var obj = Expression.Parameter(typeof(IRuleFunctions), "o");
            var mParams = method.GetParameters();
            var p0 = Expression.Parameter(mParams[0].ParameterType, "o0");
            var p1 = Expression.Parameter(mParams[1].ParameterType, "o1");
            var p2 = Expression.Parameter(mParams[2].ParameterType, "o2");
            if (method.ReturnType == typeof(object))
            {
                Expression<Func<IRuleFunctions, AttributeValue, AttributeValue, TLast, object>> exprObj =
                Expression.Lambda<Func<IRuleFunctions, AttributeValue, AttributeValue, TLast, object>>(
                    Expression.Convert(
                        Expression.Call(
                            Expression.Convert(obj, method.DeclaringType),
                            method,
                            Expression.Convert(p0, mParams[0].ParameterType),
                            Expression.Convert(p1, mParams[1].ParameterType),
                            Expression.Convert(p2, mParams[2].ParameterType)),
                        typeof(object)),
                    obj, p0, p1, p2);
                var func = exprObj.Compile();
                return (f, p, pb, pc) =>
                {
                    var r = func(f, p, pb, pc);
                    return r is AttributeValue ? (AttributeValue)r : new AttributeValue(r, f.CallingContext);
                };
            }

            Expression<Func<IRuleFunctions, AttributeValue, AttributeValue, TLast, AttributeValue>> expr =
                Expression.Lambda<Func<IRuleFunctions, AttributeValue, AttributeValue, TLast, AttributeValue>>(
                    Expression.Convert(
                        Expression.Call(
                            Expression.Convert(obj, method.DeclaringType),
                            method,
                            Expression.Convert(p0, mParams[0].ParameterType),
                            Expression.Convert(p1, mParams[1].ParameterType),
                            Expression.Convert(p2, mParams[2].ParameterType)),
                        typeof(AttributeValue)),
                    obj, p0, p1, p2);

            return expr.Compile();
        }

        public static Func<IRuleFunctions, AttributeValue, AttributeValue, AttributeValue, TLast, AttributeValue> Build4<TLast>(MethodInfo method)
        {
            var obj = Expression.Parameter(typeof(IRuleFunctions), "o");
            var mParams = method.GetParameters();
            var p0 = Expression.Parameter(mParams[0].ParameterType, "o0");
            var p1 = Expression.Parameter(mParams[1].ParameterType, "o1");
            var p2 = Expression.Parameter(mParams[2].ParameterType, "o2");
            var p3 = Expression.Parameter(mParams[3].ParameterType, "o3");
            if (method.ReturnType == typeof(object))
            {
                Expression<Func<IRuleFunctions, AttributeValue, AttributeValue, AttributeValue, TLast, object>> exprObj =
                Expression.Lambda<Func<IRuleFunctions, AttributeValue, AttributeValue, AttributeValue, TLast, object>>(
                    Expression.Convert(
                        Expression.Call(
                            Expression.Convert(obj, method.DeclaringType),
                            method,
                            Expression.Convert(p0, mParams[0].ParameterType),
                            Expression.Convert(p1, mParams[1].ParameterType),
                            Expression.Convert(p2, mParams[2].ParameterType),
                            Expression.Convert(p3, mParams[3].ParameterType)),
                        typeof(object)),
                    obj, p0, p1, p2, p3);
                var func = exprObj.Compile();
                return (f, p, pb, pc, pd) =>
                {
                    var r = func(f, p, pb, pc, pd);
                    return r is AttributeValue ? (AttributeValue)r : new AttributeValue(r, f.CallingContext);
                };
            }

            Expression<Func<IRuleFunctions, AttributeValue, AttributeValue, AttributeValue, TLast, AttributeValue>> expr =
                Expression.Lambda<Func<IRuleFunctions, AttributeValue, AttributeValue, AttributeValue, TLast, AttributeValue>>(
                    Expression.Convert(
                        Expression.Call(
                            Expression.Convert(obj, method.DeclaringType),
                            method,
                            Expression.Convert(p0, mParams[0].ParameterType),
                            Expression.Convert(p1, mParams[1].ParameterType),
                            Expression.Convert(p2, mParams[2].ParameterType),
                            Expression.Convert(p3, mParams[3].ParameterType)),
                        typeof(AttributeValue)),
                    obj, p0, p1, p2, p3);

            return expr.Compile();
        }
    }
}
