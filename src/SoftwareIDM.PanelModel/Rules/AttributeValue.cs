﻿using System;
using System.Linq;
using MongoDB.Bson;
using Newtonsoft.Json;

namespace SoftwareIDM.PanelModel.Rules
{
    /// <summary>
    /// Wrapper class to abstract the type of values in the Rule Engine, and to support late-binding of values
    /// </summary>
    public class AttributeValue : IEquatable<AttributeValue>, IComparable<AttributeValue>
    {
        private AttributeValue() { }

        object _Value = null;
        public object Value
        {
            get
            {
                LateBind();
                return _Value;
            }
            set
            {
                var val = value;
                if (val is BsonValue)
                {
                    val = BsonTypeMapper.MapToDotNetValue((BsonValue)val);
                }

                _Value = val;
            }
        }

        public bool AsBoolean
        {
            get
            {
                return (bool)CoerceType.SafeCoerce(AttributeType.Boolean, Value);
            }
        }

        public object AsType(AttributeType type)
        {
            return CoerceType.SafeCoerce(type, Value);
        }

        public AttributeType DataType
        {
            get
            {
                LateBind();
                return CoerceType.FindType(_Value);
            }
        }

        public Rule Rule { get; set; }

        public object Context { get; set; }

        public object CallingContext { get; set; }

        public AttributeValue(Rule rule, object context, object callingContext)
        {
            Rule = rule;
            Context = context;
            CallingContext = callingContext;
        }

        public AttributeValue(object val, object callingContext)
        {
            Value = val;
            CallingContext = callingContext;
        }

        public void Bind()
        {
            LateBind();
        }

        bool bound = false;
        void LateBind()
        {
            if (!bound)
            {
                bound = true;
                if (Rule != null)
                {
                    Value = Rule.BindValue(Context, CallingContext).Value;
                    // remove references after binding value
                    //Rule = null;
                    //Context = null;
                    //CallingContext = null;
                }
            }
        }

        public bool Equals(AttributeValue other)
        {
            if (Value == null)
            {
                return other == null || other.Value == null;
            }

            if (other == null || other.Value == null)
            {
                return Value == null;
            }

            // coerce both values so that the types match. e.g. Int64 not Int 32
            var val = CoerceType.SafeCoerce(DataType, Value);
            var coerce = CoerceType.SafeCoerce(DataType, other.Value);
            if (DataType == AttributeType.Binary)
            {
                // something that couldn't be made into a binary
                if (coerce == null) { return false; }
                return Helpers.Compare.UnsafeByteEquals((byte[])val, (byte[])coerce);
            }

            return val.Equals(coerce);
        }

        public int CompareTo(AttributeValue other)
        {
            if (Value == null)
            {
                if (other.Value == null)
                    return 0;
                return int.MinValue;
            }

            if (other.Value == null)
            {
                return int.MaxValue;
            }

            switch (DataType)
            {
                case AttributeType.Binary:
                    return CompareBin((byte[])Value, (byte[])CoerceType.SafeCoerce(AttributeType.Binary, other.Value));
                case AttributeType.Other:
                    if (other.Value == Value)
                    {
                        return 0;
                    }
                    else
                    {
                        return JsonConvert.SerializeObject(Value).CompareTo(JsonConvert.SerializeObject(other.Value));
                    }
                default:
                    return ((IComparable)CoerceType.SafeCoerce(DataType, Value)).CompareTo(CoerceType.SafeCoerce(DataType, other.Value));
            }
        }

        private int CompareBin(byte[] me, byte[] other)
        {
            if (me.Length > other.Length)
            {
                var hold = new byte[me.Length];
                other.CopyTo(hold, me.Length - other.Length);
                other = hold;
            }
            else if (other.Length > me.Length)
            {
                var hold = new byte[other.Length];
                me.CopyTo(hold, other.Length - me.Length);
                me = hold;
            }

            var ret = 0;
            for (int i = 0; i < me.Length; i++)
            {
                if (me[i] != other[i])
                {
                    ret = me[i].CompareTo(other[i]);
                    break;
                }
            }

            return ret;
        }

        public override int GetHashCode()
        {
            return _Value == null ? base.GetHashCode() : _Value.GetHashCode();
        }

        public override string ToString()
        {
            return (string)CoerceType.SafeCoerce(AttributeType.String, Value);
        }
    }
}