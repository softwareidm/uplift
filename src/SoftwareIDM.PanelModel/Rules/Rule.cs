﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using SoftwareIDM.PanelModel.Attributes;

namespace SoftwareIDM.PanelModel.Rules
{

    // Interfaces for Rule Extension library
    public interface IRuleFunctions
    {
        object CallingContext { get; set; }
    }

    public interface ISpecialValues
    {
        string Name { get; }

        Dictionary<string, object> Values(object callingContext);
    }



    [ClassDoc(Name = "Rule Function")]
    public class RuleFunction
    {
        [MemberDoc(
            @"The function name should be a full function signature, with the name, parenthesis, and optional list of arguments. 
The last argument may have square braces[] to indicate that multiple parameters can be aggregated into a list. 
It may also have curly braces{} to indicate multiple named parameters may be aggregated inot a dictionary.", Name = "Function Name")]
        [UI(UIAttrKind.Text, Size = 6, Header = true, Placeholder = "FunctionName(arg1, arg2, arg3[])")]
        public string Name { get; set; }

        [MemberDoc("PowerShell implemented functions are only available in PanelService or Uplift")]
        [UI(UIAttrKind.Select, Size = 3, Description = true, Default = "Rule", Choices = "Rule|PowerShell")]
        public string Kind { get; set; }

        [MemberDoc("Help description/documentation for using  this rule function")]
        [UI(UIAttrKind.Text, Size = 12, Description = true)]
        public string Description { get; set; }

        [MemberDoc("Rule engine rule to express the body of the method. The arguments are accessible from the context of the rule.", Name = "Rule")]
        [UI(UIAttrKind.LongText, CSSClass = "input-monospace", Placeholder = "Rule, PS Script, or PS Script Path", RuleHelp = true, RuleWarnOnly = true, Description = true, RuleLang = "js:helpers.uplift.langChoice")]
        public string Rule { get; set; }

        public Func<List<Rule>, Dictionary<string, Rule>, Func<object, object, AttributeValue>> Implementation { get; set; }

        public RuleFunction() { }
    }

    public enum RuleType
    {
        Literal,     // A String literal. e.g. "Hello World."
        Integer,     // A positive or negative integer. e.g. 42, 0x200
        Double,      // A positive or negative double. e.g. 42.0
        Bool,        // Rule is literally true or false
        Null,        // Produces a null value
        Function,    // A more complicated rule, represents a function to invoke
        Parenthetical, //A parenthetical expression, typically to group items for operator precedence
        Field,       // A field lookup on an object
        SubField,    // A field lookup on the results of another rule
        Operator,    // Combines rules with an operator
        Empty        // An empty Rule does nothing.
    }

    public enum ComparisonOperator
    {
        Lt = 1,
        Gt = 2,
        Lte = 3,
        Gte = 4,
        Eq = 5,
        Neq = 6,
        And = 7,
        Or = 8,
        Plus = 9
    }

    static class Operators
    {
        static AttributeValue Plus(AttributeValue left, AttributeValue right, object callingContext)
        {
            if ((left.DataType == AttributeType.Long || left.DataType == AttributeType.Double) &&
                        right.DataType == AttributeType.Long || right.DataType == AttributeType.Double)
            {
                if (left.DataType == AttributeType.Double || right.DataType == AttributeType.Double)
                {
                    return new AttributeValue((double)left.AsType(AttributeType.Double) + (double)right.AsType(AttributeType.Double), callingContext);
                }

                return new AttributeValue((long)left.AsType(AttributeType.Long) + (long)right.AsType(AttributeType.Long), callingContext);
            }

            return new AttributeValue(left.ToString() + right.ToString(), callingContext);
        }

        static Func<AttributeValue, AttributeValue, object, AttributeValue>[] map;

        static Func<AttributeValue, AttributeValue, object, AttributeValue>[] Map
        {
            get
            {
                if (map == null)
                {
                    map = new Func<AttributeValue, AttributeValue, object, AttributeValue>[]
                    {
                        null,
                        delegate (AttributeValue left, AttributeValue right, object call) { return new AttributeValue(left.CompareTo(right) < 0, call); }, // Lt
                        delegate (AttributeValue left, AttributeValue right, object call) { return new AttributeValue(left.CompareTo(right) > 0, call); }, // Gt
                        delegate (AttributeValue left, AttributeValue right, object call) { return new AttributeValue(left.CompareTo(right) <= 0, call); }, // Lte
                        delegate (AttributeValue left, AttributeValue right, object call) { return new AttributeValue(left.CompareTo(right) >= 0, call); }, // Gte
                        delegate (AttributeValue left, AttributeValue right, object call) { return new AttributeValue(left.Equals(right), call); }, // Eq
                        delegate (AttributeValue left, AttributeValue right, object call) { return new AttributeValue(!left.Equals(right), call); }, // Neq
                        delegate (AttributeValue left, AttributeValue right, object call) { return new AttributeValue(left.AsBoolean && right.AsBoolean, call); }, // And
                        delegate (AttributeValue left, AttributeValue right, object call) { return new AttributeValue(left.AsBoolean || right.AsBoolean, call); }, // Or
                        Plus, // Plus
                    };
                }

                return map;
            }
        }

        static Dictionary<string, ComparisonOperator> ops;

        public static Dictionary<string, ComparisonOperator> Ops
        {
            get
            {
                if (ops == null)
                {
                    ops = new Dictionary<string, ComparisonOperator>
                    {
                        { "<", ComparisonOperator.Lt },
                        { ">", ComparisonOperator.Gt },
                        { "<=", ComparisonOperator.Lte },
                        { ">=", ComparisonOperator.Gte },
                        { "==", ComparisonOperator.Eq },
                        { "!=", ComparisonOperator.Neq },
                        { "&&", ComparisonOperator.And },
                        { "||", ComparisonOperator.Or },
                        { "+", ComparisonOperator.Plus }
                    };
                }

                return ops;
            }
        }

        public static Func<AttributeValue, AttributeValue, object, AttributeValue> Operate(ComparisonOperator oper)
        {
            return Map[(int)oper];
        }
    }

    public class Rule
    {
        Func<object, object, AttributeValue> _bindValue = null;
        public AttributeValue BindValue(object context, object callingContext)
        {
            return _bindValue(context, callingContext);
        }

        List<Rule> _parameters;
        Dictionary<string, Rule> _namedParameters;
        public RuleType RuleType { get; set; }
        public string Token { get; set; }

        string _fullToken;

        public ComparisonOperator ComparisonOperator { get; set; }

        readonly Regex reFunc = new Regex(@"^(\w+)\(");
        // negative lookahead in case we have val==something
        readonly Regex reNamedParam = new Regex(@"^(\w+)(?:=|\s=\s)(?!=)");
        readonly Regex reNumber = new Regex(@"^(?:0x[\dA-F]+|-?\d+(?:\.\d+)?)", RegexOptions.IgnoreCase);
        readonly Regex reField = new Regex(@"^[\w\d][\w\d\-: ]*(?:[-.][\w\d\-: ]+)*");
        readonly Regex reSubField = new Regex(@"^\.[\w\d-]+(?:[-.][\w\d\- :]+)*");
        readonly Regex reBool = new Regex(@"^(?:true|false)", RegexOptions.IgnoreCase);
        readonly Regex reLit = new Regex(@"^"".*?""", RegexOptions.Singleline);
        readonly Regex reParamLit = new Regex(@"^\$"".*?""");
        readonly Regex reNull = new Regex(@"^null", RegexOptions.IgnoreCase);
        readonly Regex reOperator = new Regex(@"^(>=|<=|>|<|==|!=|&&|\|\||\+)");
        readonly Regex reInlineComment = new Regex(@"(?<!\\)//.*?\n", RegexOptions.Singleline);
        readonly Regex reBlockComment = new Regex(@"/\*.+?\*/", RegexOptions.Singleline);

        Dictionary<string, MethodInfo> Methods { get; set; }

        private Rule() { }

        public static Rule Build(string rule)
        {
            var ret = new Rule(rule, out string _);
            return ret;
        }

        private Rule(string rule, out string left)
        {
            Methods = RuleEngine.Functions;
            rule = rule.Trim();
            _fullToken = rule;
            if (reInlineComment.IsMatch(rule))
            {
                rule = reInlineComment.Replace(rule, delegate (Match m) { return ""; }).Trim();
            }

            if (reBlockComment.IsMatch(rule))
            {
                rule = reBlockComment.Replace(rule, delegate (Match m) { return ""; }).Trim();
            }

            left = "";
            if (string.IsNullOrEmpty(rule))
            {
                RuleType = RuleType.Empty;
                return;
            }

            var prevLength = rule.Length;
            while (rule.Length > 0)
            {
                if (reFunc.IsMatch(rule))
                {
                    Token = reFunc.Match(rule).Groups[1].Value;
                    RuleType = RuleType.Function;
                    rule = rule.Substring(Token.Length + 1);
                    _parameters = new List<Rule>();
                    _namedParameters = new Dictionary<string, Rule>(StringComparer.OrdinalIgnoreCase);
                    while (!rule.StartsWith(")"))
                    {
                        if (rule.Length == 0)
                        {
                            throw new FormatException("Unable to parse rule, unmatched parenthesis.");
                        }

                        if (reNamedParam.IsMatch(rule))
                        {
                            var match = reNamedParam.Match(rule);
                            var name = match.Groups[1].Value;
                            rule = rule.Substring(match.Length);
                            var add = new Rule(rule, out rule);
                            add._fullToken = add._fullToken.Substring(0, add._fullToken.Length - rule.Length);
                            _namedParameters.Add(name, add);
                        }
                        else
                        {
                            var add = new Rule(rule, out rule);
                            add._fullToken = add._fullToken.Substring(0, add._fullToken.Length - rule.Length);
                            _parameters.Add(add);
                        }

                        if (rule.StartsWith(","))
                        {
                            rule = rule.Substring(1);
                        }

                        rule = rule.Trim();
                    }

                    rule = rule.Substring(1); // remove close paren
                    _bindValue = BindFunc();
                }
                else if (rule.StartsWith("("))
                {
                    Token = "(";
                    RuleType = RuleType.Parenthetical;
                    rule = rule.Substring(1).Trim();
                    _parameters = new List<Rule> { new Rule(rule, out rule) };
                    _bindValue = _parameters[0].BindValue;

                    if (!rule.StartsWith(")"))
                    {
                        throw new FormatException("Unable to parse rule, unmatched parenthesis.");
                    }

                    rule = rule.Substring(1); // remove close paren
                }
                else if (reNumber.IsMatch(rule))
                {
                    Token = reNumber.Match(rule).Groups[0].Value;
                    rule = rule.Substring(Token.Length);
                    if (Token.Contains('.'))
                    {
                        RuleType = RuleType.Double;
                        var val = new AttributeValue(double.Parse(Token), null);
                        _bindValue = (a, b) =>
                        {
                            val.CallingContext = b;
                            return val;
                        };
                    }
                    else
                    {
                        RuleType = RuleType.Integer;
                        AttributeValue val;
                        if (Token.StartsWith("0x"))
                        {
                            val = new AttributeValue(Convert.ToInt64(Token, 16), null);
                        }
                        else
                        {
                            val = new AttributeValue(long.Parse(Token), null);
                        }

                        _bindValue = (a, b) =>
                        {
                            val.CallingContext = b;
                            return val;
                        };
                    }
                }
                else if (reBool.IsMatch(rule))
                {
                    Token = reBool.Match(rule).Groups[0].Value;
                    RuleType = RuleType.Bool;
                    var val = new AttributeValue(bool.Parse(Token), null);
                    _bindValue = (a, b) =>
                    {
                        val.CallingContext = b;
                        return val;
                    };
                    rule = rule.Substring(Token.Length);
                }
                else if (reNull.IsMatch(rule))
                {
                    Token = "null";
                    RuleType = RuleType.Null;
                    var val = new AttributeValue(null, null);
                    _bindValue = (a, b) =>
                    {
                        val.CallingContext = b;
                        return val;
                    };
                    rule = rule.Substring(Token.Length);
                }
                else if (reField.IsMatch(rule))
                {
                    Token = reField.Match(rule).Value;
                    RuleType = RuleType.Field;
                    rule = rule.Substring(Token.Length);
                    Token = Token.Trim();
                    _bindValue = BindField();
                }
                else if (reLit.IsMatch(rule))
                {
                    var litToken = new StringBuilder();
                    int i;
                    for (i = 1; i < rule.Length; i++)
                    {
                        if (rule[i] == '"' && (rule[i - 1] != '\\' || rule[i - 2] == '\\'))
                        {
                            break;
                        }
                        else
                        {
                            litToken.Append(rule[i]);
                        }
                    }

                    Token = litToken.ToString();
                    rule = rule.Substring(i + 1);
                    foreach (var pair in _specials)
                    {
                        Token = Regex.Replace(Token, pair.Key, pair.Value);
                    }

                    var val = new AttributeValue(Token, null);
                    _bindValue = (a, b) =>
                    {
                        val.CallingContext = b;
                        return val;
                    };
                }
                else if (reParamLit.IsMatch(rule))
                {
                    RuleType = RuleType.Literal;

                    bool paramOpen = false;
                    var litToken = new StringBuilder();
                    int i;
                    var subRules = new List<StringBuilder>();
                    // start at index 2 because we skip $"
                    for (i = 2; i < rule.Length; i++)
                    {
                        if (paramOpen)
                        {
                            if (rule[i] == '}' && (rule[i - 1] != '\\' || rule[i - 2] == '\\'))
                            {
                                // close param
                                litToken.Append('{').Append((subRules.Count - 1).ToString()).Append('}');
                                paramOpen = false;
                            }
                            else
                            {
                                // append param
                                subRules[subRules.Count - 1].Append(rule[i]);
                            }
                        }
                        else
                        {
                            if (rule[i] == '{' && (rule[i - 1] != '\\' || rule[i - 2] == '\\'))
                            {
                                paramOpen = true;
                                subRules.Add(new StringBuilder());
                            }
                            else if (rule[i] == '"' && (rule[i - 1] != '\\' || rule[i - 2] == '\\'))
                            {
                                break;
                            }
                            else
                            {
                                litToken.Append(rule[i]);
                            }
                        }
                    }

                    Token = litToken.ToString();
                    rule = rule.Substring(i + 1);
                    foreach (var pair in _specials)
                    {
                        Token = Regex.Replace(Token, pair.Key, pair.Value);
                    }

                    var subs = (from s in subRules
                                select Build(s.ToString())).ToList();

                    _bindValue = (a, b) => new AttributeValue(
                        String.Format(Token, (from r in subs
                                              select r.GetValue(a, b)).ToArray()), b);
                }
                else if (reSubField.IsMatch(rule))
                {
                    Token = reSubField.Match(rule).Groups[0].Value;
                    rule = rule.Substring(Token.Length);
                    Token = "context" + Token.Trim();
                    RuleType = RuleType.SubField;
                    _bindValue = BindSubField(new Rule
                    {
                        Token = Token,
                        Methods = Methods,
                        RuleType = RuleType,
                        _parameters = _parameters,
                        _namedParameters = _namedParameters,
                        ComparisonOperator = ComparisonOperator,
                        _bindValue = _bindValue
                    });
                }
                else if (reOperator.IsMatch(rule))
                {
                    // copy current rule into first parameter
                    var add = new Rule
                    {
                        Token = Token,
                        Methods = Methods,
                        RuleType = RuleType,
                        _parameters = _parameters,
                        _namedParameters = _namedParameters,
                        ComparisonOperator = ComparisonOperator,
                        _bindValue = _bindValue
                    };

                    _parameters = new List<Rule> { add };
                    Token = "";

                    var match = reOperator.Match(rule);
                    ComparisonOperator = Operators.Ops[match.Groups[1].Value];
                    Token = match.Groups[1].Value;
                    RuleType = RuleType.Operator;
                    _parameters.Add(new Rule(rule.Substring(match.Length), out rule));

                    if (_parameters.Count == 2)
                    {
                        var getter = Operators.Operate(ComparisonOperator);
                        _bindValue = (a, b) => getter(_parameters[0].GetValue(a, b), _parameters[1].GetValue(a, b), b);
                    }
                    else
                    {
                        _bindValue = _parameters[0]._bindValue;
                    }
                }
                else
                {
                    if (rule.Trim().Length == prevLength)
                    {
                        throw new FormatException("Unable to parse remainder of rule: " + rule);
                    }

                    left = rule.Trim();
                    return;
                }

                rule = rule.Trim();
                if (rule.Length == prevLength)
                {
                    throw new FormatException("Unable to parse remainder of rule: " + rule);
                }

            } // while
        }


        readonly static Dictionary<string, string> _specials = new Dictionary<string, string> {
            { @"(?<!\\)\\r", "\r" },
            { @"(?<!\\)\\n", "\n" },
            { @"(?<!\\)\\t", "\t" },
            { @"(?<!\\)\\""", @"""" },
            { @"\\\\", @"\" }
        };

        readonly static ConcurrentDictionary<string, ConcurrentDictionary<string, Tuple<Func<object, object, object>, PropertyInfo>>> _propertyCache = new ConcurrentDictionary<string, ConcurrentDictionary<string, Tuple<Func<object, object, object>, PropertyInfo>>>();
        readonly static ConcurrentDictionary<string, ConcurrentDictionary<string, Tuple<Func<object, object, object>, FieldInfo>>> _fieldCache = new ConcurrentDictionary<string, ConcurrentDictionary<string, Tuple<Func<object, object, object>, FieldInfo>>>();
        readonly static ConcurrentDictionary<string, List<Tuple<Func<indexer, object, object, object>, PropertyInfo>>> _itemsCache = new ConcurrentDictionary<string, List<Tuple<Func<indexer, object, object, object>, PropertyInfo>>>();

        Tuple<Func<object, object, object>, PropertyInfo> PropGetter(Type type, string path)
        {
            if (!_propertyCache.TryGetValue(type.FullName, out var typeSet))
            {
                typeSet = new ConcurrentDictionary<string, Tuple<Func<object, object, object>, PropertyInfo>>(StringComparer.OrdinalIgnoreCase);
                foreach (var prop in type.GetProperties(BindingFlags.Public | BindingFlags.Instance))
                {
                    if (prop.Name == "Item") { continue; }
                    typeSet[prop.Name] = null;
                }

                _propertyCache[type.FullName] = typeSet;
            }

            if (typeSet.TryGetValue(path, out var ret))
            {
                if (ret == null)
                {
                    PropertyInfo prop;
                    try
                    {
                        prop = type.GetProperty(path, BindingFlags.Public | BindingFlags.Instance | BindingFlags.IgnoreCase);
                    }
                    catch (AmbiguousMatchException)
                    {
                        prop = type.GetProperty(path, BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly | BindingFlags.IgnoreCase);
                    }

                    ret = Tuple.Create(RuleLambdas.BuildGetAccessor(prop.GetGetMethod()), prop);
                    typeSet[path] = ret;
                }

                return ret;
            }

            return null;
        }
        Tuple<Func<object, object, object>, FieldInfo> FieldGetter(Type type, string path)
        {
            if (!_fieldCache.TryGetValue(type.FullName, out var typeSet))
            {
                typeSet = new ConcurrentDictionary<string, Tuple<Func<object, object, object>, FieldInfo>>(StringComparer.OrdinalIgnoreCase);
                foreach (var prop in type.GetFields(BindingFlags.Public | BindingFlags.Instance))
                {
                    typeSet[prop.Name] = null;
                }

                _fieldCache[type.FullName] = typeSet;
            }

            if (typeSet.TryGetValue(path, out var ret))
            {
                if (ret == null)
                {
                    FieldInfo field;
                    try
                    {
                        field = type.GetField(path, BindingFlags.Public | BindingFlags.Instance | BindingFlags.IgnoreCase);
                    }
                    catch (AmbiguousMatchException)
                    {
                        field = type.GetField(path, BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly | BindingFlags.IgnoreCase);
                    }

                    ret = Tuple.Create<Func<object, object, object>, FieldInfo>((o, c) => field.GetValue(o), field);
                    typeSet[path] = ret;
                }

                return ret;
            }

            return null;
        }

        delegate object indexer(object o, object c);

        List<Tuple<Func<indexer, object, object, object>, PropertyInfo>> ItemGetter(Type type)
        {
            if (!_itemsCache.TryGetValue(type.FullName, out var items))
            {
                var its = type.GetProperties(BindingFlags.Public | BindingFlags.Instance).Where(it => it.Name == "Item" && it.GetIndexParameters()[0].ParameterType != typeof(object)).ToList();
                if (its.Count > 0)
                {
                    var hasKeyMeth = type.GetMethod("ContainsKey");
                    Func<object, object, object> hasKey = null;
                    if (hasKeyMeth != null)
                    {
                        hasKey = RuleLambdas.BuildIndexAccessor(hasKeyMeth);
                    }

                    items = new List<Tuple<Func<indexer, object, object, object>, PropertyInfo>>();
                    foreach (var item in its)
                    {
                        var indexGet = RuleLambdas.BuildIndexAccessor(item.GetGetMethod());
                        items.Add(Tuple.Create<Func<indexer, object, object, object>, PropertyInfo>((indexer, o, c) =>
                        {
                            var indexVal = indexer(o, c);
                            try
                            {
                                if (hasKey != null)
                                {
                                    if (!(bool)hasKey(o, indexVal))
                                    {
                                        if (indexVal is string str && str.StartsWith("_"))
                                        {
                                            indexVal = ((string)indexVal).Substring(1);
                                            if ((bool)hasKey(o, indexVal))
                                            {
                                                return indexGet(o, indexVal);
                                            }
                                            else
                                            {
                                                return null;
                                            }
                                        }
                                        else
                                        {
                                            return null;
                                        }
                                    }
                                }

                                return indexGet(o, indexVal);
                            }
                            catch (IndexOutOfRangeException) { return null; }
                            catch (KeyNotFoundException)
                            {
                                if (indexVal is string str && str.StartsWith("_"))
                                {
                                    indexVal = ((string)indexVal).Substring(1);
                                    try
                                    {
                                        return indexGet(o, indexVal);
                                    }
                                    catch
                                    {
                                        return null;
                                    }
                                }
                            }
                            catch
                            {
                                return null;
                            }

                            return null;
                        }, item));

                    }

                    _itemsCache[type.FullName] = items;
                }
                else
                {
                    _itemsCache[type.FullName] = null;
                }
            }

            return items;
        }

        public override string ToString()
        {
            return _fullToken;
        }

        public AttributeValue GetValue(object context, object callingContext)
        {
            return new AttributeValue(this, context, callingContext);
        }

        Func<object, object, object> fieldGetter;

        Func<object, object, object> BuildGetter(string[] path)
        {
            object dynamicGetter(object o, object c)
            {
                if (o is AttributeValue av && av.Value != null && av.DataType == AttributeType.Other)
                {
                    return DynamicGetter(path, av.Value, c);
                }
                else
                {
                    return DynamicGetter(path, o, c);
                }
            }

            return dynamicGetter;
        }

        object DynamicGetter(string[] path, object o, object c)
        {
            bool set = false;
            for (int i = 0; i < path.Length; i++)
            {
                var p = path[i];
                if (p.ToLower() == "context") { continue; }
                if (o == null) { return null; }
                var type = o.GetType();
                var propGet = PropGetter(type, p);
                if (propGet != null)
                {
                    o = propGet.Item1(o, c);
                    set = true;
                    continue;
                }

                var fieldGet = FieldGetter(type, p);
                if (fieldGet != null)
                {
                    o = fieldGet.Item1(o, c);
                    set = true;
                    continue;
                }

                var indexes = ItemGetter(type);
                indexer ixer = null;
                if (p == "special")
                {
                    var name = path[i + 1];
                    var key = path[i + 2];
                    ixer = (o, c) => GetSpecial(name, key, c).Value;
                    i += 2;
                }

                Tuple<Func<indexer, object, object, object>, PropertyInfo> applyItem = null;
                foreach (var item in indexes)
                {
                    if (p == "special")
                    {
                        applyItem = item;
                        break;
                    }
                    else
                    {
                        // need to see if we can find an appropriate indexer
                        var indexParams = item.Item2.GetIndexParameters();
                        if (indexParams[0].ParameterType == typeof(int) && int.TryParse(p, out int outI))
                        {
                            if (outI < 0)
                            {
                                var lengthP = PropGetter(type, "Count") ?? PropGetter(type, "Length");
                                if (lengthP != null)
                                {
                                    ixer = (o, c) => (int)lengthP.Item1(o, c) + outI;
                                }
                                else
                                {
                                    ixer = (o, c) => outI;
                                }
                            }
                            else
                            {
                                ixer = (o, c) => outI;
                            }
                        }
                        else if (indexParams[0].ParameterType == typeof(byte) && byte.TryParse(p, out byte outB))
                        {
                            ixer = (o, c) => outB;
                        }
                        else if (indexParams[0].ParameterType == typeof(uint) && uint.TryParse(p, out uint outU))
                        {
                            ixer = (o, c) => outU;
                        }
                        else if (indexParams[0].ParameterType == typeof(long) && long.TryParse(p, out long outL))
                        {
                            ixer = (o, c) => outL;
                        }
                        else if (indexParams[0].ParameterType == typeof(Guid) && Guid.TryParse(p, out Guid outG))
                        {
                            ixer = (o, c) => outG;
                        }
                        else if (indexParams[0].ParameterType == typeof(DateTime) && DateTime.TryParse(p, out DateTime outD))
                        {
                            ixer = (o, c) => outD;
                        }
                        else if (indexParams[0].ParameterType == typeof(byte[]) && TryBytes(p, out byte[] outBy))
                        {
                            ixer = (o, c) => outBy;
                        }
                        else if (indexParams[0].ParameterType == typeof(string))
                        {
                            ixer = (o, c) => p;
                        }

                        if (ixer != null)
                        {
                            applyItem = item;
                            break;
                        }
                    }
                }

                if (applyItem != null)
                {
                    o = applyItem.Item1(ixer, o, c);
                    set = true;
                    continue;
                }
            }

            if (!set) { return null; }

            return o;
        }

        static bool TryBytes(object val, out byte[] ret)
        {
            ret = null;
            if (val is byte[] b)
            {
                ret = b;
                return true;
            }
            else if (val is string str)
            {
                try
                {
                    ret = Convert.FromBase64String(str);
                    return true;
                }
                catch { }
            }

            return false;
        }

        Func<object, object, AttributeValue> BindField()
        {
            var path = Token.Split('.');

            if (path.Length == 3 && path[0] == "special")
            {
                return (a, b) => GetSpecial(path[1], path[2], b);
            }

            if (path.Length == 1 && path[0].ToLower() == "context")
            {
                return (a, b) => a is AttributeValue av ? av : new AttributeValue(a, b);
            }

            return (context, callingContext) =>
            {
                if (context == null) { return new AttributeValue(null, callingContext); }
                if (fieldGetter == null)
                {
                    fieldGetter = BuildGetter(path);
                }

                object ret = fieldGetter(context, callingContext);

                return ret is AttributeValue av ? av : new AttributeValue(ret, callingContext);
            };
        }
        
        Func<object, object, AttributeValue> BindSubField(Rule parent)
        {
            var path = Token.Split('.');
            return (context, callingContext) =>
            {
                var subContext = parent.GetValue(context, callingContext).Value;
                if (subContext == null) { return new AttributeValue(null, callingContext); }
                if (fieldGetter == null)
                {
                    fieldGetter = BuildGetter(path);
                }

                var ret = fieldGetter(subContext, callingContext);
                return ret is AttributeValue av ? av : new AttributeValue(ret, callingContext);
            };
        }

        Dictionary<string, Dictionary<string, object>> specialValues = null;

        AttributeValue GetSpecial(string name, string key, object callingContext)
        {
            if (specialValues == null)
            {
                specialValues = new Dictionary<string, Dictionary<string, object>>(StringComparer.OrdinalIgnoreCase);
            }

            if (!specialValues.ContainsKey(name))
            {
                var spec = RuleEngine.SpecialValues[name];
                specialValues[name] = spec.Values(callingContext);
            }

            var lookup = specialValues[name];
            if (lookup.ContainsKey(key))
            {
                return new AttributeValue(lookup[key], callingContext);
            }

            return new AttributeValue(null, callingContext);
        }

        enum LastType
        {
            Attr,
            Array,
            Dict
        }
        
        Func<object, object, AttributeValue> BindFunc()
        {
            if (!Methods.TryGetValue(Token, out MethodInfo method))
            {
                // wrong binding context if we don't copy it into local scope
                var token = Token;
                var parameters = _parameters;
                var namedParameters = _namedParameters;
                return (context, callingContext) =>
                {
                    if (RuleEngine.RuleCache == null)
                    {
                        throw new KeyNotFoundException($"Unable to find function {token} in loaded classes, custom functions not defined");
                    }

                    var cache = RuleEngine.RuleCache(callingContext);
                    if (!cache.Functions.TryGetValue(token, out var rf))
                    {
                        throw new KeyNotFoundException($"Unable to find function {token} in loaded classes or custom functions");
                    }

                    if (rf.Implementation == null) { throw new ArgumentException("Custom Rule Function implementation not bound."); }

                    return rf.Implementation(parameters, namedParameters)(context, callingContext);
                };
            }

            var mParams = method.GetParameters();
            var args = new List<object>();

            int positionalArg = 0;
            var lastType = LastType.Attr;
            for (int i = 0; i < mParams.Length; i++)
            {
                if (i < mParams.Length - 1)
                {
                    if (_namedParameters.ContainsKey(mParams[i].Name))
                    {
                        args.Add(_namedParameters[mParams[i].Name]);
                    }
                    else if (_parameters.Count > positionalArg)
                    {
                        args.Add(_parameters[positionalArg]);
                        positionalArg++;
                    }
                    else
                    {
                        args.Add(null);
                    }
                }
                else if (mParams.Last().ParameterType.IsArray)
                {
                    var array = new List<Rule>();
                    while (positionalArg < _parameters.Count)
                    {
                        array.Add(_parameters[positionalArg]);
                        positionalArg++;
                    }

                    args.Add(array);
                    lastType = LastType.Array;
                }
                else if (mParams.Last().ParameterType.IsAssignableFrom(typeof(Dictionary<string, AttributeValue>)))
                {
                    var named = new Dictionary<string, Rule>();
                    foreach (var key in _namedParameters.Keys)
                    {
                        named[key] = _namedParameters[key];
                    }

                    args.Add(named);
                    lastType = LastType.Dict;
                }
                else
                {
                    if (_namedParameters.ContainsKey(mParams[i].Name))
                    {
                        args.Add(_namedParameters[mParams[i].Name]);
                    }
                    else if (_parameters.Count > positionalArg)
                    {
                        args.Add(_parameters[positionalArg]);
                        positionalArg++;
                    }
                    else
                    {
                        args.Add(null);
                    }
                }
            }

            Type targetType = method.DeclaringType;
            AttributeValue fallback(object a, object b)
            {
                var target = (IRuleFunctions)Activator.CreateInstance(targetType);
                target.CallingContext = b;
                var copy = new object[args.Count];
                int lasti = args.Count - 1;
                for (int i = 0; i < lasti; i++)
                {
                    copy[i] = AttrInvoke(args[i], a, b);
                }

                var last = args[lasti];
                if (last is List<Rule>)
                {
                    copy[lasti] = ArrayInvoke(args[lasti], a, b);
                }
                else if (last is Dictionary<string, AttributeValue>)
                {
                    copy[lasti] = DictInvoke(args[lasti], a, b);
                }
                else
                {
                    copy[lasti] = AttrInvoke(args[lasti], a, b);
                }

                var ret = method.IsStatic ? method.Invoke(null, copy) : method.Invoke(target, copy);
                return ret is AttributeValue av ? av : new AttributeValue(ret, b);
            }

            if (method.IsStatic) { return fallback; }

            switch (args.Count)
            {
                case 0:
                    var func0 = RuleLambdas.Build0(method);
                    return (a, b) =>
                    {
                        var target = (IRuleFunctions)Activator.CreateInstance(targetType);
                        target.CallingContext = b;
                        return func0(target);
                    };
                case 1:
                    switch (lastType)
                    {
                        case LastType.Array:
                            var func_a = RuleLambdas.Build1<AttributeValue[]>(method);
                            return (a, b) =>
                            {
                                var target = (IRuleFunctions)Activator.CreateInstance(targetType);
                                target.CallingContext = b;
                                return func_a(target, ArrayInvoke(args[0], a, b));
                            };
                        case LastType.Dict:
                            var func_d = RuleLambdas.Build1<Dictionary<string, AttributeValue>>(method);
                            return (a, b) =>
                            {
                                var target = (IRuleFunctions)Activator.CreateInstance(targetType);
                                target.CallingContext = b;
                                return func_d(target, DictInvoke(args[0], a, b));
                            };
                        default:
                            var func = RuleLambdas.Build1<AttributeValue>(method);
                            return (a, b) =>
                            {
                                var target = (IRuleFunctions)Activator.CreateInstance(targetType);
                                target.CallingContext = b;
                                return func(target, AttrInvoke(args[0], a, b));
                            };
                    }
                    
                case 2:
                    switch (lastType)
                    {
                        case LastType.Array:
                            var func_a = RuleLambdas.Build2<AttributeValue[]>(method);
                            return (a, b) =>
                            {
                                var target = (IRuleFunctions)Activator.CreateInstance(targetType);
                                target.CallingContext = b;
                                return func_a(target, AttrInvoke(args[0], a, b), ArrayInvoke(args[1], a, b));
                            };
                        case LastType.Dict:
                            var func_d = RuleLambdas.Build2<Dictionary<string, AttributeValue>>(method);
                            return (a, b) =>
                            {
                                var target = (IRuleFunctions)Activator.CreateInstance(targetType);
                                target.CallingContext = b;
                                return func_d(target, AttrInvoke(args[0], a, b), DictInvoke(args[1], a, b));
                            };
                        default:
                            var func = RuleLambdas.Build2<AttributeValue>(method);
                            return (a, b) =>
                            {
                                var target = (IRuleFunctions)Activator.CreateInstance(targetType);
                                target.CallingContext = b;
                                return func(target, AttrInvoke(args[0], a, b), AttrInvoke(args[1], a, b));
                            };
                    }
                case 3:
                    switch (lastType)
                    {
                        case LastType.Array:
                            var func_a = RuleLambdas.Build3<AttributeValue[]>(method);
                            return (a, b) =>
                            {
                                var target = (IRuleFunctions)Activator.CreateInstance(targetType);
                                target.CallingContext = b;
                                return func_a(target, AttrInvoke(args[0], a, b), AttrInvoke(args[1], a, b), ArrayInvoke(args[2], a, b));
                            };
                        case LastType.Dict:
                            var func_d = RuleLambdas.Build3<Dictionary<string, AttributeValue>>(method);
                            return (a, b) =>
                            {
                                var target = (IRuleFunctions)Activator.CreateInstance(targetType);
                                target.CallingContext = b;
                                return func_d(target, AttrInvoke(args[0], a, b), AttrInvoke(args[1], a, b), DictInvoke(args[2], a, b));
                            };
                        default:
                            var func = RuleLambdas.Build3<AttributeValue>(method);
                            return (a, b) =>
                            {
                                var target = (IRuleFunctions)Activator.CreateInstance(targetType);
                                target.CallingContext = b;
                                return func(target, AttrInvoke(args[0], a, b), AttrInvoke(args[1], a, b), AttrInvoke(args[2], a, b));
                            };
                    }
                case 4:
                    switch (lastType)
                    {
                        case LastType.Array:
                            var func_a = RuleLambdas.Build4<AttributeValue[]>(method);
                            return (a, b) =>
                            {
                                var target = (IRuleFunctions)Activator.CreateInstance(targetType);
                                target.CallingContext = b;
                                return func_a(target, AttrInvoke(args[0], a, b), AttrInvoke(args[1], a, b), AttrInvoke(args[2], a, b), ArrayInvoke(args[3], a, b));
                            };
                        case LastType.Dict:
                            var func_d = RuleLambdas.Build4<Dictionary<string, AttributeValue>>(method);
                            return (a, b) =>
                            {
                                var target = (IRuleFunctions)Activator.CreateInstance(targetType);
                                target.CallingContext = b;
                                return func_d(target, AttrInvoke(args[0], a, b), AttrInvoke(args[1], a, b), AttrInvoke(args[2], a, b), DictInvoke(args[3], a, b));
                            };
                        default:
                            var func = RuleLambdas.Build4<AttributeValue>(method);
                            return (a, b) =>
                            {
                                var target = (IRuleFunctions)Activator.CreateInstance(targetType);
                                target.CallingContext = b;
                                return func(target, AttrInvoke(args[0], a, b), AttrInvoke(args[1], a, b), AttrInvoke(args[2], a, b), AttrInvoke(args[3], a, b));
                            };
                    }
                default:
                    return fallback;
            }
        }

        static AttributeValue AttrInvoke(object arg, object context, object callingContext)
        {
            if (arg == null)
            {
                return new AttributeValue(null, context, callingContext);
            }

            return new AttributeValue((Rule)arg, context, callingContext);
        }

        static AttributeValue[] ArrayInvoke(object arg, object context, object callingContext)
        {
            return (from a in (List<Rule>)arg
                    select a.GetValue(context, callingContext)).ToArray();
        }

        static Dictionary<string, AttributeValue> DictInvoke(object arg, object context, object callingContext)
        {
            var ret = new Dictionary<string, AttributeValue>();
            foreach (var a in ((Dictionary<string, Rule>)arg))
            {
                ret[a.Key] = a.Value.GetValue(context, callingContext);
            }

            return ret;
        }
    }

}
