﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using PhoneNumbers;
using SoftwareIDM.PanelModel.Attributes;

namespace SoftwareIDM.PanelModel.Rules
{
    public partial class CommonStringFunctions : IRuleFunctions
    {
        struct CountryCode
        {
            public string Name;
            public string Alpha2;
            public string Alpha3;
            public string Numeric;
            public string ShortName;
        }

        [MemberDoc("Obtain variations on country based on ISO 2-letter, 3-letter, name, and numeric. outFormat should be XX, XXX, ###, Name, or Short Name")]
        public AttributeValue ConvertISOCountry(AttributeValue value, AttributeValue outFormat)
        {
            if (_lookup == null)
            {
                _lookup = new Dictionary<string, CountryCode>(StringComparer.OrdinalIgnoreCase);
                foreach (var code in _data)
                {
                    _lookup[code.Name] = code;
                    _lookup[code.Alpha2] = code;
                    _lookup[code.Alpha3] = code;
                    _lookup[code.Numeric] = code;
                    if (code.ShortName != null)
                    {
                        _lookup[code.ShortName] = code;
                    }
                }
            }

            if (!string.IsNullOrEmpty(value.ToString()) && _lookup.TryGetValue(value.ToString(), out var country))
            {
                switch ((outFormat.ToString() ?? "").ToLower().Replace(" ", ""))
                {
                    case "xx":
                        return new AttributeValue(country.Alpha2, CallingContext);
                    case "xxx":
                        return new AttributeValue(country.Alpha3, CallingContext);
                    case "###":
                        return new AttributeValue(country.Numeric, CallingContext);
                    case "name":
                        return new AttributeValue(country.Name, CallingContext);
                    case "shortname":
                        return new AttributeValue(country.ShortName ?? country.Name, CallingContext);
                }
            }

            return new AttributeValue(null, CallingContext);
        }

        static Dictionary<string, CountryCode> _lookup = null;

        static List<CountryCode> _data = new List<CountryCode> {
            new CountryCode {
              Name = "Afghanistan",
              Alpha2 = "AF",
              Alpha3 = "AFG",
              Numeric = "004"
            },
            new CountryCode {
              Name = "Åland Islands",
              Alpha2 = "AX",
              Alpha3 = "ALA",
              Numeric = "248",
              ShortName = "Aland Islands",
            },
            new CountryCode {
              Name = "Albania",
              Alpha2 = "AL",
              Alpha3 = "ALB",
              Numeric = "008",
            },
            new CountryCode {
              Name = "Algeria",
              Alpha2 = "DZ",
              Alpha3 = "DZA",
              Numeric = "012",
            },
            new CountryCode {
              Name = "American Samoa",
              Alpha2 = "AS",
              Alpha3 = "ASM",
              Numeric = "016",
            },
            new CountryCode {
              Name = "Andorra",
              Alpha2 = "AD",
              Alpha3 = "AND",
              Numeric = "020",
            },
            new CountryCode {
              Name = "Angola",
              Alpha2 = "AO",
              Alpha3 = "AGO",
              Numeric = "024",
            },
            new CountryCode {
              Name = "Anguilla",
              Alpha2 = "AI",
              Alpha3 = "AIA",
              Numeric = "660",
            },
            new CountryCode {
              Name = "Antarctica",
              Alpha2 = "AQ",
              Alpha3 = "ATA",
              Numeric = "010",
            },
            new CountryCode {
              Name = "Antigua and Barbuda",
              Alpha2 = "AG",
              Alpha3 = "ATG",
              Numeric = "028",
            },
            new CountryCode {
              Name = "Argentina",
              Alpha2 = "AR",
              Alpha3 = "ARG",
              Numeric = "032",
            },
            new CountryCode {
              Name = "Armenia",
              Alpha2 = "AM",
              Alpha3 = "ARM",
              Numeric = "051",
            },
            new CountryCode {
              Name = "Aruba",
              Alpha2 = "AW",
              Alpha3 = "ABW",
              Numeric = "533",
            },
            new CountryCode {
              Name = "Australia",
              Alpha2 = "AU",
              Alpha3 = "AUS",
              Numeric = "036",
            },
            new CountryCode {
              Name = "Austria",
              Alpha2 = "AT",
              Alpha3 = "AUT",
              Numeric = "040",
            },
            new CountryCode {
              Name = "Azerbaijan",
              Alpha2 = "AZ",
              Alpha3 = "AZE",
              Numeric = "031",
            },
            new CountryCode {
              Name = "Bahamas (the)",
              Alpha2 = "BS",
              Alpha3 = "BHS",
              Numeric = "044",
              ShortName = "Bahamas"
            },
            new CountryCode {
              Name = "Bahrain",
              Alpha2 = "BH",
              Alpha3 = "BHR",
              Numeric = "048",
            },
            new CountryCode {
              Name = "Bangladesh",
              Alpha2 = "BD",
              Alpha3 = "BGD",
              Numeric = "050",
            },
            new CountryCode {
              Name = "Barbados",
              Alpha2 = "BB",
              Alpha3 = "BRB",
              Numeric = "052",
            },
            new CountryCode {
              Name = "Belarus",
              Alpha2 = "BY",
              Alpha3 = "BLR",
              Numeric = "112",
            },
            new CountryCode {
              Name = "Belgium",
              Alpha2 = "BE",
              Alpha3 = "BEL",
              Numeric = "056",
            },
            new CountryCode {
              Name = "Belize",
              Alpha2 = "BZ",
              Alpha3 = "BLZ",
              Numeric = "084",
            },
            new CountryCode {
              Name = "Benin",
              Alpha2 = "BJ",
              Alpha3 = "BEN",
              Numeric = "204",
            },
            new CountryCode {
              Name = "Bermuda",
              Alpha2 = "BM",
              Alpha3 = "BMU",
              Numeric = "060",
            },
            new CountryCode {
              Name = "Bhutan",
              Alpha2 = "BT",
              Alpha3 = "BTN",
              Numeric = "064",
            },
            new CountryCode {
              Name = "Bolivia (Plurinational State of)",
              Alpha2 = "BO",
              Alpha3 = "BOL",
              Numeric = "068",
              ShortName = "Bolivia"
            },
            new CountryCode {
              Name = "Bonaire, Sint Eustatius and Saba",
              Alpha2 = "BQ",
              Alpha3 = "BES",
              Numeric = "535",
            },
            new CountryCode {
              Name = "Bosnia and Herzegovina",
              Alpha2 = "BA",
              Alpha3 = "BIH",
              Numeric = "070",
            },
            new CountryCode {
              Name = "Botswana",
              Alpha2 = "BW",
              Alpha3 = "BWA",
              Numeric = "072",
            },
            new CountryCode {
              Name = "Bouvet Island",
              Alpha2 = "BV",
              Alpha3 = "BVT",
              Numeric = "074",
            },
            new CountryCode {
              Name = "Brazil",
              Alpha2 = "BR",
              Alpha3 = "BRA",
              Numeric = "076",
            },
            new CountryCode {
              Name = "British Indian Ocean Territory (the)",
              Alpha2 = "IO",
              Alpha3 = "IOT",
              Numeric = "086",
              ShortName = "British Indian Ocean Territory"
            },
            new CountryCode {
              Name = "Brunei Darussalam",
              Alpha2 = "BN",
              Alpha3 = "BRN",
              Numeric = "096",
              ShortName = "Brunei"
            },
            new CountryCode {
              Name = "Bulgaria",
              Alpha2 = "BG",
              Alpha3 = "BGR",
              Numeric = "100",
            },
            new CountryCode {
              Name = "Burkina Faso",
              Alpha2 = "BF",
              Alpha3 = "BFA",
              Numeric = "854",
            },
            new CountryCode {
              Name = "Burundi",
              Alpha2 = "BI",
              Alpha3 = "BDI",
              Numeric = "108",
            },
            new CountryCode {
              Name = "Cabo Verde",
              Alpha2 = "CV",
              Alpha3 = "CPV",
              Numeric = "132",
              ShortName = "Cape Verde"
            },
            new CountryCode {
              Name = "Cambodia",
              Alpha2 = "KH",
              Alpha3 = "KHM",
              Numeric = "116",
            },
            new CountryCode {
              Name = "Cameroon",
              Alpha2 = "CM",
              Alpha3 = "CMR",
              Numeric = "120",
            },
            new CountryCode {
              Name = "Canada",
              Alpha2 = "CA",
              Alpha3 = "CAN",
              Numeric = "124",
            },
            new CountryCode {
              Name = "Cayman Islands (the)",
              Alpha2 = "KY",
              Alpha3 = "CYM",
              Numeric = "136",
              ShortName = "Cayman Islands"
            },
            new CountryCode {
              Name = "Central African Republic (the)",
              Alpha2 = "CF",
              Alpha3 = "CAF",
              Numeric = "140",
              ShortName = "Central African Republic"
            },
            new CountryCode {
              Name = "Chad",
              Alpha2 = "TD",
              Alpha3 = "TCD",
              Numeric = "148",
            },
            new CountryCode {
              Name = "Chile",
              Alpha2 = "CL",
              Alpha3 = "CHL",
              Numeric = "152",
            },
            new CountryCode {
              Name = "China",
              Alpha2 = "CN",
              Alpha3 = "CHN",
              Numeric = "156",
            },
            new CountryCode {
              Name = "Christmas Island",
              Alpha2 = "CX",
              Alpha3 = "CXR",
              Numeric = "162",
            },
            new CountryCode {
              Name = "Cocos (Keeling) Islands (the)",
              Alpha2 = "CC",
              Alpha3 = "CCK",
              Numeric = "166",
              ShortName = "Cocos Islands"
            },
            new CountryCode {
              Name = "Colombia",
              Alpha2 = "CO",
              Alpha3 = "COL",
              Numeric = "170",
            },
            new CountryCode {
              Name = "Comoros (the)",
              Alpha2 = "KM",
              Alpha3 = "COM",
              Numeric = "174",
              ShortName = "Comoros"
            },
            new CountryCode {
              Name = "Congo (the Democratic Republic of the)",
              Alpha2 = "CD",
              Alpha3 = "COD",
              Numeric = "180",
              ShortName = "Democratic Republic of the Congo"
            },
            new CountryCode {
              Name = "Congo (the)",
              Alpha2 = "CG",
              Alpha3 = "COG",
              Numeric = "178",
              ShortName = "Republic of the Congo"
            },
            new CountryCode {
              Name = "Cook Islands (the)",
              Alpha2 = "CK",
              Alpha3 = "COK",
              Numeric = "184",
              ShortName = "Cook Islands"
            },
            new CountryCode {
              Name = "Costa Rica",
              Alpha2 = "CR",
              Alpha3 = "CRI",
              Numeric = "188",
            },
            new CountryCode {
              Name = "Côte d'Ivoire",
              Alpha2 = "CI",
              Alpha3 = "CIV",
              Numeric = "384",
              ShortName = "Ivory Coast"
            },
            new CountryCode {
              Name = "Croatia",
              Alpha2 = "HR",
              Alpha3 = "HRV",
              Numeric = "191",
            },
            new CountryCode {
              Name = "Cuba",
              Alpha2 = "CU",
              Alpha3 = "CUB",
              Numeric = "192",
            },
            new CountryCode {
              Name = "Curaçao",
              Alpha2 = "CW",
              Alpha3 = "CUW",
              Numeric = "531",
              ShortName = "Curacao"
            },
            new CountryCode {
              Name = "Cyprus",
              Alpha2 = "CY",
              Alpha3 = "CYP",
              Numeric = "196",
            },
            new CountryCode {
              Name = "Czechia",
              Alpha2 = "CZ",
              Alpha3 = "CZE",
              Numeric = "203",
              ShortName = "Czech Republic"
            },
            new CountryCode {
              Name = "Denmark",
              Alpha2 = "DK",
              Alpha3 = "DNK",
              Numeric = "208",
            },
            new CountryCode {
              Name = "Djibouti",
              Alpha2 = "DJ",
              Alpha3 = "DJI",
              Numeric = "262",
            },
            new CountryCode {
              Name = "Dominica",
              Alpha2 = "DM",
              Alpha3 = "DMA",
              Numeric = "212",
            },
            new CountryCode {
              Name = "Dominican Republic (the)",
              Alpha2 = "DO",
              Alpha3 = "DOM",
              Numeric = "214",
              ShortName = "Dominican Republic"
            },
            new CountryCode {
              Name = "Ecuador",
              Alpha2 = "EC",
              Alpha3 = "ECU",
              Numeric = "218",
            },
            new CountryCode {
              Name = "Egypt",
              Alpha2 = "EG",
              Alpha3 = "EGY",
              Numeric = "818",
            },
            new CountryCode {
              Name = "El Salvador",
              Alpha2 = "SV",
              Alpha3 = "SLV",
              Numeric = "222",
            },
            new CountryCode {
              Name = "Equatorial Guinea",
              Alpha2 = "GQ",
              Alpha3 = "GNQ",
              Numeric = "226",
            },
            new CountryCode {
              Name = "Eritrea",
              Alpha2 = "ER",
              Alpha3 = "ERI",
              Numeric = "232",
            },
            new CountryCode {
              Name = "Estonia",
              Alpha2 = "EE",
              Alpha3 = "EST",
              Numeric = "233",
            },
            new CountryCode {
              Name = "Ethiopia",
              Alpha2 = "ET",
              Alpha3 = "ETH",
              Numeric = "231",
            },
            new CountryCode {
              Name = "Falkland Islands (the) [Malvinas]",
              Alpha2 = "FK",
              Alpha3 = "FLK",
              Numeric = "238",
              ShortName = "Falkland Islands"
            },
            new CountryCode {
              Name = "Faroe Islands (the)",
              Alpha2 = "FO",
              Alpha3 = "FRO",
              Numeric = "234",
              ShortName = "Faroe Islands"
            },
            new CountryCode {
              Name = "Fiji",
              Alpha2 = "FJ",
              Alpha3 = "FJI",
              Numeric = "242",
            },
            new CountryCode {
              Name = "Finland",
              Alpha2 = "FI",
              Alpha3 = "FIN",
              Numeric = "246",
            },
            new CountryCode {
              Name = "France",
              Alpha2 = "FR",
              Alpha3 = "FRA",
              Numeric = "250",
            },
            new CountryCode {
              Name = "French Guiana",
              Alpha2 = "GF",
              Alpha3 = "GUF",
              Numeric = "254",
            },
            new CountryCode {
              Name = "French Polynesia",
              Alpha2 = "PF",
              Alpha3 = "PYF",
              Numeric = "258",
            },
            new CountryCode {
              Name = "French Southern Territories (the)",
              Alpha2 = "TF",
              Alpha3 = "ATF",
              Numeric = "260",
              ShortName = "French Southern Territories"
            },
            new CountryCode {
              Name = "Gabon",
              Alpha2 = "GA",
              Alpha3 = "GAB",
              Numeric = "266",
            },
            new CountryCode {
              Name = "Gambia (the)",
              Alpha2 = "GM",
              Alpha3 = "GMB",
              Numeric = "270",
              ShortName = "Gambia"
            },
            new CountryCode {
              Name = "Georgia",
              Alpha2 = "GE",
              Alpha3 = "GEO",
              Numeric = "268",
            },
            new CountryCode {
              Name = "Germany",
              Alpha2 = "DE",
              Alpha3 = "DEU",
              Numeric = "276",
            },
            new CountryCode {
              Name = "Ghana",
              Alpha2 = "GH",
              Alpha3 = "GHA",
              Numeric = "288",
            },
            new CountryCode {
              Name = "Gibraltar",
              Alpha2 = "GI",
              Alpha3 = "GIB",
              Numeric = "292",
            },
            new CountryCode {
              Name = "Greece",
              Alpha2 = "GR",
              Alpha3 = "GRC",
              Numeric = "300",
            },
            new CountryCode {
              Name = "Greenland",
              Alpha2 = "GL",
              Alpha3 = "GRL",
              Numeric = "304",
            },
            new CountryCode {
              Name = "Grenada",
              Alpha2 = "GD",
              Alpha3 = "GRD",
              Numeric = "308",
            },
            new CountryCode {
              Name = "Guadeloupe",
              Alpha2 = "GP",
              Alpha3 = "GLP",
              Numeric = "312",
            },
            new CountryCode {
              Name = "Guam",
              Alpha2 = "GU",
              Alpha3 = "GUM",
              Numeric = "316",
            },
            new CountryCode {
              Name = "Guatemala",
              Alpha2 = "GT",
              Alpha3 = "GTM",
              Numeric = "320",
            },
            new CountryCode {
              Name = "Guernsey",
              Alpha2 = "GG",
              Alpha3 = "GGY",
              Numeric = "831",
            },
            new CountryCode {
              Name = "Guinea",
              Alpha2 = "GN",
              Alpha3 = "GIN",
              Numeric = "324",
            },
            new CountryCode {
              Name = "Guinea-Bissau",
              Alpha2 = "GW",
              Alpha3 = "GNB",
              Numeric = "624",
            },
            new CountryCode {
              Name = "Guyana",
              Alpha2 = "GY",
              Alpha3 = "GUY",
              Numeric = "328",
            },
            new CountryCode {
              Name = "Haiti",
              Alpha2 = "HT",
              Alpha3 = "HTI",
              Numeric = "332",
            },
            new CountryCode {
              Name = "Heard Island and McDonald Islands",
              Alpha2 = "HM",
              Alpha3 = "HMD",
              Numeric = "334",
              ShortName = "Heard and Mcdonald Islands"
            },
            new CountryCode {
              Name = "Holy See (the)",
              Alpha2 = "VA",
              Alpha3 = "VAT",
              Numeric = "336",
              ShortName = "Vatican"
            },
            new CountryCode {
              Name = "Honduras",
              Alpha2 = "HN",
              Alpha3 = "HND",
              Numeric = "340",
            },
            new CountryCode {
              Name = "Hong Kong",
              Alpha2 = "HK",
              Alpha3 = "HKG",
              Numeric = "344",
            },
            new CountryCode {
              Name = "Hungary",
              Alpha2 = "HU",
              Alpha3 = "HUN",
              Numeric = "348",
            },
            new CountryCode {
              Name = "Iceland",
              Alpha2 = "IS",
              Alpha3 = "ISL",
              Numeric = "352",
            },
            new CountryCode {
              Name = "India",
              Alpha2 = "IN",
              Alpha3 = "IND",
              Numeric = "356",
            },
            new CountryCode {
              Name = "Indonesia",
              Alpha2 = "ID",
              Alpha3 = "IDN",
              Numeric = "360",
            },
            new CountryCode {
              Name = "Iran (Islamic Republic of)",
              Alpha2 = "IR",
              Alpha3 = "IRN",
              Numeric = "364",
              ShortName = "Iran"
            },
            new CountryCode {
              Name = "Iraq",
              Alpha2 = "IQ",
              Alpha3 = "IRQ",
              Numeric = "368",
            },
            new CountryCode {
              Name = "Ireland",
              Alpha2 = "IE",
              Alpha3 = "IRL",
              Numeric = "372",
            },
            new CountryCode {
              Name = "Isle of Man",
              Alpha2 = "IM",
              Alpha3 = "IMN",
              Numeric = "833",
            },
            new CountryCode {
              Name = "Israel",
              Alpha2 = "IL",
              Alpha3 = "ISR",
              Numeric = "376",
            },
            new CountryCode {
              Name = "Italy",
              Alpha2 = "IT",
              Alpha3 = "ITA",
              Numeric = "380",
            },
            new CountryCode {
              Name = "Jamaica",
              Alpha2 = "JM",
              Alpha3 = "JAM",
              Numeric = "388",
            },
            new CountryCode {
              Name = "Japan",
              Alpha2 = "JP",
              Alpha3 = "JPN",
              Numeric = "392",
            },
            new CountryCode {
              Name = "Jersey",
              Alpha2 = "JE",
              Alpha3 = "JEY",
              Numeric = "832",
            },
            new CountryCode {
              Name = "Jordan",
              Alpha2 = "JO",
              Alpha3 = "JOR",
              Numeric = "400",
            },
            new CountryCode {
              Name = "Kazakhstan",
              Alpha2 = "KZ",
              Alpha3 = "KAZ",
              Numeric = "398",
            },
            new CountryCode {
              Name = "Kenya",
              Alpha2 = "KE",
              Alpha3 = "KEN",
              Numeric = "404",
            },
            new CountryCode {
              Name = "Kiribati",
              Alpha2 = "KI",
              Alpha3 = "KIR",
              Numeric = "296",
            },
            new CountryCode {
              Name = "Korea (the Democratic People's Republic of)",
              Alpha2 = "KP",
              Alpha3 = "PRK",
              Numeric = "408",
              ShortName = "North Korea"
            },
            new CountryCode {
              Name = "Korea (the Republic of)",
              Alpha2 = "KR",
              Alpha3 = "KOR",
              Numeric = "410",
              ShortName = "South Korea"
            },
            new CountryCode {
              Name = "Kuwait",
              Alpha2 = "KW",
              Alpha3 = "KWT",
              Numeric = "414",
            },
            new CountryCode {
              Name = "Kyrgyzstan",
              Alpha2 = "KG",
              Alpha3 = "KGZ",
              Numeric = "417",
            },
            new CountryCode {
              Name = "Lao People's Democratic Republic (the)",
              Alpha2 = "LA",
              Alpha3 = "LAO",
              Numeric = "418",
              ShortName = "Laos"
            },
            new CountryCode {
              Name = "Latvia",
              Alpha2 = "LV",
              Alpha3 = "LVA",
              Numeric = "428",
            },
            new CountryCode {
              Name = "Lebanon",
              Alpha2 = "LB",
              Alpha3 = "LBN",
              Numeric = "422",
            },
            new CountryCode {
              Name = "Lesotho",
              Alpha2 = "LS",
              Alpha3 = "LSO",
              Numeric = "426",
            },
            new CountryCode {
              Name = "Liberia",
              Alpha2 = "LR",
              Alpha3 = "LBR",
              Numeric = "430",
            },
            new CountryCode {
              Name = "Libya",
              Alpha2 = "LY",
              Alpha3 = "LBY",
              Numeric = "434",
            },
            new CountryCode {
              Name = "Liechtenstein",
              Alpha2 = "LI",
              Alpha3 = "LIE",
              Numeric = "438",
            },
            new CountryCode {
              Name = "Lithuania",
              Alpha2 = "LT",
              Alpha3 = "LTU",
              Numeric = "440",
            },
            new CountryCode {
              Name = "Luxembourg",
              Alpha2 = "LU",
              Alpha3 = "LUX",
              Numeric = "442",
            },
            new CountryCode {
              Name = "Macao",
              Alpha2 = "MO",
              Alpha3 = "MAC",
              Numeric = "446",
            },
            new CountryCode {
              Name = "Macedonia (the former Yugoslav Republic of)",
              Alpha2 = "MK",
              Alpha3 = "MKD",
              Numeric = "807",
              ShortName = "Macedonia"
            },
            new CountryCode {
              Name = "Madagascar",
              Alpha2 = "MG",
              Alpha3 = "MDG",
              Numeric = "450",
            },
            new CountryCode {
              Name = "Malawi",
              Alpha2 = "MW",
              Alpha3 = "MWI",
              Numeric = "454",
            },
            new CountryCode {
              Name = "Malaysia",
              Alpha2 = "MY",
              Alpha3 = "MYS",
              Numeric = "458",
            },
            new CountryCode {
              Name = "Maldives",
              Alpha2 = "MV",
              Alpha3 = "MDV",
              Numeric = "462",
            },
            new CountryCode {
              Name = "Mali",
              Alpha2 = "ML",
              Alpha3 = "MLI",
              Numeric = "466",
            },
            new CountryCode {
              Name = "Malta",
              Alpha2 = "MT",
              Alpha3 = "MLT",
              Numeric = "470",
            },
            new CountryCode {
              Name = "Marshall Islands (the)",
              Alpha2 = "MH",
              Alpha3 = "MHL",
              Numeric = "584",
              ShortName = "Marshall Islands"
            },
            new CountryCode {
              Name = "Martinique",
              Alpha2 = "MQ",
              Alpha3 = "MTQ",
              Numeric = "474",
            },
            new CountryCode {
              Name = "Mauritania",
              Alpha2 = "MR",
              Alpha3 = "MRT",
              Numeric = "478",
            },
            new CountryCode {
              Name = "Mauritius",
              Alpha2 = "MU",
              Alpha3 = "MUS",
              Numeric = "480",
            },
            new CountryCode {
              Name = "Mayotte",
              Alpha2 = "YT",
              Alpha3 = "MYT",
              Numeric = "175",
            },
            new CountryCode {
              Name = "Mexico",
              Alpha2 = "MX",
              Alpha3 = "MEX",
              Numeric = "484",
            },
            new CountryCode {
              Name = "Micronesia (Federated States of)",
              Alpha2 = "FM",
              Alpha3 = "FSM",
              Numeric = "583",
              ShortName = "Micronesia"
            },
            new CountryCode {
              Name = "Moldova (the Republic of)",
              Alpha2 = "MD",
              Alpha3 = "MDA",
              Numeric = "498",
              ShortName = "Moldova"
            },
            new CountryCode {
              Name = "Monaco",
              Alpha2 = "MC",
              Alpha3 = "MCO",
              Numeric = "492",
            },
            new CountryCode {
              Name = "Mongolia",
              Alpha2 = "MN",
              Alpha3 = "MNG",
              Numeric = "496",
            },
            new CountryCode {
              Name = "Montenegro",
              Alpha2 = "ME",
              Alpha3 = "MNE",
              Numeric = "499",
            },
            new CountryCode {
              Name = "Montserrat",
              Alpha2 = "MS",
              Alpha3 = "MSR",
              Numeric = "500",
            },
            new CountryCode {
              Name = "Morocco",
              Alpha2 = "MA",
              Alpha3 = "MAR",
              Numeric = "504",
            },
            new CountryCode {
              Name = "Mozambique",
              Alpha2 = "MZ",
              Alpha3 = "MOZ",
              Numeric = "508",
            },
            new CountryCode {
              Name = "Myanmar",
              Alpha2 = "MM",
              Alpha3 = "MMR",
              Numeric = "104",
            },
            new CountryCode {
              Name = "Namibia",
              Alpha2 = "NA",
              Alpha3 = "NAM",
              Numeric = "516",
            },
            new CountryCode {
              Name = "Nauru",
              Alpha2 = "NR",
              Alpha3 = "NRU",
              Numeric = "520",
            },
            new CountryCode {
              Name = "Nepal",
              Alpha2 = "NP",
              Alpha3 = "NPL",
              Numeric = "524",
            },
            new CountryCode {
              Name = "Netherlands (the)",
              Alpha2 = "NL",
              Alpha3 = "NLD",
              Numeric = "528",
              ShortName = "Netherlands"
            },
            new CountryCode {
              Name = "New Caledonia",
              Alpha2 = "NC",
              Alpha3 = "NCL",
              Numeric = "540",
            },
            new CountryCode {
              Name = "New Zealand",
              Alpha2 = "NZ",
              Alpha3 = "NZL",
              Numeric = "554",
            },
            new CountryCode {
              Name = "Nicaragua",
              Alpha2 = "NI",
              Alpha3 = "NIC",
              Numeric = "558",
            },
            new CountryCode {
              Name = "Niger (the)",
              Alpha2 = "NE",
              Alpha3 = "NER",
              Numeric = "562",
              ShortName = "Niger"
            },
            new CountryCode {
              Name = "Nigeria",
              Alpha2 = "NG",
              Alpha3 = "NGA",
              Numeric = "566",
            },
            new CountryCode {
              Name = "Niue",
              Alpha2 = "NU",
              Alpha3 = "NIU",
              Numeric = "570",
            },
            new CountryCode {
              Name = "Norfolk Island",
              Alpha2 = "NF",
              Alpha3 = "NFK",
              Numeric = "574",
            },
            new CountryCode {
              Name = "Northern Mariana Islands (the)",
              Alpha2 = "MP",
              Alpha3 = "MNP",
              Numeric = "580",
              ShortName = "Northern Mariana Islands"
            },
            new CountryCode {
              Name = "Norway",
              Alpha2 = "NO",
              Alpha3 = "NOR",
              Numeric = "578",
            },
            new CountryCode {
              Name = "Oman",
              Alpha2 = "OM",
              Alpha3 = "OMN",
              Numeric = "512",
            },
            new CountryCode {
              Name = "Pakistan",
              Alpha2 = "PK",
              Alpha3 = "PAK",
              Numeric = "586",
            },
            new CountryCode {
              Name = "Palau",
              Alpha2 = "PW",
              Alpha3 = "PLW",
              Numeric = "585",
            },
            new CountryCode {
              Name = "Palestine, State of",
              Alpha2 = "PS",
              Alpha3 = "PSE",
              Numeric = "275",
              ShortName = "Palestine"
            },
            new CountryCode {
              Name = "Panama",
              Alpha2 = "PA",
              Alpha3 = "PAN",
              Numeric = "591",
            },
            new CountryCode {
              Name = "Papua New Guinea",
              Alpha2 = "PG",
              Alpha3 = "PNG",
              Numeric = "598",
            },
            new CountryCode {
              Name = "Paraguay",
              Alpha2 = "PY",
              Alpha3 = "PRY",
              Numeric = "600",
            },
            new CountryCode {
              Name = "Peru",
              Alpha2 = "PE",
              Alpha3 = "PER",
              Numeric = "604",
            },
            new CountryCode {
              Name = "Philippines (the)",
              Alpha2 = "PH",
              Alpha3 = "PHL",
              Numeric = "608",
              ShortName = "Philippines"
            },
            new CountryCode {
              Name = "Pitcairn",
              Alpha2 = "PN",
              Alpha3 = "PCN",
              Numeric = "612",
            },
            new CountryCode {
              Name = "Poland",
              Alpha2 = "PL",
              Alpha3 = "POL",
              Numeric = "616",
            },
            new CountryCode {
              Name = "Portugal",
              Alpha2 = "PT",
              Alpha3 = "PRT",
              Numeric = "620",
            },
            new CountryCode {
              Name = "Puerto Rico",
              Alpha2 = "PR",
              Alpha3 = "PRI",
              Numeric = "630",
            },
            new CountryCode {
              Name = "Qatar",
              Alpha2 = "QA",
              Alpha3 = "QAT",
              Numeric = "634",
            },
            new CountryCode {
              Name = "Réunion",
              Alpha2 = "RE",
              Alpha3 = "REU",
              Numeric = "638",
            },
            new CountryCode {
              Name = "Romania",
              Alpha2 = "RO",
              Alpha3 = "ROU",
              Numeric = "642",
            },
            new CountryCode {
              Name = "Russian Federation (the)",
              Alpha2 = "RU",
              Alpha3 = "RUS",
              Numeric = "643",
              ShortName = "Russia"
            },
            new CountryCode {
              Name = "Rwanda",
              Alpha2 = "RW",
              Alpha3 = "RWA",
              Numeric = "646",
            },
            new CountryCode {
              Name = "Saint Barthélemy",
              Alpha2 = "BL",
              Alpha3 = "BLM",
              Numeric = "652",
              ShortName = "Saint Barthelemy"
            },
            new CountryCode {
              Name = "Saint Helena, Ascension and Tristan da Cunha",
              Alpha2 = "SH",
              Alpha3 = "SHN",
              Numeric = "654",
              ShortName = "Saint Helena"
            },
            new CountryCode {
              Name = "Saint Kitts and Nevis",
              Alpha2 = "KN",
              Alpha3 = "KNA",
              Numeric = "659",
            },
            new CountryCode {
              Name = "Saint Lucia",
              Alpha2 = "LC",
              Alpha3 = "LCA",
              Numeric = "662",
            },
            new CountryCode {
              Name = "Saint Martin (French part)",
              Alpha2 = "MF",
              Alpha3 = "MAF",
              Numeric = "663",
              ShortName = "Saint Martin"
            },
            new CountryCode {
              Name = "Saint Pierre and Miquelon",
              Alpha2 = "PM",
              Alpha3 = "SPM",
              Numeric = "666",
            },
            new CountryCode {
              Name = "Saint Vincent and the Grenadines",
              Alpha2 = "VC",
              Alpha3 = "VCT",
              Numeric = "670",
              ShortName = "Saint Vincent and Grenadines"
            },
            new CountryCode {
              Name = "Samoa",
              Alpha2 = "WS",
              Alpha3 = "WSM",
              Numeric = "882",
            },
            new CountryCode {
              Name = "San Marino",
              Alpha2 = "SM",
              Alpha3 = "SMR",
              Numeric = "674",
            },
            new CountryCode {
              Name = "Sao Tome and Principe",
              Alpha2 = "ST",
              Alpha3 = "STP",
              Numeric = "678",
            },
            new CountryCode {
              Name = "Saudi Arabia",
              Alpha2 = "SA",
              Alpha3 = "SAU",
              Numeric = "682",
            },
            new CountryCode {
              Name = "Senegal",
              Alpha2 = "SN",
              Alpha3 = "SEN",
              Numeric = "686",
            },
            new CountryCode {
              Name = "Serbia",
              Alpha2 = "RS",
              Alpha3 = "SRB",
              Numeric = "688",
            },
            new CountryCode {
              Name = "Seychelles",
              Alpha2 = "SC",
              Alpha3 = "SYC",
              Numeric = "690",
            },
            new CountryCode {
              Name = "Sierra Leone",
              Alpha2 = "SL",
              Alpha3 = "SLE",
              Numeric = "694",
            },
            new CountryCode {
              Name = "Singapore",
              Alpha2 = "SG",
              Alpha3 = "SGP",
              Numeric = "702",
            },
            new CountryCode {
              Name = "Sint Maarten (Dutch part)",
              Alpha2 = "SX",
              Alpha3 = "SXM",
              Numeric = "534",
              ShortName = "Sint Maarten"
            },
            new CountryCode {
              Name = "Slovakia",
              Alpha2 = "SK",
              Alpha3 = "SVK",
              Numeric = "703",
            },
            new CountryCode {
              Name = "Slovenia",
              Alpha2 = "SI",
              Alpha3 = "SVN",
              Numeric = "705",
            },
            new CountryCode {
              Name = "Solomon Islands",
              Alpha2 = "SB",
              Alpha3 = "SLB",
              Numeric = "090",
            },
            new CountryCode {
              Name = "Somalia",
              Alpha2 = "SO",
              Alpha3 = "SOM",
              Numeric = "706",
            },
            new CountryCode {
              Name = "South Africa",
              Alpha2 = "ZA",
              Alpha3 = "ZAF",
              Numeric = "710",
            },
            new CountryCode {
              Name = "South Georgia and the South Sandwich Islands",
              Alpha2 = "GS",
              Alpha3 = "SGS",
              Numeric = "239",
            },
            new CountryCode {
              Name = "South Sudan",
              Alpha2 = "SS",
              Alpha3 = "SSD",
              Numeric = "728",
            },
            new CountryCode {
              Name = "Spain",
              Alpha2 = "ES",
              Alpha3 = "ESP",
              Numeric = "724",
            },
            new CountryCode {
              Name = "Sri Lanka",
              Alpha2 = "LK",
              Alpha3 = "LKA",
              Numeric = "144",
            },
            new CountryCode {
              Name = "Sudan (the)",
              Alpha2 = "SD",
              Alpha3 = "SDN",
              Numeric = "729",
              ShortName = "Sudan"
            },
            new CountryCode {
              Name = "Suriname",
              Alpha2 = "SR",
              Alpha3 = "SUR",
              Numeric = "740",
            },
            new CountryCode {
              Name = "Svalbard and Jan Mayen",
              Alpha2 = "SJ",
              Alpha3 = "SJM",
              Numeric = "744",
            },
            new CountryCode {
              Name = "Swaziland",
              Alpha2 = "SZ",
              Alpha3 = "SWZ",
              Numeric = "748",
            },
            new CountryCode {
              Name = "Sweden",
              Alpha2 = "SE",
              Alpha3 = "SWE",
              Numeric = "752",
            },
            new CountryCode {
              Name = "Switzerland",
              Alpha2 = "CH",
              Alpha3 = "CHE",
              Numeric = "756",
            },
            new CountryCode {
              Name = "Syrian Arab Republic",
              Alpha2 = "SY",
              Alpha3 = "SYR",
              Numeric = "760",
              ShortName = "Syria"
            },
            new CountryCode {
              Name = "Taiwan (Province of China)",
              Alpha2 = "TW",
              Alpha3 = "TWN",
              Numeric = "158",
              ShortName = "Taiwan"
            },
            new CountryCode {
              Name = "Tajikistan",
              Alpha2 = "TJ",
              Alpha3 = "TJK",
              Numeric = "762",
            },
            new CountryCode {
              Name = "Tanzania, United Republic of",
              Alpha2 = "TZ",
              Alpha3 = "TZA",
              Numeric = "834",
              ShortName = "Tanzania"
            },
            new CountryCode {
              Name = "Thailand",
              Alpha2 = "TH",
              Alpha3 = "THA",
              Numeric = "764",
            },
            new CountryCode {
              Name = "Timor-Leste",
              Alpha2 = "TL",
              Alpha3 = "TLS",
              Numeric = "626",
              ShortName = "East Timor"
            },
            new CountryCode {
              Name = "Togo",
              Alpha2 = "TG",
              Alpha3 = "TGO",
              Numeric = "768",
            },
            new CountryCode {
              Name = "Tokelau",
              Alpha2 = "TK",
              Alpha3 = "TKL",
              Numeric = "772",
            },
            new CountryCode {
              Name = "Tonga",
              Alpha2 = "TO",
              Alpha3 = "TON",
              Numeric = "776",
            },
            new CountryCode {
              Name = "Trinidad and Tobago",
              Alpha2 = "TT",
              Alpha3 = "TTO",
              Numeric = "780",
            },
            new CountryCode {
              Name = "Tunisia",
              Alpha2 = "TN",
              Alpha3 = "TUN",
              Numeric = "788",
            },
            new CountryCode {
              Name = "Turkey",
              Alpha2 = "TR",
              Alpha3 = "TUR",
              Numeric = "792",
            },
            new CountryCode {
              Name = "Turkmenistan",
              Alpha2 = "TM",
              Alpha3 = "TKM",
              Numeric = "795",
            },
            new CountryCode {
              Name = "Turks and Caicos Islands (the)",
              Alpha2 = "TC",
              Alpha3 = "TCA",
              Numeric = "796",
              ShortName = "Turks and Caicos Islands"
            },
            new CountryCode {
              Name = "Tuvalu",
              Alpha2 = "TV",
              Alpha3 = "TUV",
              Numeric = "798",
            },
            new CountryCode {
              Name = "Uganda",
              Alpha2 = "UG",
              Alpha3 = "UGA",
              Numeric = "800",
            },
            new CountryCode {
              Name = "Ukraine",
              Alpha2 = "UA",
              Alpha3 = "UKR",
              Numeric = "804",
            },
            new CountryCode {
              Name = "United Arab Emirates (the)",
              Alpha2 = "AE",
              Alpha3 = "ARE",
              Numeric = "784",
              ShortName = "United Arab Emirates"
            },
            new CountryCode {
              Name = "United Kingdom of Great Britain and Northern Ireland (the)",
              Alpha2 = "GB",
              Alpha3 = "GBR",
              Numeric = "826",
              ShortName = "United Kingdom"
            },
            new CountryCode {
              Name = "United States Minor Outlying Islands (the)",
              Alpha2 = "UM",
              Alpha3 = "UMI",
              Numeric = "581",
              ShortName = "US Minor Outlying Islands"
            },
            new CountryCode {
              Name = "United States of America (the)",
              Alpha2 = "US",
              Alpha3 = "USA",
              Numeric = "840",
              ShortName = "United States"
            },
            new CountryCode {
              Name = "Uruguay",
              Alpha2 = "UY",
              Alpha3 = "URY",
              Numeric = "858",
            },
            new CountryCode {
              Name = "Uzbekistan",
              Alpha2 = "UZ",
              Alpha3 = "UZB",
              Numeric = "860",
            },
            new CountryCode {
              Name = "Vanuatu",
              Alpha2 = "VU",
              Alpha3 = "VUT",
              Numeric = "548",
            },
            new CountryCode {
              Name = "Venezuela (Bolivarian Republic of)",
              Alpha2 = "VE",
              Alpha3 = "VEN",
              Numeric = "862",
              ShortName = "Venezuela"
            },
            new CountryCode {
              Name = "Viet Nam",
              Alpha2 = "VN",
              Alpha3 = "VNM",
              Numeric = "704",
              ShortName = "Vietnam"
            },
            new CountryCode {
              Name = "Virgin Islands (British)",
              Alpha2 = "VG",
              Alpha3 = "VGB",
              Numeric = "092",
              ShortName = "British Virgin Islands"
            },
            new CountryCode {
              Name = "Virgin Islands (U.S.)",
              Alpha2 = "VI",
              Alpha3 = "VIR",
              Numeric = "850",
              ShortName = "U.S. Virgin Islands"
            },
            new CountryCode {
              Name = "Wallis and Futuna",
              Alpha2 = "WF",
              Alpha3 = "WLF",
              Numeric = "876",
            },
            new CountryCode {
              Name = "Western Sahara*",
              Alpha2 = "EH",
              Alpha3 = "ESH",
              Numeric = "732",
            },
            new CountryCode {
              Name = "Yemen",
              Alpha2 = "YE",
              Alpha3 = "YEM",
              Numeric = "887",
            },
            new CountryCode {
              Name = "Zambia",
              Alpha2 = "ZM",
              Alpha3 = "ZMB",
              Numeric = "894",
            },
            new CountryCode {
              Name = "Zimbabwe",
              Alpha2 = "ZW",
              Alpha3 = "ZWE",
              Numeric = "716",
            }
        };
    }
}
