﻿using System;
using System.Collections.Generic;
using System.ServiceProcess;
using SoftwareIDM.PanelModel.Attributes;
using SoftwareIDM.PanelModel.Models;

namespace SoftwareIDM.PanelModel.Rules
{
    /// <summary>
    /// Lookup numeric values of Modification type Flags Enum
    /// </summary>
    [ClassDoc("Special values for single-value attribute names")]
    public class ModTypeSpecial : ISpecialValues
    {
        public string Name
        {
            get { return "Object Modification Type"; }
        }
        
        public Dictionary<string, object> Values(object callingContext)
        {
            return new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase)
            {
                { "None", (long)(int)ModType.None },
                { "Add",  (long)(int)ModType.Add },
                { "Add Disconnect",  (long)(int)ModType.AddDisconnect },
                { "Clear Pending",  (long)(int)ModType.ClearPendingStatus },
                { "Connect",  (long)(int)ModType.Connect },
                { "Data Retention",  (int)ModType.DataRetention },
                { "Delete",  (long)(int)ModType.Delete },
                { "Disconnect",  (long)(int)ModType.Disconnect },
                { "Error",  (long)(int)ModType.Error },
                { "Move",  (long)(int)ModType.Move },
                { "Set Pending",  (long)(int)ModType.PendingStatus },
                { "Rename",  (long)(int)ModType.Rename },
                { "Update",  (long)(int)ModType.Update }
            };
        }
    }

    [ClassDoc("Special values for service status enumeration")]
    public class ServiceStatusSpecial : ISpecialValues
    {
        public string Name
        {
            get { return "Service Status"; }
        }
        
        public Dictionary<string, object> Values(object callingContext)
        {
            return new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase)
            {
                { "Continue Pending", (long)(int)ServiceControllerStatus.ContinuePending },
                { "Paused", (long)(int)ServiceControllerStatus.Paused },
                { "Pause Pending", (long)(int)ServiceControllerStatus.PausePending },
                { "Running", (long)(int)ServiceControllerStatus.Running },
                { "Start Pending", (long)(int)ServiceControllerStatus.StartPending },
                { "Stopped", (long)(int)ServiceControllerStatus.Stopped },
                { "StopPending", (long)(int)ServiceControllerStatus.StopPending }
            };
        }
    }

    [ClassDoc("Special values for step run status enumeration")]
    public class StepStatusSpecial : ISpecialValues
    {
        public string Name
        {
            get { return "Schedule Step Status"; }
        }
        
        public Dictionary<string, object> Values(object callingContext)
        {
            return new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase)
            {
                { "Failed", (long)(int)RunStatus.Failed },
                { "Failed Start", (long)(int)RunStatus.FailedStart },
                { "Finished", (long)(int)RunStatus.Finished },
                { "Not Started", (int)RunStatus.NotStarted },
                { "Running", (long)(int)RunStatus.Running },
                { "Skipped", (long)(int)RunStatus.Skipped },
                { "Cancelled", (long)(int)RunStatus.Cancelled },
                { "Timed Out", (long)(int)RunStatus.TimedOut }
            };
        }
    }
}
