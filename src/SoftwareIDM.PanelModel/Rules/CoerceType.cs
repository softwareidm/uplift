﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using MongoDB.Bson;

namespace SoftwareIDM.PanelModel.Rules
{
    public enum AttributeType
    {
        Null,
        String,
        Long,
        Double,
        Binary,
        Boolean,
        Guid,
        DateTime,
        TimeSpan,
        Enum,
        Other,
        Json,
        ObjectId
    }

    delegate object Converter(object value);

    static class FromNull
    {
        static object ToBinary(object val) { return null; }

        static object ToBoolean(object val) { return false; }

        static object ToDateTime(object val) { return DateTime.MinValue; }

        static object ToDouble(object val) { return 0D; }

        static object ToGuid(object val) { return Guid.Empty; }

        static object ToObjectId(object val) { return ObjectId.Empty; }

        static object ToJson(object val) { return null; }

        static object ToLong(object val) { return 0L; }

        static object ToNull(object val) { return null; }

        static object ToOther(object val) { return null; }

        static object ToString(object val) { return null; }

        static object ToTimeSpan(object val) { return new TimeSpan(0); }

        public static void Init(Action<AttributeType, Converter> fromTo)
        {
            fromTo(AttributeType.Binary, ToBinary);
            fromTo(AttributeType.Boolean, ToBoolean);
            fromTo(AttributeType.ObjectId, ToObjectId);
            fromTo(AttributeType.DateTime, ToDateTime);
            fromTo(AttributeType.Double, ToDouble);
            fromTo(AttributeType.Guid, ToGuid);
            fromTo(AttributeType.Json, ToJson);
            fromTo(AttributeType.Long, ToLong);
            fromTo(AttributeType.Null, ToNull);
            fromTo(AttributeType.Other, ToOther);
            fromTo(AttributeType.String, ToString);
            fromTo(AttributeType.TimeSpan, ToTimeSpan);
        }
    }

    static class FromBinary
    {
        static object ToBinary(object val) { return val; }

        static object ToBoolean(object val) { return ((byte[])val).Length > 0; }

        static object ToDateTime(object val)
        {
            if (((byte[])val).Length == 8)
                return DateTime.FromBinary(BitConverter.ToInt64((byte[])val, 0));
            return DateTime.MinValue;
        }

        static object ToDouble(object val)
        {
            if (((byte[])val).Length == 8)
                return BitConverter.ToDouble((byte[])val, 0);
            return 0.0D;
        }

        static object ToLong(object val)
        {
            var v = (byte[])val;
            if (v.Length >= 8)
                return BitConverter.ToInt64(v, 0);
            if (v.Length >= 4)
                return (long)BitConverter.ToInt32(v, 0);
            if (v.Length >= 2)
                return (long)BitConverter.ToInt16(v, 0);
            if (v.Length == 1)
                return (long)v[0];
            return 0L;
        }

        static object ToString(object val) { return Convert.ToBase64String((byte[])val); }

        static object ToGuid(object val) { return new Guid((byte[])val); }

        static object ToObjectId(object val) { return new ObjectId((byte[])val); }

        static object ToJson(object val) { return JsonConvert.SerializeObject(val); }

        public static void Init(Action<AttributeType, Converter> fromTo)
        {
            fromTo(AttributeType.Binary, ToBinary);
            fromTo(AttributeType.Boolean, ToBoolean);
            fromTo(AttributeType.DateTime, ToDateTime);
            fromTo(AttributeType.Double, ToDouble);
            fromTo(AttributeType.Long, ToLong);
            fromTo(AttributeType.String, ToString);
            fromTo(AttributeType.Guid, ToObjectId);
            fromTo(AttributeType.ObjectId, ToGuid);
            fromTo(AttributeType.Json, ToJson);
        }

    }

    static class FromBoolean
    {
        static object ToBinary(object val) { return (bool)val ? new byte[] { 1 } : Array.Empty<byte>(); }
        static object ToBoolean(object val) { return val; }
        static object ToDouble(object val) { return (bool)val ? 1.0D : 0.0D; }
        static object ToLong(object val) { return (bool)val ? 1L : 0L; }
        static object ToString(object val) { return val.ToString(); }
        static object ToJson(object val) { return JsonConvert.SerializeObject(val); }

        public static void Init(Action<AttributeType, Converter> fromTo)
        {
            fromTo(AttributeType.Binary, ToBinary);
            fromTo(AttributeType.Boolean, ToBoolean);
            fromTo(AttributeType.Double, ToDouble);
            fromTo(AttributeType.Long, ToLong);
            fromTo(AttributeType.String, ToString);
            fromTo(AttributeType.Json, ToJson);
        }
    }

    static class FromDateTime
    {
        static object ToDateTime(object val) { return val; }
        static object ToBoolean(object val) { return (DateTime)val > DateTime.MinValue; }
        static object ToBinary(object val) { return BitConverter.GetBytes(((DateTime)val).ToBinary()); }
        static object ToLong(object val) { return ((DateTime)val).ToBinary(); }
        static object ToString(object val) { return val.ToString(); }
        static object ToJson(object val) { return JsonConvert.SerializeObject(val); }
        static object ToObjectId(object val) { return ObjectId.GenerateNewId((DateTime)val); }

        public static void Init(Action<AttributeType, Converter> fromTo)
        {
            fromTo(AttributeType.DateTime, ToDateTime);
            fromTo(AttributeType.Boolean, ToBoolean);
            fromTo(AttributeType.Binary, ToBinary);
            fromTo(AttributeType.Long, ToLong);
            fromTo(AttributeType.ObjectId, ToObjectId);
            fromTo(AttributeType.String, ToString);
            fromTo(AttributeType.Json, ToJson);
        }
    }

    static class FromTimeSpan
    {
        static object ToTimeSpan(object val) { return val; }
        static object ToBoolean(object val) { return (TimeSpan)val != TimeSpan.Zero; }
        static object ToLong(object val) { return ((TimeSpan)val).Ticks; }
        static object ToString(object val) { return val.ToString(); }
        static object ToJson(object val) { return JsonConvert.SerializeObject(val); }

        public static void Init(Action<AttributeType, Converter> fromTo)
        {
            fromTo(AttributeType.TimeSpan, ToTimeSpan);
            fromTo(AttributeType.Boolean, ToBoolean);
            fromTo(AttributeType.Long, ToLong);
            fromTo(AttributeType.String, ToString);
            fromTo(AttributeType.Json, ToJson);
        }
    }

    static class FromDouble
    {
        static object ToDouble(object val)
        {
            if (val is float)
            {
                return (double)val;
            }

            return val;
        }

        static object ToBinary(object val)
        {
            if (val is double db)
                return BitConverter.GetBytes(db);
            return BitConverter.GetBytes((float)val);
        }

        static object ToBoolean(object val)
        {
            if (val is double db)
                return db != 0.0;
            return (double)(float)val != 0.0;
        }

        static object ToLong(object val)
        {
            if (val is double db)
                return (long)db;
            return (long)(double)(float)val;
        }

        static object ToString(object val) { return val.ToString(); }

        static object ToJson(object val) { return JsonConvert.SerializeObject(val); }

        public static void Init(Action<AttributeType, Converter> fromTo)
        {
            fromTo(AttributeType.Double, ToDouble);
            fromTo(AttributeType.Binary, ToBinary);
            fromTo(AttributeType.Boolean, ToBoolean);
            fromTo(AttributeType.Long, ToLong);
            fromTo(AttributeType.String, ToString);
            fromTo(AttributeType.Json, ToJson);
        }
    }

    static class FromGuid
    {
        static object ToGuid(object val) { return val; }

        static object ToBinary(object val) { return ((Guid)val).ToByteArray(); }

        static object ToBoolean(object val) { return (Guid)val != Guid.Empty; }

        static object ToString(object val) { return val.ToString(); }

        static object ToJson(object val) { return JsonConvert.SerializeObject(val); }

        public static void Init(Action<AttributeType, Converter> fromTo)
        {
            fromTo(AttributeType.Guid, ToGuid);
            fromTo(AttributeType.Binary, ToBinary);
            fromTo(AttributeType.Boolean, ToBoolean);
            fromTo(AttributeType.String, ToString);
            fromTo(AttributeType.Json, ToJson);
        }
    }

    static class FromObjectId
    {
        static object ToObjectId(object val) { return val; }

        static object ToBinary(object val) { return ((ObjectId)val).ToByteArray(); }

        static object ToDateTime(object val) { return ((ObjectId)val).Timestamp; }

        static object ToBoolean(object val) { return (ObjectId)val != ObjectId.Empty; }

        static object ToString(object val) { return val.ToString(); }

        static object ToJson(object val) { return JsonConvert.SerializeObject(val); }

        public static void Init(Action<AttributeType, Converter> fromTo)
        {
            fromTo(AttributeType.ObjectId, ToObjectId);
            fromTo(AttributeType.Binary, ToBinary);
            fromTo(AttributeType.Boolean, ToBoolean);
            fromTo(AttributeType.DateTime, ToDateTime);
            fromTo(AttributeType.String, ToString);
            fromTo(AttributeType.Json, ToJson);
        }
    }

    static class FromLong
    {
        static long MakeLong(object i)
        {
            if (i is long l)
            {
                return l;
            }
            else if (i is int j)
            {
                return j;
            }
            else if (i is short s)
            {
                return s;
            }
            else if (i is byte b)
            {
                return b;
            }
            else if (i is ushort u)
            {
                return u;
            }
            else if (i is uint ui)
            {
                return ui;
            }
            else if (i is ulong ul)
            {
                return (long)ul;
            }

            return 0L;
        }

        static object ToLong(object val) { return MakeLong(val); }

        static object ToBinary(object val)
        {
            return BitConverter.GetBytes(MakeLong(val));
        }

        static object ToBoolean(object val)
        {
            return MakeLong(val) != 0;
        }

        static object ToDouble(object val)
        {
            return (double)MakeLong(val);
        }

        static object ToDateTime(object val) { return DateTime.FromBinary(MakeLong(val)); }

        static object ToTimeSpan(object val) { return new TimeSpan(MakeLong(val)); }

        static object ToString(object val) { return val.ToString(); }

        static object ToJson(object val) { return JsonConvert.SerializeObject(val); }

        public static void Init(Action<AttributeType, Converter> fromTo)
        {
            fromTo(AttributeType.Long, ToLong);
            fromTo(AttributeType.Binary, ToBinary);
            fromTo(AttributeType.Boolean, ToBoolean);
            fromTo(AttributeType.Double, ToDouble);
            fromTo(AttributeType.DateTime, ToDateTime);
            fromTo(AttributeType.TimeSpan, ToTimeSpan);
            fromTo(AttributeType.String, ToString);
            fromTo(AttributeType.Json, ToJson);
        }
    }

    static class FromString
    {
        static object ToString(object val) { return val; }

        static object ToBinary(object val) { return Convert.FromBase64String((string)val); }

        static object ToBoolean(object val)
        {
            if (bool.TryParse((string)val, out bool v))
            {
                return v;
            }

            return ((string)val).Length > 0;
        }

        static object ToDateTime(object val)
        {
            DateTime.TryParse((string)val, out var v);
            return v;
        }

        static object ToTimeSpan(object val)
        {
            TimeSpan.TryParse((string)val, out var v);
            return v;
        }

        static object ToDouble(object val)
        {
            double.TryParse((string)val, out double v);
            return v;
        }

        static object ToGuid(object val)
        {
            Guid.TryParse((string)val, out var v);
            return v;
        }

        static object ToObjectId(object val)
        {
            ObjectId.TryParse((string)val, out var v);
            return v;
        }

        static object ToLong(object val)
        {
            long.TryParse((string)val, out long v);
            return v;
        }

        static object ToJson(object val)
        {
            try
            {
                Newtonsoft.Json.Linq.JObject.Parse((string)val);
                return (string)val;
            }
            catch
            {
                return JsonConvert.SerializeObject(val);
            }
        }

        public static void Init(Action<AttributeType, Converter> fromTo)
        {
            fromTo(AttributeType.String, ToString);
            fromTo(AttributeType.Binary, ToBinary);
            fromTo(AttributeType.Boolean, ToBoolean);
            fromTo(AttributeType.DateTime, ToDateTime);
            fromTo(AttributeType.TimeSpan, ToTimeSpan);
            fromTo(AttributeType.Double, ToDouble);
            fromTo(AttributeType.Guid, ToGuid);
            fromTo(AttributeType.ObjectId, ToObjectId);
            fromTo(AttributeType.Long, ToLong);
            fromTo(AttributeType.Json, ToJson);
        }
    }

    static class FromOther
    {
        static object ToOther(object val) { return val; }

        static object ToString(object val) { return JsonConvert.SerializeObject(val, Formatting.Indented); }

        static object ToJson(object val) { return JsonConvert.SerializeObject(val, Formatting.Indented); }

        static object ToBoolean(object val) { return val != null; }

        static object ToLong(object val) { return val != null ? 1L : 0L; }

        static object ToDouble(object val) { return val != null ? 1D : 0D; }


        public static void Init(Action<AttributeType, Converter> fromTo)
        {
            fromTo(AttributeType.Other, ToOther);
            fromTo(AttributeType.String, ToString);
            fromTo(AttributeType.Json, ToJson);
            fromTo(AttributeType.Boolean, ToBoolean);
            fromTo(AttributeType.Long, ToLong);
            fromTo(AttributeType.Double, ToDouble);
        }
    }

    static class FromEnum
    {
        static object ToString(object val) { return val.ToString(); }

        static object ToBoolean(object val)
        {
            return (int)val > 0;
        }

        static object ToDouble(object val)
        {
            return (double)(int)val;
        }

        static object ToLong(object val)
        {
            return (long)(int)val;
        }

        static object ToJson(object val)
        {
            try
            {
                return JsonConvert.SerializeObject(val, new Newtonsoft.Json.Converters.StringEnumConverter());
            }
            catch
            {
                return JsonConvert.SerializeObject(val);
            }
        }

        public static void Init(Action<AttributeType, Converter> fromTo)
        {
            fromTo(AttributeType.String, ToString);
            fromTo(AttributeType.Boolean, ToBoolean);
            fromTo(AttributeType.Double, ToDouble);
            fromTo(AttributeType.Long, ToLong);
            fromTo(AttributeType.Json, ToJson);
        }
    }

    /// <summary>
    /// Performance oriented from->to type converter for Rule Engine
    /// </summary>
    public static class CoerceType
    {
        static readonly object mapLock = new object { };
        static Converter[,] _Map;
        static Converter[,] Map
        {
            get
            {
                if (_Map == null)
                {
                    lock (mapLock)
                    {
                        var max = (int)Enum.GetValues(typeof(AttributeType)).Cast<AttributeType>().Last();
                        _Map = new Converter[max + 1, max + 1];

                        FromNull.Init(delegate (AttributeType to, Converter converter)
                        {
                            _Map[(int)AttributeType.Null, (int)to] = converter;
                        });

                        FromBinary.Init(delegate (AttributeType to, Converter converter)
                        {
                            _Map[(int)AttributeType.Binary, (int)to] = converter;
                        });

                        FromBoolean.Init(delegate (AttributeType to, Converter converter)
                        {
                            _Map[(int)AttributeType.Boolean, (int)to] = converter;
                        });

                        FromDateTime.Init(delegate (AttributeType to, Converter converter)
                        {
                            _Map[(int)AttributeType.DateTime, (int)to] = converter;
                        });

                        FromTimeSpan.Init(delegate (AttributeType to, Converter converter)
                        {
                            _Map[(int)AttributeType.TimeSpan, (int)to] = converter;
                        });

                        FromDouble.Init(delegate (AttributeType to, Converter converter)
                        {
                            _Map[(int)AttributeType.Double, (int)to] = converter;
                        });

                        FromGuid.Init(delegate (AttributeType to, Converter converter)
                        {
                            _Map[(int)AttributeType.Guid, (int)to] = converter;
                        });

                        FromObjectId.Init(delegate (AttributeType to, Converter converter)
                        {
                            _Map[(int)AttributeType.ObjectId, (int)to] = converter;
                        });

                        FromLong.Init(delegate (AttributeType to, Converter converter)
                        {
                            _Map[(int)AttributeType.Long, (int)to] = converter;
                        });

                        FromString.Init(delegate (AttributeType to, Converter converter)
                        {
                            _Map[(int)AttributeType.String, (int)to] = converter;
                        });

                        FromEnum.Init(delegate (AttributeType to, Converter converter)
                        {
                            _Map[(int)AttributeType.Enum, (int)to] = converter;
                        });

                        FromOther.Init(delegate (AttributeType to, Converter converter)
                        {
                            _Map[(int)AttributeType.Other, (int)to] = converter;
                        });

                        _Map[(int)AttributeType.Json, (int)AttributeType.Json] = delegate (object val) { return val; };
                    }
                }

                return _Map;
            }
        }


        static readonly Dictionary<Type, AttributeType> TypeMap = new Dictionary<Type, AttributeType>
        {
            { typeof(string), AttributeType.String },
            { typeof(byte[]), AttributeType.Binary },
            { typeof(bool), AttributeType.Boolean },
            { typeof(DateTime), AttributeType.DateTime },
            { typeof(TimeSpan), AttributeType.TimeSpan },
            { typeof(float), AttributeType.Double },
            { typeof(double), AttributeType.Double },
            { typeof(Guid), AttributeType.Guid },
            { typeof(ObjectId), AttributeType.ObjectId },
            { typeof(short), AttributeType.Long },
            { typeof(int), AttributeType.Long },
            { typeof(long), AttributeType.Long },
            { typeof(ushort), AttributeType.Long },
            { typeof(uint), AttributeType.Long },
            { typeof(ulong), AttributeType.Long },
            { typeof(byte), AttributeType.Long }
        };

        public static AttributeType FindType(object value)
        {
            if (value == null)
            {
                return AttributeType.Null;
            }

            if (!TypeMap.ContainsKey(value.GetType()))
            {
                if (value is Enum)
                {
                    return AttributeType.Enum;
                }

                return AttributeType.Other;
            }

            return TypeMap[value.GetType()];
        }

        /// <summary>
        /// Attempts type coercion and silently returns null if the types are not compatible
        /// </summary>
        public static object SafeCoerce(AttributeType type, object value)
        {
            var oType = FindType(value);
            if (Map[(int)FindType(value), (int)type] != null)
            {
                try
                {
                    return Map[(int)oType, (int)type](value);
                }
                catch { return null; }
            }

            return null;
        }

        /// <summary>
        /// Attempts type coercion and throws an argument exception if there is no compatible mapping
        /// </summary>
        public static object Coerce(AttributeType type, object value)
        {
            if (value == null)
            {
                return null;
            }

            var oType = FindType(value);
            if (type == oType) { return value; }

            if (Map[(int)FindType(value), (int)type] != null)
            {
                return Map[(int)oType, (int)type](value);
            }

            throw new ArgumentException($"Unable to convert {oType} to {type}");
        }
    }

    public static class CoerceExtension
    {
        public static long ToLong(this AttributeValue val)
        {
            return (long)val.AsType(AttributeType.Long);
        }

        public static byte[] ToBinary(this AttributeValue val)
        {
            return (byte[])val.AsType(AttributeType.Binary);
        }

        public static DateTime ToDateTime(this AttributeValue val)
        {
            return (DateTime)val.AsType(AttributeType.DateTime);
        }

        public static double ToDouble(this AttributeValue val)
        {
            return (double)val.AsType(AttributeType.Double);
        }

        public static Guid ToGuid(this AttributeValue val)
        {
            return (Guid)val.AsType(AttributeType.Guid);
        }

        public static ObjectId ToObjectId(this AttributeValue val)
        {
            return (ObjectId)val.AsType(AttributeType.ObjectId);
        }

        public static TimeSpan ToTimeSpan(this AttributeValue val)
        {
            return (TimeSpan)val.AsType(AttributeType.TimeSpan);
        }
    }
}