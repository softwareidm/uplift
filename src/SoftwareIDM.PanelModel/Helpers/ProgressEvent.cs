﻿using System;
using Microsoft.Extensions.Logging;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace SoftwareIDM.PanelModel.Helpers
{
    /// <summary>
    /// Message for Progress event, used to write output in PanelTool and PanelService
    /// </summary>
    public class ProgressEventArgs : EventArgs
    {
        [BsonElement("ts")]
        public DateTime TimeStamp { get; set; }

        [BsonElement("l")]
        public LogLevel Level { get; set; }

        [BsonElement("m")]
        public string Message { get; set; }

        [BsonIgnore]
        [JsonIgnore]
        public int EventId { get; set; }

        public ProgressEventArgs() { }

        public ProgressEventArgs(LogLevel level, string message, int eventId)
        {
            TimeStamp = DateTime.UtcNow;
            Message = message.Replace("{", "{{").Replace("}", "}}");
            EventId = eventId;
            Level = level;
        }

        public ProgressEventArgs(string message, int eventId)
        {
            TimeStamp = DateTime.UtcNow;
            Level = LogLevel.Information;
            Message = message.Replace("{", "{{").Replace("}", "}}");
            EventId = eventId;
        }
    }

    /// <summary>
    /// Report data from scans and runs to service or PanelTool
    /// </summary>
    public delegate void ProgressEvent(object sender, ProgressEventArgs e);
}
