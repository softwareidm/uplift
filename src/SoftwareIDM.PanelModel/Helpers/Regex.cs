﻿using System;
using System.Text.RegularExpressions;

namespace SoftwareIDM.PanelModel.Helpers
{
    public static class RegexHelper
    {
        public static Regex GuidMatch { get { return new Regex(@"^[{|\(]?[0-9a-fA-F]{8}[-]?([0-9a-fA-F]{4}[-]?){3}[0-9a-fA-F]{12}[\)|}]?$"); } }

        public static Regex ModuleMatch { get { return new Regex(@"SoftwareIDM\.\w+Module"); } }

        public static Regex EmailMatch { get { return new Regex(@"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$"); } }
    }
}