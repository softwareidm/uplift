﻿using System;
using System.Collections.Generic;
using System.Linq;
using SoftwareIDM.PanelModel.Models;

namespace SoftwareIDM.PanelModel.Helpers
{
    /// <summary>
    /// Helpers, primarily for web application for comparing join links and attribute sets to generate diffs for change records
    /// </summary>
    public static class Compare
    {
        /// <summary>
        /// Compare a CS/MV join (or other link) to see if they are the same (same object same connected state)
        /// </summary>
        public static ModType CompareLink(Link oldLink, Link newLink)
        {
            if (newLink == null)
            {
                if (oldLink == null || !oldLink.Connected)
                {
                    return 0;
                }
                else
                {
                    return ModType.Disconnect;
                }
            }

            if (oldLink == null || oldLink.Connected != newLink.Connected || oldLink.Other != newLink.Other)
            {
                if (newLink.Connected)
                {
                    return ModType.Connect;
                }
                else
                {
                    return ModType.Disconnect;
                }
            }

            return 0;
        }

        // Copyright (c) 2008-2013 Hafthor Stefansson
        // Distributed under the MIT/X11 software license
        // Ref: http://www.opensource.org/licenses/mit-license.php.
        public static unsafe bool UnsafeByteEquals(byte[] a1, byte[] a2)
        {
            if (a1 == a2) return true;
            if (a1 == null || a2 == null || a1.Length != a2.Length)
                return false;
            fixed (byte* p1 = a1, p2 = a2)
            {
                byte* x1 = p1, x2 = p2;
                int l = a1.Length;
                for (int i = 0; i < l / 8; i++, x1 += 8, x2 += 8)
                    if (*((long*)x1) != *((long*)x2)) return false;
                if ((l & 4) != 0) { if (*((int*)x1) != *((int*)x2)) return false; x1 += 4; x2 += 4; }
                if ((l & 2) != 0) { if (*((short*)x1) != *((short*)x2)) return false; x1 += 2; x2 += 2; }
                if ((l & 1) != 0) if (*((byte*)x1) != *((byte*)x2)) return false;
                return true;
            }
        }

        /// <summary>
        /// Compare a list of joins to see if there are any changes to join state
        /// </summary>
        public static ModType CompareLinkSet(List<Link> oldLinks, List<Link> newLinks, DateTime timeStamp, out List<Link> changes)
        {
            ModType ret = 0;
            changes = new List<Link>();
            var oldUntouched = new List<Link>();
            var newTouched = new List<Guid>();
            if (oldLinks != null)
            {
                foreach (var link in oldLinks)
                {
                    bool touched = false;
                    if (newLinks != null)
                    {
                        for (int i = 0; i < newLinks.Count; i++)
                        {
                            if (link.Other != Guid.Empty && link.Other == newLinks[i].Other)
                            {
                                touched = true;
                                newTouched.Add(link.Other);
                                var comp = CompareLink(link, newLinks[i]);
                                if (comp > 0)
                                {
                                    changes.Add(newLinks[i]);
                                    ret |= comp;
                                }
                            }
                        }
                    }

                    if (!touched && link.Connected) // don't need to investigate disconnector link
                    {
                        oldUntouched.Add(link);
                    }
                }
            }

            // disconnected or deleted
            foreach (var oLink in oldUntouched)
            {
                var date = timeStamp;
                var dlink = new Link
                {
                    Other = oLink.Other,
                    Connected = false,
                    TimeStamp = date
                };

                changes.Add(dlink);
                ret |= ModType.Disconnect;
            }

            if (newLinks != null)
            {
                foreach (var nLink in newLinks)
                {
                    if (nLink.Other != Guid.Empty && !newTouched.Contains(nLink.Other))
                    {
                        ret |= CompareLink(null, nLink);
                        changes.Add(nLink);
                    }
                }
            }

            return ret;
        }

        /// <summary>
        /// Compare two attribute dictionaries and find attributes that have been added, deleted, or updated
        /// </summary>
        public static AttributeDictionary<AttributeChange> CompareSingleAttrs(AttributeDictionary<object> oldA, AttributeDictionary<object> newA)
        {
            var changes = new AttributeDictionary<AttributeChange>();
            if (oldA == null || oldA.Count == 0)
            {
                if (newA == null || newA.Count == 0)
                    return changes;

                foreach (var attr in newA)
                {
                    changes.Add(attr.Key, new AttributeChange { Kind = AModt.Add, Value = attr.Value });
                }
            }
            else
            {
                if (newA == null || newA.Count == 0)
                {
                    foreach (var attr in oldA)
                    {
                        changes.Add(attr.Key, new AttributeChange { Kind = AModt.Delete, Value = null });
                    }
                }
                else
                {
                    // interesting case: both have values
                    foreach (var oAttr in oldA)
                    {
                        if (newA.TryGetValue(oAttr.Key, out object val))
                        {
                            if (val == null)
                            {
                                if (oAttr.Value != null)
                                {
                                    changes.Add(oAttr.Key, new AttributeChange { Kind = AModt.Update, Value = null });
                                }
                            }
                            else if (val is byte[] vb && oAttr.Value is byte[] ob)
                            {
                                if (!UnsafeByteEquals(vb, ob))
                                {
                                    changes.Add(oAttr.Key, new AttributeChange { Kind = AModt.Update, Value = val });
                                }
                            }
                            else if (!val.Equals(oAttr.Value))
                            {
                                changes.Add(oAttr.Key, new AttributeChange { Kind = AModt.Update, Value = val });
                            }
                        }
                        else
                        {
                            changes.Add(oAttr.Key, new AttributeChange { Kind = AModt.Delete, Value = null });
                        }
                    }

                    foreach (var nAttr in newA)
                    {
                        if (!oldA.ContainsKey(nAttr.Key))
                        {
                            changes.Add(nAttr.Key, new AttributeChange { Kind = AModt.Add, Value = nAttr.Value });
                        }
                    }
                }
            }

            return changes;
        }
    }
}