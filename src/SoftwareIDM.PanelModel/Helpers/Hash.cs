﻿using System;
using System.Security.Cryptography;

namespace SoftwareIDM.PanelModel.Helpers
{
    /// <summary>
    /// Helper for generating 16 byte data hashes
    /// </summary>
    public static class Hash
    {
        public static byte[] Get(byte[] data)
        {
            return Get(data, 16);
        }

        public static byte[] Get(byte[] data, int length)
        {
            byte[] ret;

            using (var prov = new SHA256CryptoServiceProvider())
            {
                ret = prov.ComputeHash(data);
            }
            
            if (length > 0 && length <= data.Length)
            {
                ret = ret.SubArray(0, length);
            }

            
            return ret;
        }
    }
}