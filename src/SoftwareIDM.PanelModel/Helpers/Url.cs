﻿using System;
using System.Text.RegularExpressions;

namespace SoftwareIDM.PanelModel.Helpers
{
    /// <summary>
    /// Helpers for working with values for URLs
    /// </summary>
    public static class Url
    {
        /// <summary>
        /// Slugify a value using the same algorithm as slugify() in the js application
        /// </summary>
        public static string Slugify(this string field)
        {
            var ret = Regex.Replace(field.ToLower().Trim(), @"[^\w\s\-]", "");
            ret = Regex.Replace(ret, @"[\s\-]+", "-");
            return ret;
        }

        /// <summary>
        /// Convert a byte[] to a URL save base64 value. Slightly different from the python
        /// algorithm because IIS. Note the value does not need URI encoding
        /// </summary>
        public static string ToUrlBase64(this byte[] data)
        {
            var str = Convert.ToBase64String(data);
            return Regex.Replace(str, @"[\+/=]", delegate (Match m)
            {
                switch (m.Value)
                {
                    case "+":
                        return "-";
                    case "/":
                        return "_";
                    default: // "=":
                        return "~";
                }
            });
        }

        /// <summary>
        /// Converts a URL save base64 string to a byte array.
        /// </summary>
        public static byte[] FromUrlBase64(this string data)
        {
            return Convert.FromBase64String(
                Regex.Replace(data, @"[\-_~]", delegate (Match m)
                {
                    switch (m.Value)
                    {
                        case "-":
                            return "+";
                        case "_":
                            return "/";
                        default: // "~"
                            return "=";
                    }
                })
            );
        }

        public const string ISODateFormat = "yyyy-MM-ddTHH:mm:ss.fffZ";
        public static string FormatISODate(this DateTime value)
        {
            return value.ToString(ISODateFormat);
        }

        /// <summary>
        /// Friendly print a timestamp
        /// </summary>
        public static string FormatTimeSpan(TimeSpan value)
        {
            if (value == new TimeSpan())
            {
                return "N/A";
            }

            var res = "{0} {1}, {2} {3}";
            if (value.Days > 0)
            {
                return string.Format(res, value.Days, value.Days == 1 ? "day" : "days", value.Hours, value.Hours == 1 ? "hour" : "hours");
            }

            if (value.Hours > 0)
            {
                return string.Format(res, value.Hours, value.Hours == 1 ? "hour" : "hours", value.Minutes, value.Minutes == 1 ? "minute" : "minutes");
            }

            if (value.Minutes > 0)
            {
                return string.Format(res, value.Minutes, value.Minutes == 1 ? "minute" : "minutes", value.Seconds, value.Seconds == 1 ? "second" : "seconds");
            }

            return string.Format(res, value.Seconds, value.Seconds == 1 ? "second" : "seconds", value.Milliseconds, value.Milliseconds, "milliseconds");
        }
    }
}