﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using SoftwareIDM.AzureKeyVaultShim;

namespace SoftwareIDM.PanelModel.Helpers
{
    public enum AuthType
    {
        Windows,
        Azure
    }

    /// <summary>
    /// Strongly typed reading of config.json, properies that may be needed both server and client side.
    /// </summary>
    public static class Conf
    {
        static IConfiguration configuration = null;

        public static AuthType Auth { get { return (AuthType)Enum.Parse(typeof(AuthType), Configuration["Auth:Mode"]); } }


        static bool? hashesForceUpdate = null;
        public static bool HashesForceUpdate
        {
            get
            {
                if (!hashesForceUpdate.HasValue)
                {
                    hashesForceUpdate = false;
                    if (Configuration["Application:HashesForceUpdate"] != null)
                    {
                        hashesForceUpdate = bool.Parse(Configuration["Application:HashesForceUpdate"]);
                    }
                }

                return hashesForceUpdate.Value;
            }
        }

        static bool? disableDeleteProtection = null;
        public static bool DisableDeleteProtection
        {
            get
            {
                if (!disableDeleteProtection.HasValue)
                {
                    disableDeleteProtection = false;
                    if (Configuration["Application:DisableDeleteProtection"] != null)
                    {
                        disableDeleteProtection = bool.Parse(Configuration["Application:DisableDeleteProtection"]);
                    }
                }

                return disableDeleteProtection.Value;
            }
        }

        static string _protectMode = null;
        static DataProtectionScope ProtectModeScope
        {
            get
            {
                var mode = "LocalMachine";
                if (_protectMode == null)
                {
                    _protectMode = Configuration["Auth:ProtectMode"] ?? "None";
                    if (_protectMode != "None")
                    {
                        mode = _protectMode;
                    }
                }

                return (DataProtectionScope)Enum.Parse(typeof(DataProtectionScope), mode, true);
            }
        }

        public static string ProtectMode => Configuration["Auth:ProtectMode"];

        static void UpdateJson(string path, string save, ref JObject json)
        {
            if (json == null)
            {
                json = JObject.Parse(File.ReadAllText(ConfigPath));
            }

            var split = path.Split(':');
            var j = json;
            for (int i = 0; i < split.Length - 1; i++)
            {
                j = (JObject)j[split[i]];
            }

            j[split[split.Length - 1]] = save;
        }

        public static string Encrypt(string value, bool failIfNone)
        {
            if (ProtectMode == "KeyVault")
            {
                return "ec:" + ProtectionHelper.Protect(value, ProtectionHelper.GetClient(Configuration));
            }
            else if (ProtectMode == "DPAPI-NG")
            {
                return "ec:" + ProtectionHelper.Protect(value, Configuration["Auth:DataProtectionSid"]);
            }
            else if (ProtectMode == "CurrentUser" || ProtectMode == "LocalMachine")
            {
                var bits = Encoding.UTF8.GetBytes(value);
                var protectedBits = ProtectedData.Protect(bits, new Guid("e25023b1-ebb9-4636-a6d2-89bb5ba551b6").ToByteArray(), ProtectModeScope);
                return "ec:" + protectedBits.ToUrlBase64();
            }
            else if (failIfNone)
            {
                throw new ArgumentException("Unable to encrypt value. ProtectMode is None");
            }

            return value;
        }

        public static string Decrypt(string value)
        {
            if (value.StartsWith("ec:")) { value = value.Substring(3); }
            else { return value; }
            if (ProtectMode == "KeyVault")
            {
                return ProtectionHelper.UnProtect(value, ProtectionHelper.GetClient(Configuration));
            }
            else if (ProtectMode == "DPAPI-NG")
            {
                return ProtectionHelper.UnProtect(value);
            }
            else if (ProtectMode == "CurrentUser" || ProtectMode == "LocalMachine")
            {
                var protectedBits = value.FromUrlBase64();
                var bits = ProtectedData.Unprotect(protectedBits, new Guid("e25023b1-ebb9-4636-a6d2-89bb5ba551b6").ToByteArray(), ProtectModeScope);
                return Encoding.UTF8.GetString(bits);
            }
            else
            {
                throw new ArgumentException("Unable to decrypt value. ProtectMode is None");
            }
        }

        static readonly ConcurrentDictionary<string, string> _secrets = new ConcurrentDictionary<string, string>();

        public static string Protected(params string[] paths)
        {
            bool changed = false;
            string val = null;
            JObject json = null;
            foreach (var path in paths)
            {
                val = Configuration[path];
                if (string.IsNullOrEmpty(val))
                {
                    continue;
                }

                if (val.Equals("keyvault", StringComparison.OrdinalIgnoreCase))
                {
                    if (_secrets.TryGetValue(path, out string secret))
                    {
                        val = secret;
                        continue;
                    }

                    var client = ProtectionHelper.GetSecret(Configuration);
                    var keyName = path.Split(':').Last();
                    var secretBundle = client.GetSecretAsync(keyName).GetAwaiter().GetResult();
                    _secrets[path] = secretBundle.Value.Value;
                    val = secretBundle.Value.Value;
                    continue;
                }

                if (val.StartsWith("ec:"))
                {
                    val = Decrypt(val);
                }
                else
                {
                    var save = Encrypt(val, false);
                    UpdateJson(path, save, ref json);
                    changed = true;
                }
            }

            if (changed)
            {
                File.WriteAllText(ConfigPath, json.ToString(Newtonsoft.Json.Formatting.Indented));
                Reset();
            }

            return val;
        }

        public static void Reset()
        {
            configuration = null;
        }

        static string _basePath;
        static string _file;
        public static void Set(string basePath)
        {
            Reset();
            _basePath = basePath;
        }
        public static void Set(string basePath, string file)
        {
            Reset();
            _basePath = basePath;
            _file = file;
        }

        public static string DataProtectionSid { get { return Configuration["Auth:DataProtectionSid"] ?? Configuration["Application:DataProtectionSid"]; } }

        public static string ConfigPath
        {
            get
            {
                return Path.Combine(_basePath, _file ?? "config.json");
            }
        }
        
        public static IConfiguration Configuration
        {
            get
            {
                if (configuration == null)
                {
                    if (_basePath == null)
                    {
                        _basePath = AppContext.BaseDirectory;
                    }

                    var builder = new ConfigurationBuilder()
                        .SetBasePath(_basePath)
                        .AddJsonFile(_file ?? "config.json")
                        .AddEnvironmentVariables();
                    configuration = builder.Build();
                }

                return configuration;
            }
        }
    }
}