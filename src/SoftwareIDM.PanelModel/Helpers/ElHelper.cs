﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using MongoDB.Bson.Serialization;

namespace SoftwareIDM.PanelModel.Helpers
{
    /// <summary>
    /// Translates from the .NET class property name, to the Bson serialized property name.
    /// Typically only needed server-side
    /// </summary>
    public static class ElHelper<T>
    {
        /// <summary>
        /// Looks up the element name of a property
        /// </summary>
        public static string FieldName(Expression<Func<T, object>> memberExpression)
        {
            MemberExpression operand;
            if (memberExpression.Body is MemberExpression)
            {
                operand = (MemberExpression)memberExpression.Body;
            }
            else
            {
                operand = (MemberExpression)((UnaryExpression)memberExpression.Body).Operand;
            }

            var name = operand.Member.Name;
            var type = typeof(T);
            BsonClassMap map = null;
            BsonMemberMap mem = null;
            while (mem == null && type != typeof(object))
            {
                map = BsonClassMap.LookupClassMap(type);
                mem = map.GetMemberMap(name);
                type = type.BaseType;
            }

            return mem.ElementName;
        }

        /// <summary>
        /// Looks up the element name of a property
        /// </summary>
        public static string FieldName<P>(Expression<Func<T, P>> memberExpression)
        {
            MemberExpression operand = (MemberExpression)memberExpression.Body;
            var name = operand.Member.Name;
            var type = typeof(T);
            BsonClassMap map = null;
            BsonMemberMap mem = null;
            while (mem == null && type != typeof(object))
            {
                map = BsonClassMap.LookupClassMap(type);
                mem = map.GetMemberMap(name);
                type = type.BaseType;
            }

            return mem.ElementName;
        }

        public static string FieldName(string member)
        {
            var type = typeof(T);
            BsonClassMap map = null;
            BsonMemberMap mem = null;
            while (mem == null && type != typeof(object))
            {
                map = BsonClassMap.LookupClassMap(type);
                mem = map.GetMemberMap(member);
                type = type.BaseType;
            }

            return mem.ElementName;
        }

        /// <summary>
        /// Looks up a set of element names for a set of property expressions
        /// </summary>
        public static List<string> FieldList(params Expression<Func<T, object>>[] memberExpressions)
        {
            return memberExpressions.Select(x => FieldName(x)).ToList();
        }
    }
}