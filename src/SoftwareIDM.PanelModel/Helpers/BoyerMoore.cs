﻿

namespace SoftwareIDM.PanelModel.Helpers
{
    /// <summary>
    /// Simple implementation of BoyerMoore substring search algorithm, but generalized to byte arrays.
    /// </summary>
    public static class BoyerMoore
    {
        public static byte[] SubArray(this byte[] data, int start, int length)
        {
            var ret = new byte[length];
            for (int i = start; i < start + length; i++)
            {
                ret[i - start] = data[i];
            }

            return ret;
        }

        // byte[] IndexOf using BoyerMoore algorithm
        public static int FindBytes(this byte[] haystack, byte[] needle, int start)
        {
            if (needle.Length == 0)
            {
                return 0;
            }

            int[] lookup = MakeCharTable(needle);

            int i = needle.Length - 1 + start;
            var lastByte = needle[needle.Length - 1];
            while (i < haystack.Length)
            {
                var check = haystack[i];
                if (check == lastByte)
                {
                    bool found = true;
                    for (int j = needle.Length - 2; j >= 0; j--)
                    {
                        if (haystack[i - needle.Length + j + 1] != needle[j])
                        {
                            found = false;
                            break;
                        }
                    }

                    if (found)
                    {
                        return i - needle.Length + 1;
                    }
                    else
                    {
                        i++;
                    }
                }
                else
                {
                    i += lookup[check];
                }
            }

            return -1;
        }

        static int[] MakeCharTable(byte[] needle)
        {
            int[] table = new int[256];
            for (int i = 0; i < table.Length; i++)
            {
                table[i] = needle.Length;
            }

            for (int i = 0; i < needle.Length - 1; i++)
            {
                table[needle[i]] = needle.Length - i - 1;
            }

            return table;
        }
    }
}