﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoftwareIDM.PanelModel.Helpers
{
    public static class TypeHelper
    {
        /// <summary>
        /// Helper to format a type in a fully qualified format that works for calling GetType, and matches the JSON $type serialization
        /// </summary>
        public static string TypeName(Type type)
        {
            if (type.AssemblyQualifiedName == null || type.AssemblyQualifiedName.Split(',').Length < 2)
            {
                return type.Name;
            }

            return type.AssemblyQualifiedName.Split(',')[0].Trim() + ", " + type.AssemblyQualifiedName.Split(',')[1].Trim();
        }

        /// <summary>
        /// Given a namespace classname, and assembly, return a fully qualified type name
        /// </summary>
        public static Type Get(string space, string name, string assembly)
        {
            if (!assembly.StartsWith("SoftwareIDM"))
            {
                assembly = "SoftwareIDM." + assembly;
            }

            if (string.IsNullOrEmpty(space))
            {
                return Type.GetType($"{assembly}.{name}, {assembly}");
            }

            return Type.GetType($"{assembly}.{space}.{name}, {assembly}");
        }
    }
}
