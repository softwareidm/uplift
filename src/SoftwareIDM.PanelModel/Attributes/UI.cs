﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace SoftwareIDM.PanelModel.Attributes
{
    public enum UIAttrKind
    {
        None,
        Text,
        Select,
        LongText,
        Bool,
        Encrypted,
        Label,
        Hidden,
        Object,
        Strips,
        CheckList
    }

    public enum UIAttrType
    {
        None = 0,
        String,
        Int32,
        Double,
        Boolean,
        TimeSpan,
        DateTime,
        Guid,
        Email
    }

    [Flags]
    public enum DownloadFormats
    {
        None = 0,
        Json = 0x1,
        Xml = 0x2,
        Excel = 0x4,
        Delimited = 0x8
    }

    public class SelectChoice
    {
        public string Value { get; set; }

        public string Text { get; set; }
    }

    [AttributeUsage(AttributeTargets.Property)]
    public sealed class BlockRuleAttribute : Attribute { }

    [AttributeUsage(AttributeTargets.Class)]
    public sealed class ValidateStepAttribute : Attribute { }

    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Class | AttributeTargets.Interface)]
    public class UIAttribute : Attribute
    {
        /// <summary>
        /// input type:
        /// Text is a text input
        /// LongText is a textarea
        /// Bool is a checkbox
        /// Encrypted is a password field
        /// Label creates a label for binding and displaying the value of the attribute
        /// Hidden makes a hidden element with no label. It allows default value initialization for non-displayed values.
        /// Object looks up the child object type and renders a settings section for it.
        /// Strips binds a list of a specific object type
        /// Checklist populates values in a string list by binding Choices to checkboxes
        /// </summary>
        public UIAttrKind Kind { get; set; }

        /// <summary>
        /// Don't use. This serves as the required flag in override scenarios
        /// </summary>
        public bool? NRequired { get; set; }

        /// <summary>
        /// Mark as required for validation
        /// </summary>
        public bool Required { get; set; }

        /// <summary>
        /// Whether to show a warning alert when removing an object from a strip. Can only be used with Kind = Strips
        /// </summary>
        public bool WarnRemove { get; set; }

        /// <summary>
        /// If a display is a panel/strip, this places it in the heading instead of the body.
        /// </summary>
        public bool Header { get; set; }

        /// <summary>
        /// If this is a sub-type based strip select, parent filters the parent type this can be added to
        /// </summary>
        public Type Parent { get; set; }

        /// <summary>
        /// Displays only the input without a label to identify it. Text and Select boxes will have a Placeholder value
        /// </summary>
        public bool NoLabel { get; set; }

        /// <summary>
        /// Adds handling for rule language choice, Can be Rule or PowerShell, if null default is Rule.
        /// </summary>
        public string RuleLang { get; set; }

        /// <summary>
        /// Adds handling for rule help, places the help icon in the box. The following Rule* properties allow further control over rule engine UI.
        /// </summary>
        public bool RuleHelp { get; set; }

        /// <summary>
        /// Sets the rule help object lookup to a specific object type. Can be either a fully qualified type name, or a javascript function with js:
        /// </summary>
        public string RuleObject { get; set; }

        /// <summary>
        /// Sets complete js settings for the rule dialog display, typically: js:helpers.myName
        /// </summary>
        public string RuleDialog { get; set; }

        /// <summary>
        /// Sets the rule help object to indicate that braces are required for rule templating
        /// </summary>
        public bool RuleBraces { get; set; }

        /// <summary>
        /// Sets the rule help validation to only present warnings, never a blocking validation error
        /// </summary>
        public bool RuleWarnOnly { get; set; }

        /// <summary>
        /// Values to populate in drop down list (or checklist). Two formats are supported:
        /// id:text|id2:text2
        /// js:objectName, where objectName is a select2 definition
        /// If Kind is Strip and the data type is a list, then choices can contain a list of $type/name options
        /// If Choices is for a multi-value List`string it must be for a multi-select js:select2
        /// </summary>
        public string Choices { get; set; }

        /// <summary>
        /// Indicates if a strip type needs to be unique in the list of added strips. This is used when a settings section can only have one of each type, as with a PowerShell scan.
        /// Unique can also be used to indicate that a particular attribute, e.g. Name, should be unique in a collection of strips.
        /// </summary>
        public bool Unique { get; set; }

        /// <summary>
        /// Value to indicate how wide an input should be. This should be expressed as a backbone col-md-{n} where {n} is the Size
        /// Default is 6 for most inputs, but 12 for LongText, Strips, and Object
        /// </summary>
        public int Size { get; set; }

        /// <summary>
        /// Indicates wether to show the MemberDoc description as a full-time label, instead of just on the tooltip.
        /// </summary>
        public bool Description { get; set; }

        /// <summary>
        /// Default initial value. If it starts with js:function, then the specified javascript function will be used to generate the value.
        /// </summary>
        public string Default { get; set; }

        /// <summary>
        /// Attach a css class to an input. The most common is input-monospace
        /// </summary>
        public string CSSClass { get; set; }

        /// <summary>
        /// Placeholder override value for inputs
        /// </summary>
        public string Placeholder { get; set; }

        /// <summary>
        /// Allows input of javascript function to execute when this element is bound. e.g. js:helpers.reportFieldBind
        /// </summary>
        public string OnBind { get; set; }

        /// <summary>
        /// Indicates whether to show a duplicate button in strip headers
        /// </summary>
        public bool Duplicate { get; set; }

        /// <summary>
        /// Gives controller name to use for upload and download objects of this type.
        /// </summary>
        public string Download { get; set; }

        /// <summary>
        /// Lists what formats are permitted for downloading
        /// </summary>
        public DownloadFormats DownloadFormats { get; set; }

        /// <summary>
        /// Add data type validation to a field
        /// </summary>
        public UIAttrType TypeValid { get; set; }

        /// <summary>
        /// Validate a numeric type between min:max inclusive
        /// </summary>
        public string Range { get; set; }

        public UIAttribute() { }

        public UIAttribute(UIAttrKind kind)
        {
            Kind = kind;
        }

        public UIAttribute(UIAttrKind kind, bool required)
        {
            Kind = kind;
            Required = required;
            NRequired = required;
        }

        /// <summary>
        /// Populate JSON serializable representation of UI settings
        /// </summary>
        public virtual void Handle(Dictionary<string, object> doc)
        {
            if (Kind != UIAttrKind.None)
            {
                doc["Kind"] = Kind.ToString();
            }

            if (!string.IsNullOrEmpty(Default))
            {
                doc["Default"] = Default;
            }
        
            if (Required)
            {
                doc["Required"] = Required;
            }

            if (NRequired.HasValue)
            {
                doc["Required"] = NRequired.Value;
            }

            if (TypeValid != UIAttrType.None)
            {
                doc["TypeValid"] = TypeValid.ToString();
            }

            if (Header)
            {
                doc["Header"] = true;
            }

            if (Unique)
            {
                doc["Unique"] = true;
            }

            if (NoLabel)
            {
                doc["NoLabel"] = true;
            }

            if (!string.IsNullOrEmpty(CSSClass))
            {
                doc["CSSClass"] = CSSClass;
            }

            if (RuleHelp)
            {
                doc["RuleHelp"] = true;
            }

            if (!string.IsNullOrEmpty(RuleLang))
            {
                doc["RuleLang"] = RuleLang;
            }

            if (RuleWarnOnly)
            {
                doc["RuleWarnOnly"] = true;
            }

            if (Duplicate)
            {
                doc["Duplicate"] = true;
            }

            if (WarnRemove)
            {
                doc["WarnRemove"] = true;
            }

            if (!string.IsNullOrEmpty(Range))
            {
                doc["Range"] = Range;
            }

            if (!string.IsNullOrEmpty(Download))
            {
                doc["Download"] = Download;
                doc["DownloadFormats"] = DownloadFormats.Json.ToString();
                if (DownloadFormats != DownloadFormats.None)
                {
                    doc["DownloadFormats"] = DownloadFormats.ToString();
                }
            }

            if (!string.IsNullOrEmpty(RuleDialog))
            {
                doc["RuleDialog"] = RuleDialog;
            }
            else
            {
                if (RuleBraces)
                {
                    doc["RuleBraces"] = true;
                }

                if (!string.IsNullOrEmpty(RuleObject))
                {
                    doc["RuleObject"] = RuleObject;
                }
            }

            if (Description)
            {
                doc["HelpLabel"] = true;
            }

            if (Size > 0)
            {
                doc["Size"] = Size;
            }
            else if (!doc.ContainsKey("Size"))
            {
                if (Kind == UIAttrKind.LongText || Kind == UIAttrKind.Strips || Kind == UIAttrKind.Object)
                {
                    doc["Size"] = 12;
                }
                else
                {
                    doc["Size"] = 6;
                }
            }

            if (!string.IsNullOrEmpty(Placeholder))
            {
                doc["Placeholder"] = Placeholder;
            }

            if (!string.IsNullOrEmpty(OnBind))
            {
                doc["OnBind"] = OnBind;
            }

            if (!string.IsNullOrEmpty(Choices))
            {
                var choices = new List<SelectChoice>();
                foreach (var ch in Choices.Split('|'))
                {
                    if (ch.Contains(':'))
                    {
                        var s = ch.Split(':');
                        choices.Add(new SelectChoice { Value = s[0], Text = s[1] });
                    }
                    else
                    {
                        choices.Add(new SelectChoice { Value = ch, Text = ch });
                    }
                }

                doc["Choices"] = choices.ToArray();
            }
        }
    }
}