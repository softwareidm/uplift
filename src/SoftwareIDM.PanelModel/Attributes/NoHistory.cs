﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoftwareIDM.PanelModel.Attributes
{
    /// <summary>
    /// Used to indicate an object record as being instrinsically a single point in time with no history tracking
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class NoHistoryAttribute : Attribute { }
}
