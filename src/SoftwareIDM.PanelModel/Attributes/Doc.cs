﻿using System;

namespace SoftwareIDM.PanelModel.Attributes
{
    /// <summary>
    /// Attribute that controls:
    ///     Marking objects to be mapped for BSON serialization
    ///     Property names and descriptions for rule engine helper
    ///     Property names and descriptions for Settings UI
    ///     Explicit links to documentation site
    ///     Flagging objects (such as settings structures) as requiring a certain licensing level to work with. This is documentary, not enforced.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface)]
    public sealed class ClassDoc : Attribute
    {
        /// <summary>
        /// Human friendly name of the object. Masks the class name in UI.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Description shows up UI in tooltips, rule docs, and help labels
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Whether type should appear in Object Types dropdown list for the Rule Engine helper
        /// </summary>
        public bool RuleDoc { get; set; }

        /// <summary>
        /// If DocIgnore is true, not only will the type be excluded from the object helper dropdown list, it will also be excluded from settings ui types.
        /// </summary>
        public bool DocIgnore { get; set; }

        /// <summary>
        /// If Deprecated, type will not be selectable to add in dropdowns
        /// </summary>
        public bool Deprecated { get; set; }

        /// <summary>
        /// Link to additional type specific documentation
        /// </summary>
        public string Link { get; set; }

        /// <summary>
        /// License requirement indication, primarily used for Provider settings
        /// </summary>
        public string License { get; set; }

        public ClassDoc() { }

        public ClassDoc(string desc)
        {
            Description = desc;
        }

        public ClassDoc(string desc, string name)
        {
            Description = desc;
            Name = name;
        }
    }

    /// <summary>
    /// Attribute for documenting properties and methods in settings and rule docs
    /// </summary>
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Property | AttributeTargets.Field)]
    public sealed class MemberDoc : Attribute
    {
        /// <summary>
        /// User friendly name override
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Description to display in rule helper
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Used to indicate a dependency for field inclusion for deserialization.
        /// For example, HistoryRecord.Counters depends on HistoryRecord.EncodedCounters
        /// </summary>
        public string Depends { get; set; }

        /// <summary>
        /// List of possible values for an indexable object like an AttributeDictionary. Should be something that works for the Choices property of UIAttribute
        /// </summary>
        public string RuleIndexChoices { get; set; }
        
        /// <summary>
        /// Skip the property or method in rule documentation
        /// </summary>
        public bool Ignore { get; set; }

        public MemberDoc() { }

        public MemberDoc(string desc)
        {
            Description = desc;
        }

        public MemberDoc(string desc, string name)
        {
            Name = name;
            Description = desc;
        }

        public MemberDoc(bool ignore)
        {
            Ignore = ignore;
        }
    }
}