﻿using System;
using System.Collections.Generic;
using SoftwareIDM.PanelModel.Attributes;

namespace SoftwareIDM.PanelModel.Models
{
    public static class HyperSyncConstants
    {
        public const byte ATTRIBUTEFLOWRULE = 120;
        public const byte EVENTRULE = 121;
        public const byte STATERULE = 122;
        public const byte SYNCERROR = 123;
        public const byte ADD = 124;
        public const byte DELETE = 125;
        public const byte THRESHOLDNOTIFICATION = 126;
        public const byte THROTTLESKIP = 127;
        public const byte THRESHOLDSKIP = 128;

        public const string HyperSyncSilo = "2cd9b7e7-27a0-4d17-972e-94013e7626f0";
        public static Guid HyperSyncProvider { get { return Guid.Parse(HyperSyncSilo); } }
    }

    [ClassDoc("Run history counter values for HyperSync")]
    public class HyperSyncCounters : IHistoryCounters
    {
        public Dictionary<string, byte> Counters
        {
            get
            {
                return new Dictionary<string, byte>
                {
                    { "Attribute Flow Rule", HyperSyncConstants.ATTRIBUTEFLOWRULE },
                    { "Event Rule", HyperSyncConstants.EVENTRULE },
                    { "State Rule", HyperSyncConstants.STATERULE },
                    { "Sync Error", HyperSyncConstants.SYNCERROR },
                    { "Hyperverse Add", HyperSyncConstants.ADD },
                    { "Hyperverse Delete", HyperSyncConstants.DELETE },
                    { "Threshold Notification", HyperSyncConstants.THRESHOLDNOTIFICATION },
                    { "Throttle Skip", HyperSyncConstants.THROTTLESKIP },
                    { "Threshold Skip", HyperSyncConstants.THRESHOLDSKIP }
                };
            }
        }
    }
}
