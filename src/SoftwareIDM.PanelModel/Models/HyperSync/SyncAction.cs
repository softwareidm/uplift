﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using SoftwareIDM.PanelModel.Attributes;

namespace SoftwareIDM.PanelModel.Models
{
    public enum SyncStatus
    {
        Pending,
        ReviewNeeded,
        Approved,
        Rejected,
        Triggered,
        Expired,
        Failed
    }

    [ClassDoc("Object for managing queued sync processing activities", Name = "HyperSync Queue", RuleDoc = true)]
    public class SyncAction
    {
        public Guid Id { get; set; }

        [BsonElement("ts")]
        public DateTime TimeStamp { get; set; }

        [BsonElement("t")]
        public Guid TriggerId { get; set; }

        [BsonElement("r")]
        public Guid RuleId { get; set; }

        [BsonElement("s")]
        public SyncStatus Status { get; set; }

        [BsonIgnoreIfNull]
        [BsonElement("e")]
        public DateTime? Expires { get; set; }

        [BsonElement("n")]
        public string Name { get; set; }

        [BsonElement("an")]
        public string ActionName { get; set; }

        [BsonElement("si")]
        public bool Simulated { get; set; }

        [BsonIgnoreIfDefault]
        [BsonElement("mv")]
        public bool Hyperverse { get; set; }

        [BsonElement("rn")]
        public Guid RunId { get; set; }

        [BsonIgnoreIfDefault]
        [BsonElement("wid")]
        public Guid WorkflowId { get; set; }

        [BsonIgnoreIfDefault]
        [BsonElement("a")]
        public object Anchor { get; set; }

        [BsonElement("d")]
        public AttributeDictionary<object> Data { get; set; } = new AttributeDictionary<object>();

        [MemberDoc("References are synchronized as Guid[] ids in the target silo. If a value other than a guid is desired for rule processing, a transformation should be made via rule expression.")]
        [BsonElement("re")]
        public AttributeDictionary<List<Guid>> References { get; set; } = new AttributeDictionary<List<Guid>>();

        [MemberDoc("Index into Flow Data dictionary")]
        public object this[string key]
        {
            get
            {
                if (Data.TryGetValue(key, out var ret))
                {
                    return ret;
                }

                if (References.TryGetValue(key, out var refs))
                {
                    return refs;
                }

                return null;
            }
        }

        [BsonIgnoreIfNull]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        [BsonElement("ap")]
        public string Approver { get; set; }

        [BsonIgnoreIfNull]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        [BsonElement("apn")]
        public string ApproverName { get; set; }

        [BsonIgnoreIfNull]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        [BsonElement("atc")]
        public AttributeDictionary<SyncAttributeChange> AttributeChanges { get; set; }

        [BsonIgnore]
        [JsonIgnore]
        public string ApproveLink { get; set; }

        [BsonIgnore]
        [JsonIgnore]
        public string RejectLink { get; set; }

        [BsonIgnore]
        public virtual string Type { get { return GetType().Name; } }
    }

    [ClassDoc("Sync attribute change", Name = "Sync Attribute Change")]
    public class SyncAttributeChange
    {
        [BsonElement("o")]
        public object Original { get; set; }

        [BsonElement("v")]
        public object Value { get; set; }

        public SyncAttributeChange() { }

        public SyncAttributeChange(object original, object value)
        {
            var oType = (original ?? "").GetType();
            var vType = (value ?? "").GetType();
            if (oType.Name == "SyncRef") { original = original.ToString(); }
            if (vType.Name == "SyncRef") { value = value.ToString(); }

            Original = original;
            Value = value;
        }
    }
}
