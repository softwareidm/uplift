﻿using SoftwareIDM.PanelModel.Attributes;
using System;

namespace SoftwareIDM.PanelModel.Models
{
    [ClassDoc("Object Record representation of HyperSync metaverse object", Name = "HyperSync Record", RuleDoc = true)]
    public class HyperSyncRecord : ObjectRecord
    {
    }

    public class HyperSyncHistory : HistoryRecord
    {
        public override Type ObjectDetailType => typeof(SyncAction);

        public HyperSyncHistory(bool delta) : base()
        {
            Provider = ScheduleConstants.ProviderId;
            RecordOf = "HyperSync Panel";
            Argument = delta ? "Delta": "Full";
            Result = "success";
            StartDate = DateTime.UtcNow;
        }
    }
}
