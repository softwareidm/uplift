﻿using System;
using System.Collections.Generic;
using SoftwareIDM.PanelModel.Attributes;
using SoftwareIDM.PanelModel.Helpers;

namespace SoftwareIDM.PanelModel.Models
{
    [MongoDB.Bson.Serialization.Attributes.BsonIgnoreExtraElements]
    [ValidateStep]
    [ClassDoc("Run an arbitrary program or script", Name = "Run Program")]
    public class RunProgramStep : ScheduleStep
    {
        [MemberDoc("Name of program to run. If it is not on the PATH variable, the full file system path is required.")]
        [UI(UIAttrKind.Text, Required = true, Size = 3, Header = true, NoLabel = true, RuleHelp = true, RuleBraces = true)]
        public string Program { get; set; }

        [MemberDoc("Arguments to pass to the program.", Name = "Arguments")]
        [UI(UIAttrKind.Text, Size = 6, Header = true, NoLabel = true, RuleHelp = true, RuleBraces = true)]
        public string Args { get; set; }

        [UI(RuleObject = "SoftwareIDM.PanelModel.Models.RunProgramStep, SoftwareIDM.PanelModel")]
        public override string ConditionRule {
            get { return base.ConditionRule; }
            set { base.ConditionRule = value; }
        }

        [UI(RuleObject = "SoftwareIDM.PanelModel.Models.ProgramHistoryRecord, SoftwareIDM.PanelModel")]
        public override string SkipRule
        {
            get { return base.SkipRule; }
            set { base.SkipRule = value; }
        }

        public override List<ScheduleLock> Locks(string scheduler)
        {
            return new List<ScheduleLock> { new ScheduleLock(scheduler + Program + Args, ScheduleLockLevel.Exclusive) };
        }

        public override string Name
        {
            get { return string.IsNullOrEmpty(Args) ? Program : Program + " " + Args; }
        }

        public override Type RunType()
        {
            return TypeHelper.Get("Steps", GetType().Name, "PanelClient");
        }
    }
}
