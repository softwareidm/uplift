﻿using System;
using System.Collections.Generic;
using System.Linq;
using SoftwareIDM.PanelModel.Attributes;
using SoftwareIDM.PanelModel.Helpers;
using MongoDB.Bson.Serialization.Attributes;

namespace SoftwareIDM.PanelModel.Models
{
    [ClassDoc(
@"Looks up the most recent value of a performance counter. <br />
To lookup Category, Intance, and Counter names: 
<ol>
<li>Run MMC</li><li>Add Performance Monitoring snapin</li>
<li>Create a user defined Data collector set</li><li>Add a new Data Collector and choose ""Performance counter data collector""</li>
<li>Add performance counters</li>
<li>As you select counters, note the category, instance, and counter names.</li>
</ol>
Result <code>Value</code> is the float value, <code>StringValue</code> is the value as a string. There is no default fail rule.", "Performance Counter")]
    public class PerformanceProbe : HealthProbe
    {
        [UI(UIAttrKind.Text, true, NoLabel = true, Header = true)]
        [MemberDoc(Name = "Counter Name")]
        public string CounterName { get; set; }

        [UI(UIAttrKind.Text, true, Default = "localhost", RuleHelp = true, RuleBraces = true, RuleObject = "SoftwareIDM.PanelModel.Models.Health, SoftwareIDM.PanelModel")]
        [MemberDoc(Description = "Server to retrieve performance counter from")]
        public string Computer { get; set; }

        [UI(UIAttrKind.Text, true)]
        [MemberDoc(Name = "Category Name")]
        public string CategoryName { get; set; }

        [UI(UIAttrKind.Text)]
        [MemberDoc(Name = "Instance Name")]
        public string InstanceName { get; set; }

        [MemberDoc("Rule to reformat raw performance counter (type double) into user-friendly display value", Name = "Display Value Rule")]
        [UI(UIAttrKind.Text, RuleHelp = true, Description = true)]
        public string StringValueRule { get; set; }

        public override Type RunType()
        {
            return TypeHelper.Get("Steps", GetType().Name, "PanelClient");
        }
    }

    [BsonDiscriminator("pfrpr", Required = true), ClassDoc("Probe result for performance counter", "Performance Probe Result")]
    public class PerformanceProbeResult : ProbeResult
    {
        public PerformanceProbeResult()
        {
            Instances = new Dictionary<string, double>();
        }

        [BsonDictionaryOptions(Representation = MongoDB.Bson.Serialization.Options.DictionaryRepresentation.ArrayOfArrays)]
        [BsonElement("i")]
        [MemberDoc("Per instance results.")]
        public Dictionary<string, double> Instances { get; set; }

        [BsonElement("fr")]
        public string FormatRule { get; set; }

        public override string StringValue
        {
            get
            {
                if (Instances.Count == 0)
                {
                    if (!string.IsNullOrEmpty(FormatRule))
                    {
                        return Rules.RuleEngine.GetValue(FormatRule, Value, null).ToString() ?? "N/A";
                    }

                    return (Value ?? "N/A").ToString();
                }

                if (!string.IsNullOrEmpty(FormatRule))
                {
                    string.Join(", ", from d in Instances
                                      select $"{d.Key}: {Rules.RuleEngine.GetValue(FormatRule, d.Value, null).ToString() ?? "N/A"}");
                }

                return string.Join(", ", from d in Instances
                                         select $"{d.Key}: {d.Value}");
            }
        }
    }
}
