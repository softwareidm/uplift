﻿using System;
using System.Collections.Generic;
using SoftwareIDM.PanelModel.Attributes;
using SoftwareIDM.PanelModel.Helpers;

namespace SoftwareIDM.PanelModel.Models.Steps
{
    [MongoDB.Bson.Serialization.Attributes.BsonIgnoreExtraElements]
    [ClassDoc(@"Reset Scan Cache.<br/>
WARNING: this step should be only be used to reset cached hashes files silos for scans that are not currently running.", Name = "Reset Scan Cache")]
    public class ResetScanCache : ScheduleStep
    {
        [MemberDoc("Server to clear selected cache files from data folder on.", Name = "Server to Reset")]
        [UI(UIAttrKind.Select, Header = true, Choices = "js:helpers.panelServiceInstances", Size = 6)]
        public override List<string> PreferredSchedulers { get => base.PreferredSchedulers; set => base.PreferredSchedulers = value; }

        [MemberDoc("List of silos to reset", Name = "Silos to Reset")]
        [UI(UIAttrKind.Select, Required = true, Size = 12, Choices = "js:helpers.silosSelect")]
        public List<string> Silos { get; set; }

        public override List<ScheduleLock> Locks(string scheduler)
        {
            return new List<ScheduleLock> { new ScheduleLock { Name = scheduler, Level = ScheduleLockLevel.Exclusive } };
        }

        public override string Name => $"Reset Silo Cache {string.Join(", ", Silos)} on {string.Join(", ", PreferredSchedulers)}";

        public override Type RunType()
        {
            return TypeHelper.Get("Steps", GetType().Name, "PanelClient");
        }
    }
}
