﻿using System;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using SoftwareIDM.PanelModel.Attributes;
using SoftwareIDM.PanelModel.Helpers;

namespace SoftwareIDM.PanelModel.Models
{
    [ClassDoc("Value for downloading a parameterized report", Name = "Report Parameter Value")]
    public class ReportParameterValue
    {
        [UI(UIAttrKind.Select, Size = 3, Required = true, Header = true, NoLabel = true, Choices = "js:helpers.report.parameterSelect")]
        public string Parameter { get; set; }

        [UI(UIAttrKind.Text, Size = 6, Required = true, Header = true, NoLabel = true, RuleHelp = true, RuleBraces = true)]
        public string Value { get; set; }
    }

    [ClassDoc("Report download definition", Name = "Report Download Definition")]
    public class ReportDefinition
    {
        [MemberDoc("Name of the report to send", Name = "Report")]
        [UI(UIAttrKind.Select, Size = 4, Required = true, Header = true, NoLabel = true, Choices = "js:helpers.report.stepSelect")]
        public string Report { get; set; }
        
        [MemberDoc("Download file type", Name = "File Type")]
        [UI(UIAttrKind.Select, Size = 4, Required = true, Choices = "application/vnd.ms-excel:Excel|text/html:HTML|text/plain:Delimited|application/json:JSON|text/xml:XML")]
        public string FileType { get; set; }

        [MemberDoc(Name = "Row Limit")]
        [UI(UIAttrKind.Text, Size = 6, Default = "0")]
        public int Limit { get; set; }

        [JsonProperty(ItemTypeNameHandling = TypeNameHandling.Objects)]
        [MemberDoc("List of parameters to use in generating a parameterized report. Specify a value for each parameter in the desired report.", Name = "Report Parameters")]
        [UI(UIAttrKind.Strips, Description = true)]
        public List<ReportParameterValue> ReportParameters { get; set; }
    }

    [BsonIgnoreExtraElements]
    [ClassDoc("Send a report", Name = "Send Report")]
    public class SendReport : ScheduleStep
    {
        [MemberDoc("Who to send report to, comma separated", Name="Recipient")]
        [UI(UIAttrKind.Text, Size = 4, Required = true, Header = true, NoLabel = true, RuleHelp = true, RuleBraces = true)]
        public string Recipient { get; set; }

        [MemberDoc("Message from override", Name = "From")]
        [UI(UIAttrKind.Text, Size = 6, RuleHelp = true, RuleBraces = true)]
        public string From { get; set; }

        [MemberDoc("Message cc, comma separated", Name = "CC")]
        [UI(UIAttrKind.Text, Size = 6, RuleHelp = true, RuleBraces = true)]
        public string CC { get; set; }

        [MemberDoc("Message bcc, comma separated", Name = "BCC")]
        [UI(UIAttrKind.Text, Size = 6, RuleHelp = true, RuleBraces = true)]
        public string BCC { get; set; }

        [UI(UIAttrKind.Text, Size = 4, Header = true, NoLabel = true, Required = true, RuleHelp = true, RuleBraces = true)]
        public string Subject { get; set; }

        [UI(UIAttrKind.LongText, Size = 6, Required = true, RuleHelp = true, RuleBraces = true)]
        public string Message { get; set;}

        [MemberDoc("Whether to treat the email body as HTML", Name = "Send as HTML")]
        [UI(UIAttrKind.Bool, Size = 6)]
        public bool SendAsHtml { get; set; }

        [JsonProperty(ItemTypeNameHandling = TypeNameHandling.Objects)]
        [MemberDoc("List of reports to download and attach or embed", Name = "Reports")]
        [UI(UIAttrKind.Strips, Duplicate = true, Description = true)]
        public List<ReportDefinition> Reports { get; set; }

        [UI(RuleObject = "SoftwareIDM.PanelModel.Models.SendReportStep, SoftwareIDM.PanelModel")]
        public override string ConditionRule
        {
            get { return base.ConditionRule; }
            set { base.ConditionRule = value; }
        }

        [UI(RuleObject = "SoftwareIDM.PanelModel.Models.HistoryRecord, SoftwareIDM.PanelModel")]
        public override string SkipRule
        {
            get { return base.SkipRule; }
            set { base.SkipRule = value; }
        }

        public override List<ScheduleLock> Locks(string scheduler)
        {
            return (from p in Reports
                    select new ScheduleLock(scheduler + p.Report.Split('|')[0], ScheduleLockLevel.Exclusive)).Distinct().ToList();
        }

        public override string Name
        {
            get
            {
                return $"Send Report {Subject}";
            }
        }

        public override Type RunType()
        {
            return TypeHelper.Get("Steps", GetType().Name, "PanelClient");
        }

        public SendReport()
        {
            Reports = new List<ReportDefinition>();
        }
    }

    [BsonIgnoreExtraElements]
    [ClassDoc("Download reports to the filesystem", Name = "Download Report")]
    public class DownloadReport : ScheduleStep
    {

        [JsonProperty(ItemTypeNameHandling = TypeNameHandling.Objects)]
        [MemberDoc("List of reports to download and attach or embed", Name = "Reports")]
        [UI(UIAttrKind.Strips, Duplicate = true, Description = true)]
        public List<ReportDefinition> Reports { get; set; }

        [UI(UIAttrKind.Text, Required = true, Header = true, NoLabel = true, Size = 6)]
        [MemberDoc("Directory to save report to.")]
        public string Directory { get; set; }

        [MemberDoc("Rule engine expression receives a Report Definition and returns a file name (with extension) to save the report as.", Name="File Rule")]
        [UI(UIAttrKind.LongText, RuleHelp = true, RuleObject = "SoftwareIDM.PanelModel.Models.ReportDefinition, SoftwareIDM.PanelModel", Description = true, Default = "$\"{Slugify(Split(Report, \"\\|\").1)} {DateTimeFormat(Now(), \"yy-MM-dd HH-mm-ss\")}.txt\"")]
        public string FileRule { get; set; }

        [UI(RuleObject = "SoftwareIDM.PanelModel.Models.DownloadReportStep, SoftwareIDM.PanelModel")]
        public override string ConditionRule
        {
            get { return base.ConditionRule; }
            set { base.ConditionRule = value; }
        }

        [UI(RuleObject = "SoftwareIDM.PanelModel.Models.HistoryRecord, SoftwareIDM.PanelModel")]
        public override string SkipRule
        {
            get { return base.SkipRule; }
            set { base.SkipRule = value; }
        }

        public override List<ScheduleLock> Locks(string scheduler)
        {
            return (from p in Reports
                    select new ScheduleLock(scheduler + p.Report.Split('|')[0], ScheduleLockLevel.Exclusive)).ToList();
        }

        public override string Name
        {
            get
            {
                return "Download Report " + string.Join(", ", from p in Reports
                                                          select p.Report.Split('|')[1]);
            }
        }

        public override Type RunType()
        {
            return TypeHelper.Get("Steps", GetType().Name, "PanelClient");
        }

        public DownloadReport()
        {
            Reports = new List<ReportDefinition>();
        }
    }
}
