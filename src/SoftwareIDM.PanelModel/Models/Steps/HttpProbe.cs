﻿using System;
using SoftwareIDM.PanelModel.Attributes;
using SoftwareIDM.PanelModel.Helpers;

namespace SoftwareIDM.PanelModel.Models
{
    [ClassDoc(Name = "Http Request", Description = "Gets response code of HTTP GET. <br />Result <code>Value</code> is numeric HTTP response code or null. The default value of <code>(Value >= 400) && (Value < 200)</code> will fail if the result is not in the 300 or 200 range.")]
    public class HttpProbe : HealthProbe
    {
        [UI(UIAttrKind.Text, true, NoLabel = true, Header = true, RuleHelp = true, RuleBraces = true, RuleObject = "SoftwareIDM.PanelModel.Models.Health, SoftwareIDM.PanelModel")]
        [MemberDoc("The Url to request.")]
        public string Url { get; set; }

        [UI(UIAttrKind.Text)]
        [MemberDoc("The username to connect with (optional).")]
        public string User { get; set; }

        [UI(UIAttrKind.Encrypted)]
        [MemberDoc("The password to connect with (optional).")]
        public string Password { get; set; }
        
        [UI(Default = "(Value >= 400) && (Value < 200)")]
        public override string FailRule { get { return base.FailRule; } set { base.FailRule = value; } }

        public override Type RunType()
        {
            return TypeHelper.Get("Steps", GetType().Name, "PanelClient");
        }
    }
}
