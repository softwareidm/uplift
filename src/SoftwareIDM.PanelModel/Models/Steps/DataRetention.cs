﻿using System;
using System.Collections.Generic;
using SoftwareIDM.PanelModel.Attributes;
using SoftwareIDM.PanelModel.Helpers;

namespace SoftwareIDM.PanelModel.Models
{

    [MongoDB.Bson.Serialization.Attributes.BsonIgnoreExtraElements]
    [ClassDoc("Purge stored data older than the specified date. Step is deprecated. Use Data Retention security setting instead.", Name = "Data Retention - Deprecated", Deprecated = true)]
    public class DataRetentionStep : ScheduleStep
    {
        [MemberDoc("Multiplier for Unit to determine timespan")]
        [UI(UIAttrKind.Text, Required = true, Size = 2, Header = true, NoLabel = true, TypeValid = UIAttrType.Double)]
        public double Quantity { get; set; }

        [MemberDoc("Time unit for determining retention period.")]
        [UI(UIAttrKind.Select, Size = 3, Header = true, NoLabel = true, Choices = "js:helpers.timeUnitSelect")]
        public string Unit { get; set; }

        [UI(RuleObject = "SoftwareIDM.PanelModel.Models.DataRetentionStep, SoftwareIDM.PanelModel")]
        public override string ConditionRule { get { return base.ConditionRule; } set { base.ConditionRule = value; } }

        [UI(RuleObject = "SoftwareIDM.PanelModel.Models.HistoryRecord, SoftwareIDM.PanelModel")]
        public override string SkipRule { get { return base.SkipRule; } set { base.SkipRule = value; } }

        public override List<ScheduleLock> Locks(string scheduler)
        {
            return new List<ScheduleLock> { new ScheduleLock(Name, ScheduleLockLevel.Exclusive) };
        }

        public override string Name
        {
            get { return "Data Retention Policy"; }
        }

        public override Type RunType()
        {
            return TypeHelper.Get("Steps", GetType().Name, "PanelClient");
        }
    }
}
