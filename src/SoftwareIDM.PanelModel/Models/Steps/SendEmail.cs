﻿using System;
using System.Collections.Generic;
using SoftwareIDM.PanelModel.Attributes;
using SoftwareIDM.PanelModel.Helpers;

namespace SoftwareIDM.PanelModel.Models
{
    [MongoDB.Bson.Serialization.Attributes.BsonIgnoreExtraElements]
    [ClassDoc("Send an email", Name = "Send Email")]
    public class SendEmail : ScheduleStep
    {
        [MemberDoc("Who to send message to, comma separated", Name="Recipient")]
        [UI(UIAttrKind.Text, Size = 6, Required = true, Header = true, NoLabel = true, RuleHelp = true, RuleBraces = true)]
        public string Recipient { get; set; }

        [MemberDoc("Message from override", Name = "From")]
        [UI(UIAttrKind.Text, Size = 6, RuleHelp = true, RuleBraces = true)]
        public string From { get; set; }

        [MemberDoc("Message cc, comma separated", Name = "CC")]
        [UI(UIAttrKind.Text, Size = 6, RuleHelp = true, RuleBraces = true)]
        public string CC { get; set; }

        [MemberDoc("Message bcc, comma separated", Name = "BCC")]
        [UI(UIAttrKind.Text, Size = 6, RuleHelp = true, RuleBraces = true)]
        public string BCC { get; set; }

        [UI(UIAttrKind.Text, Size = 6, Required = true, RuleHelp = true, RuleBraces = true)]
        public string Subject { get; set; }

        [UI(UIAttrKind.LongText, Size = 6, Required = true, RuleHelp = true, RuleBraces = true)]
        public string Message { get; set;}

        [MemberDoc("Whether to treat the email body as HTML (default is plain text)", Name = "Send as HTML")]
        [UI(UIAttrKind.Bool, Size = 6, Description = true)]
        public bool SendAsHtml { get; set; }

        [MemberDoc("Whether to send text message instead of email (requires Twilio API)", Name = "Send as SMS")]
        [UI(UIAttrKind.Bool, Size = 6)]
        public bool SendAsSMS { get; set; }

        [UI(RuleObject = "SoftwareIDM.PanelModel.Models.SendEmail, SoftwareIDM.PanelModel")]
        public override string ConditionRule
        {
            get { return base.ConditionRule; }
            set { base.ConditionRule = value; }
        }

        [UI(RuleObject = "SoftwareIDM.PanelModel.Models.HistoryRecord, SoftwareIDM.PanelModel")]
        public override string SkipRule
        {
            get { return base.SkipRule; }
            set { base.SkipRule = value; }
        }

        public override List<ScheduleLock> Locks(string scheduler)
        {
            return new List<ScheduleLock> { new ScheduleLock(scheduler + "Email", ScheduleLockLevel.Exclusive) };
        }

        public override string Name
        {
            get { return "Send Email " + Subject; }
        }

        public override Type RunType()
        {
            return TypeHelper.Get("Steps", GetType().Name, "PanelClient");
        }
    }
}
