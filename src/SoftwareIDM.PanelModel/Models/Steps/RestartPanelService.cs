﻿using System;
using System.Collections.Generic;
using SoftwareIDM.PanelModel.Attributes;
using SoftwareIDM.PanelModel.Helpers;

namespace SoftwareIDM.PanelModel.Models.Steps
{
    [MongoDB.Bson.Serialization.Attributes.BsonIgnoreExtraElements]
    [ClassDoc(@"Restart Panel Service.<br/>
WARNING: this step should only be run inside a schedule marked with the Exclusive flag, because it will kill the operating instance of Panel Service. <br/>
To run this step, Panel service account must have full control permission on the SoftwareIDM.PanelService service.<br/>
This may be granted using the Panel Service setup utility.", Name = "Restart Panel Service")]
    public class RestartPanelService : ScheduleStep
    {
        [MemberDoc("Server to run Upgrade Panel Tools step on.", Name = "Server to Restart Panel Service")]
        [UI(UIAttrKind.Select, Header = true, Choices = "js:helpers.panelServiceInstances", Size = 6)]
        public override List<string> PreferredSchedulers { get => base.PreferredSchedulers; set => base.PreferredSchedulers = value; }

        public override List<ScheduleLock> Locks(string scheduler)
        {
            return new List<ScheduleLock> { new ScheduleLock { Name = scheduler, Level = ScheduleLockLevel.Exclusive } };
        }

        public override string Name => "Restart Panel Service on " + string.Join(", ", PreferredSchedulers);

        public override Type RunType()
        {
            return TypeHelper.Get("Steps", GetType().Name, "PanelClient");
        }
    }
}
