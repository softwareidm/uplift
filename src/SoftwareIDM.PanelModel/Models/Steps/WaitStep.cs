﻿using System;
using System.Collections.Generic;
using SoftwareIDM.PanelModel.Attributes;
using SoftwareIDM.PanelModel.Helpers;

namespace SoftwareIDM.PanelModel.Models
{
    [MongoDB.Bson.Serialization.Attributes.BsonIgnoreExtraElements]
    [ClassDoc("Wait for a timespan before continuing to other steps", Name = "Wait Delay")]
    public class WaitStep : ScheduleStep
    {
        [MemberDoc("Duration of time to wait. If blank or 0 the step will complete immediately.", Name = "Time Span")]
        [UI(UIAttrKind.Text, Size = 4, Header = true, NoLabel = true, Placeholder = "0.00:00:00 - Wait", TypeValid = UIAttrType.TimeSpan)]
        public string TimeSpan { get; set; }

        public override List<ScheduleLock> Locks(string scheduler)
        {
            return new List<ScheduleLock>();
        }

        public override string Name
        {
            get { return "Wait " + TimeSpan; }
        }

        public override Type RunType()
        {
            return TypeHelper.Get("Steps", GetType().Name, "PanelClient");
        }
    }
}
