﻿using System;
using SoftwareIDM.PanelModel.Attributes;
using SoftwareIDM.PanelModel.Helpers;

namespace SoftwareIDM.PanelModel.Models
{

    [ClassDoc(Name = "EWS Available", Description = "Checks that EWS is available. <br />Result <code>Value</code> is boolean, StringValue is \"Available\" or \"Connect Failed\".")]
    public class EWSProbe : HealthProbe
    {
        [UI(UIAttrKind.Text, true, NoLabel = true, Header = true, Size = 6, RuleHelp = true, RuleBraces = true, RuleObject = "SoftwareIDM.PanelModel.Models.Health, SoftwareIDM.PanelModel")]
        [MemberDoc(Name = "Exchange Host", Description = "FQDN of Exchange Web Service host")]
        public string Host { get; set; }

        [UI(UIAttrKind.Bool)]
        [MemberDoc(Name = "Use SSL")]
        public bool UseSSL { get; set; }

        [UI(UIAttrKind.Text),
        MemberDoc("The username to connect with (optional).")]
        public string User { get; set; }

        [UI(UIAttrKind.Encrypted)]
        [MemberDoc("The password to connect with (optional).")]
        public string Password { get; set; }

        [UI(Default = "Not(Value)")]
        public override string FailRule { get { return base.FailRule; } set { base.FailRule = value; } }

        public override Type RunType()
        {
            return TypeHelper.Get("Steps", GetType().Name, "PanelClient");
        }
    }
}
