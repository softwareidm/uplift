﻿using System;
using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using SoftwareIDM.PanelModel.Attributes;
using SoftwareIDM.PanelModel.Helpers;

namespace SoftwareIDM.PanelModel.Models
{
    [ClassDoc("Connection properties for PowerShell steps")]
    public class PowerShellConnection
    {
        [BsonElement("ur"), BsonIgnoreIfDefault]
        [MemberDoc(Name = "Use PS Remoting", Description = "Whether to use PowerShell Remoting settings to connect to a remote URI with credentials. Most settings are ignored if Use Remoting is False.")]
        [UI(UIAttrKind.Bool)]
        public bool UseRemoting { get; set; }

        [BsonElement("hs"), BsonIgnoreIfNull]
        [UI(UIAttrKind.Text)]
        public string Host { get; set; }

        [MemberDoc(Name = "User", Description = "User is optional, enter in format appropriate to selected authentication mechanism")]
        [UI(UIAttrKind.Text, Size = 4)]
        public string User { get; set; }

        [BsonElement("pw"), BsonIgnoreIfNull]
        [MemberDoc("Password may alternatively be specified in the Panel:Password app.config setting")]
        [UI(UIAttrKind.Encrypted, Size = 4)]
        public string Password { get; set; }


        [BsonElement("am"), BsonIgnoreIfNull]
        [MemberDoc(Name = "Auth Mechanism")]
        [UI(UIAttrKind.Select, Default = "Default", Size = 4, Choices = "Default|Basic|Credssp|Digest|Kerberos|Negotiate|NegotiateWithImplicitCredential")]
        public string AuthMechanism { get; set; }


        [BsonElement("uri"), BsonIgnoreIfNull]
        [MemberDoc(Name = "URI", Description = "URI Path after Host")]
        [UI(UIAttrKind.Text, Default = "/wsman")]
        public string Uri { get; set; }
        
        [BsonElement("sri"), BsonIgnoreIfNull]
        [MemberDoc(Name = "Schema URI")]
        [UI(UIAttrKind.Text, Default = "http://schemas.microsoft.com/powershell/Microsoft.PowerShell")]
        public string SchemaUri { get; set; }
        
        [BsonElement("ss"), BsonIgnoreIfDefault]
        [MemberDoc(Name = "Use SSL")]
        [UIAttribute(UIAttrKind.Bool, Size = 4)]
        public bool UseSSL { get; set; }
        
        [BsonElement("pr"), BsonIgnoreIfDefault]
        [UI(UIAttrKind.Text, true, Size = 4, Default = "5985", TypeValid = UIAttrType.Int32)]
        public int Port { get; set; }

        [BsonElement("ts"), BsonIgnoreIfDefault]
        [MemberDoc(Name = "Operation Timeout Seconds")]
        [UI(UIAttrKind.Text, true, Size = 4, Default = "240", TypeValid = UIAttrType.Int32)]
        public int TimeoutSeconds { get; set; }

        [BsonElement("mr"), BsonIgnoreIfDefault]
        [MemberDoc(Name = "Max Redirect Count")]
        [UI(UIAttrKind.Text, true, Size = 4, Default = "3", TypeValid = UIAttrType.Int32)]
        public int MaxRedirect { get; set; }
    }

    [ClassDoc("Parameter rule for using rule engine with PowerShell script", Name = "PowerShell Parameter")]
    public class PowerShellParameter
    {
        [MemberDoc("Parameter name")]
        [UI(UIAttrKind.Text, Required = true, Unique = true, NoLabel = true, Header = true, Size = 2)]
        public string Name { get; set; }

        [MemberDoc("Parameter value")]
        [UI(UIAttrKind.Text, NoLabel = true, Header = true, Size = 8, RuleHelp = true, RuleBraces = true)]
        public string Value { get; set; }
    }

    [BsonIgnoreExtraElements]
    [ValidateStep]
    [ClassDoc("Run scheduled PowerShell Step.", Name = "Run PowerShell")]
    public class PowerShellStep : ScheduleStep
    {
        [MemberDoc(Name = "Name")]
        [UI(UIAttrKind.Text, Required = true, Size = 3, Header = true, NoLabel = true)]
        public string ScriptName { get; set; }

        [JsonProperty(ItemTypeNameHandling = TypeNameHandling.Objects)]
        [MemberDoc("List of parameters to pass to PowerShell script, use Rule Engine templating for calculated values", Name = "Parameters")]
        [UI(UIAttrKind.Strips, Description = true)]
        public List<PowerShellParameter> Parameters { get; set; } = new List<PowerShellParameter>();

        [BlockRule]
        [UI(UIAttrKind.LongText, true, CSSClass="input-monospace", RuleHelp = true, RuleWarnOnly = true, RuleLang = "PowerShell")]
        public string Script { get; set; }

        [UI(UIAttrKind.Object)]
        public PowerShellConnection Connection { get; set; }

        [UI(RuleObject = "SoftwareIDM.PanelModel.Models.PowerShellStep, SoftwareIDM.PanelModel")]
        public override string ConditionRule { get { return base.ConditionRule; } set { base.ConditionRule = value; } }

        [UI(RuleObject = "SoftwareIDM.PanelModel.Models.ProgramHistoryRecord, SoftwareIDM.PanelModel")]
        public override string SkipRule { get { return base.SkipRule; } set { base.SkipRule = value; } }

        public override List<ScheduleLock> Locks(string scheduler)
        {
            return new List<ScheduleLock> { new ScheduleLock("PowerShell:" + ScriptName, ScheduleLockLevel.Exclusive) };
        }

        public override string Name
        {
            get { return ScriptName; }
        }

        public override Type RunType()
        {
            return TypeHelper.Get("Steps", GetType().Name, "PanelClient");
        }
    }

    [ValidateStep]
    [ClassDoc("Run PowerShell Script Probe.", Name = "PowerShell Probe")]
    public class PowerShellProbe : HealthProbe
    {
        [JsonProperty(ItemTypeNameHandling = TypeNameHandling.Objects)]
        [MemberDoc("List of parameters to pass to PowerShell script, use Rule Engine templating for calculated values", Name = "Parameters")]
        [UI(UIAttrKind.Strips, Description = true)]
        public List<PowerShellParameter> Parameters { get; set; } = new List<PowerShellParameter>();

        [BlockRule]
        [UI(UIAttrKind.LongText, true, CSSClass = "input-monospace", RuleHelp = true, RuleWarnOnly = true, RuleLang = "PowerShell")]
        public string Script { get; set; }

        [UI(UIAttrKind.Object)]
        public PowerShellConnection Connection { get; set; }

        public override Type RunType()
        {
            return TypeHelper.Get("Steps", GetType().Name, "PanelClient");
        }
    }
}
