﻿using System;
using System.Collections.Generic;
using SoftwareIDM.PanelModel.Attributes;
using SoftwareIDM.PanelModel.Helpers;

namespace SoftwareIDM.PanelModel.Models
{

    [ClassDoc("Query history and evaluate results. <br />The default Object Rule value of <code>Count</code> with the Fail rule value of <code>Value == 0</code> will fail if there are no results in the time window.", "History Search")]
    public class HistoryProbe : HealthProbe
    {
        [UI(UIAttrKind.Select, Choices = "js:helpers.providerSelect")]
        [MemberDoc(Name = "Provider", Description = "Filter based on Provider value.")]
        public string Provider { get; set; }

        [UI(UIAttrKind.Select, Choices = "js:helpers.recordSelect")]
        [MemberDoc(Name = "History Type", Description = "Filter based on Record Of value.")]
        public string RecordOf { get; set; }

        [UI(UIAttrKind.Select, Choices = "js:helpers.argumentSelect")]
        [MemberDoc(Name = "Argument", Description = "Filter based on Argument value.")]
        public string Argument { get; set; }

        [UI(UIAttrKind.Select, Choices = "js:helpers.resultSelect")]
        [MemberDoc(Name = "Result", Description = "Filter based on Result value.")]
        public string Result { get; set; }

        [UI(UIAttrKind.Select, true, Choices = "js:helpers.timeUnitSelect")]
        [MemberDoc(Name = "Time Filter Unit", Description = "Filter based on run started after value.")]
        public string FromUnit { get; set; }

        [UI(UIAttrKind.Text, true, Size = 6, TypeValid = UIAttrType.Int32)]
        [MemberDoc(Name = "Time Filter Quantity", Description = "Filter based on run started after value.")]
        public int FromQuantity { get; set; }

        [UI(UIAttrKind.Text, true, Description = true, RuleHelp = true, Default = "Count", RuleObject = "SoftwareIDM.PanelModel.Models.HistoryRecord, SoftwareIDM.PanelModel")]
        [MemberDoc(Name = "Object Rule", Description = "Convert returned history data set into a single value to place in the Probe Result Value property. Data Structure is { Count: n Data: [HistoryRecord] }")]
        public string ObjectRule { get; set; }

        [UI(Default = "Value == 0", RuleObject = "SoftwareIDM.PanelModel.Models.ProbeResult, SoftwareIDM.PanelModel")]
        public override string FailRule { get { return base.FailRule; } set { base.FailRule = value; } }

        public override Type RunType()
        {
            return TypeHelper.Get("Steps", GetType().Name, "PanelClient");
        }
    }
}
