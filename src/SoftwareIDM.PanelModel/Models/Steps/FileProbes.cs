﻿using System;
using MongoDB.Bson.Serialization.Attributes;
using SoftwareIDM.PanelModel.Attributes;
using SoftwareIDM.PanelModel.Helpers;

namespace SoftwareIDM.PanelModel.Models
{
    [ClassDoc("Time since File Created or Modified. <br />Result <code>Value</code> is TimeSpan. The default fail rule of <code>Value > Hours(24)</code> will fail if the file has not been updated in the last 24 hours.", "File Time Stamp")]
    public class FileTimeStampProbe : HealthProbe
    {
        [UI(UIAttrKind.Text, true, NoLabel = true, Header = true, RuleHelp = true, RuleBraces = true, RuleObject = "SoftwareIDM.PanelModel.Models.Health, SoftwareIDM.PanelModel")]
        [MemberDoc(Name = "File Name", Description = "The full UNC path to the file.")]
        public string FileName { get; set; }

        [UI(UIAttrKind.Select, true, Choices = "Created|Modified")]
        [MemberDoc(Name = "Time Stamp", Description = "Which timestamp to read.")]
        public string TimeStamp { get; set; }

        [UI(Default = "Value > Hours(24)")]
        public override string FailRule { get { return base.FailRule; } set { base.FailRule = value; } }

        public override Type RunType()
        {
            return TypeHelper.Get("Steps", GetType().Name, "PanelClient");
        }
    }

    [ClassDoc("Verify that file exists. <br />Result <code>Value</code> is boolean. StringValue is \"Exists\" or \"Does Not Exist\". The default fail rule of <code>Not(Value)</code> will fail if the file does not exist.", "File Exists")]
    public class FileExistsProbe : HealthProbe
    {
        [UI(UIAttrKind.Text, true, NoLabel = true, Header = true, RuleHelp = true, RuleBraces = true, RuleObject = "SoftwareIDM.PanelModel.Models.Health, SoftwareIDM.PanelModel")]
        [MemberDoc(Name = "File Name", Description = "The full UNC path to the file.")]
        public string FileName { get; set; }

        [UI(Default = "Not(Value)")]
        public override string FailRule { get { return base.FailRule; } set { base.FailRule = value; } }

        public override Type RunType()
        {
            return TypeHelper.Get("Steps", GetType().Name, "PanelClient");
        }
    }

    [BsonDiscriminator("fepr", Required = true), ClassDoc(Name = "File Exists Probe Result")]
    public sealed class FileExistsProbeResult : ProbeResult
    {
        public override string StringValue
        {
            get
            {
                if (Value == null) { return "Does Not Exist"; }
                return (bool)Value ? "Exists" : "Does Not Exist";
            }
        }
    }

    [ClassDoc("Size of File. <br />Result <code>Value</code> is file length in bytes. StringValue is translated into string. The default fail rule of <code>Value < 1024</code> will fail if the file is less than 1 KiB", "File Size")]
    public class FileSizeProbe : HealthProbe
    {
        [UI(UIAttrKind.Text, true, NoLabel = true, Header = true, RuleHelp = true, RuleBraces = true, RuleObject = "SoftwareIDM.PanelModel.Models.Health, SoftwareIDM.PanelModel")]
        [MemberDoc(Name = "File Name", Description = "The full UNC path to the file.")]
        public string FileName { get; set; }

        [UI(Default = "Value < 1024")]
        public override string FailRule { get { return base.FailRule; } set { base.FailRule = value; } }

        public override Type RunType()
        {
            return TypeHelper.Get("Steps", GetType().Name, "PanelClient");
        }
    }

    [BsonDiscriminator("fspr", Required = true), ClassDoc(Name = "File Size Probe Result")]
    public sealed class FileSizeProbeResult : ProbeResult
    {
        static string FormatBytes(long bytes)
        {
            string[] suf = { "Bytes", "KB", "MB", "GB", "TB", "PB", "EB" };
            if (bytes == 0)
                return "0 Bytes";
            int place = Convert.ToInt32(Math.Floor(Math.Log(bytes, 1024)));
            double num = bytes / Math.Pow(1024, place);
            return string.Format("{0:N1} {1}", num, suf[place]);
        }

        public override string StringValue
        {
            get
            {
                if (Value == null || (long)Value == 0)
                {
                    return "Failed";
                }

                return FormatBytes((long)Value);
            }
        }
    }
}
