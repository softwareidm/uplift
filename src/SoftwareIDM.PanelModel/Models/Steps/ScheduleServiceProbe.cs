﻿using System;
using System.Collections.Generic;
using SoftwareIDM.PanelModel.Attributes;
using SoftwareIDM.PanelModel.Helpers;

namespace SoftwareIDM.PanelModel.Models
{

    [ClassDoc("Validate that schedule service is running on indicated servers. The default fail rule of <code>Value.Count</code> fails if a listed server has gone more than 5 minutes without checking in.", "Schedule Service Probe")]
    public class ScheduleServiceProbe : HealthProbe
    {
        [UI(UIAttrKind.Select, true, Choices = "js:helpers.panelServiceInstances")]
        [MemberDoc(Name = "Panel Services", Description = "List of Panel Services to check.")]
        public List<string> Servers { get; set; }

        [MemberDoc("Value is list of servers that have gone more than 5 minutes without schedule service checkin", Name = "Fail Rule")]
        [UI(Default = "Value.Count", Description = true)]
        public override string FailRule { get { return base.FailRule; } set { base.FailRule = value; } }

        public override Type RunType()
        {
            return TypeHelper.Get("Steps", GetType().Name, "PanelClient");
        }
    }
}
