﻿using System;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson.Serialization.Attributes;
using SoftwareIDM.PanelModel.Attributes;
using SoftwareIDM.PanelModel.Helpers;

namespace SoftwareIDM.PanelModel.Models
{
    [ClassDoc("Connection properties for database probes.")]
    public class DatabaseConnection
    {
        [UI(UIAttrKind.Text, true, Size = 3, RuleHelp = true, RuleBraces = true, RuleObject = "SoftwareIDM.PanelModel.Models.Health, SoftwareIDM.PanelModel")]
        [MemberDoc("The name of the server/instance the database is on.")]
        public string Server { get; set; }

        [UI(UIAttrKind.Text, Required = true, Size = 3, RuleHelp = true, RuleBraces = true, RuleObject = "SoftwareIDM.PanelModel.Models.Health, SoftwareIDM.PanelModel")]
        [MemberDoc("The name of the database check.")]
        public string Database { get; set; }

        [UI(UIAttrKind.Text, Size = 3)]
        [MemberDoc("The username to connect with (optional).")]
        public string User { get; set; }

        [UI(UIAttrKind.Encrypted, Size = 3)]
        [MemberDoc("The password to connect with (optional).")]
        public string Password { get; set; }
    }

    [BsonDiscriminator("apr", Required = true)]
    [ClassDoc(Name = "DB Available Probe Result")]
    public class AvailableProbeResult : ProbeResult
    {
        public override string StringValue
        {
            get
            {
                return (bool)Value ? "Available" : "Connect Failed";
            }
        }
    }

    [ClassDoc("Checks if a database can be connected to.<br />Probe Result <code>Value</code> is boolean, <code>StringValue</code> is \"Available\" or \"Connect Failed\". The default fail rule of <code>Not(Value)</code> will fail if the database is not available.", "SQL Available")]
    public class DBAvailableProbe : HealthProbe
    {
        [UI(UIAttrKind.Object)]
        public DatabaseConnection Connection { get; set; }

        [UI(Default = "Not(Value)")]
        public override string FailRule { get { return base.FailRule; } set { base.FailRule = value; } }

        public override Type RunType()
        {
            return TypeHelper.Get("Steps", GetType().Name, "PanelClient");
        }
    }

    [ClassDoc("Checks the size of the database or transaction log. <br />Probe Result <code>Value</code> is number of KBytes. StringValue is \"Failed\" or number with single decimal (e.g. 20.5 GB). There is no default fail rule.", "SQL Size")]
    public class DBSizeProbe : HealthProbe
    {
        [UI(UIAttrKind.Object)]
        public DatabaseConnection Connection { get; set; }

        [UI(UIAttrKind.Select, Choices = "Database:Database|Log:Log", Description = true, Default = "Database")]
        [MemberDoc("Whether to check size of database or transaction log.")]
        public string Check { get; set; }

        public override Type RunType()
        {
            return TypeHelper.Get("Steps", GetType().Name, "PanelClient");
        }
    }

    [ValidateStep]
    [ClassDoc("Executes a scalar SQL query and evaluates the result with the Fail Rule. There is no default fail rule.", "SQL Query")]
    public class DBQueryProbe : HealthProbe
    {
        [UI(UIAttrKind.Object)]
        public DatabaseConnection Connection { get; set; }

        [UI(UIAttrKind.LongText, Required = true)]
        [MemberDoc("SQL Query to execute.")]
        public string Query { get; set; }

        public override Type RunType()
        {
            return TypeHelper.Get("Steps", GetType().Name, "PanelClient");
        }
    }

    [BsonDiscriminator("dbspr", Required = true)]
    [ClassDoc(Name = "DB Size Probe Result")]
    public class DBSizeProbeResult : ProbeResult
    {
        static string FormatKBytes(long kbytes)
        {
            string[] suf = { "KB", "MB", "GB", "TB", "PB", "EB" };
            if (kbytes == 0)
                return "0 KB";
            int place = Convert.ToInt32(Math.Floor(Math.Log(kbytes, 1024)));
            double num = kbytes / Math.Pow(1024, place);
            return string.Format("{0:N1} {1}", num, suf[place]);
        }

        public override string StringValue
        {
            get
            {
                if (Value == null || (int)Value == 0)
                {
                    return "Failed";
                }

                return FormatKBytes((int)Value);
            }
        }
    }

    [ClassDoc(@"Checks how long it has been since a database backup was performed. <br />
Probe Result <code>Value</code> is TimeSpan. <code>StringValue</code> is user friendly printed. <br/>
The default fail rule of <code>CoerceTimeSpan(Value) > Days(7)</code> will fail if the database has not been backed up in the last week.", "SQL Backup")]
    public class DBBackupProbe : HealthProbe
    {
        [UI(UIAttrKind.Object)]
        public DatabaseConnection Connection { get; set; }

        [UI(Default = "CoerceTimeSpan(Value) > Days(7)")]
        public override string FailRule { get { return base.FailRule; } set { base.FailRule = value; } }

        public override Type RunType()
        {
            return TypeHelper.Get("Steps", GetType().Name, "PanelClient");
        }
    }

    [BsonDiscriminator("tspr", Required = true)]
    [ClassDoc(Name = "TimeSpan Probe Result")]
    public class TimeSpanProbeResult : ProbeResult
    {
        public override string StringValue
        {
            get
            {
                if (Value == null) { return "N/A"; };
                if (TimeSpan.TryParse(Value.ToString(), out TimeSpan val))
                {
                    return Url.FormatTimeSpan(val);
                }

                return "N/A";
            }
        }
    }

    [ClassDoc(@"Checks the free space on each disk attached to the SQL Server. <br />
Probe Result has a <code>Disks</code> dictionary property with drive letter as key, and number of MBytes as value. <code>StringValue</code> is comma separated list, with formatted values (e.g. D: 20.5 GB). <br/>
The default fail rule of <code>MapOr(Disks, Value <= 1000)</code> will fail if any attached drive has less than 1 GB of free space.", "SQL Disk Space")]
    public class DBDiskProbe : HealthProbe
    {
        [UI(UIAttrKind.Object)]
        public DatabaseConnection Connection { get; set; }

        [UI(Default = "MapOr(Disks, Value <= 1000)", RuleObject = "SoftwareIDM.PanelModel.Models.DBDiskProbeResult, SoftwareIDM.PanelModel")]
        public override string FailRule { get { return base.FailRule; } set { base.FailRule = value; } }

        public override Type RunType()
        {
            return TypeHelper.Get("Steps", GetType().Name, "PanelClient");
        }
    }

    [BsonDiscriminator("dbdpr", Required = true), ClassDoc("Probe result for database disk space", "DB Disk Probe Result")]
    public class DBDiskProbeResult : ProbeResult
    {
        public DBDiskProbeResult()
        {
            Disks = new Dictionary<string, int>();
        }

        static string FormatMBytes(long mbytes)
        {
            string[] suf = { "MB", "GB", "TB", "PB", "EB" };
            if (mbytes == 0)
                return "0 MB";
            int place = Convert.ToInt32(Math.Floor(Math.Log(mbytes, 1024)));
            double num = mbytes / Math.Pow(1024, place);
            return string.Format("{0:N1} {1}", num, suf[place]);
        }

        [BsonElement("d"), MemberDoc("Free disk space per mounted drive.")]
        public Dictionary<string, int> Disks { get; set; }

        public override string StringValue
        {
            get
            {
                if (Disks.Count == 0)
                {
                    return "N/A";
                }

                return string.Join(", ", from d in Disks
                                         select $"{d.Key}: {FormatMBytes(d.Value)}");
            }
        }
    }
}
