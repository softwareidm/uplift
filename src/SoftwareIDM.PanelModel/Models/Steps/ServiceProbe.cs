﻿using System;
using System.ServiceProcess;
using MongoDB.Bson.Serialization.Attributes;
using SoftwareIDM.PanelModel.Attributes;
using SoftwareIDM.PanelModel.Helpers;

namespace SoftwareIDM.PanelModel.Models
{
    [ClassDoc(@"Checks run status of a service. <br />
Result <code>Value</code> is enum <code>ServiceControllerStatus</code> (4 is running). StringValue is translated into string (e.g. Running, Stopped).<br />
The default fail rule of <code>StringValue != ""Running""</code> fails if the service is not running", "Service Status")]
    public class ServiceProbe : HealthProbe
    {
        [UI(UIAttrKind.Text, true, Size = 4, RuleHelp = true, RuleBraces = true, RuleObject = "SoftwareIDM.PanelModel.Models.Health, SoftwareIDM.PanelModel")]
        [MemberDoc("The network name of the server the service is installed on.")]
        public string Server { get; set; }

        [UI(UIAttrKind.Text, true, Size = 4)]
        [MemberDoc("The name of the service to check.")]
        public string Service { get; set; }

        [UI(Default = "StringValue != \"Running\"")]
        public override string FailRule { get { return base.FailRule; } set { base.FailRule = value; } }

        public override Type RunType()
        {
            return TypeHelper.Get("Steps", GetType().Name, "PanelClient");
        }
    }

    [BsonDiscriminator("spr", Required = true), ClassDoc(Name = "Service Status Probe Result")]
    public sealed class ServiceProbeResult : ProbeResult
    {
        public ServiceProbeResult(ServiceControllerStatus status)
        {
            Value = (int)status;
        }

        public override string StringValue
        {
            get
            {
                return Enum.GetName(typeof(ServiceControllerStatus), Value);
            }
        }
    }
}
