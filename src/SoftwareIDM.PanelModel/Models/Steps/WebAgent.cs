﻿using System;
using System.Collections.Generic;
using SoftwareIDM.PanelModel.Attributes;

namespace SoftwareIDM.PanelModel.Models.Steps
{
    [ValidateStep]
    [ClassDoc("Run Identity Panel join rule full processing.", Name = "Apply Join Rules")]
    public class ApplyJoinStep : ScheduleStep
    {
        [UI(UIAttrKind.Label, Size = 4, Header = true, Default = "Apply Join Rules")]
        public string Label { get => "Apply Join Rules"; set { } }

        [MemberDoc("Limit join application to a particular silo", Name = "Silo")]
        [UI(UIAttrKind.Select, Choices = "js:helpers.siloSelect")]
        public string Silo { get; set; }

        [UI(UIAttrKind.Hidden, Default = "")]
        public override string ConditionRule { get { return base.ConditionRule; } set { base.ConditionRule = value; } }
        [UI(UIAttrKind.Hidden, Default = "")]
        public override string SkipRule { get { return base.SkipRule; } set { base.SkipRule = value; } }
        [UI(UIAttrKind.Hidden, Default = "false")]
        public override bool PauseOnCondition { get => base.PauseOnCondition; set => base.PauseOnCondition = value; }
        [UI(UIAttrKind.Hidden, Default = "false")]
        public override bool ASync { get => base.ASync; set => base.ASync = value; }
        [UI(UIAttrKind.Hidden)]
        public override List<string> ManualLocks { get => base.ManualLocks; set => base.ManualLocks = value; }
        [UI(UIAttrKind.Hidden, Default = "false")]
        public override bool RemoveDefaultLocks { get => base.RemoveDefaultLocks; set => base.RemoveDefaultLocks = value; }
        [UI(UIAttrKind.Hidden, Default = "")]
        public override string Timeout { get => base.Timeout; set => base.Timeout = value; }

        public override List<ScheduleLock> Locks(string scheduler)
        {
            return new List<ScheduleLock> { new ScheduleLock(Name, ScheduleLockLevel.Exclusive), new ScheduleLock("Agent Processing", ScheduleLockLevel.Exclusive) };
        }

        [UI(UIAttrKind.Hidden)]
        public override List<string> PreferredSchedulers { get => new List<string> { "Web" }; set { } }

        public override string Name => $"Apply Join Rules";

        public override Type RunType() => Type.GetType($"SoftwareIDM.WebAgent.{GetType().Name}, WebAgent");
    }

    [ValidateStep]
    [ClassDoc("Run Identity Panel unique index reprocessing.", Name = "Unique Index Processing")]
    public class UniqueIndexStep : ScheduleStep
    {
        [UI(UIAttrKind.Label, Size = 4, Header = true, Default = "Unique Index Processing")]
        public string Label { get => "Unique Index Processing"; set { } }

        [UI(UIAttrKind.Hidden, Default = "")]
        public override string ConditionRule { get { return base.ConditionRule; } set { base.ConditionRule = value; } }
        [UI(UIAttrKind.Hidden, Default = "")]
        public override string SkipRule { get { return base.SkipRule; } set { base.SkipRule = value; } }
        [UI(UIAttrKind.Hidden, Default = "false")]
        public override bool PauseOnCondition { get => base.PauseOnCondition; set => base.PauseOnCondition = value; }
        [UI(UIAttrKind.Hidden, Default = "false")]
        public override bool ASync { get => base.ASync; set => base.ASync = value; }
        [UI(UIAttrKind.Hidden)]
        public override List<string> ManualLocks { get => base.ManualLocks; set => base.ManualLocks = value; }
        [UI(UIAttrKind.Hidden, Default = "false")]
        public override bool RemoveDefaultLocks { get => base.RemoveDefaultLocks; set => base.RemoveDefaultLocks = value; }
        [UI(UIAttrKind.Hidden, Default = "")]
        public override string Timeout { get => base.Timeout; set => base.Timeout = value; }

        public override List<ScheduleLock> Locks(string scheduler)
        {
            return new List<ScheduleLock> { new ScheduleLock(Name, ScheduleLockLevel.Exclusive), new ScheduleLock("Agent Processing", ScheduleLockLevel.Exclusive) };
        }

        [UI(UIAttrKind.Hidden)]
        public override List<string> PreferredSchedulers { get => new List<string> { "Web" }; set { } }

        public override string Name => $"Unique Index Processing";

        public override Type RunType() => Type.GetType($"SoftwareIDM.WebAgent.{GetType().Name}, WebAgent");
    }

    [ValidateStep]
    [ClassDoc("Run Identity Panel unique index reprocessing.", Name = "Reindex Search")]
    public class ReindexStep : ScheduleStep
    {
        [UI(UIAttrKind.Label, Size = 4, Header = true, Default = "Reindex Search")]
        public string Label { get => "Reindex Search"; set { } }

        [MemberDoc("Limit reindex to a particular silo", Name = "Silo")]
        [UI(UIAttrKind.Select, Choices = "js:helpers.siloSelect")]
        public string Silo { get; set; }

        [MemberDoc("Whether to truncate the current index contents before indexing")]
        [UI(UIAttrKind.Bool, Size = 6, Default = "true")]
        public bool Truncate { get; set; }

        [UI(UIAttrKind.Hidden, Default = "")]
        public override string ConditionRule { get { return base.ConditionRule; } set { base.ConditionRule = value; } }
        [UI(UIAttrKind.Hidden, Default = "")]
        public override string SkipRule { get { return base.SkipRule; } set { base.SkipRule = value; } }
        [UI(UIAttrKind.Hidden, Default = "false")]
        public override bool PauseOnCondition { get => base.PauseOnCondition; set => base.PauseOnCondition = value; }
        [UI(UIAttrKind.Hidden, Default = "false")]
        public override bool ASync { get => base.ASync; set => base.ASync = value; }
        [UI(UIAttrKind.Hidden)]
        public override List<string> ManualLocks { get => base.ManualLocks; set => base.ManualLocks = value; }
        [UI(UIAttrKind.Hidden, Default = "false")]
        public override bool RemoveDefaultLocks { get => base.RemoveDefaultLocks; set => base.RemoveDefaultLocks = value; }
        [UI(UIAttrKind.Hidden, Default = "")]
        public override string Timeout { get => base.Timeout; set => base.Timeout = value; }

        public override List<ScheduleLock> Locks(string scheduler)
        {
            return new List<ScheduleLock> { new ScheduleLock(Name, ScheduleLockLevel.Exclusive), new ScheduleLock("Agent Processing", ScheduleLockLevel.Exclusive) };
        }

        [UI(UIAttrKind.Hidden)]
        public override List<string> PreferredSchedulers { get => new List<string> { "Web" }; set { } }

        public override string Name => $"Reindex Search";

        public override Type RunType() => Type.GetType($"SoftwareIDM.WebAgent.{GetType().Name}, WebAgent");
    }

    [ValidateStep]
    [ClassDoc("Run HyperSync Panel and/or Access Panel full processing.", Name = "Run Full Sync")]
    public class HyperSyncFullStep : ScheduleStep
    {
        [UI(UIAttrKind.Label, Size = 3, Header = true, Default = "Full Sync")]
        public string Label { get => "Full Sync"; set { } }

        [MemberDoc("Synchronize HyperSync full processing", Name = "HyperSync Panel")]
        [UI(UIAttrKind.Bool, Size = 3, Header = true, Default = "true")]
        public bool HyperSync { get; set; }

        [MemberDoc("Synchronize Access Panel full processing", Name = "Access Panel")]
        [UI(UIAttrKind.Bool, Size = 3, Header = true, Default = "false")]
        public bool AccessPanel { get; set; }

        [UI(UIAttrKind.Hidden, Default = "")]
        public override string ConditionRule { get { return base.ConditionRule; } set { base.ConditionRule = value; } }
        [UI(UIAttrKind.Hidden, Default = "")]
        public override string SkipRule { get { return base.SkipRule; } set { base.SkipRule = value; } }
        [UI(UIAttrKind.Hidden, Default = "false")]
        public override bool PauseOnCondition { get => base.PauseOnCondition; set => base.PauseOnCondition = value; }
        [UI(UIAttrKind.Hidden, Default = "false")]
        public override bool ASync { get => base.ASync; set => base.ASync = value; }
        [UI(UIAttrKind.Hidden)]
        public override List<string> ManualLocks { get => base.ManualLocks; set => base.ManualLocks = value; }
        [UI(UIAttrKind.Hidden, Default = "false")]
        public override bool RemoveDefaultLocks { get => base.RemoveDefaultLocks; set => base.RemoveDefaultLocks = value; }
        [UI(UIAttrKind.Hidden, Default = "")]
        public override string Timeout { get => base.Timeout; set => base.Timeout = value; }

        public override List<ScheduleLock> Locks(string scheduler)
        {
            return new List<ScheduleLock> {
                new ScheduleLock(Name, ScheduleLockLevel.Exclusive),
                new ScheduleLock("HyperSync", ScheduleLockLevel.Shared),
                new ScheduleLock("AccessPanel", ScheduleLockLevel.Shared),
                new ScheduleLock("Agent Processing", ScheduleLockLevel.Shared) };
        }

        [UI(UIAttrKind.Hidden)]
        public override List<string> PreferredSchedulers { get => new List<string> { "Web" }; set { } }

        public override string Name
        {
            get
            {
                var names = new List<string>();
                if (HyperSync) { names.Add("HyperSync"); }
                if (AccessPanel) { names.Add("Access Panel"); }
                return $"Full Sync {string.Join(" and ", names)}";
            }
        }

        public override Type RunType() => Type.GetType($"SoftwareIDM.WebAgent.{GetType().Name}, WebAgent");
    }

    [ValidateStep]
    [ClassDoc("Run HyperSync Panel and/or Access Panel delta processing.", Name = "Run Delta Sync")]
    public class HyperSyncDeltaStep : ScheduleStep
    {
        [UI(UIAttrKind.Label, Size = 3, Header = true, Default = "Delta Sync")]
        public string Label { get => "Delta Sync"; set { } }

        [MemberDoc("Synchronize HyperSync delta processing", Name = "HyperSync Panel")]
        [UI(UIAttrKind.Bool, Size = 3, Header = true, Default = "true")]
        public bool HyperSync { get; set; }

        [MemberDoc("Synchronize Access Panel delta processing", Name = "Access Panel")]
        [UI(UIAttrKind.Bool, Size = 3, Header = true, Default = "true")]
        public bool AccessPanel { get; set; }

        [UI(UIAttrKind.Hidden, Default = "")]
        public override string ConditionRule { get { return base.ConditionRule; } set { base.ConditionRule = value; } }
        [UI(UIAttrKind.Hidden, Default = "")]
        public override string SkipRule { get { return base.SkipRule; } set { base.SkipRule = value; } }
        [UI(UIAttrKind.Hidden, Default = "false")]
        public override bool PauseOnCondition { get => base.PauseOnCondition; set => base.PauseOnCondition = value; }
        [UI(UIAttrKind.Hidden, Default = "false")]
        public override bool ASync { get => base.ASync; set => base.ASync = value; }
        [UI(UIAttrKind.Hidden)]
        public override List<string> ManualLocks { get => base.ManualLocks; set => base.ManualLocks = value; }
        [UI(UIAttrKind.Hidden, Default = "false")]
        public override bool RemoveDefaultLocks { get => base.RemoveDefaultLocks; set => base.RemoveDefaultLocks = value; }
        [UI(UIAttrKind.Hidden, Default = "")]
        public override string Timeout { get => base.Timeout; set => base.Timeout = value; }

        public override List<ScheduleLock> Locks(string scheduler)
        {
            return new List<ScheduleLock> { new ScheduleLock("HyperSync", ScheduleLockLevel.Shared), new ScheduleLock("AccessPanel", ScheduleLockLevel.Shared) };
        }

        [UI(UIAttrKind.Hidden)]
        public override List<string> PreferredSchedulers { get => new List<string> { "Web" }; set { } }

        public override string Name
        {
            get
            {
                var names = new List<string>();
                if (HyperSync) { names.Add("HyperSync"); }
                if (AccessPanel) { names.Add("Access Panel"); }
                return $"Delta Sync {string.Join(" and ", names)}";
            }
        }

        public override Type RunType() => Type.GetType($"SoftwareIDM.WebAgent.{GetType().Name}, WebAgent");
    }
}
