﻿using System;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson.Serialization.Attributes;
using SoftwareIDM.PanelModel.Attributes;
using SoftwareIDM.PanelModel.Helpers;

namespace SoftwareIDM.PanelModel.Models
{
    [ClassDoc(Name = "Resolve DNS", Description = "Resolves DNS for a host name. <br />Result <code>Value</code> is a List of string IP address or null, StringValue is comma separated list of IP addresses or \"Not Found\". The default fail rule of <code>Not(Value.Count)</code> will fail if the DNS name does not resolve to 1 or more IP addresses.")]
    public class DNSProbe : HealthProbe
    {
        [UI(UIAttrKind.Text, true, NoLabel = true, Header = true, Size = 6, RuleHelp = true, RuleBraces = true, RuleObject = "SoftwareIDM.PanelModel.Models.Health, SoftwareIDM.PanelModel")]
        [MemberDoc(Name = "DNS Name", Description = "FQDN of DNS entry to resolve")]
        public string Host { get; set; }
        
        [UI(Default = "Not(Value.Count)")]
        public override string FailRule { get { return base.FailRule; } set { base.FailRule = value; } }

        public override Type RunType()
        {
            return TypeHelper.Get("Steps", GetType().Name, "PanelClient");
        }
    }

    [BsonDiscriminator("dnspr", Required = true), ClassDoc(Name = "DNS Probe Result")]
    public class DNSProbeResult : ProbeResult
    {
        public override string StringValue
        {
            get
            {
                if (Value != null && ((List<string>)Value).Count > 0)
                {
                    return string.Join(", ", from v in (List<string>)Value
                                             select v);
                }

                return "Not Found";
            }
        }
    }
}
