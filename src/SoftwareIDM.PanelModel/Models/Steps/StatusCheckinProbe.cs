﻿using System;
using SoftwareIDM.PanelModel.Attributes;
using SoftwareIDM.PanelModel.Helpers;

namespace SoftwareIDM.PanelModel.Models
{

    [ClassDoc("Query Panel status checkins. <br />Result <code>Value</code> is <code>List&lt;NoteRecord></code> of most recent checkin notes within time range (up to 10). <br />The default Fail Rule of <code>Value == 0</code> fails if there are no checkins in the time window.", "Status Checkin Search")]
    public class StatusCheckinProbe : HealthProbe
    {
        [UI(UIAttrKind.Select, true, Choices = "js:helpers.timeUnitSelect")]
        [MemberDoc(Name = "Time Filter Unit", Description = "Filter based on run started after value.")]
        public string FromUnit { get; set; }

        [UI(UIAttrKind.Text, true, Size = 6, TypeValid = UIAttrType.Int32)]
        [MemberDoc(Name = "Time Quantity", Description = "Filter based on run started after value.")]
        public int FromQuantity { get; set; }

        [UI(UIAttrKind.Text, true, Description = true, RuleHelp = true, Default = "Count", RuleObject = "SoftwareIDM.PanelModel.Models.NoteRecord, SoftwareIDM.PanelModel")]
        [MemberDoc(Name = "Object Rule", Description = "Convert returned history data set into a single value to place in the Probe Result Value property. Data Structure is { Count: n Data: [NoteRecord] }")]
        public string ObjectRule { get; set; }

        [UI(Default = "Value == 0", RuleObject = "SoftwareIDM.PanelModel.Models.ProbeResult, SoftwareIDM.PanelModel")]
        public override string FailRule { get { return base.FailRule; } set { base.FailRule = value; } }

        public override Type RunType()
        {
            return TypeHelper.Get("Steps", GetType().Name, "PanelClient");
        }
    }
}
