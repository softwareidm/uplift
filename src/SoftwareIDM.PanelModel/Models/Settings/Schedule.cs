﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using SoftwareIDM.PanelModel.Attributes;
using SoftwareIDM.PanelModel.Helpers;

namespace SoftwareIDM.PanelModel.Models
{
    public enum ScheduleLockLevel
    {
        Shared,
        Exclusive
    }

    public interface IRunScheduleStep : IDisposable
    {
        Task<HistoryRecord> Run(ScheduleStep data, Dictionary<string, string> redact, CancellationToken cts);

        event ProgressEvent ReportProgress;
    }

    /// <summary>
    /// Settings class for the scheduler
    /// </summary>
    [ClassDoc("Scheduler settings", Name = "Schedule Settings", RuleDoc = true)]
    public class Schedules : JsonSettings
    {
        [MemberDoc("Whether the schedule engine is enabled. Schedules can still be run manually when disabled")]
        [UI(UIAttrKind.Bool, Default = "true", Size = 4, Description = true)]
        public bool Enabled { get; set; }

        [MemberDoc("Whether data history collection is enabled. Explicitly scheduled data scans will still be performed.", Name = "Disable History")]
        [UI(UIAttrKind.Bool, Default = "true", Size = 4, Description = true)]
        public bool DisableHistory { get; set; }

        [MemberDoc("List of host names where Panel Service is installed. Must be correctly set by user, values are case-insensitive.")]
        public List<string> PanelServiceInstances { get; set; }
        
        [JsonProperty(ItemTypeNameHandling = TypeNameHandling.Objects)]
        [MemberDoc("List of environment variables to substitute into rule values (using special.Environment.Name)", Name = "Environment Variables")]
        [UI(UIAttrKind.Strips, Description = true, Duplicate = true)]
        public List<EnvironmentRoot> EnvironmentVariables { get; set; } = new List<EnvironmentRoot>();

        [JsonProperty(ItemTypeNameHandling = TypeNameHandling.Objects)]
        [MemberDoc("List of custom rule functions that may be used to encapsulate complicated rule engine logic", Name = "Rule Functions")]
        [UI(UIAttrKind.Strips, Description = true, Duplicate = true, CSSClass = "rule-functions")]
        public List<Rules.RuleFunction> RuleFunctions { get; set; } = new List<Rules.RuleFunction>();

        [JsonProperty(ItemTypeNameHandling = TypeNameHandling.Objects)]
        [MemberDoc("List of aliases to make step dispatch environment agnostic", Name = "Server Aliases")]
        [UI(UIAttrKind.Strips, Description = true, Duplicate = true, CSSClass = "server-aliases")]
        public List<ServerAlias> ServerAliases { get; set; } = new List<ServerAlias>();

        [JsonIgnore]
        [BsonIgnore]
        public Dictionary<string, string> Aliases
        {
            get
            {
                if (_aliases == null)
                {
                    _aliases = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
                    foreach (var alias in ServerAliases)
                    {
                        _aliases[alias.Name] = alias.Server;
                    }

                    foreach (var inst in PanelServiceInstances ?? new List<string>())
                    {
                        _aliases[inst] = inst;
                    }
                }

                return _aliases;
            }
        }

        Dictionary<string, string> _aliases;

        public Schedules() { }
    }

    [Serializable]
    public class AbortException : Exception {
        public AbortException(string message) : base(message)
        {
        }
        public AbortException(string message, Exception innerException) : base(message, innerException)
        {
        }
        protected AbortException(System.Runtime.Serialization.SerializationInfo serializationInfo, System.Runtime.Serialization.StreamingContext streamingContext)
        {   
        }
        public AbortException()
        {
        }
    }

    [Flags]
    public enum WeekOfMonth
    {
        First = 0,
        Second = 1,
        Third = 2,
        Fourth = 3,
        Fifth = 4
    }

    [ClassDoc("Structure for defining when schedules should run automatically", Name = "Schedule Time")]
    public class ScheduleTime
    {
        [MemberDoc("Which week of month to run, counted as 7 days, not as a partial calendar week")]
        [UI(UIAttrKind.CheckList, Header = true, Size = 3, Choices = "0:1|1:2|2:3|3:4|4:5", Default = "js:helpers.enums.scheduleWeeks")]
        public List<WeekOfMonth> Weeks { get; set; }

        [MemberDoc("Which days of the week to run, default every day")]
        [UI(UIAttrKind.CheckList, Header = true, Size = 3, Choices = "0:S|1:M|2:T|3:W|4:R|5:F|6:S", Default = "js:helpers.enums.scheduleDays")]
        public List<DayOfWeek> Days { get; set; }

        [MemberDoc("Time Zone offset for this schedule", Name = "Time Zone")]
        [UI(UIAttrKind.Select, Header = true, NoLabel = true, Size = 5, Choices = "js:helpers.timeZones", Default = "UTC;0;(UTC) Coordinated Universal Time;Coordinated Universal Time;Coordinated Universal Time;;")]
        public string TimeZone { get; set; }

        [MemberDoc("First start time in day")]
        [UI(UIAttrKind.Text, Required = true, NoLabel = true, Header = true, Size = 3, Placeholder = "Time hh:mm:ss", CSSClass = "header-next-row", TypeValid = UIAttrType.TimeSpan)]
        public TimeSpan? Time { get; set; }

        [MemberDoc("Optional interval to wait between repeats")]
        [UI(UIAttrKind.Text, Header = true, NoLabel = true, Size = 3, Placeholder = "Interval hh:mm:ss", CSSClass = "header-next-row", TypeValid = UIAttrType.TimeSpan)]
        public TimeSpan? Interval { get; set; }

        [MemberDoc("Stop repeating schedule at Until time")]
        [UI(UIAttrKind.Text, Header = true, NoLabel = true, Size = 3, Placeholder = "Until hh:mm:ss", CSSClass = "header-next-row", TypeValid = UIAttrType.TimeSpan)]
        public TimeSpan? Until { get; set; }
    }

    [BsonIgnoreExtraElements]
    [ClassDoc("Settings for a single step in a schedule")]
    public class ScheduleStep
    {
        [MemberDoc("If Condition Rule is defined, the step will only run if it returns true. Evaluated by PanelService, context is step definition Data", Name = "Condition Rule")]
        [UI(UIAttrKind.LongText, Size = 6, CSSClass = "input-monospace", RuleHelp = true, Description = true)]
        public virtual string ConditionRule { get; set; }
        
        [MemberDoc("Skip Rule can be used to return a number of steps to skip after this step. Evaluated by PanelService, context is the History Record generated by running the step.", Name = "Skip Rule")]
        [UI(UIAttrKind.LongText, Size = 6, CSSClass = "input-monospace", RuleHelp = true, Description = true)]
        public virtual string SkipRule { get; set; }

        [MemberDoc(
            Description = @"If enabled, the entire schedule will be set to a paused state if the condition rule returns false, or if the skip rule returns != 0. <br/>
It is recommended that a workflow be created to trigger a notification based on setting the Interactive property of 'Schedule Instance'.",
            Name = "Rule(s) Pause Schedule"
        )]
        [UI(UIAttrKind.Bool, Size = 12, Description = true)]
        public virtual bool PauseOnCondition { get; set; }

        [MemberDoc("Whether to run step asynchronously. Even if step is ASync, the scheduler will respect locking rules defined by step implementation")]
        [UI(UIAttrKind.Bool, Header = true, Size = 1, OnBind = "js:helpers.bindStepCondition")]
        public virtual bool ASync { get; set; }

        [MemberDoc("Preferred PanelService host to run this step with.", Name = "Preferred Servers")]
        [UI(UIAttrKind.Select, Choices = "js:helpers.panelServiceInstances", Size = 6)]
        public virtual List<string> PreferredSchedulers { get; set; }

        [MemberDoc("If a timeout is defined, the schedule step will abort after running for the specified time.", Name = "Timeout")]
        [UI(UIAttrKind.Text, TypeValid = UIAttrType.TimeSpan, Size = 4, Placeholder = "00:00:00")]
        public virtual string Timeout { get; set; }

        [MemberDoc("Optional list of manual step locks to add to default lock settings. Wrap in [rule] to use Rule Engine.", Name = "Additional Exclusive Locks")]
        [UI(UIAttrKind.Select, Size = 12, Description = true, Choices = "js:helpers.tagSelect")]
        public virtual List<string> ManualLocks { get; set; }

        [MemberDoc(Name = "Remove Default Locks")]
        [UI(UIAttrKind.Bool, Size = 4)]
        public virtual bool RemoveDefaultLocks { get; set; }

        public virtual List<ScheduleLock> GetLocks(string scheduler, Func<string, string> ruleResolve)
        {
            var ret = RemoveDefaultLocks ? new List<ScheduleLock>() : Locks(scheduler);
            if (ManualLocks != null)
            {
                foreach (var l in ManualLocks)
                {
                    if (l.StartsWith("[") && l.EndsWith("]"))
                    {
                        ret.Add(new ScheduleLock { Level = ScheduleLockLevel.Exclusive, Name = ruleResolve(l.Substring(1, l.Length - 2)) });
                    }
                    else
                    {
                        ret.Add(new ScheduleLock { Level = ScheduleLockLevel.Exclusive, Name = l });
                    }
                }
            }

            return ret;
        }

        // don't persist to database, since not queried on, and chosen dynamically
        [BsonIgnore, MemberDoc("Name generated by implementation")]
        public virtual string Name { get { return ""; } }

        public virtual List<ScheduleLock> Locks(string scheduler)
        {
            throw new NotImplementedException();
        }

        public virtual Type RunType()
        {
            throw new NotImplementedException();
        }

        public string GetHash()
        {
            var temp = PreferredSchedulers;
            PreferredSchedulers = null;
            var json = JsonConvert.SerializeObject(this);
            PreferredSchedulers = temp;
            var bytes = System.Text.Encoding.UTF8.GetBytes(json);
            var hash = Hash.Get(bytes);
            return hash.ToUrlBase64();
        }
    }

    [ClassDoc("Scheduler exclusivity control lock")]
    public class ScheduleLock
    {
        [BsonElement("n")]
        public string Name { get; set; }

        [BsonElement("l")]
        public ScheduleLockLevel Level { get; set; }

        public string InstanceName
        {
            get
            {
                return Name + "|" + Level.ToString();
            }
        }

        public string LevelName(ScheduleLockLevel level)
        {
            return Name + "|" + level.ToString();
        }

        public ScheduleLock(string instanceName)
        {
            var split = instanceName.Split('|');
            Name = split[0];
            Level = (ScheduleLockLevel)Enum.Parse(typeof(ScheduleLockLevel), split[1]);
        }

        public ScheduleLock() { }

        public ScheduleLock(string name, ScheduleLockLevel level)
        {
            Name = name;
            Level = level;
        }
    }

    [ClassDoc("Security settings for a single schedule", Name = "Schedule Security")]
    public class ScheduleSecurity
    {
        [MemberDoc("Roles that can queue a schedule to be executed immediately.")]
        [UI(Kind = UIAttrKind.Select, Choices = "js:helpers.rolesSelect")]
        public List<string> Invoke { get; set; }

        [MemberDoc("Roles that can edit this schedule. Administrators will be able to edit all schedules.")]
        [UI(Kind = UIAttrKind.Select, Choices = "js:helpers.rolesSelect")]
        public List<string> Edit { get; set; }
    }

    public enum SchedulePriority
    {
        Normal = 0,
        Low = -1,
        High = 1
    }

    [ClassDoc("Settings for a single schedule", Name = "Schedule", RuleDoc = true)]
    public class Schedule
    {
        [BsonId]
        [UI(Kind = UIAttrKind.Hidden, Default = "js:helpers.getGuid")]
        public Guid Id { get; set; }

        [MemberDoc("Display name of schedule")]
        [UI(Kind = UIAttrKind.Text, Required = true, Header = true, NoLabel = true, Unique = true, Size = 3)]
        public string Name { get; set; }

        [MemberDoc("Whether individual schedule is enabled.")]
        [UI(UIAttrKind.Bool, Size = 2, Header = true, Default = "true")]
        public bool Enabled { get; set; }

        [MemberDoc("Tags to filter schedules view")]
        [UI(Kind = UIAttrKind.Hidden)]//, Header = true, NoLabel = true, Choices = "js:helpers.scheduleTagSelect", Size = 4)]
        public List<string> Tags { get; set; }

        [JsonProperty(ItemTypeNameHandling = TypeNameHandling.Objects)]
        [MemberDoc("Start time settings for schedule. If no time is specified, the schedule may still be run manually.")]
        [UI(Kind = UIAttrKind.Strips, CSSClass = "times", Duplicate = true)]
        [BsonIgnoreIfNull]
        public List<ScheduleTime> Times { get; set; }

        [JsonProperty(ItemTypeNameHandling = TypeNameHandling.Objects)]
        [MemberDoc("Sequence of steps to perform with schedule")]
        [UI(Kind = UIAttrKind.Strips, CSSClass = "steps", Duplicate = true)]
        public List<ScheduleStep> Steps { get; set; }

        [JsonProperty(TypeNameHandling = TypeNameHandling.Objects)]
        [MemberDoc("Security settings for schedule.")]
        [UI(Kind = UIAttrKind.Object, CSSClass = "security")]
        public ScheduleSecurity Security { get; set; }

        [JsonConverter(typeof(Newtonsoft.Json.Converters.StringEnumConverter))]
        [MemberDoc("Priority level to run this schedule at.")]
        [UI(Kind = UIAttrKind.Select, Choices = "Normal|Low|High", Default = "Normal", Size = 3)]
        public SchedulePriority Priority { get; set; }

        [MemberDoc("Whether schedule can only run in exclusive mode, meaning no other schedules can run at the same time")]
        [UI(UIAttrKind.Bool, Size = 6, Description = true, Default = "false")]
        public bool Exclusive { get; set; }

        public Schedule()
        {
            Steps = new List<ScheduleStep>();
        }
    }

    /// <summary>
    /// Placeholder class for model binder settings
    /// </summary>
    [ClassDoc(RuleDoc = false, Name = "Schedules")]
    public class ScheduleList
    {
        [JsonProperty(ItemTypeNameHandling = TypeNameHandling.Objects)]
        [UI(Kind = UIAttrKind.Strips, CSSClass = "schedules", NoLabel = true, Duplicate = true, Download = "schedules", WarnRemove = true)]
        public List<Schedule> Data { get; set; }
    }

    [ClassDoc("Aliases for server references", Name = "Server Alias")]
    public class ServerAlias
    {
        [MemberDoc("Alias Name")]
        [UI(UIAttrKind.Text, NoLabel = true, Required = true, Unique = true, Header = true, Size = 4)]
        public virtual string Name { get; set; }

        [MemberDoc("Server to apply alias to")]
        [UI(UIAttrKind.Select, Choices = "js:helpers.panelServiceInstance", Required = true, NoLabel = true, Header = true, Size = 4)]
        public virtual string Server { get; set; }
    }


    [ClassDoc("", Name = "Environment Variable Base Class")]
    public class EnvironmentRoot
    {
        [MemberDoc("Environment variable name")]
        [UI(UIAttrKind.Text, Required = true, Unique = true, NoLabel = true, Header = true, Size = 3)]
        public virtual string Name { get; set; }

        [MemberDoc("Environment variable value")]
        [UI(UIAttrKind.Text, NoLabel = true, Header = true, Size = 7)]
        public virtual string Value { get; set; }
    }

    [ClassDoc("Environment variable for health check or schedule", Name = "Environment Variable")]
    public class EnvironmentVar : EnvironmentRoot { }

    [ClassDoc("Environment variable for health check or schedule", Name = "Multi-line Environment Variable")]
    public class LongEnvironmentVar : EnvironmentRoot
    {
        public override string Name { get => base.Name; set => base.Name = value; }

        [UI(UIAttrKind.LongText, NoLabel = true, Header = true, Size = 7)]
        public override string Value { get => base.Value; set => base.Value = value; }
    }

    [ClassDoc("Environment variable for health check", Name = "Encrypted Environment Variable")]
    public class CipheredEnvironmentVar : EnvironmentRoot
    {
        public override string Name { get => base.Name; set => base.Name = value; }

        [UI(UIAttrKind.Encrypted, NoLabel = true, Header = true, Size = 7)]
        public override string Value { get => base.Value; set => base.Value = value; }
    }
}