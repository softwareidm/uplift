﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using SoftwareIDM.PanelModel.Attributes;

namespace SoftwareIDM.PanelModel.Models
{
    [ClassDoc("Base class for settings objects", Name = "Panel Settings", RuleDoc = true)]
    public class JsonSettings
    {
        // mask Property for bson id automap
        [JsonIgnore, BsonId, MemberDoc(Ignore = true)]
        public string Id { get { return GetType().Name; } set { } }

        protected bool MergeListSettings(JsonSettings other)
        {
            int mergeCount = 0;
            var props = GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
            PropertyInfo mergeProperty = null;
            foreach (var prop in props)
            {
                if (prop.PropertyType.IsGenericType && 
                    prop.PropertyType.GetGenericTypeDefinition() == typeof(List<>) &&
                    prop.PropertyType.GetGenericArguments()[0] != typeof(string))
                {
                    var pVal = prop.GetValue(other);
                    if (pVal == null) { continue; }
                    var c = prop.PropertyType.GetProperty("Count");
                    if (c != null && c.GetValue(pVal) is int count && count > 0)
                    {
                        mergeCount += count;
                        mergeProperty = prop;
                    }
                }
            }

            // exactly one collection is populated
            if (mergeCount == 1)
            {
                var self = mergeProperty.GetValue(this);
                var merge = mergeProperty.GetValue(other);
                var add = merge.GetType().GetProperty("Item").GetValue(merge, new object[] { 0 });
                bool replaced = false;

                var count = (int)self.GetType().GetProperty("Count").GetValue(self);
                var idProp = add.GetType().GetProperty("Id", BindingFlags.Public | BindingFlags.Instance);
                var nameProp = add.GetType().GetProperty("Name", BindingFlags.Public | BindingFlags.Instance);

                for (int i = 0; i < count; i++)
                {
                    var compare = self.GetType().GetProperty("Item").GetValue(self, new object[] { i });
                    if (idProp != null && idProp.GetValue(compare).Equals(idProp.GetValue(add)))
                    {
                        replaced = true;
                    }
                    else if (nameProp != null && nameProp.GetValue(compare).Equals(nameProp.GetValue(add)))
                    {
                        replaced = true;
                    }

                    if (replaced)
                    {
                        var item = self.GetType().GetProperty("Item");
                        item.SetValue(self, add, new object[] { i });
                        break;
                    }
                }

                if (!replaced)
                {
                    self.GetType().GetMethod("Add").Invoke(self, new object[] { add });
                }

                return true;
            }
            else
            {
                // complete replacement
                return false;
            }
        }
    }

    [ClassDoc("Validation errors list")]
    public class ValidError
    {
        public string Message { get; set; }

        public string Context { get; set; }
    }

    [ClassDoc("Exception Representation for reporting errors")]
    public class ErrorPost
    {
        public ErrorPost() { }

        public ErrorPost(Exception e, string context)
        {
            Context = context;
            Message = e.Message;
            StackTrace = e.StackTrace;
            UserName = System.Threading.Thread.CurrentPrincipal.Identity.Name;
            if (e.Data != null && e.Data.Count > 0)
            {
                Data = new List<string>();
                foreach (DictionaryEntry pair in e.Data)
                {
                    if (pair.Key != null && pair.Value != null)
                    {
                        Data.Add($"{pair.Key}: {pair.Value}");
                    }
                }
            }
        }

        public List<string> Data { get; set; }

        public string Message { get; set; }

        public string StackTrace { get; set; }

        public string Context { get; set; }

        public string UserName { get; set; }
    }
}