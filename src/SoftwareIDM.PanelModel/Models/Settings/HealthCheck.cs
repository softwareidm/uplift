﻿using System;
using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using SoftwareIDM.PanelModel.Attributes;
using SoftwareIDM.PanelModel.Helpers;

namespace SoftwareIDM.PanelModel.Models
{
    [BsonIgnoreExtraElements]
    [BsonDiscriminator("Health", Required = true)]
    [ClassDoc("Settings for advanced health check engine (deprecated within Providers, run upgrade to move to Extensions)", Name = "Health Check Settings", License = "advanced-health", Deprecated = true)]
    public class Health : Provider
    {
        [MemberDoc("Seconds to rest between completing and starting a round of health checks, default is 60", Name = "Polling Interval (seconds)")]
        [UI(UIAttrKind.Text, Size = 3, Default = "60", Required = true)]
        public int Interval { get; set; }

        [JsonProperty(ItemTypeNameHandling = TypeNameHandling.Objects)]
        [MemberDoc("List of health checks to perform", Name = "Health Checks")]
        [UI(UIAttrKind.Strips, Description = true, WarnRemove = true, Duplicate = true)]
        public List<HealthDefinition> Data { get; set; }

        public override Type ProviderNameHelper()
        {
            return TypeHelper.Get("Helpers", "EmptyNamesHelper", "PanelModule");
        }

        public Health()
        {
            Data = new List<HealthDefinition>();
        }
    }

    [BsonIgnoreExtraElements]
    [BsonDiscriminator("HealthExtension", Required = true)]
    [ClassDoc("Settings for advanced health check engine", Name = "Health Check Settings", License = "advanced-health")]
    public class HealthExtension : Extension
    {
        [MemberDoc("Seconds to rest between completing and starting a round of health checks, default is 60", Name = "Polling Interval (seconds)")]
        [UI(UIAttrKind.Text, Size = 3, Default = "60", Required = true)]
        public int Interval { get; set; }

        [JsonProperty(ItemTypeNameHandling = TypeNameHandling.Objects)]
        [MemberDoc("List of health checks to perform", Name = "Health Checks")]
        [UI(UIAttrKind.Strips, Description = true, WarnRemove = true, Duplicate = true)]
        public List<HealthDefinition> Data { get; set; }
        public HealthExtension()
        {
            Data = new List<HealthDefinition>();
        }
    }

    [BsonDiscriminator("pr", Required = true)]
    [ClassDoc("Represents value or error produced by a health probe", Name = "Probe Result")]
    public class ProbeResult
    {
        [BsonElement("p"), MemberDoc("Whether or not the probe passed, based on the results of Fail Rule evaluation")]
        public bool Passed { get; set; }

        [BsonIgnoreIfNull, BsonElement("v"), MemberDoc("Object value of the health probe, often an integer")]
        public object Value { get; set; }

        [BsonIgnoreIfNull, BsonElement("r"), MemberDoc("Rule to evaluate whether health probe failed")]
        public string FailRule { get; set; }

        [BsonElement("e"), BsonIgnoreIfNull, MemberDoc("Error message if executing probe caused an exception")]
        public string Error { get; set; }

        [BsonElement("t"), BsonIgnoreIfNull, MemberDoc("Stack trace if executing probe caused an exception")]
        public string ErrorTrace { get; set; }

        public ProbeResult() { }

        [BsonIgnore, JsonProperty, MemberDoc("String conversion for probe value")]
        public virtual string StringValue
        {
            get
            {
                if (Error != null) { return "Error"; }
                return Value == null ? "N/A" : new Rules.AttributeValue(Value, null).ToString();
            }
        }
    }

    [ClassDoc("Settings element for a single health probe", Name = "Health Probe")]
    public class HealthProbe
    {
        [MemberDoc("Display name of probe")]
        [UI(UIAttrKind.Text, Required = true, Unique = true, Size = 3, NoLabel = true, Header = true)]
        public string Name { get; set; }

        [MemberDoc("Rule to determine whether to run the health check. A true return value means the probe will be executed.", Name = "Condition Rule")]
        [UI(UIAttrKind.LongText, Size = 6, CSSClass = "input-monospace", RuleHelp = true, RuleObject = "SoftwareIDM.PanelModel.Models.HealthProbe, SoftwareIDM.PanelModel")]
        public virtual string ConditionRule { get; set; }

        [MemberDoc("Rule to determine whether probe fails. A true return value means the probe failed.", Name = "Fail Rule")]
        [UI(UIAttrKind.LongText, Size = 6, CSSClass="input-monospace", RuleHelp = true, RuleObject = "SoftwareIDM.PanelModel.Models.ProbeResult, SoftwareIDM.PanelModel")]
        public virtual string FailRule { get; set; }

        public virtual Type RunType()
        {
            throw new NotImplementedException();
        }

        [JsonIgnore]
        [BsonIgnore]
        [MemberDoc(Ignore = true)]
        public bool VarsApplied { get; set; }

        public string GetHash()
        {
            var json = JsonConvert.SerializeObject(this);
            var bytes = System.Text.Encoding.UTF8.GetBytes(json);
            var hash = Hash.Get(bytes);
            return hash.ToUrlBase64();
        }
    }

    [ClassDoc("Settings details for a single health check", Name = "Health Check")]
    public class HealthDefinition
    {
        [MemberDoc("Health check display name")]
        [UI(UIAttrKind.Text, Required = true, Unique = true, NoLabel = true, Header = true, Size = 3)]
        public string Name { get; set; }

        [MemberDoc("Preferred Panel Service to use for executing the health check", Name = "Preferred Servers")]
        [UI(UIAttrKind.Select, Size = 4, Choices = "js:helpers.panelServiceInstances")]
        public List<string> PreferredServices { get; set; }

        [JsonProperty(ItemTypeNameHandling = TypeNameHandling.Objects)]
        [MemberDoc("List of probes that make up this health check")]
        [UI(UIAttrKind.Strips, Description = true, Duplicate = true)]
        public List<HealthProbe> Probes { get; set; }
        
        public HealthDefinition()
        {
        }
    }
}