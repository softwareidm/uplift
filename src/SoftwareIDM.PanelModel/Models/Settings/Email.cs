﻿using System;
using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using SoftwareIDM.PanelModel.Attributes;

namespace SoftwareIDM.PanelModel.Models
{
    [ClassDoc("Wrapper for user credentials in settings that applies strong encryption to passwords", Name = "Panel Credential")]
    public class PanelCredential
    {
        [MemberDoc("User name for login, should be full domain qualified form")]
        [UI(UIAttrKind.Text, RuleHelp = true, RuleBraces = true)]
        public virtual string User { get; set; }

        [MemberDoc("Password value, encrypted by the UI as it's set, may use special.Environment.EncryptedVarName. This value will be overridden if *.exe.config->appSettings contains the key PanelPassword.")]
        [UI(UIAttrKind.Encrypted)]
        public virtual string Password { get; set; }

        [JsonIgnore]
        [BsonIgnore]
        public virtual string ConfigKey { get { return "Panel:Password"; } }
    }

    [BsonDiscriminator("Email", Required = true)]
    [ClassDoc("Settings for sending emails<br/>Fill out SMTP server settings or Graph API settings. If using Graph API, the Microsoft Graph Mail.Send application permission must be assigned.", Name = "Email Settings", RuleDoc = true)]
    public class Email : JsonSettings
    {
        [MemberDoc("Whether to use email to report exceptions to SoftwareIDM", Name = "Report Errors")]
        [UI(UIAttrKind.Hidden, Default = "false", Description = true)]
        public bool ReportErrors { get; set; }

        [MemberDoc("SMTP Server host name", Name = "SMTP Server")]
        [UI(UIAttrKind.Text, RuleHelp = true, RuleBraces = true)]
        public string Server { get; set; }

        [MemberDoc("Port number for SMTP")]
        [UI(UIAttrKind.Text, Default = "25", TypeValid = UIAttrType.Int32, Size = 2)]
        public int Port { get; set; }

        [MemberDoc("Whether to use a secure connection", Name = "TLS Encryption")]
        [UI(UIAttrKind.Bool, Default = "false", Size = 4, Description = true)]
        public bool UseTLS { get; set; }

        [MemberDoc("Tenant ID for sending email with Graph API.", Name = "Azure Tenant ID")]
        [UI(UIAttrKind.Text, Size = 6, RuleHelp = true, RuleBraces = true)]
        public string Tenant { get; set; }

        [MemberDoc("Limit messages per minute per sender per Panel Service (Note: default is Microsoft Graph limit for a single sender address).", Name = "Rate Limit")]
        [UI(UIAttrKind.Text, Size = 4, Default = "30", TypeValid = UIAttrType.Int32, Description = true)]
        public string RateLimit { get; set; }

        [JsonProperty(TypeNameHandling = TypeNameHandling.Objects)]
        [MemberDoc("Optional SMTP user credentials.<br/>If using Graph API, values are required. Enter Application ID Guid, and application secret.")]
        [UI(UIAttrKind.Object, Description = true)]
        public PanelCredential Credential { get; set; }

        [MemberDoc("Sender email address. If using Microsoft Graph, MUST be sender userPrincipalName<br/>May provide display name with format: \"name\" &lt;user@host&gt; May be comma separated to rotate between multiple", Name = "From Address")]
        [UI(UIAttrKind.Text, RuleHelp = true, RuleBraces = true, Description = true)]
        public string FromAddress { get; set; }

        [MemberDoc("Recipient for messages for which a recipient can't be determined", Name = "Fallback Recipient")]
        [UI(UIAttrKind.Text, RuleHelp = true, RuleBraces = true, Description = true)]
        public string FallbackRecipient { get; set; }

        [MemberDoc("Which server(s) may be used to send a test message", Name = "Test Message Service")]
        [UI(UIAttrKind.Select, Choices = "js:helpers.panelServiceInstances", Description = true)]
        public List<string> TestServer { get; set; }

        [MemberDoc("Sender number. Optional, for use with Twilio API", Name = "From SMS Number")]
        [UI(UIAttrKind.Text, RuleHelp = true, RuleBraces = true, Description = true)]
        public string SmsFrom { get; set; }

        [JsonProperty(TypeNameHandling = TypeNameHandling.Objects)]
        [MemberDoc("Optional Twilio user credentials (used for sending SMS messages).<br/>Enter Account Sid, and Auth Token.", Name = "Twilio Credential")]
        [UI(UIAttrKind.Object, Description = true)]
        public PanelCredential TwilioCredential { get; set; }
    }
}