﻿using System;
using System.Collections.Generic;
using System.Reflection;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using SoftwareIDM.PanelModel.Attributes;

namespace SoftwareIDM.PanelModel.Models
{
    [BsonIgnoreExtraElements]
    [BsonDiscriminator("Extensions", Required = true)]
    [ClassDoc("Settings for functionality extensions.", Name = "Extension Settings", RuleDoc = true)]
    public class Extensions : JsonSettings
    {
        public Extensions()
        {
            Data = new List<Extension>();
        }

        [MemberDoc("List of functionality extensions", Name = "Extensions")]
        [JsonProperty(ItemTypeNameHandling = TypeNameHandling.Objects)]
        [UI(Kind = UIAttrKind.Strips, CSSClass = "extensions", NoLabel = true, Duplicate = true, Download = "js:helpers.extensionDownload", WarnRemove = true)]
        public List<Extension> Data { get; set; }

        // changed is only updated when we have a new extension Id
        [JsonIgnore]
        public DateTime Changed { get; set; }

        public bool Merge(Extensions other)
        {
            Changed = DateTime.UtcNow;
            if (other != null && other.Data != null && other.Data.Count > 0)
            {
                if (other.Data.Count > 1)
                {
                    Data = other.Data;
                    return true;
                }

                foreach (var dat in other.Data)
                {
                    bool found = false;
                    for (int i = 0; i < Data.Count; i++)
                    {
                        if (Data[i].Id == dat.Id)
                        {
                            Data[i] = dat;
                            found = true;
                            break;
                        }
                    }

                    if (!found)
                    {
                        Changed = DateTime.UtcNow;
                        Data.Add(dat);
                    }
                }
            }

            return true;
        }
    }

    public interface IExtension
    {
        public string Name { get; set; }
        public string TypeLabel { get; set; }
        public bool Enabled { get; set; }
        public Guid Id { get; set; }
    }

    [ClassDoc("Extension settings base class.")]
    public class Extension : IExtension
    {
        [MemberDoc("Display name of extension")]
        [UI(UIAttrKind.Text, Size = 3, Required = true, Unique = true, Header = true, NoLabel = true)]
        public string Name { get; set; }

        [BsonIgnore]
        [UI(UIAttrKind.Label, Size = 3, Header = true)]
        public string TypeLabel { get => GetType().GetCustomAttribute<ClassDoc>().Name; set { } }

        [MemberDoc("Whether this provider connection is enabled.", Name = "Provider Enabled")]
        [UI(UIAttrKind.Bool, Default = "true")]
        public bool Enabled { get; set; }

        [MemberDoc("Auto-generated unique Id for namespacing silos in provider.")]
        [UI(UIAttrKind.Hidden, Default = "js:helpers.getGuid")]
        public Guid Id { get; set; }
    }
}
