﻿using System;
using System.Collections.Generic;
using System.Reflection;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using SoftwareIDM.PanelModel.Attributes;

namespace SoftwareIDM.PanelModel.Models
{
    [BsonIgnoreExtraElements]
    [BsonDiscriminator("Providers", Required = true)]
    [ClassDoc("Settings for provider connections.", Name = "Provider Settings", RuleDoc = true)]
    public class Providers : JsonSettings
    {
        public Providers()
        {
            Data = new List<Provider>();
        }

        [MemberDoc("List of provider connections", Name = "Provider Connections")]
        [JsonProperty(ItemTypeNameHandling = TypeNameHandling.Objects)]
        [UI(Kind = UIAttrKind.Strips, CSSClass = "providers", NoLabel = true, Duplicate = true, Download = "js:helpers.providerDownload", WarnRemove = true)]
        public List<Provider> Data { get; set; }

        // changed is only updated when we have a new provider Id
        [JsonIgnore]
        public DateTime Changed { get; set; }

        public bool Merge(Providers other)
        {
            Changed = DateTime.UtcNow;
            if (other != null && other.Data != null && other.Data.Count > 0)
            {
                if (other.Data.Count > 1)
                {
                    Data = other.Data;
                    return true;
                }

                foreach (var dat in other.Data)
                {
                    bool found = false;
                    for (int i = 0; i < Data.Count; i++)
                    {
                        if (Data[i].Id == dat.Id)
                        {
                            Data[i] = dat;
                            found = true;
                            break;
                        }
                    }

                    if (!found)
                    {
                        Changed = DateTime.UtcNow;
                        Data.Add(dat);
                    }
                }
            }

            return true;
        }
    }

    [ClassDoc("Provider connection base class.")]
    public class Provider
    {

        [MemberDoc("Display name of connection")]
        [UI(UIAttrKind.Text, Size = 3, Required = true, Unique = true, Header = true, NoLabel = true)]
        public string Name { get; set; }

        [BsonIgnore]
        [UI(UIAttrKind.Label, Size = 3, Header = true)]
        public string TypeLabel { get => GetType().GetCustomAttribute<ClassDoc>().Name; set { } }

        [MemberDoc("Whether this provider connection is enabled.", Name = "Provider Enabled")]
        [UI(UIAttrKind.Bool, Default = "true")]
        public bool Enabled { get; set; }

        [MemberDoc("Auto-generated unique Id for namespacing silos in provider.")]
        [UI(UIAttrKind.Hidden, Default = "js:helpers.getGuid")]
        public Guid Id { get; set; }

        [BsonIgnoreIfNull]
        [MemberDoc("Servers to use for writing export changes back to end provider. Only applicable when Identity Panel MAs are used.", Name = "Connector Writeback Server(s)")]
        [UI(UIAttrKind.Hidden)]
        public virtual List<string> ConnectorWritebackServers { get; set; }

        public virtual Type ProviderWriteback() { return null; }

        public virtual Type ProviderSystem() { return null; }

        [BsonIgnore]
        [JsonProperty]
        public virtual bool IsScan { get { return false; } }

        public virtual Type ProviderNameHelper()
        {
            return null;
        }
    }
}