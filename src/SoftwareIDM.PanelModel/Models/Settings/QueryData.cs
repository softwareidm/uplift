﻿using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SoftwareIDM.PanelModel.Attributes;

namespace SoftwareIDM.PanelModel.Models
{
    public enum DataSubOp
    {
        And,
        Or,
        Not
    }

    public enum DataOp
    {
        Eq,
        Ne,
        Lt,
        Gt,
        Lte,
        Gte,
        In,
        NotNull,
        Null,
        Contains,
        StartsWith,
        EndsWith,
        SizeEq,
        // Deprecated, use NotNull and Null instead
        Exists,
        NotExists
    }

    [ClassDoc("Settings for selecting data", Name = "Query Data Set")]
    public class QueryData
    {
        [MemberDoc("Name of data set")]
        [UI(Kind = UIAttrKind.Text, Size = 3, Required = true, Unique = true, Header = true, NoLabel = true)]
        public string Name { get; set; }

        [MemberDoc("Object Type to select data set from", Name = "Data Type")]
        [UI(Kind = UIAttrKind.Select, Required = true, Size = 6, Header = true, NoLabel = true, Choices = "js:helpers.rule.objectSelect")]
        public string Type { get; set; }

        [JsonProperty(ItemTypeNameHandling = TypeNameHandling.Objects)]
        [MemberDoc("Data filters to restrict query results", Name = "Query Filters")]
        [UI(Kind = UIAttrKind.Strips, Description = true)]
        public virtual List<FilterClause> Clauses { get; set; }

        [JsonProperty(ItemTypeNameHandling = TypeNameHandling.Objects)]
        [MemberDoc("Sub query to provide filters that are not easily achieved with simple 'And' logic.", Name = "Sub Query")]
        [UI(UIAttrKind.Strips, Description = true)]
        public virtual List<SubQuery> SubQueries { get; set; }

        [MemberDoc("Optional list of data fields to retrieve from the database. An empty list will return all fields.", Name = "Include Fields")]
        [UI(Kind = UIAttrKind.Select, Size = 12, Description = true, Choices = "js:helpers.rule.fieldSelect")]
        public List<string> Include { get; set; }

        public int? Limit { get; set; }
        
        public QueryData()
        {
            Clauses = new List<FilterClause>();
        }
    }

    [MongoDB.Bson.Serialization.Attributes.BsonIgnoreExtraElements]
    [ClassDoc("Sub Query for data filtering. May have no more than one sub-query of each type (And/Or/Not).", Name = "Sub Query Filter")]
    public class SubQuery
    {
        [JsonConverter(typeof(StringEnumConverter))]
        [MemberDoc("Query Operator")]
        [UI(UIAttrKind.Select, Required = true, Header = true, Choices = "Or|Not|And", Default = "Or")]
        public DataSubOp Operator { get; set; }

        [JsonProperty(ItemTypeNameHandling = TypeNameHandling.Objects)]
        [MemberDoc("Data filters to restrict query results", Name = "Query Filters")]
        [UI(Kind = UIAttrKind.Strips, Description = true)]
        public List<FilterClause> Clauses { get; set; }
    }

    [ClassDoc("Filter clause for data query", Name = "Query Filter")]
    public class FilterClause
    {
        [MemberDoc("Object Path to query against in database.", Name = "Field Path")]
        [UI(UIAttrKind.Text, Header = true, NoLabel = true, Size = 4, Required = true, RuleHelp = true, RuleDialog = "js:helpers.report.fieldRuleHelp")]
        public string Field { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        [UI(
            UIAttrKind.Select,
            Default = "Eq",
            Header = true,
            NoLabel = true,
            Size = 2,
            Required = true,
            Choices = "Eq: == |Ne: != |Lt: < |Gt: > |Lte: <=|Gte: >=|In:In|NotNull:Not Null|Null:Null|Contains:Contains|StartsWith:Starts With|EndsWith:Ends With|SizeEq:[].Length ==")]
        public DataOp Operation { get; set; }

        [MemberDoc("Value for database query filter. Is processed by rule engine before being used.")]
        [UI(UIAttrKind.Text, Size = 5, Header = true, NoLabel = true, CSSClass = "input-monospace", RuleHelp = true, RuleObject = "SoftwareIDM.PanelModel.Models.FilterClause, SoftwareIDM.PanelModel")]
        public string Value { get; set; }

        public object ObjectValue { get; set; }
    }
}
