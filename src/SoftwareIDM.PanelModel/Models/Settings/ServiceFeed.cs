﻿using System;
using Newtonsoft.Json;
using SoftwareIDM.PanelModel.Attributes;
using SoftwareIDM.PanelModel.Models;

namespace SoftwareIDM.PanelModel.Models
{
#if !ISAZURE
     
    [MongoDB.Bson.Serialization.Attributes.BsonIgnoreExtraElements]
    [ClassDoc(@"
<p><strong>DEPRECATED</strong> in favor of new SaaS options - Provider for configuring Identity Panel managed service console.<p>
<p>Contact customerservice@softwareidm.com if you have questions.</p>
<p><strong>Prerequisites</strong></p>
<ol>
<li>Register a tenant on identitypanel.com or in your preferred region</li>
<li>Go through the setup walkthrough to configure your tenant</li>
<li>Fill out the settings below and save changes</li>
</ol>
<p><strong>After configuring:</strong></p>
<ol>
<li>Change SoftwareIDM Maintenance Service to use a service account with a user profile</li>
<li>Open a command prompt and do <pre>runas /u:domain\service account cmd</pre></li>
<li>Run <pre>WebAgent.exe --feed</pre> as the service account to establish Azure login context</li>
<li>After logging in, restart SoftwareIDM Maintenance Service</li>
</ol>
<p>Data collection will run once per minute. Progress will appear on the dashboard console if configured.</p>
", Name = "Service Feed", Deprecated = true)]
    public class ServiceFeed : Provider
    {

        [MemberDoc("Paste in the Host name from cloud Identity Panel 'Install Tools' page", Name = "Host")]
        [UI(UIAttrKind.Text, Size = 6, Description = true, Required = true, Default = "https://identitypanel.com/")]
        public string Host { get; set; }

        [MemberDoc("Paste in the Tenant ID from cloud Identity Panel 'Install Tools' page", Name = "Tenant ID")]
        [UI(UIAttrKind.Text, Size = 6, Description = true, Required = true)]
        public string TenantId { get; set; }
        
    }
#endif
}
