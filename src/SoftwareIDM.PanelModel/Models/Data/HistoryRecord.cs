﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.Options;
using Newtonsoft.Json;
using SoftwareIDM.PanelModel.Attributes;
using SoftwareIDM.PanelModel.Helpers;

namespace SoftwareIDM.PanelModel.Models
{
    public interface IHistoryCounters
    {
        Dictionary<string, byte> Counters { get; }
    }

    [ClassDoc("Schedule history counter values")]
    public class ScheduleCounters : IHistoryCounters
    {
        public Dictionary<string, byte> Counters
        {
            get
            {
                return new Dictionary<string, byte>
                {
                    { "Error", HistoryRecord.ERROR },
                    { "Run Schedule", ScheduleConstants.ScheduleRun },
                    { "Step Failed", ScheduleConstants.StepFail },
                    { "Step Finished", ScheduleConstants.StepFinish },
                    { "Step Queued", ScheduleConstants.StepQueue },
                    { "Step Skipped", ScheduleConstants.StepSkip },
                    { "Step Cancelled", ScheduleConstants.StepCancel },
                    { "Step Started", ScheduleConstants.StepStart },
                    { "Std Out Length", ProgramHistoryRecord.STD_OUT },
                    { "Std Err Length", ProgramHistoryRecord.STD_ERR }
                };
            }
        }
    }

    [ClassDoc("A mapping from history counter to Object Id", Name = "Statistic Ref")]
    public class StatRef
    {
        [BsonElement("i")]
        [JsonConverter(typeof(ObjectRefConverter))]
        [MemberDoc("Id of referenced object")]
        public Guid ObjectID { get; set; }

        [BsonElement("s"), MemberDoc("List of counters that incremented for this object")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification = "Serialized data property")]
        public byte[] Statistics { get; set; }

        public StatRef() { }
    }

    [BsonSerializer(typeof(ListMaxSerializer<StatList, StatRef>))]
    public class StatList : List<StatRef> { }

    public class StatRefConverter : IListConverter<StatRef>
    {
        public StatRef Read(MemoryStream stream)
        {
            var guid = new byte[16];
            if (stream.Position + 16 > stream.Length)
            {
                stream.Read(guid, 0, (int)(stream.Length - stream.Position));
                return null;
            }

            stream.Read(guid, 0, 16);
            var statCount = stream.ReadByte(); // we know it will fit in a byte because one run can have at most 40 counters of 5 bytes each
            var ret = new StatRef { ObjectID = new Guid(guid), Statistics = new byte[statCount] };
            stream.Read(ret.Statistics, 0, statCount);

            return ret;
        }

        public void Write(StatRef obj, MemoryStream stream)
        {
            if (obj == null || obj.Statistics == null || obj.ObjectID == null) { return; }
            stream.Write(obj.ObjectID.ToByteArray(), 0, 16);
            stream.WriteByte((byte)obj.Statistics.Length);
            stream.Write(obj.Statistics, 0, obj.Statistics.Length);
        }
    }

    [ClassDoc("Details of a unique error", Name = "Error Detail", RuleDoc = true)]
    public class ErrorDetail
    {
        [BsonId, MemberDoc("Guid derived from hash identifying the error")]
        public Guid Id { get; set; }

        [BsonElement("p"), BsonIgnoreIfDefault, MemberDoc("Id of Provider that contributes error record")]
        public Guid Provider { get; set; }

        [BsonElement("err"), MemberDoc("Short error code")]
        public string ErrorCode { get; set; }

        [BsonIgnore, MemberDoc(Ignore = true)]
        public string Type { get { return "ErrorDetail"; } }

        [BsonElement("details"), BsonIgnoreIfNull, MemberDoc("Dictionary of name/message values describing the error")]
        public AttributeDictionary<string> ErrorDetails { get; set; }

        [JsonIgnore]
        [BsonElement("sd")]
        public DateTime Saved { get; set; }

        public ErrorDetail()
        {
            ErrorDetails = new AttributeDictionary<string>();
        }

        public void SetId(Guid providerId)
        {
            var doc = new BsonDocument(
                new List<BsonElement>
                {
                    new BsonElement("ErrorCode", ErrorCode),
                    new BsonElement("ErrorDetails", new BsonArray(ErrorDetails.Values.ToArray()))
                }
            );

            Id = NamedGuid.Create(providerId, Hash.Get(doc.ToBson()));
            Provider = providerId;
        }
    }

    [BsonSerializer(typeof(ListMaxSerializer<HistoryErrorList, HistoryError>))]
    public class HistoryErrorList : List<HistoryError> { }

    public class HistoryError
    {
        [MemberDoc("General category of error")]
        public int Category { get; set; }

        [MemberDoc(Ignore = true)]
        public string Type { get { return this.GetType().Name; } }

        [JsonConverter(typeof(ErrorRefConverter))]
        [MemberDoc("Id of Error Detail reference")]
        public Guid DetailId { get; set; }

        [MemberDoc(Ignore = true)]
        public ErrorDetail Detail { get; set; }
        
        [MemberDoc("Timestamp of error")]
        public DateTime? TimeStamp { get; set; }

        [MemberDoc("Timestamp error first occurred (not always populated)")]
        public DateTime? FirstOccurred { get; set; }

        [JsonConverter(typeof(ObjectRefConverter))]
        [MemberDoc("Id of Object Record that triggered the error")]
        public Guid? ObjectId { get; set; }

        [MemberDoc(Ignore = true)]
        public object Object { get; set; }

        [MemberDoc("DN of Object Record that triggered the error")]
        public string DN { get; set; }

        [MemberDoc(Ignore = true)]
        public string DisplayName { get; set; }

        [MemberDoc(Ignore = true)]
        public List<int> Counts { get; set; }
    }

    public class HistoryErrorConverter : IListConverter<HistoryError>
    {
        const int DN = 0x1;
        const int DISPLAYNAME = 0x2;
        const int O1 = 0x4;
        const int O2 = 0x8; // legacy from SyncError CS and MV
        readonly byte[] COUNTS = new byte[] { 0x10, 0x20, 0x40, 0x80 };

        public HistoryError Read(MemoryStream stream)
        {
            if (stream.Position + 16 + 16 + 1 > stream.Length)
            {
                stream.Position = stream.Length - 1;
                return null;
            }

            var ret = new HistoryError() { Category = stream.ReadByte() };
            var detailId = new byte[16];
            stream.Read(detailId, 0, 16);
            ret.DetailId = new Guid(detailId);

            var stamp = new byte[8];
            stream.Read(stamp, 0, 8);
            var stampI = BitConverter.ToInt64(stamp, 0);
            if (stampI > 0)
            {
                ret.TimeStamp = new DateTime(stampI);
            }

            stamp = new byte[8];
            stream.Read(stamp, 0, 8);
            stampI = BitConverter.ToInt64(stamp, 0);
            if (stampI > 0)
            {
                ret.FirstOccurred = new DateTime(stampI);
            }

            var mask = stream.ReadByte();
            byte[] lengthBuffer;
            byte[] buffer;
            ushort length;
            if ((mask & DN) > 0)
            {
                lengthBuffer = new byte[2];
                stream.Read(lengthBuffer, 0, 2);
                length = BitConverter.ToUInt16(lengthBuffer, 0);
                buffer = new byte[length];
                stream.Read(buffer, 0, length);
                ret.DN = Encoding.UTF8.GetString(buffer);
            }

            if ((mask & DISPLAYNAME) > 0)
            {
                lengthBuffer = new byte[2];
                stream.Read(lengthBuffer, 0, 2);
                length = BitConverter.ToUInt16(lengthBuffer, 0);
                buffer = new byte[length];
                stream.Read(buffer, 0, length);
                ret.DisplayName = Encoding.UTF8.GetString(buffer);
            }

            if ((mask & O1) > 0)
            {
                buffer = new byte[16];
                stream.Read(buffer, 0, 16);
                ret.ObjectId = new Guid(buffer);
            }

            if ((mask & O2) > 0)
            {
                buffer = new byte[16];
                stream.Read(buffer, 0, 16);
                ret.ObjectId = new Guid(buffer);
            }

            for (int i = 0; i < COUNTS.Length; i++)
            {
                if ((mask & COUNTS[i]) > 0)
                {
                    if (ret.Counts == null)
                    {
                        ret.Counts = new List<int>();
                        ret.Counts.AddRange(from j in COUNTS
                                            select 0);
                    }

                    buffer = new byte[4];
                    stream.Read(buffer, 0, 4);
                    ret.Counts[i] = BitConverter.ToInt32(buffer, 0);
                }
            }

            return ret;
        }

        public void Write(HistoryError error, MemoryStream stream)
        {
            if (error == null || error.DetailId == null)
            {
                return;
            }

            stream.WriteByte((byte)error.Category);
            stream.Write(error.DetailId.ToByteArray(), 0, 16);
            if (error.TimeStamp.HasValue)
            {
                stream.Write(BitConverter.GetBytes(error.TimeStamp.Value.Ticks), 0, 8);
            }
            else
            {
                stream.Write(BitConverter.GetBytes(0L), 0, 8);
            }

            if (error.FirstOccurred.HasValue)
            {
                stream.Write(BitConverter.GetBytes(error.FirstOccurred.Value.Ticks), 0, 8);
            }
            else
            {
                stream.Write(BitConverter.GetBytes(0L), 0, 8);
            }

            var buffer = new MemoryStream();
            int mask = 0;
            if (!string.IsNullOrEmpty(error.DN))
            {
                mask |= DN;
                buffer.Write(BitConverter.GetBytes((ushort)error.DN.Length), 0, 2);
                buffer.Write(Encoding.UTF8.GetBytes(error.DN), 0, error.DN.Length);
            }

            if (!string.IsNullOrEmpty(error.DisplayName))
            {
                mask |= DISPLAYNAME;
                buffer.Write(BitConverter.GetBytes((ushort)error.DisplayName.Length), 0, 2);
                buffer.Write(Encoding.UTF8.GetBytes(error.DisplayName), 0, error.DisplayName.Length);
            }

            if (error.ObjectId.HasValue)
            {
                mask |= O1;
                buffer.Write(error.ObjectId.Value.ToByteArray(), 0, 16);
            }

            if (error.Counts != null)
            {
                for (int i = 0; i < error.Counts.Count; i++)
                {
                    if (error.Counts[i] != 0)
                    {
                        mask |= COUNTS[i];
                        buffer.Write(BitConverter.GetBytes(error.Counts[i]), 0, 4);
                    }
                }
            }

            var bytes = buffer.ToArray();
            stream.WriteByte((byte)mask);
            stream.Write(bytes, 0, bytes.Length);
        }
    }

    /// <summary>
    /// Base model for history operations (MA Run, Full Scan, Program Result, Cloud Scan etc.)
    /// </summary>
    [ClassDoc("Base type for history operations (MA Run, Full Scan, Program Result, Cloud Scan etc.)", Name = "History Record", RuleDoc = true)]
    [BsonDiscriminator("hr", Required = true)]
    public class HistoryRecord
    {
        public const byte ERROR = 59;

        [BsonId, MemberDoc("Unique Id of History Record")]
        public Guid Id { get; set; }

        public HistoryRecord()
        {
            Id = Guid.NewGuid();
            PanelService = Environment.MachineName;
        }

        [BsonElement("p"), BsonIgnoreIfDefault, MemberDoc("Id of Provider that contributes history record")]
        public Guid Provider { get; set; }

        [BsonElement("ps"), BsonIgnoreIfNull, MemberDoc("Panel Service contributing history record")]
        public string PanelService { get; set; }

        [BsonElement("r"), MemberDoc("Result summary of history")]
        public string Result { get; set; }

        [BsonIgnore, MemberDoc("Instance type of History Record: e.g. RunRecord, ProgramHistoryRecord")]
        public virtual string Type { get { return GetType().Name; } }

        [BsonElement("w"), MemberDoc("Scope of History Record, e.g. Management Agent, Program, Scan type")]
        public object RecordOf { get; set; }

        [BsonElement("a"), BsonIgnoreIfNull, MemberDoc("Parameter to History Record, e.g. Run Profile, Program arguments")]
        public object Argument { get; set; }

        [BsonElement("sDt"), BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public DateTime StartDate { get; set; }

        [BsonElement("eDt"), BsonDateTimeOptions(Kind = DateTimeKind.Utc), MemberDoc("If EndDate is null, the History item is still running")]
        public DateTime? EndDate { get; set; }

        [BsonElement("Os"), BsonIgnoreIfNull,
         ListMax(1024 * 1024 * 10),
         ListConverter(typeof(StatRefConverter)), MemberDoc("List of Statistic Refs, one entry per referenced object"),
         JsonIgnore]
        public StatList ObjectDetails { get; set; }

        Dictionary<Guid, List<byte>> _objectDetailCache;
        uint[] ctrCache;

        public void CounterTally(byte ctr)
        {
            if (ctrCache == null) { ctrCache = new uint[256]; }
            ctrCache[ctr]++;
        }
        public void CounterTally(byte ctr, uint count)
        {
            if (ctrCache == null) { ctrCache = new uint[256]; }
            ctrCache[ctr] += count;
        }

        /// <summary>
        /// Must call FinishDetails() to persist data passed to this
        /// </summary>
        public void DetailTally(Guid objId, byte counter)
        {
            if (_objectDetailCache == null) { _objectDetailCache = new Dictionary<Guid, List<byte>>(); }
            if (!_objectDetailCache.TryGetValue(objId, out List<byte> changes))
            {
                changes = new List<byte>();
                _objectDetailCache[objId] = changes;
            }

            changes.Add(counter);
        }

        public void ErrorTally(HistoryError item)
        {
            if (Errors == null) { Errors = new HistoryErrorList(); }
            DetailTally(item.ObjectId.Value, (byte)item.Category);
            Errors.Add(item);
        }

        public void DetailFinish()
        {
            if (ObjectDetails == null) { ObjectDetails = new StatList(); }

            uint[] counters;
            bool merge = false;
            if (ctrCache != null)
            {
                counters = ctrCache;
                merge = true;
            }
            else
            {
                counters = new uint[256];
            }

            if (_objectDetailCache != null)
            {
                merge = true;
                foreach (var pair in _objectDetailCache)
                {
                    if (pair.Value == null || pair.Value.Count == 0) { continue; }
                    ObjectDetails.Add(new StatRef { ObjectID = pair.Key, Statistics = pair.Value.ToArray() });

                    foreach (byte b in pair.Value)
                    {
                        counters[b]++;
                    }
                }
            }

            if (merge)
            {
                var ctrs = Counters;
                if (ctrs != null)
                {
                    foreach (var pair in ctrs)
                    {
                        counters[pair.Key] += (byte)pair.Value;
                    }
                }

                EncodedCounters = EncodeCounters(counters);
            }
        }

        [JsonIgnore, MemberDoc(Ignore = true)]
        public virtual Type ObjectDetailType { get { return typeof(ObjectRecord); } }

        [BsonElement("errs"), BsonIgnoreIfNull, ListMax(1024 * 1024 * 2), ListConverter(typeof(HistoryErrorConverter)), MemberDoc("List of errors")]
        public HistoryErrorList Errors { get; set; }

        [BsonElement("c"), BsonIgnoreIfNull, JsonIgnore, MemberDoc(Ignore = true)]
        public byte[] EncodedCounters { get; set; }

        internal static Type[] GetObjectTypes
        {
            get
            {
                var ret = new List<Type>();
                foreach (var assem in AppDomain.CurrentDomain.GetAssemblies())
                {
                    if (assem.FullName.Contains("SoftwareIDM"))
                    {
                        foreach (var t in assem.ExportedTypes)
                        {
                            if (t.IsAssignableFrom(typeof(HistoryRecord)) && t != typeof(HistoryRecord))
                            {
                                ret.Add(t);
                            }
                        }
                    }
                }

                return ret.ToArray();
            }
        }

        [BsonElement("cc")]
        [BsonDictionaryOptions(DictionaryRepresentation.Document)]
        [MemberDoc("Change counters for history decoded from binary structure", Depends = "EncodedCounters", RuleIndexChoices = "js:helpers.counterSelect")]
        public virtual NumberDictionary<int> Counters
        {
            get
            {
                return DecodeCountersNum(EncodedCounters);
            }
            set { }
        }

        #region Counter Encoding

        public const byte UI8 = 0x00;
        public const byte UI16 = 0x40;
        public const byte UI24 = 0x80;
        public const byte UI32 = 0xc0;
        public const byte UIMask = 0x3f;

        static void Write(MemoryStream stream, uint val)
        {
            var bytes = BitConverter.GetBytes(val);
            if (!BitConverter.IsLittleEndian)
            {
                bytes = bytes.Reverse().ToArray();
            }

            if (val <= UIMask)
            {
                stream.Write(new byte[] { bytes[0] }, 0, 1);
            }
            else if (val <= UIMask << 8)
            {
                stream.Write(new byte[] { (byte)(UI16 | bytes[1]), bytes[0] }, 0, 2);
            }
            else if (val <= UIMask << 16)
            {
                stream.Write(new byte[] { (byte)(UI24 | bytes[2]), bytes[1], bytes[0] }, 0, 3);
            }
            else
            {
                stream.Write(new byte[] { (byte)(UI32 | bytes[3]), bytes[2], bytes[1], bytes[0] }, 0, 4);
            }
        }

        static uint Read(byte[] counters, ref int i)
        {
            uint val = counters[i];
            byte[] temp;
            if ((val & UI32) == UI32)
            {
                temp = new byte[] { counters[i + 3], counters[i + 2], counters[i + 1], (byte)(counters[i] & UIMask) };
                i += 4;
            }
            else if ((val & UI24) == UI24)
            {
                temp = new byte[] { counters[i + 2], counters[i + 1], (byte)(counters[i] & UIMask), 0 };
                i += 3;
            }
            else if ((val & UI16) == UI16)
            {
                temp = new byte[] { counters[i + 1], (byte)(counters[i] & UIMask), 0, 0 };
                i += 2;
            }
            else
            {
                temp = new byte[] { counters[i], 0, 0, 0 };
                i++;
            }

            if (!BitConverter.IsLittleEndian)
            {
                temp = temp.Reverse().ToArray();
            }

            return BitConverter.ToUInt32(temp, 0);
        }

        /// <summary>
        /// Encode counters into a compact byte array
        /// </summary>
        public static byte[] EncodeCounters(Dictionary<uint, uint> counters)
        {
            var ret = new MemoryStream();
            foreach (var pair in counters)
            {
                Write(ret, pair.Key);
                Write(ret, pair.Value);
            }

            var reta = ret.ToArray();
            return reta.Length > 0 ? reta : null;
        }

        /// <summary>
        /// Encode counters into a compact byte array
        /// </summary>
        public static byte[] EncodeCounters(NumberDictionary<int> counters)
        {
            var ret = new MemoryStream();
            foreach (var pair in counters)
            {
                Write(ret, (uint)pair.Key);
                Write(ret, (uint)pair.Value);
            }

            var reta = ret.ToArray();
            return reta.Length > 0 ? reta : null;
        }

        /// <summary>
        /// Encode counters into a compact byte array
        /// </summary>
        public static byte[] EncodeCounters(uint[] counters)
        {
            var ret = new MemoryStream();
            for (uint i = 0; i < counters.Length; i++)
            {
                if (counters[i] > 0)
                {
                    Write(ret, i);
                    Write(ret, counters[i]);
                }
            }

            var reta = ret.ToArray();
            return reta.Length > 0 ? reta : null;
        }

        /// <summary>
        /// Decode compact counter array into dictionary
        /// </summary>
        public static NumberDictionary<int> DecodeCountersNum(byte[] counters)
        {
            var ret = new NumberDictionary<int>();
            if (counters == null)
            {
                return ret;
            }

            for (int i = 0; i < counters.Length;)
            {
                uint ctr = Read(counters, ref i);
                uint val = Read(counters, ref i);
                ret.Add(ctr, (int)val);
            }

            return ret;
        }

        /// <summary>
        /// Decode compact counter array into dictionary
        /// </summary>
        public static Dictionary<uint, uint> DecodeCounters(byte[] counters)
        {
            var ret = new Dictionary<uint, uint>();
            if (counters == null)
            {
                return ret;
            }

            for (int i = 0; i < counters.Length;)
            {
                uint ctr = Read(counters, ref i);
                uint val = Read(counters, ref i);
                ret.Add(ctr, val);
            }

            return ret;
        }

        #endregion
    }
}