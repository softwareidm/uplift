﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using SoftwareIDM.PanelModel.Attributes;

namespace SoftwareIDM.PanelModel.Models
{
    [ClassDoc("Data record for historical notes on Identity Panel objects", Name = "Notes Record", RuleDoc = true)]
    public class NoteRecord
    {
        [BsonId]
        public ObjectId Id { get; set; }

        [BsonElement("oid"), BsonIgnoreIfDefault]
        [JsonConverter(typeof(NoteRefConverter))]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public Guid ObjectId { get; set; }

        [BsonElement("ts")]
        public DateTime TimeStamp { get; set; }

        [BsonElement("ots")]
        [BsonIgnoreIfNull]
        public DateTime? ObjectTimeStamp { get; set; }

        [BsonElement("msg")]
        public string Message { get; set; }

        [BsonIgnoreIfNull]
        [BsonElement("iss")]
        public string Issue { get; set; }

        [BsonIgnoreIfNull]
        [BsonElement("h")]
        public string Health { get; set; }

        [BsonElement("u")]
        public string User { get; set; }

        [BsonElement("uf")]
        public string UserFriendly { get; set; }

        [BsonElement("p")]
        [BsonIgnoreIfDefault]
        public bool Private { get; set; }

        [BsonElement("am"), BsonIgnoreIfNull]
        public AttributeDictionary<string> AttributeMessages { get; set; }
    }
}
