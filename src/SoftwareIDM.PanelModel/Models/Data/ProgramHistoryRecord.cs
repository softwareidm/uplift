﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using SoftwareIDM.PanelModel.Attributes;
using SoftwareIDM.PanelModel.Helpers;


namespace SoftwareIDM.PanelModel.Models
{
    [BsonDiscriminator("ph", Required = true)]
    [ClassDoc("History record for result of running a program or script", Name = "Program History", RuleDoc = true)]
    public class ProgramHistoryRecord : HistoryRecord
    {
        public const byte STD_OUT = 60;
        public const byte STD_ERR = 61;

        public ProgramHistoryRecord() : base()
        {
            Id = Guid.NewGuid();
            Provider = ScheduleConstants.ProviderId;
            Result = "";
            StartDate = DateTime.UtcNow;
        }

        [BsonIgnore, MemberDoc("Accessor to map RecordOf to Program name")]
        public string Program
        {
            get { return (string)RecordOf; }
            set { RecordOf = value; }
        }

        [BsonIgnore, MemberDoc("Accessor to map Argument to Args")]
        public string Args
        {
            get { return (string)Argument; }
            set { Argument = value; }
        }

        [BsonElement("o"), MemberDoc("First kilobyte of output to StdOut")]
        public string StdOut { get; set; }

        [BsonElement("ot"), BsonIgnoreIfNull, MemberDoc("Whether StdOut was truncated")]
        public bool? StdOutTruncated { get; set; }

        [BsonElement("e"), MemberDoc("First kilobyte of output to StdErr")]
        public string StdErr { get; set; }

        [BsonElement("et"), BsonIgnoreIfNull, MemberDoc("Whether StdErr was truncated")]
        public bool? StdErrTruncated { get; set; }

        public void Finish(string stdOut, string stdErr)
        {
            EndDate = DateTime.UtcNow;
            var counters = new Dictionary<uint, uint>
            {
                { STD_OUT, string.IsNullOrEmpty(stdOut) ? (uint)0 : (uint)1 },
                { STD_ERR, string.IsNullOrEmpty(stdErr) ? (uint)0 : (uint)1 }
            };

            EncodedCounters = EncodeCounters(counters);
            if (!string.IsNullOrEmpty(stdOut))
            {
                StdOut = stdOut;
                if (stdOut.Length > 10 * 1024)
                {
                    StdOut = StdOut.Substring(0, 10 * 1024);
                    StdOutTruncated = true;
                }
            }

            if (!string.IsNullOrEmpty(stdErr))
            {
                Result = "Error";
                StdErr = stdErr;
                if (stdErr.Length > 10 * 1024)
                {
                    StdErr = StdErr.Substring(0, 10 * 1024);
                    StdErrTruncated = true;
                }
            }
            else
            {
                Result = "success";
            }
        }

    }
}