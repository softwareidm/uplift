﻿using System;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using SoftwareIDM.PanelModel.Attributes;
using SoftwareIDM.PanelModel.Helpers;

namespace SoftwareIDM.PanelModel.Models
{
    public enum RunStatus
    {
        NotStarted = 0,
        Running = 1,
        FailedStart = 2,
        Finished = 3,
        Failed = 4,
        TimedOut = 5,
        Skipped = 6,
        Cancelled = 7
    }

    public static class ScheduleConstants
    {
        public const byte ScheduleRun = 70;
        public const byte StepQueue = 71;
        public const byte StepStart = 72;
        public const byte StepFinish = 73;
        public const byte StepFail = 74;
        public const byte StepSkip = 75;
        public const byte StepCancel = 76;

        public static Guid ProviderId { get { return Guid.Parse("4194ad23-e8ee-4e0a-af09-4d7f63d0c3d9"); } }
    }

    public class StepRecord
    {
        [BsonElement("i")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification = "Serialized data property")]
        public byte[] Id { get; set; }

        [BsonElement("n")]
        public string Name { get; set; }

        [BsonElement("s")]
        public string Service { get; set; }

        [BsonElement("sDt"), BsonIgnoreIfDefault]
        public DateTime Started { get; set; }

        [BsonElement("du"), BsonIgnoreIfDefault]
        public TimeSpan Duration { get; set; }
        
        [BsonElement("h")]
        [BsonIgnoreIfNull]
        [JsonConverter(typeof(HistoryRefConverter))]
        public Guid? HistoryId { get; set; }

        [BsonElement("r")]
        [JsonConverter(typeof(Newtonsoft.Json.Converters.StringEnumConverter))]
        public RunStatus Result { get; set; }

        [BsonElement("e"), BsonIgnoreIfNull]
        public string Error { get; set; }
    }

    [BsonDiscriminator("sch", Required = true), ClassDoc("History Record of executing a schedule", Name = "Schedule Record", RuleDoc = true)]
    public class ScheduleRecord : HistoryRecord
    {
        public override Type ObjectDetailType
        {
            get
            {
                return typeof(HistoryRecord);
            }
        }
        
        [BsonElement("stp")]
        public List<StepRecord> Steps { get; set; }

        public ScheduleRecord() : base() { }

        public ScheduleRecord(Schedule schedule, DateTime dispatch)
            : this()
        {
            // use named guid in case we hit a duplicate key error on the schedule
            Id = NamedGuid.Create(schedule.Id, dispatch.ToString());
            Provider = ScheduleConstants.ProviderId;
            RecordOf = "Schedule Run";
            Argument = schedule.Name;

            var counters = Counters;

            if (!counters.ContainsKey(ScheduleConstants.ScheduleRun))
            {
                counters.Add(ScheduleConstants.ScheduleRun, 1);
            }

            if (!counters.ContainsKey(ScheduleConstants.StepQueue))
            {
                counters.Add(ScheduleConstants.StepQueue, 0);
            }

            foreach (var _ in schedule.Steps)
            {
                counters[ScheduleConstants.StepQueue]++;
            }

            EncodedCounters = EncodeCounters(counters);
            StartDate = dispatch;
        }

        public void AddSteps(List<StepInstance> steps)
        {
            Steps = (from s in steps
                     select new StepRecord { Id = s.Id, Name = s.Step.Name, Result = RunStatus.NotStarted }).ToList();
        }

        public virtual void TallySkip(int index)
        {
            var counters = Counters;
            if (!counters.ContainsKey(ScheduleConstants.StepSkip))
            {
                counters[ScheduleConstants.StepSkip] = 1;
            }
            else
            {
                counters[ScheduleConstants.StepSkip]++;
            }

            Steps[index].Result = RunStatus.Skipped;
            EncodedCounters = EncodeCounters(counters);
        }

        public virtual void TallyStep(StepInstance step)
        {
            for (int i = 0; i < Steps.Count; i++)
            {
                if (Steps[i].Id.SequenceEqual(step.Id))
                {
                    Steps[i].Name = step.Step.Name;
                    Steps[i].Service = step.Service;
                    Steps[i].Result = step.Status;
                    Steps[i].Error = step.Error;
                    Steps[i].HistoryId = step.StepHistoryId;
                    Steps[i].Started = step.Started;
                    if (step.Started != default && step.Finished != default)
                    {
                        Steps[i].Duration = step.Finished - step.Started;
                    }

                    break;
                }
            }

            uint counter;
            switch (step.Status)
            {
                case RunStatus.FailedStart:
                case RunStatus.Failed:
                case RunStatus.TimedOut:
                    counter = ScheduleConstants.StepFail;
                    break;
                case RunStatus.Finished:
                    counter = ScheduleConstants.StepFinish;
                    break;
                case RunStatus.Running:
                    counter = ScheduleConstants.StepStart;
                    break;
                case RunStatus.Cancelled:
                    counter = ScheduleConstants.StepCancel;
                    break;
                default:
                    counter = ScheduleConstants.StepSkip;
                    break;
            }
            
            var counters = Counters;
            var objDetails = new Dictionary<Guid, List<uint>>();
            if (ObjectDetails == null)
            {
                ObjectDetails = new StatList();
            }

            foreach (var stat in ObjectDetails)
            {
                objDetails[stat.ObjectID] = (from b in stat.Statistics
                                             select (uint)b).ToList();
            }

            if (!counters.ContainsKey(counter))
            {
                counters[counter] = 0;
            }

            counters[counter] += 1;

            if (step.StepHistoryId.HasValue)
            {
                if (!objDetails.ContainsKey(step.StepHistoryId.Value))
                {
                    objDetails.Add(step.StepHistoryId.Value, new List<uint> { counter });
                }
                else
                {
                    objDetails[step.StepHistoryId.Value].Add(counter);
                }
            }

            EncodedCounters = EncodeCounters(counters);

            ObjectDetails = new StatList();
            foreach (var pair in objDetails)
            {
                var add = new byte[pair.Value.Count];
                for (int i = 0; i < pair.Value.Count; i++)
                {
                    add[i] = (byte)pair.Value[i];
                }

                ObjectDetails.Add(new StatRef { ObjectID = pair.Key, Statistics = add });
            }
        }

        public virtual void SavePrep(DateTime now)
        {
            var counters = Counters;
            int queue = counters.ContainsKey(ScheduleConstants.StepQueue) ? counters[ScheduleConstants.StepQueue] : 0;

            int finish = 0;
            if (counters.ContainsKey(ScheduleConstants.StepFail))
            {
                finish += counters[ScheduleConstants.StepFail];
            }

            if (counters.ContainsKey(ScheduleConstants.StepFinish))
            {
                finish += counters[ScheduleConstants.StepFinish];
            }

            if (counters.ContainsKey(ScheduleConstants.StepSkip))
            {
                finish += counters[ScheduleConstants.StepSkip];
            }

            if (finish == queue)
            {
                if (!counters.ContainsKey(ScheduleConstants.StepFail) || counters[ScheduleConstants.StepFail] == 0)
                {
                    Result = "Success";
                }
                else
                {
                    Result = "Steps Failed";
                }

                EndDate = now;
            }
        }
    }

    [ClassDoc("Schedule Status report")]
    public class ScheduleStatus
    {
        public Dictionary<string, DateTime> LastServiceProbes { get; set; }

        public List<ScheduleInstance> Schedules { get; set; }

        public List<StepInstance> Steps { get; set; }

        public ScheduleStatus()
        {
            LastServiceProbes = new Dictionary<string, DateTime>(StringComparer.OrdinalIgnoreCase);
            Schedules = new List<ScheduleInstance>();
            Steps = new List<StepInstance>();
        }
    }

    [ClassDoc("Instance of an executing schedule", RuleDoc = true, Name = "Schedule Instance")]
    public class ScheduleInstance
    {
        [BsonId, JsonConverter(typeof(Url64Converter))]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification = "Serialized data property")]
        // schedule id + time
        public byte[] Id { get; set; }

        public ScheduleInstance() { }

        public ScheduleInstance(Guid scheduleId, DateTime dispatch)
        {
            Dispatch = dispatch;
            ScheduleId = scheduleId;
            Id = new byte[16 + 8];
            scheduleId.ToByteArray().CopyTo(Id, 0);
            BitConverter.GetBytes(dispatch.ToBinary()).CopyTo(Id, 16);
        }

        [BsonElement("i")]
        public bool Interactive { get; set; }

        [BsonElement("r")]
        public bool Running { get; set; }

        [BsonElement("e")]
        public bool Exclusive { get; set; }

        [BsonElement("s")]
        public Guid ScheduleId { get; set; }

        [BsonElement("n")]
        public string Name { get; set; }

        [BsonElement("p")]
        public SchedulePriority Priority { get; set; }

        [BsonElement("dt")]
        public DateTime Dispatch { get; set; }

        [BsonElement("h")]
        public Guid HistoryId { get; set; }

        [BsonElement("eDt")]
        [BsonIgnoreIfNull]
        public DateTime? EndDate { get; set; }
    }

    [ClassDoc("Instance of a pending, executing, or completed Schedule Step", Name = "Step Instance", RuleDoc = true)]
    public class StepInstance : IComparable<StepInstance>
    {
        public StepInstance()
        {
            Locks = new List<string>();
        }

        byte[] _id;

        [BsonId, JsonConverter(typeof(Url64Converter))]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification = "Serialized data property")]
        // schedule instance id + order
        public byte[] Id
        {
            get
            {
                if (ScheduleInstance != null)
                {
                    return GetId(ScheduleInstance, Order);
                }

                return _id;
            }
            set { _id = value; }
        }

        public static byte[] GetId(byte[] scheduleInstance, int order)
        {
            var ret = new byte[16 + 8 + 4];
            scheduleInstance.CopyTo(ret, 0);
            BitConverter.GetBytes(order).CopyTo(ret, 16 + 8);
            return ret;
        }

        public int CompareTo(StepInstance other)
        {
            if (other == null) { return 1; }

            return Order.CompareTo(other.Order);
        }

        [BsonElement("sn")]
        public string Service { get; set; }

        [BsonElement("si")]
        public Guid ServiceInstance { get; set; }

        [BsonElement("ln")]
        public List<string> Locks { get; set; }

        [BsonElement("o")]
        public int Order { get; set; }

        [BsonElement("s"), JsonConverter(typeof(Url64Converter))]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification = "Serialized data property")]
        public byte[] ScheduleInstance { get; set; }

        [BsonElement("sa")]
        public RunStatus Status { get; set; }

        [BsonElement("e"), BsonIgnoreIfNull]
        public string Error { get; set; }

        [BsonElement("sDt"), BsonIgnoreIfDefault]
        public DateTime Started { get; set; }

        [BsonElement("eDt"), BsonIgnoreIfDefault]
        public DateTime Finished { get; set; }

        [BsonElement("i")]
        public bool InProcess { get; set; }

        [BsonElement("p")]
        public bool Paused { get; set; }

        [BsonElement("h"), BsonIgnoreIfNull]
        public Guid? StepHistoryId { get; set; }

        [BsonElement("st")]
        public ScheduleStep Step { get; set; }

        [BsonElement("sk")]
        public int Skip { get; set; }
    }
}