﻿using System;
using System.Collections.Generic;
using SoftwareIDM.PanelModel.Attributes;

namespace SoftwareIDM.PanelModel.Models
{

    [ClassDoc("Identity Panel Graph join representation", Name = "Identity Panel Graph")]
    public class IdentityGraph : IEquatable<IdentityGraph>
    {
        public Dictionary<Guid, ObjectRecord> ById { get; set; } = new Dictionary<Guid, ObjectRecord>();
        public Dictionary<string, ObjectRecord> BySilo { get; set; } = new Dictionary<string, ObjectRecord>();

        public ObjectRecord Root { get; set; }

        public Dictionary<string, List<object>> Multis { get; set; } = new Dictionary<string, List<object>>();

        public Dictionary<string, IdentityGraph> SingleRefs { get; set; } = new Dictionary<string, IdentityGraph>();

        public Dictionary<string, ListWrap<IdentityGraph>> MultiRefs { get; set; } = new Dictionary<string, ListWrap<IdentityGraph>>();

        public Dictionary<string, int> RefIdCount { get; set; } = new Dictionary<string, int>();

        public bool Equals(IdentityGraph other)
        {
            if (other != null && Root != null && other.ById.ContainsKey(Root.Id))
            {
                return true;
            }

            return false;
        }
    }
}
