﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Options;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using SoftwareIDM.PanelModel.Attributes;
using SoftwareIDM.PanelModel.Helpers;

namespace SoftwareIDM.PanelModel.Models
{
    [Flags]
    public enum ModType
    {
        None = 0x0,
        Add = 0x1,
        Update = 0x2,
        Rename = 0x4,
        Move = 0x8,
        Delete = 0x10,
        Connect = 0x20,
        Disconnect = 0x40,
        Error = 0x80,
        PendingStatus = 0x100,
        AddDisconnect = 0x200,
        ClearPendingStatus = 0x400,
        DataRetention = 0x800,
        Delta = 0x1000
    }

    public enum AModt
    {
        NotConfigured = 0,
        Add = 1,
        Replace = 2,
        Update = 3,
        Delete = 4,
        Remove = 5
    }

    [ClassDoc("Modification to an attribute value", Name = "Attribute Change")]
    public class AttributeChange
    {
        [BsonElement("k"), MemberDoc("What kind of change was made to the attribute: NotConfigured, Add, Replace, Update, Delete, Remove")]
        public AModt Kind { get; set; }

        [BsonElement("v")]
        [MemberDoc("New attribute value")]
        public object Value { get; set; }
    }

    /// <summary>
    /// Class for representing a join
    /// </summary>
    public class Link
    {
        [BsonElement("c"), MemberDoc("Whether the link is currently connected. Will always be true except in a Change record.")]
        public bool Connected { get; set; }

        [BsonIgnoreIfDefault]
        [BsonElement("o")]
        [JsonConverter(typeof(ObjectRefConverter))]
        [MemberDoc("Id of the connected Object Record")]
        public Guid Other { get; set; }

        [BsonIgnoreIfNull]
        [BsonElement("s")]
        [MemberDoc("Silo of the connected Object Record")]
        public string Silo { get; set; }

        [BsonElement("dt"), MemberDoc("Timestamp of the link should only be populated if the provider can detect a timestamp different from the Object Change timestamp")]
        public DateTime TimeStamp { get; set; }
    }

    /// <summary>
    /// Class for representing the hash for change comparison on an object. Uses efficient non-standard binary serialization
    /// </summary>
    public class ObjectCompareHash : ICustomSerializer
    {
        [BsonId]
        public Guid Id { get; set; }

        [BsonElement("h")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification = "DTO (data transfer object")]
        public byte[] Hash { get; set; }

        [BsonElement("l"), BsonIgnoreIfNull]
        public List<Link> Links { get; set; }

        public ObjectCompareHash() { }

        public List<string> SupportedMediaTypes
        {
            get { return new List<string> { "application/bson" }; }
        }

        public async Task WriteToStream(Stream stream, string mediaType)
        {
            // ID
            await stream.WriteAsync(Id.ToByteArray(), 0, 16);

            // Hash
            await stream.WriteAsync(Hash ?? new byte[16], 0, 16);

            if (Links != null)
            {
                var links = (from l in Links where l.Connected select l).ToArray();
                // Links note: for hash timestamp not needed
                await stream.WriteAsync(new byte[] { (byte)links.Length }, 0, 1);
                foreach (var link in links)
                {
                    await stream.WriteAsync(link.Other.ToByteArray(), 0, 16);
                }
            }
            else
            {
                await stream.WriteAsync(new byte[] { 0 }, 0, 1);
            }
        }

        public object ReadFromStream(Stream stream, string mediaType)
        {
            var buff = new byte[16];
            stream.Read(buff, 0, 16);
            Id = new Guid(buff);
            buff = new byte[16];
            stream.Read(buff, 0, 16);
            Hash = buff;

            var lLen = stream.ReadByte();
            if (lLen > 0)
            {
                Links = new List<Link>(lLen);
                for (int j = 0; j < lLen; j++)
                {
                    buff = new byte[16];
                    stream.Read(buff, 0, 16);
                    Links.Add(new Link { Connected = true, Other = new Guid(buff) });
                }
            }

            return this;
        }
    }

    /// <summary>
    /// Common base class for a change to an object that can appear in Time Traveler (e.g. MVRecord, CSRecord, CloudRecord)
    /// </summary>
    [ClassDoc("Base class for representing a change to an object", Name = "Object Change")]
    public class ObjectChange
    {
        [BsonElement("dt"), BsonDateTimeOptions(Kind = DateTimeKind.Utc), MemberDoc("Timestamp when change was tracked")]
        public DateTime TimeStamp { get; set; }

        [BsonElement("l"), BsonIgnoreIfNull, MemberDoc("Connected object links that changed at this timestamp. Null if no links changed")]
        public List<Link> Links { get; set; }

        [BsonElement("s"), MemberDoc("History Record Id that was the source of this change", Name = "Change Source")]
        public Guid? ChangeSource { get; set; }

        [BsonElement("m"), MemberDoc("Flags enumeration indicating what change types were made", Name = "Modification Type")]
        public ModType ModificationType { get; set; }

        [BsonElement("n"), BsonIgnoreIfNull, MemberDoc("DN is null unless it changed at this timestamp")]
        public AttributeChange DN { get; set; }


        [BsonElement("ot"), BsonIgnoreIfNull, MemberDoc("ObjectType is null unless it changed at this timestamp")]
        public AttributeChange ObjectType { get; set; }

        [BsonIgnoreIfNull, BsonElement("a"), BsonDictionaryOptions(DictionaryRepresentation.Document)]
        [MemberDoc("Single attributes that changed", Name = "Attributes", RuleIndexChoices = "js:helpers.attrData")]
        public AttributeDictionary<AttributeChange> Attributes { get; set; }

        /// <summary>
        /// Look at the old and new object, and construct the change based on any differences
        /// </summary>
        public virtual void Compare(ObjectRecord old, ObjectRecord _new, Guid? source, DateTime timeStamp)
        {
            // zero out change
            ModificationType = 0;
            Links = new List<Link>();
            Attributes = new AttributeDictionary<AttributeChange>();

            ChangeSource = source;
            TimeStamp = timeStamp;
            if (_new == null || _new.ObjectType == null || _new.Deleted.HasValue)
            {
                ModificationType = ModType.Delete;
                return;
            }

            if (old == null || old.ObjectType == null)
            {
                ModificationType = ModType.Add;
                DN = new AttributeChange { Kind = AModt.Add, Value = _new.DN };
                ObjectType = new AttributeChange { Kind = AModt.Add, Value = _new.ObjectType };
                Attributes = Helpers.Compare.CompareSingleAttrs(null, _new.Attributes);
                if (_new.Links != null && _new.Links.Count > 0)
                {
                    Links = _new.Links;
                    ModificationType |= ModType.Connect;
                }
            }
            else
            {
                if (old.DN != _new.DN)
                {
                    DN = new AttributeChange { Kind = AModt.Update, Value = _new.DN };
                    ModificationType |= ModType.Rename;
                }
                else
                {
                    DN = null;
                }

                if (old.ObjectType != _new.ObjectType)
                {
                    ObjectType = new AttributeChange { Kind = AModt.Update, Value = _new.ObjectType };
                    ModificationType |= ModType.Rename;
                }
                else
                {
                    ObjectType = null;
                }

                Attributes = Helpers.Compare.CompareSingleAttrs(old.Attributes, _new.Attributes);
                if (Attributes.Count > 0)
                {
                    ModificationType |= ModType.Update;
                }
                
                ModificationType |= Helpers.Compare.CompareLinkSet(old.Links, _new.Links, timeStamp, out List<Link> links);
                if (links != null && links.Count > 0)
                {
                    Links = links;
                }
            }
        }
    }

    /// <summary>
    /// Base class for objects that can be viewed in Time Traveler
    /// </summary>
    [BsonDiscriminator("o", Required = true)]
    [ClassDoc(Name = "Object Record", Description = "Core identity data representation", RuleDoc = true)]
    public class ObjectRecord
    {
        [BsonId, MemberDoc("Unique Id in Object Record collection")]
        public Guid Id { get; set; }

        [BsonElement("i"), MemberDoc("Identifier (usually a Guid string) of the silo or search index this object belongs to", Name = "Silo")]
        public string Silo { get; set; }

        [BsonElement("n"), MemberDoc("Name of object, not necessarily unique"), BsonIgnoreIfNull]
        public string DN { get; set; }

        [BsonElement("t"), MemberDoc("Type class of the object, e.g. User, Group", Name = "Object Type")]
        public string ObjectType { get; set; }

        [BsonElement("mt"), BsonIgnoreIfNull, MemberDoc("Last modified timestamp, includes create and delete modifications")]
        public DateTime? Modified { get; set; }

        [BsonElement("ct"), BsonIgnoreIfNull, MemberDoc("Timestamp object was first recorded")]
        public DateTime? Created { get; set; }

        [BsonElement("d"), MemberDoc("Timestamp object deletion was recorded")]
        public DateTime? Deleted { get; set; }

        [BsonElement("a")]
        [BsonIgnoreIfNull]
        [MemberDoc("Set of single-valued attributes", Name = "Attributes", RuleIndexChoices = "js:helpers.attrData")]
        [BsonDictionaryOptions(DictionaryRepresentation.Document)]
        public AttributeDictionary<object> Attributes { get; set; }

        [BsonElement("h")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification = "DTO (data transfer object")]
        public byte[] Hash { get; set; }

        [BsonElement("l"), BsonIgnoreIfNull, MemberDoc("List of objects in other silos that this object is joined to")]
        public List<Link> Links { get; set; }

        [BsonElement("j"), BsonIgnoreIfNull, MemberDoc("Constructed join-value used to query related objects, created by rule engine when saving.")]
        public List<string> JoinValues { get; set; }

        [BsonElement("ch"), MemberDoc("List of object changes. The most recent change is always at index 0, the oldest at index -1 (last).")]
        public List<ObjectChange> Changes { get; set; }

        [BsonElement("mn"), MemberDoc("List names of multi-value attributes defined by this object, true indicates reference.")]
        public AttributeDictionary<bool> MultiNames { get; set; }

        [BsonIgnore, MemberDoc("Instance Type, e.g. CSRecord, CloudRecord")]
        public virtual string Type { get { return GetType().Name; } }


        /// <summary>
        /// Used to accumulate multi-value attributes so they can be saved as a batch
        /// </summary>
        [BsonIgnore, JsonIgnore, MemberDoc(Ignore = true)]
        public Dictionary<string, MultiAttrCache> MultiAttributes { get; set; }

        public ObjectRecord() { }

        public virtual Dictionary<SearchField, object> SearchFields()
        {
            return new Dictionary<SearchField, object> {
                { new SearchField { Name="DN", BsonPath = ElHelper<ObjectRecord>.FieldName(o => o.DN) }, this.DN }
            };
        }

        public virtual ObjectRecord BuildAt(DateTime timeStamp, bool includeLinks)
        {
            if (timeStamp < Changes[Changes.Count - 1].TimeStamp)
            {
                return null;
            }

            var ret = (ObjectRecord)Activator.CreateInstance(GetType());
            ret.Id = Id;
            ret.Silo = Silo;
            ret.Changes = new List<ObjectChange>();
            ret.Attributes = new AttributeDictionary<object>();
            ret.Created = Changes[Changes.Count - 1].TimeStamp;

            Dictionary<Guid, Link> links = new Dictionary<Guid, Link>();

            for (int i = Changes.Count - 1; i >= 0; i--)
            {
                var ch = Changes[i];
                if (ch.TimeStamp > timeStamp)
                {
                    break;
                }

                ret.Changes.Insert(0, ch);
                ret.Modified = ch.TimeStamp;
                if (ch.DN != null && ch.DN.Value != null)
                {
                    ret.DN = ch.DN.Value.ToString();
                }

                if (ch.ObjectType != null && ch.ObjectType.Value != null)
                {
                    ret.ObjectType = ch.ObjectType.Value.ToString();
                }

                if ((ch.ModificationType & ModType.Delete) > 0)
                {
                    ret.Deleted = ch.TimeStamp;
                }

                if (ch.Attributes != null && ch.Attributes.Count > 0)
                {
                    foreach (var attr in ch.Attributes)
                    {
                        if (attr.Value.Kind == AModt.Delete || attr.Value.Kind == AModt.Remove && ret.Attributes.ContainsKey(attr.Key))
                        {
                            ret.Attributes.Remove(attr.Key);
                        }
                        else
                        {
                            ret.Attributes[attr.Key] = attr.Value.Value;
                        }
                    }
                }

                if (includeLinks && ch.Links != null && ch.Links.Count > 0)
                {
                    foreach (var link in ch.Links)
                    {
                        if (link.Connected && link.Other != Guid.Empty)
                        {
                            links[link.Other] = link;
                        }
                        else if (links.ContainsKey(link.Other))
                        {
                            links.Remove(link.Other);
                        }
                    }

                    ret.Links = links.Values.ToList();
                }
            }

            if (ret.ObjectType == null)
            {
                ret.ObjectType = ObjectType;
            }

            return ret;
        }
    }

    /// <summary>
    /// Class for saving Multivalue attributes or reference attributes
    /// </summary>
    [JsonConverter(typeof(MultiConverter))]
    [ClassDoc("Represents a multi-value attribute for an Object Record, including value and reference attributes", Name = "Multi Attribute", RuleDoc = true)]
    public sealed class MultiAttr
    {
        [BsonId]
        public ObjectId Id { get; set; }

        [BsonElement("ha")]
        [MemberDoc("Hash is a 16 byte hash of attribute, value, and objectId")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification = "DTO (data transfer object")]
        public byte[] Hash { get; set; }

        [BsonIgnore, MemberDoc(Ignore = true)]
        public string Type { get { return "MultiAttr"; } }

        [BsonElement("o")]
        [MemberDoc("Object that defines the multi-attribute")]
        public Guid ObjectId { get; set; }

        [BsonElement("s"), BsonIgnoreIfNull, MemberDoc("Silo of multi-attribute, used to lookup hashes for scan")]
        public string Silo { get; set; } // Indicates Silo

        [BsonElement("v"), MemberDoc("Value or Reference Id")]
        public object Value { get; set; }

        [BsonElement("ve"), BsonIgnoreIfNull, JsonProperty(NullValueHandling = NullValueHandling.Ignore), MemberDoc("If Value is more than 400 characters, the whole value is in ValueExtended, and only the first 400 is placed in Value.")]
        public object ValueExtended { get; set; }

        [BsonElement("a"), MemberDoc("Attribute Name")]
        public string Attribute { get; set; }

        [BsonElement("t"), MemberDoc("Timestamp attribute value was added")]
        public DateTime TimeStamp { get; set; }

        [BsonElement("h"), MemberDoc("Id of History Record that contributed value")]
        public Guid AddSource { get; set; }

        [BsonElement("d"), MemberDoc("Timestamp attribute was deleted")]
        public DateTime? Deleted { get; set; }

        [BsonElement("u"), BsonIgnoreIfNull, MemberDoc("Id of History Record that deleted value")]
        public Guid? DeleteSource { get; set; }

        [BsonElement("r"), BsonIgnoreIfNull, MemberDoc("Whether this is a reference attribute")]
        public bool? IsRef { get; set; }

        [BsonIgnoreIfNull]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public object Object { get; set; }

        public MultiAttr() { }

        public void SetId()
        {
            var attr = Encoding.UTF8.GetBytes(Attribute);
            var val = Encoding.UTF8.GetBytes((Value ?? "").ToString());
            var id = ObjectId.ToByteArray();
            var stream = new MemoryStream();
            stream.Write(id, 0, 16);
            stream.Write(attr, 0, attr.Length);
            stream.Write(val, 0, val.Length);
            Hash = Helpers.Hash.Get(stream.ToArray());
            stream.Dispose();
            Id = MongoDB.Bson.ObjectId.GenerateNewId();
        }
    }

    /// <summary>
    /// Multivalue cache to accumulate all the multi attributes for an object so they don't have to be saved one at a time.
    /// </summary>
    public class MultiAttrCache
    {
        [BsonElement("r")]
        public bool IsRef { get; set; }

        [BsonElement("v")]
        public List<object> Values { get; set; }

        public MultiAttrCache()
        {
            Values = new List<object>();
        }
    }

    public class MultiCompareHash : ICustomSerializer
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification = "DTO (data transfer object")]
        public byte[] Hash { get; set; } // length is 16 for hash

        [BsonElement("o")]
        public Guid ObjectId { get; set; }

        public MultiCompareHash() { }

        public List<string> SupportedMediaTypes
        {
            get { return new List<string> { "application/bson" }; }
        }

        public async Task WriteToStream(Stream stream, string mediaType)
        {
            await stream.WriteAsync(Hash, 0, 16);
            await stream.WriteAsync(ObjectId.ToByteArray(), 0, 16);
        }

        public object ReadFromStream(Stream stream, string mediaType)
        {
            Hash = new byte[16];
            stream.Read(Hash, 0, 16);

            var buff = new byte[16];
            stream.Read(buff, 0, 16);
            ObjectId = new Guid(buff);

            return null;
        }
    }
}