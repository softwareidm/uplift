﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;
using SoftwareIDM.PanelModel.Attributes;

namespace SoftwareIDM.PanelModel.Models
{
    public interface IRunHealthProbe : IDisposable
    {
        Task Init(CancellationToken cts);

        Task<ProbeResult> Test(HealthProbe data, CancellationToken cts);
    }

    [ClassDoc("Health Check history record for triggering workflow and archiving results", Name = "Health Check", RuleDoc = true)]
    public class HealthCheck
    {
        [BsonId, MemberDoc("Randomly generated unique Id of health check instance")]
        public Guid Id { get; set; }

        [BsonElement("n"), MemberDoc("Display name from settings")]
        public string Name { get; set; }

        [BsonElement("r")]
        [MemberDoc("List of evaluated probe results", RuleIndexChoices = "js:helpers.healthProbeSelect")]
        public AttributeDictionary<ProbeResult> Data { get; set; }

        [BsonElement("p"), MemberDoc("Whether the check as a whole passed or failed, based on Fail Rules of probes")]
        public bool Passed { get; set; }

        [BsonElement("a"), BsonIgnoreIfDefault, MemberDoc("Whether this check should be stored permanently, or erased after one day")]
        public bool Archive { get; set; }

        [BsonElement("t"), MemberDoc("When this health check was performed")]
        public DateTime TimeStamp { get; set; }

        [BsonElement("s"), MemberDoc("Server where this health check was evaluted")]
        public string Server { get; set; }

        [BsonIgnore, MemberDoc(Ignore = true)]
        public string Type { get { return this.GetType().Name; } }

        public HealthCheck()
        {
            Id = Guid.NewGuid();
            Data = new AttributeDictionary<ProbeResult>();
        }
    }
}