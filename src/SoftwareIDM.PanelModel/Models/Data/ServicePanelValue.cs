﻿using System;
using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;
using SoftwareIDM.PanelModel.Attributes;

namespace SoftwareIDM.PanelModel.Models
{
    [BsonDiscriminator("spivs")]
    [ClassDoc("Data class for holding results of an Identity Picker multi-selection choice in Service Panel", Name = "Service Panel Identity Values", RuleDoc = true)]
    public class ServicePanelIdentityValues
    {
        [MemberDoc("List of values being added")]
        [BsonElement("a")]
        public List<ServicePanelIdentityValue> Adds { get; set; } = new List<ServicePanelIdentityValue>();

        [MemberDoc("List of values being removed")]
        [BsonElement("d")]
        public List<ServicePanelIdentityValue> Deletes { get; set; } = new List<ServicePanelIdentityValue>();

        [MemberDoc("Complete list of values (not present on values produced by Table Identity Select)")]
        [BsonElement("f")]
        public List<ServicePanelIdentityValue> Full { get; set; } = new List<ServicePanelIdentityValue>();
    }

    [BsonDiscriminator("spiv")]
    [ClassDoc("Data class for holding results of an Identity Picker single-selection choice in Service Panel", Name = "Service Panel Identity Value", RuleDoc = true)]
    public class ServicePanelIdentityValue
    {
        [MemberDoc("Reference value in a virtual silo as Guid identifier")]
        [BsonElement("r")]
        public Guid Reference { get; set; }

        [MemberDoc("Calculated value from picker")]
        [BsonElement("v")]
        public object Value { get; set; }
    }

    [BsonDiscriminator("sprv")]
    [ClassDoc("Data class for holding results of a repeating form input in Service Panel", Name = "Service Panel Repeating Value", RuleDoc = true)]
    public class ServicePanelRepeatingValue
    {
        [MemberDoc("List of repeating mini-form payloads")]
        [BsonElement("d")]
        public List<Dictionary<string, object>> Data { get; set; } = new List<Dictionary<string, object>>();
    }
}
