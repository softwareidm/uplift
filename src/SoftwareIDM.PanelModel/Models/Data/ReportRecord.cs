﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using SoftwareIDM.PanelModel.Attributes;

namespace SoftwareIDM.PanelModel.Models
{
    [ClassDoc("Row data for a report entry", Name = "Report Data")]
    public class ReportRecord
    {
        [BsonId, MemberDoc(Ignore = true)]
        public ObjectId Id { get; set; }

        [BsonElement("st"), MemberDoc(Ignore = true)]
        public DateTime Started { get; set; }

        [BsonElement("t"), MemberDoc(Ignore = true)]
        public DateTime Touched { get; set; }

        [BsonElement("ex"), MemberDoc(Ignore = true)]
        public DateTime Expires { get; set; }

        [BsonElement("pi"), BsonIgnoreIfNull, MemberDoc(Ignore = true)]
        public DateTime? PointInTime { get; set; }

        [BsonElement("r")]
        public Guid Report { get; set; }

        [BsonElement("ri")]
        public Guid ReportId { get; set; }

        [BsonElement("d"), JsonIgnore]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification = "DTO (data transfer object")]
        public byte[] Data { get; set; }

        [BsonElement("co")]
        public int Count { get; set; }

        [BsonElement("tr")]
        public bool Truncated { get; set; }

        [BsonIgnoreIfNull]
        [BsonElement("pr")]
        public List<string> Progress { get; set; }

        [BsonElement("c")]
        public bool Completed { get; set; }

        [BsonElement("f")]
        public bool Failed { get; set; }

        [BsonElement("m")]
        public PortableNameMap Map { get; set; }

        readonly MemoryStream buffer = new MemoryStream();

        public bool Write(AttributeDictionary<object> row)
        {
            if (Map == null)
            {
                Map = new PortableNameMap();
            }

            var mapper = new PortableAttributeMapper(Map);
            var write = row.ToBson(mapper);
            buffer.Write(BitConverter.GetBytes(write.Length), 0, 4);
            buffer.Write(write, 0, write.Length);
            if (buffer.Length > 1024 * 1024 * 6)
            {
                return false;
            }

            return true;
        }

        public void Prepare()
        {
            if (Map == null) { Map = new PortableNameMap(); }
            buffer.Position = 0;
            using (var compressed = new MemoryStream())
            {
                using (var gzip = new GZipStream(compressed, CompressionMode.Compress))
                {
                    buffer.CopyTo(gzip);
                }

                Data = compressed.ToArray();
            }

            buffer.Dispose();
        }

        public void Hydrate()
        {
            var mapper = new PortableAttributeMapper(Map);
            using var inStream = new MemoryStream(Data);
            using var gzip = new GZipStream(inStream, CompressionMode.Decompress);
            using var result = new MemoryStream();
            gzip.CopyTo(result);
            result.Position = 0;

            Rows = new List<AttributeDictionary<object>>();
            while (result.Length > result.Position)
            {
                var nextB = new byte[4];
                result.Read(nextB, 0, 4);
                int next = BitConverter.ToInt32(nextB, 0);
                var readB = new byte[next];
                result.Read(readB, 0, next);
                if (mapper == null)
                {
                    Rows.Add(MongoDB.Bson.Serialization.BsonSerializer.Deserialize<AttributeDictionary<object>>(readB));
                }
                else
                {
                    Rows.Add(readB.FromBson<AttributeDictionary<object>>(mapper));
                }
            }
        }

        [BsonIgnore]
        public List<AttributeDictionary<object>> Rows { get; set; }

        public ReportRecord()
        {
            Rows = new List<AttributeDictionary<object>>();
        }
    }
}