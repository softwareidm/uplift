﻿using System;
using SoftwareIDM.PanelModel.Attributes;

namespace SoftwareIDM.PanelModel.Models
{
    [ClassDoc("Helper to construct an absolute DateTime reference from either a string date or an offset from the present", "Date Definition")]
    public class DateDefinition
    {
        [MemberDoc("Time unit for relative offset, e.g. Years, Minutes")]
        public string Unit { get; set; }

        [MemberDoc("Absolute date name, e.g. 2014-05-03")]
        public string Date { get; set; }

        [MemberDoc("Multiplier for Unit")]
        public double? Quantity { get; set; }

        public bool HasDate()
        {
            if (!string.IsNullOrEmpty(Date))
            {
                return true;
            }

            if (!string.IsNullOrEmpty(Unit) && Quantity.HasValue)
            {
                return true;
            }

            return false;
        }

        DateTime? date;
        public DateTime GetDate(DateTime relative)
        {
            if (!date.HasValue)
            {
                if (!string.IsNullOrEmpty(Date))
                {
                    date = DateTime.Parse(Date);
                }
                else
                {
                    switch (Unit)
                    {
                        case "Seconds":
                            date = relative.AddSeconds(-1 * Quantity.Value);
                            break;
                        case "Minutes":
                            date = relative.AddMinutes(-1 * Quantity.Value);
                            break;
                        case "Hours":
                            date = relative.AddHours(-1 * Quantity.Value);
                            break;
                        case "Days":
                            date = relative.Date.AddDays(-1 * Quantity.Value);
                            break;
                        case "Weeks":
                            date = relative.Date.AddDays(-1 * 7 * Quantity.Value);
                            break;
                        case "Months":
                            date = relative.Date.AddMonths(-1 * (int)Quantity.Value);
                            break;
                        default: // "Years":
                            date = relative.Date.AddYears(-1 * (int)Quantity.Value);
                            break;
                    }
                }
            }

            return date.Value.ToUniversalTime();
        }
    }
}