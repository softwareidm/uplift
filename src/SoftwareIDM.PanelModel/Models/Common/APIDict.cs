﻿using System;
using System.Collections.Generic;

namespace SoftwareIDM.PanelModel.Models
{
    /// <summary>
    /// This is really just a white listed type name so it can be used to return and save string:T results with the web app.
    /// </summary>
    [Attributes.ClassDoc("Dictionary for serializing web API")]
    public class ApiDict<T> : Dictionary<string, T>
    {
        public ApiDict() : base(StringComparer.OrdinalIgnoreCase) { }

        public ApiDict(string key, T value) : base(StringComparer.OrdinalIgnoreCase)
        {
            this[key] = value;
        }
    }
}