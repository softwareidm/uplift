﻿using System;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace SoftwareIDM.PanelModel.Models
{
    public class LogEntry
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public LogLevel LogLevel { get; set; }
        
        public string Message { get; set; }

        public string Context { get; set; }

        public int EventId { get; set; }
    }
}