﻿using System;
using System.Linq;
using System.Xml.Linq;

namespace SoftwareIDM.PanelModel.Models
{
    /// <summary>
    /// Deserialization helpers for use with MIM Test
    /// </summary>
    public class XmlParseException : Exception
    {
#pragma warning disable IDE1006 // Naming Styles
        public XElement xml { get; set; }
#pragma warning restore IDE1006 // Naming Styles
        public Type ParseType { get; set; }

        public XmlParseException(string message)
            : base(message)
        { }

        public XmlParseException(string message, XElement xml, Type ParseType)
            : base(message)
        {
            this.xml = xml;
            this.ParseType = ParseType;
        }

        public XmlParseException()
        {
        }

        public XmlParseException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }

    public static class XmlHelper
    {
        public static void RequiredAttribute(XElement xml, XName attribute, Type type)
        {
            if (xml.Attributes(attribute).Count() == 0)
            {
                if (attribute.Namespace.Equals(SI))
                {
                    attribute = attribute.LocalName;
                }

                throw new XmlParseException($"Error parsing {type.Name}. Missing attribute {attribute}", xml, type);
            }
        }
        public static void RequiredElement(XElement xml, XName element, Type type)
        {
            if (xml.Elements(element).Count() == 0)
            {
                throw new XmlParseException($"Error parsing {type.Name}. Missing element {element.LocalName}", xml, type);
            }
        }

        public static XNamespace XSI { get { return XNamespace.Get("http://www.w3.org/2001/XMLSchema-instance"); } }
        public static XNamespace SI { get { return XNamespace.Get("http://softwareidm.com/panel"); } }
    }
}
