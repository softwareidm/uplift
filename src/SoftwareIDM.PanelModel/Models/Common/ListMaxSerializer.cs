﻿using System;
using System.Collections.Generic;
using System.IO;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.Serializers;

namespace SoftwareIDM.PanelModel.Models
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public sealed class ListMaxAttribute : BsonSerializationOptionsAttribute
    {
        // constructors
        /// <summary>
        /// Initializes a new instance of the BsonDictionaryOptionsAttribute class.
        /// </summary>
        public ListMaxAttribute() { }

        /// <summary>
        /// Initializes a new instance of the BsonDictionaryOptionsAttribute class.
        /// </summary>
        /// <param name="representation">The representation to use for the Dictionary.</param>
        public ListMaxAttribute(int maxSize)
        {
            MaxSize = maxSize;
        }

        // public properties
        /// <summary>
        /// Gets or sets the external representation.
        /// </summary>
        public int MaxSize { get; set; } = 1024 * 1024 * 4;

        protected override IBsonSerializer Apply(IBsonSerializer serializer)
        {
            if (serializer.GetType().IsGenericType && serializer.GetType().GetGenericTypeDefinition() == typeof(ListMaxSerializer<,>))
            {
                serializer.GetType().GetProperty("MaxSize").SetValue(serializer, MaxSize);
            }

            return serializer;
        }
    }

    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public sealed class ListConverterAttribute : BsonSerializationOptionsAttribute
    {
        public ListConverterAttribute() { }

        public ListConverterAttribute(Type t)
        {
            ListConverter = t;
        }

        public Type ListConverter { get; set; }

        // protected methods
        /// <summary>
        /// Reconfigures the specified serializer by applying this attribute to it.
        /// </summary>
        /// <param name="serializer">The serializer.</param>
        /// <returns>A reconfigured serializer.</returns>
        protected override IBsonSerializer Apply(IBsonSerializer serializer)
        {
            if(serializer.GetType().IsGenericType && serializer.GetType().GetGenericTypeDefinition() == typeof(ListMaxSerializer<,>))
            {
                serializer.GetType().GetProperty("Converter").SetValue(serializer, Activator.CreateInstance(ListConverter));
            }

            return serializer;
        }
    }

    public interface IListConverter<T>
    {
        T Read(MemoryStream stream);
        void Write(T obj, MemoryStream stream);
    }

    public class DefaultListConverter<T> : IListConverter<T> where T : class
    {
        public T Read(MemoryStream stream)
        {
            if (stream.Position + 4 > stream.Length)
            {
                return null;
            }

            var cache = new byte[4];
            stream.Read(cache, 0, 4);
            var len = BitConverter.ToInt32(cache, 0);
            cache = new byte[len];
            stream.Read(cache, 0, len);
            return BsonSerializer.Deserialize<T>(cache);
        }

        public void Write(T obj, MemoryStream stream)
        {
            var cache = obj.ToBson();
            stream.Write(BitConverter.GetBytes(cache.Length), 0, 4);
            stream.Write(cache, 0, cache.Length);
        }

        public DefaultListConverter() { }
    }

    /// <summary>
    /// A custom Bson serializer for Lists that allows a maximum binary size to be set. Data beyond the maximum is silently omitted.
    /// </summary>
    public class ListMaxSerializer<T, U> : SerializerBase<T>, IBsonArraySerializer where T : List<U>, new() where U : class
    {
        public int MaxSize { get; set; }
        public IListConverter<U> Converter { get; set; }

        // private static fields
        private static readonly BsonValueSerializer __instance = new BsonValueSerializer();

        public ListMaxSerializer()
        {
            MaxSize = 1024 * 1 - 24 * 4;
            Converter = new DefaultListConverter<U>();
        }
        
        // public static properties
        /// <summary>
        /// Gets an instance of the BsonValueSerializer class.
        /// </summary>
        public static BsonValueSerializer Instance
        {
            get { return __instance; }
        }

        public override T Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args)
        {
            var bsonReader = context.Reader;

            var bsonType = bsonReader.GetCurrentBsonType();
            var ret = new T();
            switch (bsonType)
            {
                case BsonType.Array:
                    bsonReader.ReadStartArray();
                    while(bsonReader.ReadBsonType() != BsonType.EndOfDocument)
                    {
                        var item = BsonSerializer.Deserialize<U>(bsonReader);
                        ret.Add(item);
                    }

                    bsonReader.ReadEndArray();
                    return ret;
                case BsonType.Binary:
                    var data = bsonReader.ReadBinaryData().Bytes;
                    var stream = new MemoryStream(data, 0, data.Length);
                    while (stream.Position < stream.Length)
                    {
                        ret.Add(Converter.Read(stream));
                    }

                    return ret;
                case BsonType.Null:
                    return null;
                default:
                    var message = $"Invalid BsonType {bsonType}.";
                    throw new BsonInternalException(message);
            }
        }

        public override void Serialize(BsonSerializationContext context, BsonSerializationArgs args, T value)
        {
            var bsonWriter = context.Writer;
            if (value == null)
            {
                bsonWriter.WriteNull();
            }
            else
            {
                if (!(value is T || value is List<U>))
                {
                    var message = $"Can't serialize a {value.GetType().FullName} into a List<{typeof(U).Name}>.";
                    throw new FormatException(message);
                }

                var list = (List<U>)value;
                var stream = new MemoryStream();
                for (int i = 0; i < list.Count && stream.Position < MaxSize; i++)
                {
                    Converter.Write(list[i], stream);
                }

                bsonWriter.WriteBinaryData(new BsonBinaryData(stream.ToArray(), BsonBinarySubType.Binary));
            }
        }
        

        public bool TryGetItemSerializationInfo(out BsonSerializationInfo serializationInfo)
        {
            serializationInfo = new BsonSerializationInfo(
                null,
                BsonValueSerializer.Instance,
                typeof(U));

            return true;
        }
    }
}
