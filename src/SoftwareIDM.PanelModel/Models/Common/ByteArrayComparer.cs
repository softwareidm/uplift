﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace SoftwareIDM.PanelModel.Models
{
    /// <summary>
    /// IEqualityComparer implementation lets you use a byte[] as a dictionary key.
    /// This is important when you need to look stuff up quickly by it's hash.
    /// </summary>
    public class ByteArrayComparer : IEqualityComparer<byte[]>, IComparer<byte[]>
    {
        public bool Equals(byte[] left, byte[] right)
        {
            return Helpers.Compare.UnsafeByteEquals(left, right);
        }

        public int Compare(byte[] left, byte[] right)
        {
            int ret = 0;
            for (int i = 0; i < left.Length && i < right.Length && ret == 0; i++)
            {
                ret = left[i].CompareTo(right[i]);
            }

            if (ret == 0) { return left.Length.CompareTo(right.Length); }

            return ret;
        }

        public int GetHashCode(byte[] key)
        {
            if (key == null)
                throw new ArgumentNullException("key");
            return Convert.ToBase64String(key).GetHashCode();
        }
    }
}