﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.IO;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.Options;
using MongoDB.Bson.Serialization.Serializers;
using SoftwareIDM.PanelModel.Attributes;

namespace SoftwareIDM.PanelModel.Models
{
    /// <summary>
    /// Interface for attribute mapper implementation.
    /// Exists because AttributeDictionary is used on both web app side and client side, but only web app can do mapping.
    /// This way the client never knows about the implementation details of the attribute mapper
    /// </summary>
    public interface IAttributeMapper
    {
        Dictionary<string, string> Map(IEnumerable<string> attribute);

        Dictionary<string, string> DeMap(IEnumerable<string> attribute);

        string Map(string attribute);

        string DeMap(string attribute);

        Func<string, bool> HasAttr { get; set; }
    }

    public interface IHasMapper
    {
        IAttributeMapper Mapper { get; }
    }



    public sealed class PortableAttributeMapper : IAttributeMapper
    {
        readonly PortableNameMap _map = null;

        public PortableAttributeMapper(PortableNameMap map)
        {
            _map = map;
            _map.Init();
        }

        public Func<string, bool> HasAttr { get; set; }

        static string BaseConvert(string value, string from, string to)
        {
            long x = 0;
            foreach (var c in value)
            {
                x = x * from.Length + from.IndexOf(c);
            }

            string res = "";
            while (x > 0)
            {
                var digit = (int)(x % to.Length);
                res = to[digit] + res;
                x /= to.Length;
            }

            return res;
        }

        const string alphabet = "01234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

        public string Map(string attribute)
        {
            if (!_map.DeSafedAttributes.TryGetValue(attribute, out string ret))
            {
                ret = Map(new string[] { attribute })[attribute];
            }

            return ret;
        }

        public string DeMap(string attribute)
        {
            if (_map.ReverseAttributes.TryGetValue(attribute, out string ret))
            {
                return ret;
            }

            return attribute;
        }

        public Dictionary<string, string> Map(IEnumerable<string> attributes)
        {
            var changes = new List<string>();

            foreach (var attribute in attributes)
            {
                if (!_map.DeSafedAttributes.ContainsKey(attribute))
                {
                    changes.Add(attribute);
                }
            }

            if (changes.Count > 0)
            {
                foreach (var attr in changes)
                {
                    int i = 0;
                    var test = attr[0].ToString();
                    while (_map.ReverseAttributes.ContainsKey(test))
                    {
                        test = attr[0].ToString() + BaseConvert(i.ToString(), "0123456789", alphabet);
                        i++;
                    }

                    _map.DeSafedAttributes.Add(attr, test);
                    _map.Attributes.Add(attr.Replace(".", "\\`"), test);
                    _map.ReverseAttributes.Add(test, attr);
                }
            }

            return _map.DeSafedAttributes;
        }

        public Dictionary<string, string> DeMap(IEnumerable<string> attributes)
        {
            var ret = new Dictionary<string, string>();
            foreach (var attr in attributes)
            {
                if (_map.ReverseAttributes.ContainsKey(attr))
                {
                    var candidate = _map.ReverseAttributes[attr];
                    ret[attr] = candidate;
                }
                else
                {
                    ret[attr] = attr;
                }
            }

            return ret;
        }
    }

    [ClassDoc("Attribute name map for feed serialization")]
    public class PortableNameMap
    {
        [BsonDictionaryOptions(Representation = DictionaryRepresentation.ArrayOfArrays)]
        [BsonElement("a")]
        public Dictionary<string, string> Attributes { get; set; }

        [BsonIgnore]
        public Dictionary<string, string> DeSafedAttributes { get; set; }
        
        [BsonElement("r")]
        public Dictionary<string, string> ReverseAttributes { get; set; }

        public PortableNameMap()
        {
            Attributes = new Dictionary<string, string>();
            ReverseAttributes = new Dictionary<string, string>();
            DeSafedAttributes = new Dictionary<string, string>();
        }

        public void Init()
        {
            if (DeSafedAttributes.Count == 0)
            {
                foreach (var pair in ReverseAttributes)
                {
                    DeSafedAttributes[pair.Value] = pair.Key;
                }
            }
        }
    }

    public class AMapBsonBinaryReader : BsonBinaryReader, IHasMapper
    {
        public IAttributeMapper Mapper { get; private set; }
        public AMapBsonBinaryReader(Stream stream, IAttributeMapper mapper) : base(stream)
        {
            Mapper = mapper;
        }

        public AMapBsonBinaryReader(Stream stream, BsonBinaryReaderSettings settings, IAttributeMapper mapper) : base(stream, settings)
        {
            Mapper = mapper;
        }

        public AMapBsonBinaryReader(Stream stream) : base(stream) { }
        public AMapBsonBinaryReader(Stream stream, BsonBinaryReaderSettings settings) : base(stream, settings) { }

    }

    public class AMapBsonDocumentReader : BsonDocumentReader, IHasMapper
    {
        public IAttributeMapper Mapper { get; private set; }
        public AMapBsonDocumentReader(BsonDocument document, IAttributeMapper mapper) : base(document)
        {
            Mapper = mapper;
        }

        public AMapBsonDocumentReader(BsonDocument document, BsonDocumentReaderSettings settings, IAttributeMapper mapper) : base(document, settings)
        {
            Mapper = mapper;
        }

        public AMapBsonDocumentReader(BsonDocument document) : base(document) { }
        public AMapBsonDocumentReader(BsonDocument document, BsonDocumentReaderSettings settings) : base(document, settings) { }

    }

    public class AMapBsonDocumentWriter : BsonDocumentWriter, IHasMapper
    {
        public IAttributeMapper Mapper { get; private set; }

        public AMapBsonDocumentWriter(BsonDocument document, IAttributeMapper mapper) : base(document)
        {
            Mapper = mapper;
        }

        public AMapBsonDocumentWriter(BsonDocument document, BsonDocumentWriterSettings settings, IAttributeMapper mapper) : base(document, settings)
        {
            Mapper = mapper;
        }
        public AMapBsonDocumentWriter(BsonDocument document) : base(document) { }
        public AMapBsonDocumentWriter(BsonDocument document, BsonDocumentWriterSettings settings) : base(document, settings) { }
    }

    public class AMapBsonBinaryWriter : BsonBinaryWriter, IHasMapper
    {
        public IAttributeMapper Mapper { get; private set; }

        public AMapBsonBinaryWriter(Stream stream, IAttributeMapper mapper) : base(stream)
        {
            Mapper = mapper;
        }

        public AMapBsonBinaryWriter(Stream stream, BsonBinaryWriterSettings settings, IAttributeMapper mapper) : base(stream, settings)
        {
            Mapper = mapper;
        }

        public AMapBsonBinaryWriter(Stream stream) : base(stream) { }
        public AMapBsonBinaryWriter(Stream stream, BsonBinaryWriterSettings settings) : base(stream, settings) { }
    }

    public static class AMapExtensionMethods
    {
        public static TNominalType FromBson<TNominalType>(
            this byte[] data, IAttributeMapper mapper)
        {
            using var stream = new MemoryStream(data);
            var reader = new AMapBsonBinaryReader(stream, mapper);
            return BsonSerializer.Deserialize<TNominalType>(reader);
        }
        public static TNominalType FromBsonDocument<TNominalType>(
            this BsonDocument data, IAttributeMapper mapper)
        {
            var reader = new AMapBsonDocumentReader(data, mapper);
            return BsonSerializer.Deserialize<TNominalType>(reader);
        }

        public static byte[] ToBson<TNominalType>(
            this TNominalType obj,
            IAttributeMapper mapper,
            IBsonSerializer<TNominalType> serializer = null,
            BsonBinaryWriterSettings writerSettings = null,
            Action<BsonSerializationContext.Builder> configurator = null,
            BsonSerializationArgs args = default)
        {
            return ToBson(obj, typeof(TNominalType), mapper, writerSettings, serializer, configurator, args);
        }

        public static byte[] ToBson(
            this object obj,
            Type nominalType,
            IAttributeMapper mapper,
            BsonBinaryWriterSettings writerSettings = null,
            IBsonSerializer serializer = null,
            Action<BsonSerializationContext.Builder> configurator = null,
            BsonSerializationArgs args = default)
        {
            if (nominalType == null)
            {
                throw new ArgumentNullException(nameof(nominalType));
            }

            if (serializer == null)
            {
                serializer = BsonSerializer.LookupSerializer(nominalType);
            }

            if (serializer.ValueType != nominalType)
            {
                var message = $"Serializer type {serializer.GetType().FullName} value type does not match document types {nominalType.FullName}.";
                throw new ArgumentException(message, nameof(serializer));
            }

            using var memoryStream = new MemoryStream();
            using (var bsonWriter = new AMapBsonBinaryWriter(memoryStream, writerSettings ?? BsonBinaryWriterSettings.Defaults, mapper))
            {
                var context = BsonSerializationContext.CreateRoot(bsonWriter, configurator);
                args.NominalType = nominalType;
                serializer.Serialize(context, args, obj);
            }
            return memoryStream.ToArray();
        }

        public static BsonDocument ToBsonDocument<TNominalType>(
            this TNominalType obj,
            IAttributeMapper mapper,
            IBsonSerializer<TNominalType> serializer = null,
            Action<BsonSerializationContext.Builder> configurator = null,
            BsonSerializationArgs args = default)
        {
            return ToBsonDocument(obj, typeof(TNominalType), mapper, serializer, configurator, args);
        }

        public static BsonDocument ToBsonDocument(
            this object obj,
            Type nominalType,
            IAttributeMapper mapper,
            IBsonSerializer serializer = null,
            Action<BsonSerializationContext.Builder> configurator = null,
            BsonSerializationArgs args = default)
        {
            if (nominalType == null)
            {
                throw new ArgumentNullException(nameof(nominalType));
            }

            if (obj == null)
            {
                return null;
            }

            if (serializer == null)
            {
                var bsonDocument = obj as BsonDocument;
                if (bsonDocument != null)
                {
                    return bsonDocument; // it's already a BsonDocument
                }

                if (obj is IConvertibleToBsonDocument convertibleToBsonDocument)
                {
                    return convertibleToBsonDocument.ToBsonDocument(); // use the provided ToBsonDocument method
                }

                serializer = BsonSerializer.LookupSerializer(nominalType);
            }
            if (serializer.ValueType != nominalType)
            {
                var message = $"Serializer type {serializer.GetType().FullName} value type does not match document types {nominalType.FullName}.";
                throw new ArgumentException(message, nameof(serializer));
            }

            // otherwise serialize into a new BsonDocument
            var document = new BsonDocument();
            using (var bsonWriter = new AMapBsonDocumentWriter(document, mapper))
            {
                var context = BsonSerializationContext.CreateRoot(bsonWriter, configurator);
                args.NominalType = nominalType;
                serializer.Serialize(context, args, obj);
            }
            return document;
        }
    }

    /// <summary>
    /// Subclass of dictionary to provide auto-shortening of fields.
    /// None of the logic is in the dictionary implementation, we just need a separate Type to implement custom serialization.
    /// Instead of being two types,it's generic to (string, T)
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [BsonSerializer(SerializerType = typeof(AttrDictSerializer<>))]
    [Serializable]
    [ClassDoc("Dictionary for attribute storage that supports automatic field-shortening at serialization", "Attribute Dictionary")]
    public sealed class AttributeDictionary<T> : Dictionary<string, T>
    {
        public AttributeDictionary() : base(StringComparer.OrdinalIgnoreCase) { }

        public AttributeDictionary(IEqualityComparer<string> comparer) : base(comparer) { }

        private AttributeDictionary(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context) :
            base(info, context)
        { }
    }

    public class AttrDictSerializer<TValue> :
        DictionarySerializerBase<AttributeDictionary<TValue>>,
        IChildSerializerConfigurable,
        IDictionaryRepresentationConfigurable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DictionaryInterfaceImplementerSerializer{TDictionary}"/> class.
        /// </summary>
        public AttrDictSerializer()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DictionaryInterfaceImplementerSerializer{TDictionary}"/> class.
        /// </summary>
        /// <param name="dictionaryRepresentation">The dictionary representation.</param>
        public AttrDictSerializer(DictionaryRepresentation dictionaryRepresentation)
            : base(dictionaryRepresentation)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DictionaryInterfaceImplementerSerializer{TDictionary}"/> class.
        /// </summary>
        /// <param name="dictionaryRepresentation">The dictionary representation.</param>
        /// <param name="keySerializer">The key serializer.</param>
        /// <param name="valueSerializer">The value serializer.</param>
        public AttrDictSerializer(DictionaryRepresentation dictionaryRepresentation, IBsonSerializer keySerializer, IBsonSerializer valueSerializer)
            : base(dictionaryRepresentation, keySerializer, valueSerializer)
        {
        }

        // public methods
        /// <summary>
        /// Returns a serializer that has been reconfigured with the specified dictionary representation.
        /// </summary>
        /// <param name="dictionaryRepresentation">The dictionary representation.</param>
        /// <returns>The reconfigured serializer.</returns>
        public AttrDictSerializer<TValue> WithDictionaryRepresentation(DictionaryRepresentation dictionaryRepresentation)
        {
            if (dictionaryRepresentation == DictionaryRepresentation)
            {
                return this;
            }
            else
            {
                return new AttrDictSerializer<TValue>(dictionaryRepresentation, KeySerializer, ValueSerializer);
            }
        }

        /// <summary>
        /// Returns a serializer that has been reconfigured with the specified dictionary representation and key value serializers.
        /// </summary>
        /// <param name="dictionaryRepresentation">The dictionary representation.</param>
        /// <param name="keySerializer">The key serializer.</param>
        /// <param name="valueSerializer">The value serializer.</param>
        /// <returns>The reconfigured serializer.</returns>
        public AttrDictSerializer<TValue> WithDictionaryRepresentation(DictionaryRepresentation dictionaryRepresentation, IBsonSerializer keySerializer, IBsonSerializer valueSerializer)
        {
            if (dictionaryRepresentation == DictionaryRepresentation && keySerializer == KeySerializer && valueSerializer == ValueSerializer)
            {
                return this;
            }
            else
            {
                return new AttrDictSerializer<TValue>(dictionaryRepresentation, keySerializer, valueSerializer);
            }
        }

        /// <summary>
        /// Returns a serializer that has been reconfigured with the specified key serializer.
        /// </summary>
        /// <param name="keySerializer">The key serializer.</param>
        /// <returns>The reconfigured serializer.</returns>
        public AttrDictSerializer<TValue> WithKeySerializer(IBsonSerializer keySerializer)
        {
            if (keySerializer == KeySerializer)
            {
                return this;
            }
            else
            {
                return new AttrDictSerializer<TValue>(DictionaryRepresentation, keySerializer, ValueSerializer);
            }
        }

        /// <summary>
        /// Returns a serializer that has been reconfigured with the specified value serializer.
        /// </summary>
        /// <param name="valueSerializer">The value serializer.</param>
        /// <returns>The reconfigured serializer.</returns>
        public AttrDictSerializer<TValue> WithValueSerializer(IBsonSerializer valueSerializer)
        {
            if (valueSerializer == ValueSerializer)
            {
                return this;
            }
            else
            {
                return new AttrDictSerializer<TValue>(DictionaryRepresentation, KeySerializer, valueSerializer);
            }
        }

        // protected methods
        /// <summary>
        /// Creates the instance.
        /// </summary>
        /// <returns>The instance.</returns>
        protected override AttributeDictionary<TValue> CreateInstance()
        {
            return new AttributeDictionary<TValue>();
        }

        // explicit interface implementations
        IBsonSerializer IChildSerializerConfigurable.ChildSerializer
        {
            get { return ValueSerializer; }
        }

        IBsonSerializer IChildSerializerConfigurable.WithChildSerializer(IBsonSerializer childSerializer)
        {
            return WithValueSerializer(childSerializer);
        }

        IBsonSerializer IDictionaryRepresentationConfigurable.WithDictionaryRepresentation(DictionaryRepresentation dictionaryRepresentation)
        {
            return WithDictionaryRepresentation(dictionaryRepresentation);
        }

        private void SerializeDocumentRepresentation(BsonSerializationContext context, AttributeDictionary<TValue> value)
        {
            var bsonWriter = context.Writer;

            Dictionary<string, string> map = null;
            if (bsonWriter is IHasMapper hm && hm.Mapper != null)
            {
                map = hm.Mapper.Map(value.Keys);
            }

            bsonWriter.WriteStartDocument();
            foreach (var pair in value)
            {
                var key = map == null ? pair.Key : map[pair.Key];
                bsonWriter.WriteName(key);
                ValueSerializer.Serialize(context, pair.Value);
            }

            bsonWriter.WriteEndDocument();
        }

        /// <summary>
        /// Serializes a value.
        /// </summary>
        /// <param name="context">The serialization context.</param>
        /// <param name="args">The serialization args.</param>
        /// <param name="value">The object.</param>
        protected override void SerializeValue(BsonSerializationContext context, BsonSerializationArgs args, AttributeDictionary<TValue> value)
        {
            switch (DictionaryRepresentation)
            {
                case DictionaryRepresentation.Document:
                    SerializeDocumentRepresentation(context, value);
                    break;

                default:
                    // attribute dictionary doesn't do array of arrays or array of documents
                    var message = $"'{DictionaryRepresentation}' is not a valid IDictionary representation.";
                    throw new BsonSerializationException(message);
            }
        }

        private AttributeDictionary<TValue> DeserializeDocumentRepresentation(BsonDeserializationContext context)
        {
            var dictionary = new AttributeDictionary<TValue>(StringComparer.Ordinal);
            var bsonReader = context.Reader;
            bsonReader.ReadStartDocument();

            if (bsonReader is IHasMapper hm && hm.Mapper != null)
            {
                while (bsonReader.ReadBsonType() != BsonType.EndOfDocument)
                {
                    var key = bsonReader.ReadName();
                    var value = BsonSerializer.Deserialize<TValue>(bsonReader);
                    if (key != null)
                    {
                        dictionary.Add(key, value);
                    }
                }

                bsonReader.ReadEndDocument();

                var map = hm.Mapper.DeMap(dictionary.Keys);
                var ret = CreateInstance();
                foreach (var pair in dictionary)
                {
                    if (!map.ContainsKey(pair.Key)) { continue; }

                    ret[map[pair.Key]] = pair.Value;
                }

                return ret;
            }
            else
            {
                while (bsonReader.ReadBsonType() != BsonType.EndOfDocument)
                {
                    var key = bsonReader.ReadName();
                    var value = BsonSerializer.Deserialize<TValue>(bsonReader);
                    if (key != null)
                    {
                        dictionary.Add(key, value);
                    }
                }

                bsonReader.ReadEndDocument();
            }

            return dictionary;
        }

        protected override AttributeDictionary<TValue> DeserializeValue(BsonDeserializationContext context, BsonDeserializationArgs args)
        {
            var bsonType = context.Reader.GetCurrentBsonType();
            if (bsonType == BsonType.Document)
            {
                return DeserializeDocumentRepresentation(context);
            }

            throw CreateCannotDeserializeFromBsonTypeException(bsonType);
        }
    }

    /// <summary>
    /// Subclass of dictionary to provide saving numeric values as Mongo queryable dictionary keys
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [BsonSerializer(SerializerType = typeof(NumberDictSerializer<>))]
    [Serializable]
    [ClassDoc("Dictionary for history counters that converts numbers to strings at serialization", "Counter Dictionary")]
    public sealed class NumberDictionary<T> : Dictionary<long, T>
    {
        public NumberDictionary() : base() { }

        private NumberDictionary(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context) :
            base(info, context)
        { }
    }

    public class NumberDictSerializer<TValue> :
        DictionarySerializerBase<NumberDictionary<TValue>>,
        IChildSerializerConfigurable,
        IDictionaryRepresentationConfigurable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DictionaryInterfaceImplementerSerializer{TDictionary}"/> class.
        /// </summary>
        public NumberDictSerializer()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DictionaryInterfaceImplementerSerializer{TDictionary}"/> class.
        /// </summary>
        /// <param name="dictionaryRepresentation">The dictionary representation.</param>
        public NumberDictSerializer(DictionaryRepresentation dictionaryRepresentation)
            : base(dictionaryRepresentation)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DictionaryInterfaceImplementerSerializer{TDictionary}"/> class.
        /// </summary>
        /// <param name="dictionaryRepresentation">The dictionary representation.</param>
        /// <param name="keySerializer">The key serializer.</param>
        /// <param name="valueSerializer">The value serializer.</param>
        public NumberDictSerializer(DictionaryRepresentation dictionaryRepresentation, IBsonSerializer keySerializer, IBsonSerializer valueSerializer)
            : base(dictionaryRepresentation, keySerializer, valueSerializer)
        {
        }

        // public methods
        /// <summary>
        /// Returns a serializer that has been reconfigured with the specified dictionary representation.
        /// </summary>
        /// <param name="dictionaryRepresentation">The dictionary representation.</param>
        /// <returns>The reconfigured serializer.</returns>
        public NumberDictSerializer<TValue> WithDictionaryRepresentation(DictionaryRepresentation dictionaryRepresentation)
        {
            if (dictionaryRepresentation == DictionaryRepresentation)
            {
                return this;
            }
            else
            {
                return new NumberDictSerializer<TValue>(dictionaryRepresentation, KeySerializer, ValueSerializer);
            }
        }

        /// <summary>
        /// Returns a serializer that has been reconfigured with the specified dictionary representation and key value serializers.
        /// </summary>
        /// <param name="dictionaryRepresentation">The dictionary representation.</param>
        /// <param name="keySerializer">The key serializer.</param>
        /// <param name="valueSerializer">The value serializer.</param>
        /// <returns>The reconfigured serializer.</returns>
        public NumberDictSerializer<TValue> WithDictionaryRepresentation(DictionaryRepresentation dictionaryRepresentation, IBsonSerializer keySerializer, IBsonSerializer valueSerializer)
        {
            if (dictionaryRepresentation == DictionaryRepresentation && keySerializer == KeySerializer && valueSerializer == ValueSerializer)
            {
                return this;
            }
            else
            {
                return new NumberDictSerializer<TValue>(dictionaryRepresentation, keySerializer, valueSerializer);
            }
        }

        /// <summary>
        /// Returns a serializer that has been reconfigured with the specified key serializer.
        /// </summary>
        /// <param name="keySerializer">The key serializer.</param>
        /// <returns>The reconfigured serializer.</returns>
        public NumberDictSerializer<TValue> WithKeySerializer(IBsonSerializer keySerializer)
        {
            if (keySerializer == KeySerializer)
            {
                return this;
            }
            else
            {
                return new NumberDictSerializer<TValue>(DictionaryRepresentation, keySerializer, ValueSerializer);
            }
        }

        /// <summary>
        /// Returns a serializer that has been reconfigured with the specified value serializer.
        /// </summary>
        /// <param name="valueSerializer">The value serializer.</param>
        /// <returns>The reconfigured serializer.</returns>
        public NumberDictSerializer<TValue> WithValueSerializer(IBsonSerializer valueSerializer)
        {
            if (valueSerializer == ValueSerializer)
            {
                return this;
            }
            else
            {
                return new NumberDictSerializer<TValue>(DictionaryRepresentation, KeySerializer, valueSerializer);
            }
        }

        // protected methods
        /// <summary>
        /// Creates the instance.
        /// </summary>
        /// <returns>The instance.</returns>
        protected override NumberDictionary<TValue> CreateInstance()
        {
            return new NumberDictionary<TValue>();
        }

        // explicit interface implementations
        IBsonSerializer IChildSerializerConfigurable.ChildSerializer
        {
            get { return ValueSerializer; }
        }

        IBsonSerializer IChildSerializerConfigurable.WithChildSerializer(IBsonSerializer childSerializer)
        {
            return WithValueSerializer(childSerializer);
        }

        IBsonSerializer IDictionaryRepresentationConfigurable.WithDictionaryRepresentation(DictionaryRepresentation dictionaryRepresentation)
        {
            return WithDictionaryRepresentation(dictionaryRepresentation);
        }

        private void SerializeDocumentRepresentation(BsonSerializationContext context, NumberDictionary<TValue> value)
        {
            var bsonWriter = context.Writer;

            bsonWriter.WriteStartDocument();
            foreach (var pair in value)
            {
                bsonWriter.WriteName(pair.Key.ToString());
                ValueSerializer.Serialize(context, pair.Value);
            }

            bsonWriter.WriteEndDocument();
        }

        /// <summary>
        /// Serializes a value.
        /// </summary>
        /// <param name="context">The serialization context.</param>
        /// <param name="args">The serialization args.</param>
        /// <param name="value">The object.</param>
        protected override void SerializeValue(BsonSerializationContext context, BsonSerializationArgs args, NumberDictionary<TValue> value)
        {
            switch (DictionaryRepresentation)
            {
                case DictionaryRepresentation.Document:
                    SerializeDocumentRepresentation(context, value);
                    break;

                default:
                    // attribute dictionary doesn't do array of arrays or array of documents
                    var message = $"'{DictionaryRepresentation}' is not a valid IDictionary representation.";
                    throw new BsonSerializationException(message);
            }
        }

        private NumberDictionary<TValue> DeserializeDocumentRepresentation(BsonDeserializationContext context)
        {
            var dictionary = CreateInstance();
            var bsonReader = context.Reader;


            bsonReader.ReadStartDocument();
            while (bsonReader.ReadBsonType() != BsonType.EndOfDocument)
            {
                var key = bsonReader.ReadName();
                var value = (TValue)ValueSerializer.Deserialize(context);

                if (key != null)
                {
                    dictionary.Add(long.Parse(key), value);
                }
            }

            bsonReader.ReadEndDocument();
            
            return dictionary;
        }

        protected override NumberDictionary<TValue> DeserializeValue(BsonDeserializationContext context, BsonDeserializationArgs args)
        {
            var bsonType = context.Reader.GetCurrentBsonType();
            if (bsonType == BsonType.Document)
            {
                return DeserializeDocumentRepresentation(context);
            }
            
            throw CreateCannotDeserializeFromBsonTypeException(bsonType);
        }
    }
}