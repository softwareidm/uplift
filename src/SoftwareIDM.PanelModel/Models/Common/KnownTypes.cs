﻿using System;
using System.Collections.Generic;
using System.Reflection;
using SoftwareIDM.PanelModel.Attributes;

namespace SoftwareIDM.PanelModel.Models
{
    /// <summary>
    /// Interface to item types that should be mapped as usable for BSON or JSON serialization.
    /// In most SfotwareIDM dlls this is all the types annotated with ClassDoc
    /// </summary>
    public interface IKnownTypes
    {
        IEnumerable<Type> Types();

        Dictionary<string, Type> Resources { get; }
    }

    public class ModelTypes : IKnownTypes
    {
        public IEnumerable<Type> Types()
        {
            foreach (var type in Assembly.GetExecutingAssembly().ExportedTypes)
            {
                if (type.GetCustomAttribute<ClassDoc>() != null)
                {
                    yield return type;
                }
            }
        }

        public Dictionary<string, Type> Resources => null;
    }
}