﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace SoftwareIDM.PanelModel.Models
{
    /// <summary>
    /// Interface for implementing non-standard serialization of objects. e.g. the custom binary format used by object hash results.
    /// </summary>
    public interface ICustomSerializer
    {
        [JsonIgnore, BsonIgnore]
        List<string> SupportedMediaTypes { get; }

        /// <summary>
        /// Write to stream should serialize the the implementing object to the stream based on the media type.
        /// </summary>
        Task WriteToStream(Stream stream, string mediaType);

        /// <summary>
        /// Read from stream can 1. deserialize from the stream onto the current object, OR 2. deserialize into a new object of the current or a sub-type and return it.
        /// </summary>
        object ReadFromStream(Stream stream, string mediaType);
    }

    /// <summary>
    /// Wraps a list in a Data param for JSON security.
    /// Serializes to non-standard BSON to avoid max size constraint
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ListWrap<T> : ICustomSerializer
    {
        [JsonProperty(ItemTypeNameHandling = TypeNameHandling.Objects)]
        public virtual List<T> Data { get; set; }

        long _count;
        [JsonProperty]
        public virtual long Count { get { return _count > 0 ? _count : Data.Count; } set { _count = value; } }

        public ListWrap()
        {
            Data = new List<T>();
        }
        
        public ListWrap(List<T> data)
        {
            Data = data;
        }

        public ListWrap(IEnumerable<T> data, long count) : this(data.ToList(), count) { }
        
        public ListWrap(List<T> data, long count)
        {
            Data = data;
            _count = count;
        }

        public ListWrap(IEnumerable<T> data) : this(data.ToList()) { }

        public List<string> SupportedMediaTypes
        {
            get
            {
                var ret = new List<string> { "application/bson" };
                if (typeof(T).GetInterface("ICustomSerializer") != null)
                {
                    ret.AddRange(((ICustomSerializer)Activator.CreateInstance(typeof(T))).SupportedMediaTypes);
                }

                return ret;
            }
        }

        public virtual async Task WriteToStream(Stream stream, string mediaType)
        {
            await stream.WriteAsync(BitConverter.GetBytes(Count), 0, 8);

            if (Data != null)
            {
                if (typeof(T) == typeof(byte[]))
                {
                    foreach (var b in Data)
                    {
                        var by = b as byte[];
                        await stream.WriteAsync(BitConverter.GetBytes(by.Length), 0, 4);
                        await stream.WriteAsync(by, 0, by.Length);
                    }
                }
                else if (typeof(T) == typeof(Guid))
                {
                    foreach (object g in Data)
                    {
                        var gu = (Guid)g;
                        await stream.WriteAsync(gu.ToByteArray(), 0, 16);
                    }
                }
                else if (typeof(T) == typeof(string))
                {
                    foreach (object s in Data)
                    {
                        if (s == null)
                        {
                            await stream.WriteAsync(BitConverter.GetBytes(0), 0, 4);
                            continue;
                        }

                        var sd = System.Text.Encoding.UTF8.GetBytes((string)s);
                        await stream.WriteAsync(BitConverter.GetBytes(sd.Length), 0, 4);
                        await stream.WriteAsync(sd, 0, sd.Length);
                    }
                }
                else if (typeof(T).GetInterface("ICustomSerializer") != null)
                {
                    foreach (var obj in Data)
                    {
                        await ((ICustomSerializer)obj).WriteToStream(stream, mediaType);
                    }
                }
                else
                {
                    foreach (var obj in Data)
                    {
                        if (obj == null)
                        {
                            await stream.WriteAsync(BitConverter.GetBytes(0), 0, 4);
                            continue;
                        }

                        var data = obj.ToBson();
                        await stream.WriteAsync(BitConverter.GetBytes(data.Length), 0, 4);
                        await stream.WriteAsync(data, 0, data.Length);
                    }
                }
            }
        }

        public virtual object ReadFromStream(Stream stream, string mediaType)
        {
            var buff = new byte[8];
            stream.Read(buff, 0, 8);
            _count = BitConverter.ToInt64(buff, 0);

            if (typeof(T) == typeof(byte[]))
            {
                while (stream.Position + 4 < stream.Length)
                {
                    buff = new byte[4];
                    stream.Read(buff, 0, 4);
                    var length = BitConverter.ToInt32(buff, 0);
                    if (length == 0)
                    {
                        Data.Add(default);
                    }

                    buff = new byte[length];
                    stream.Read(buff, 0, length);

                    (Data as List<byte[]>).Add(buff);
                }
            }
            else if (typeof(T) == typeof(Guid))
            {
                while (stream.Position + 12 < stream.Length)
                {
                    buff = new byte[16];
                    stream.Read(buff, 0, 16);
                    (Data as List<Guid>).Add(new Guid(buff));
                }
            }
            else if (typeof(T) == typeof(string))
            {
                while (stream.Position + 4 < stream.Length)
                {
                    buff = new byte[4];
                    stream.Read(buff, 0, 4);
                    var len = BitConverter.ToInt32(buff, 0);
                    if (len > 0)
                    {
                        buff = new byte[len];
                        stream.Read(buff, 0, len);
                        (Data as List<string>).Add(System.Text.Encoding.UTF8.GetString(buff));
                    }
                }
            }
            else if (typeof(T).GetInterface("ICustomSerializer") != null)
            {
                while (stream.Position + 4 < stream.Length)
                {
                    var add = Activator.CreateInstance<T>();
                    object o = ((ICustomSerializer)add).ReadFromStream(stream, mediaType);
                    if (o != null)
                    {
                        Data.Add((T)o);
                    }
                    else
                    {
                        Data.Add(add);
                    }
                }
            }
            else
            {
                while (stream.Position + 4 < stream.Length)
                {
                    buff = new byte[4];
                    stream.Read(buff, 0, 4);
                    var length = BitConverter.ToInt32(buff, 0);
                    if (length == 0)
                    {
                        Data.Add(default);
                    }

                    buff = new byte[length];
                    stream.Read(buff, 0, length);

                    Data.Add(BsonSerializer.Deserialize<T>(buff));
                }
            }

            return null;
        }
    }
}