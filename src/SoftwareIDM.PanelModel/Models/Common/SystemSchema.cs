﻿using System;
using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SoftwareIDM.PanelModel.Attributes;

namespace SoftwareIDM.PanelModel.Models
{
    public class SchemaAttr
    {
        public string Name { get; set; }

        [BsonIgnoreIfDefault]
        public bool Multivalued { get; set; }

        [BsonIgnoreIfDefault]
        public bool Reference { get; set; }
        
        [BsonIgnoreIfDefault]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public bool Encrypted { get; set; }

        [BsonIgnoreIfDefault]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public bool ExportOnly { get; set; }

        [BsonIgnoreIfNull]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string ValueType { get; set; }
    }

    [ClassDoc("Object type schema description for MIM Test provider", Name = "Object Schema")]
    public class ObjectSchema
    {
        public string Name { get; set; }

        public List<string> Attributes { get; set; }

        public ObjectSchema()
        {
            Attributes = new List<string>();
        }
    }

    [BsonIgnoreExtraElements]
    [ClassDoc("Schema description for MIM Test provider", Name = "Provider Schema", RuleDoc = true)]
    public class SystemSchema
    {
        public Guid Id { get { return Helpers.NamedGuid.Create(Provider, System); } set { } }

        [MemberDoc("Identity Panel Provider Id (typically a test project)")]
        public Guid Provider { get; set; }

        [MemberDoc("Test System name")]
        public string System { get; set; }

        [BsonDictionaryOptions(Representation = MongoDB.Bson.Serialization.Options.DictionaryRepresentation.ArrayOfArrays)]
        public Dictionary<string, SchemaAttr> Attributes { get; set; }

        public List<ObjectSchema> ObjectTypes { get; set; }
        
        public CapabilitySettings Capabilities { get; set; }
        
        public SystemSchema()
        {
            Attributes = new Dictionary<string, SchemaAttr>();
            ObjectTypes = new List<ObjectSchema>();
        }
    }

    public class CapabilitySettings
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public ConnectorCapability Capabilities { get; set; } = ConnectorCapability.Read;

        [JsonConverter(typeof(StringEnumConverter))]
        public ConnectorDNStyle DistinguishedNameStyle { get; set; }

        public bool ExportPasswordInFirstPass { get; set; } = true;

        [JsonConverter(typeof(StringEnumConverter))]
        public ConnectorExportType ExportType { get; set; } = ConnectorExportType.AttributeUpdate;

        public bool IsDNAsAnchor { get; set; }

        public bool NoReferenceValuesInFirstExport { get; set; }

        public bool SupportPartitions { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public ConnectorNormalizations Normalizations { get; set; }

        public List<string> PossibleDNComponentsForProvisioning { get; set; }
    }

    [Flags]
    public enum ConnectorCapability
    {
        None = 0,
        Read = 1,
        Add = 2,
        Update = 4,
        Delete = 8,
        Rename = 16,
        Restore = 32
    }

    public enum ConnectorDNStyle
    {
        Generic = 1,
        Ldap = 2
    }

    public enum ConnectorExportType
    {
        AttributeUpdate = 1,
        AttributeReplace = 2,
        MultivaluedReferenceAttributeUpdate = 5,
        ObjectReplace = 3
    }

    public enum ConnectorNormalizations
    {
        None = 0,
        Uppercase = 1,
        RemoveAccents = 2
    }
}
