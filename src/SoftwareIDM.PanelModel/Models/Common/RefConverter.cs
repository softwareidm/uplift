﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using SoftwareIDM.PanelModel.Attributes;
using SoftwareIDM.PanelModel.Helpers;

namespace SoftwareIDM.PanelModel.Models
{
    /// <summary>
    /// The various ref converters are used to control JSON serialization of object references
    /// </summary>
    [ClassDoc("Converter for reference values")]
    public class ValueRef
    {
        public string href { get; set; }

        public ValueRef() { }

        public ValueRef(string url)
        {
            href = url;
        }
    }

    public abstract class RefConverter : JsonConverter
    {
        public abstract string Url(object id);

        public virtual object Parse(string href)
        {
            if (RegexHelper.GuidMatch.IsMatch(href))
            {
                return Guid.Parse(href);
            }
            else
            {
                var split = href.Split('/');
                var v = split[split.Length - 1].Split('?')[0];
                if (RegexHelper.GuidMatch.IsMatch(v))
                {
                    return Guid.Parse(v);
                }

                return v.FromUrlBase64();
            }
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value == null)
            {
                writer.WriteNull();
                return;
            }

            serializer.Serialize(writer, new ValueRef(Url(value)));
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.StartObject)
            {
                var vref = serializer.Deserialize<ValueRef>(reader);
                return Parse(vref.href);
            }
            else if (reader.TokenType == JsonToken.String)
            {
                return Parse(reader.Value.ToString());
            }
            else if (reader.TokenType == JsonToken.Integer)
            {
                return long.Parse(reader.Value.ToString());
            }

            return null;
        }

        public override bool CanRead
        {
            get { return true; }
        }

        public override bool CanConvert(Type objectType)
        {
            return true;
        }
    }

    public class ObjectRefConverter : RefConverter
    {
        public override string Url(object id)
        {
            if (id == null) { return null; }
            return string.Format("objects/" + ((Guid)id).ToString());
        }
    }

    public class HistoryRefConverter : RefConverter
    {
        public override string Url(object id)
        {
            if (id == null) { return null; }
            return string.Format("histories/" + ((Guid)id).ToString());
        }
    }

    public class MultiRefConverter : RefConverter
    {
        public override string Url(object id)
        {
            if (id == null) { return null; }
            return "multis/" + ((MongoDB.Bson.ObjectId)id).ToString();
        }
    }

    public class ErrorRefConverter : RefConverter
    {
        public override string Url(object id)
        {
            if (id == null) { return null; }
            return string.Format("errordetails/" + ((Guid)id).ToString());
        }
    }

    public class NoteRefConverter : RefConverter
    {
        public override string Url(object id)
        {
            if (id == null) { return null; }
            if (id is Guid gid)
            {
                return string.Format("objects/" + gid.ToString());
            }
            else if (id is MongoDB.Bson.ObjectId bid)
            {
                return "multis/" + bid.ToString();
            }

            return id.ToString();
        }
    }

    public class MultiConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value == null)
            {
                writer.WriteNull();
                return;
            }

            var multi = (MultiAttr)value;
            writer.WriteStartObject();
            writer.WritePropertyName("Id");
            writer.WriteValue(multi.Id);
            writer.WritePropertyName("Hash");
            writer.WriteValue(multi.Hash);
            writer.WritePropertyName("ObjectId");
            serializer.Serialize(writer, new ValueRef("objects/" + multi.ObjectId.ToString()));
            writer.WritePropertyName("Silo");
            writer.WriteValue(multi.Silo);
            writer.WritePropertyName("Attribute");
            writer.WriteValue(multi.Attribute);
            writer.WritePropertyName("Value");
            if (multi.IsRef.HasValue && multi.IsRef.Value)
            {
                serializer.Serialize(writer, new ValueRef("objects/" + multi.Value.ToString()));
                writer.WritePropertyName("IsRef");
                writer.WriteValue(multi.IsRef.Value);
            }
            else
            {
                writer.WriteValue(multi.Value);
            }

            if (multi.ValueExtended != null)
            {
                writer.WritePropertyName("ValueExtended");
                writer.WriteValue(multi.ValueExtended);
            }

            writer.WritePropertyName("TimeStamp");
            writer.WriteValue(multi.TimeStamp);
            writer.WritePropertyName("AddSource");
            serializer.Serialize(writer, new ValueRef("histories/" + multi.AddSource.ToString()));

            if (multi.Deleted.HasValue)
            {

                writer.WritePropertyName("Deleted");
                writer.WriteValue(multi.Deleted.Value);
                writer.WritePropertyName("DeleteSource");
                serializer.Serialize(writer, new ValueRef("histories/" + multi.DeleteSource.ToString()));
            }

            writer.WritePropertyName("Type");
            writer.WriteValue("MultiAttr");

            if (multi.Object != null)
            {
                writer.WritePropertyName("Object");
                serializer.Serialize(writer, multi.Object);
            }

            writer.WriteEndObject();
        }

        static Guid ReadRef(JsonReader reader, JsonSerializer serializer)
        {
            var v = serializer.Deserialize<ValueRef>(reader);
            if (v != null && v.href != null)
            {
                var split = v.href.Split('/');
                return Guid.Parse(split[split.Length - 1].Split('?')[0]);
            }

            return Guid.Empty;
        }

        readonly Dictionary<string, Action<MultiAttr, JsonReader, JsonSerializer>> _reads = new Dictionary<string, Action<MultiAttr, JsonReader, JsonSerializer>>
        {
            { "Id", (m, read, ser) => m.Id = MongoDB.Bson.ObjectId.Parse(ser.Deserialize<string>(read)) },
            { "Hash", (m, read, ser) => m.Hash = ser.Deserialize<byte[]>(read) },
            { "ObjectId", (m, read, ser) => m.ObjectId = ReadRef(read, ser) },
            { "Silo", (m, read, ser) => m.Silo = ser.Deserialize<string>(read) },
            { "Attribute", (m, read, ser) => m.Attribute = ser.Deserialize<string>(read) },
            { "Value", (m, read, ser) => m.Value = read.TokenType == JsonToken.StartObject ? ReadRef(read, ser) : ser.Deserialize(read) },
            { "ValueExtended", (m, read, ser) => m.ValueExtended = ser.Deserialize(read) },
            { "IsRef", (m, read, ser) => m.IsRef = ser.Deserialize<bool?>(read) },
            { "TimeStamp", (m, read, ser) => m.TimeStamp = ser.Deserialize<DateTime>(read) },
            { "AddSource", (m, read, ser) => m.AddSource = ReadRef(read, ser) },
            { "Deleted", (m, read, ser) => m.Deleted = ser.Deserialize<DateTime?>(read) },
            { "DeleteSource", (m, read, ser) => m.DeleteSource = ReadRef(read, ser) },
            { "Type", (m, read, ser) => ser.Deserialize<string>(read) },
            { "Object", (m, read, ser) => ser.Deserialize(read) }
        };

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null)
            {
                reader.Read();
                return null;
            }

            if (reader.TokenType == JsonToken.StartObject)
            {
                var ret = new MultiAttr();
                string name = null;
                while (reader.Read())
                {
                    if (reader.TokenType == JsonToken.PropertyName)
                    {
                        name = reader.Value.ToString();
                    }
                    else if (reader.TokenType == JsonToken.EndObject)
                    {
                        break;
                    }
                    else
                    {
                        _reads[name](ret, reader, serializer);
                    }
                }

                return ret;
            }

            return null;
        }

        public override bool CanRead
        {
            get { return true; }
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(MultiAttr);
        }
    }
}
