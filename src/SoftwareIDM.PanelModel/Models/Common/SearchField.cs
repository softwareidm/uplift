﻿using System;

namespace SoftwareIDM.PanelModel.Models
{
    /// <summary>
    /// Bson Model for storing information about an attribute, like for full text search or reporting.
    /// This isn't stored in the database, it's passed to the full text indexing helper, which converts it to a BsonField object
    /// and stores it in the FieldMap collection. The difference between BsonField and SphinxField is that 
    /// BsonField has a list of types instead of just one, and BsonField stors the name of the sphinx column the attribute goes in.
    /// </summary>
    public class SearchField : IEquatable<SearchField>
    {
        public string Name { get; set; }
        public string BsonPath { get; set; }
        public string Type { get; set; }

        public bool Equals(SearchField other)
        {
            return this.Name.Equals(other.Name);
        }

        /// <summary>
        /// Implement hashcode so it can be used as a Dictionary key
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as SearchField);
        }

        public override string ToString()
        {
            return $"{Name}-{BsonPath}-{Type}";
        }
    }
}