﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoftwareIDM.PanelModel.Models
{
    /// <summary>
    /// Special case exception that can be used to return a custom error message to the web application in lieu of a generic error.
    /// </summary>
    public class MessageError : Exception
    {
        public string AuthResource { get; set; }

        public bool NoLog { get; set; }

        public MessageError(string customMessage) : base(customMessage) { }

        public MessageError(string customMessage, bool noLog) : base(customMessage) { NoLog = noLog; }

        public MessageError(string customMessage, Exception e) : base(customMessage, e) { }

        public MessageError(string customMessage, string authResource, Exception e) : base(customMessage, e)
        {
            AuthResource = authResource;
        }
    }
}
