﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using Newtonsoft.Json;

namespace SoftwareIDM.PanelModel.Models
{
    public class ObjectIdConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value is ObjectId id)
            {
                writer.WriteValue(id.ToString());
            }
            else
            {
                writer.WriteNull();
            }
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.String)
            {
                return ObjectId.Parse(reader.Value.ToString());
            }
            else
            {
                reader.Read();
                return null;
            }
        }

        public override bool CanRead
        {
            get { return true; }
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(ObjectId);
        }
    }
}
