﻿using System;
using System.Collections.Generic;
using System.IO;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SoftwareIDM.PanelModel.Attributes;

namespace SoftwareIDM.PanelModel.Models
{
    [BsonIgnoreExtraElements]
    [BsonDiscriminator("ro", Required = true)]
    [ClassDoc("Role definition for grouping users based on job properties", "Access Role", RuleDoc = true)]
    public class AccessRole : AccessPrincipal
    {
        [BsonElement("o")]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public List<Guid> Owners { get; set; }

        [BsonElement("mo")]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public List<Guid> ManualOwners { get; set; }

        [BsonElement("io")]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public List<Guid> InheritedOwners { get; set; }

        [BsonElement("cr")]
        [BsonIgnoreIfNull]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public RoleCriteriaRule CriteriaRule { get; set; }

        [BsonElement("ho")]
        [BsonIgnoreIfDefault]
        public bool HierarchicalOwners { get; set; }

        [BsonElement("d")]
        public string Description { get; set; }

        [BsonElement("md")]
        [BsonIgnoreIfDefault]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string ManualDescription { get; set; }

        internal override void AppendHash(MemoryStream hashStream)
        {
            base.AppendHash(hashStream);
            if (HierarchicalOwners) { hashStream.WriteByte(1); }
            else { hashStream.WriteByte(0); }
        }

        internal override void DecodeData(MemoryStream dataStream)
        {
            base.DecodeData(dataStream);
            var ownCount = dataStream.ReadByte();
            if (ownCount > 0)
            {
                ManualOwners = new List<Guid>();
                for (int i = 0; i < ownCount; i++)
                {
                    var buff = new byte[16];
                    dataStream.Read(buff, 0, 16);
                    ManualOwners.Add(new Guid(buff));
                }
            }
        }

        internal override void AppendData(MemoryStream dataStream)
        {
            base.AppendData(dataStream);
            if (ManualOwners == null || ManualOwners.Count == 0)
            {
                dataStream.WriteByte(0);
            }
            else
            {
                dataStream.WriteByte((byte)ManualOwners.Count);
                foreach (var policy in ManualOwners)
                {
                    dataStream.Write(policy.ToByteArray(), 0, 16);
                }
            }
        }

        public override void CopyForward(object other)
        {
            base.CopyForward(other);
            var r = other as AccessRole;
            if (CriteriaRule == null)
            {
                CriteriaRule = r.CriteriaRule;
            }

            ManualDescription = r.ManualDescription;
            if (!string.IsNullOrEmpty(ManualDescription)) { Description = ManualDescription; }
            Owners = r.Owners;
            InheritedOwners = r.InheritedOwners;
        }
    }

    [ClassDoc("Criteria rule for roles", Name = "Role Criteria Rule")]
    public class RoleCriteriaRule
    {
        [BsonElement("pe")]
        public bool PermitExplicit { get; set; }

        [BsonElement("r")]
        public string Rule { get; set; }

        [BsonElement("d")]
        public string Description { get; set; }

        [BsonIgnore]
        public Rules.Rule RuleInstance
        {
            get
            {
                if (_rule == null)
                {
                    _rule = Rules.Rule.Build(Rule);
                }

                return _rule;
            }
        }

        Rules.Rule _rule;
    }

    [BsonIgnoreExtraElements]
    [ClassDoc("Access Panel role assignment represents the link between a principal and a role", Name = "Access Role Assignment", RuleDoc = true)]
    public class AccessAssignment
    {
        [JsonConverter(typeof(ObjectIdConverter))]
        public ObjectId Id { get; set; }

        [BsonElement("p")]
        public Guid PrincipalId { get; set; }

        [BsonElement("r")]
        public Guid RoleId { get; set; }

        [BsonElement("pn")]
        public string PrincipalName { get; set; }

        [BsonElement("rn")]
        public string RoleName { get; set; }

        [BsonElement("a")]
        public DateTime Added { get; set; }

        [BsonElement("d")]
        [BsonIgnoreIfNull]
        public DateTime? Deleted { get; set; }

        [BsonElement("c")]
        [JsonConverter(typeof(StringEnumConverter))]
        public AssignmentControl Control { get; set; }

        [BsonElement("pr")]
        [BsonIgnoreIfNull]
        public AccessPrincipal Principal { get; set; }

        [BsonElement("re")]
        [BsonIgnoreIfNull]
        public AccessRole Role { get; set; }

        [BsonElement("apa")]
        [BsonIgnoreIfNull]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<ObjectId> ApproveAttestations { get; set; }

        [BsonElement("rea")]
        [BsonIgnoreIfNull]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<ObjectId> RejectAttestations { get; set; }

        [BsonElement("exa")]
        [BsonIgnoreIfNull]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<ObjectId> ExpireAttestations { get; set; }

        // calculated on render so that browser knows how much UI to display for the user
        [BsonIgnore]
        [JsonConverter(typeof(StringEnumConverter))]
        public AccessLevel UserAccess { get; set; }
    }

    public enum AssignmentControl
    {
        Explicit, // explicit membership
        CriteriaComputed, // from a criteria rule
        Exclusive, // explicit exclusion to role or criteria based membership
    }
}
