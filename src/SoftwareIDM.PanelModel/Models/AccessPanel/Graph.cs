﻿using System;
using System.Collections.Generic;
using SoftwareIDM.PanelModel.Attributes;

namespace SoftwareIDM.PanelModel.Models
{
    [ClassDoc("Identity graph used for Access Panel principal object traversal. Index with Silo names or Ids.", Name = "Identity Graph", RuleDoc = true)]
    public class AccessPanelGraph
    {
        [MemberDoc(Ignore = true)]
        public IdentityGraph PanelGraph { get; set; }

        [MemberDoc("Root Object Record")]
        public ObjectRecord Root => PanelGraph.Root;

        readonly Dictionary<string, string> _siloNames;

        public AccessPanelGraph(IdentityGraph graph, Dictionary<string, string> siloNames)
        {
            PanelGraph = graph;
            _siloNames = siloNames;
        }

        [MemberDoc("Index into joined object records by Silo name or object Id")]
        public ObjectRecord this[string key]
        {
            get
            {
                ObjectRecord ret;
                if (_siloNames.TryGetValue(key, out var silo))
                {
                    PanelGraph.BySilo.TryGetValue(silo, out ret);
                }
                else if (!PanelGraph.BySilo.TryGetValue(key, out ret) && Guid.TryParse(key, out var id))
                {
                    PanelGraph.ById.TryGetValue(id, out ret);
                }

                return ret;
            }
        }

        public bool TryGetValue(string key, out ObjectRecord res)
        {
            if (PanelGraph.BySilo.TryGetValue(key, out res))
            {
                return true;
            }

            if (Guid.TryParse(key, out var id) && PanelGraph.ById.TryGetValue(id, out res))
            {
                return true;
            }

            return false;
        }
    }
}
