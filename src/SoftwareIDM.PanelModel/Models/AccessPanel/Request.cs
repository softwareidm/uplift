﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SoftwareIDM.PanelModel.Attributes;

namespace SoftwareIDM.PanelModel.Models
{
    [ClassDoc("State object for change request", Name = "Access Request", RuleDoc = true)]
    public class AccessRequest
    {
        [MemberDoc("Instance id of request")]
        [JsonConverter(typeof(ObjectIdConverter))]
        public ObjectId Id { get; set; }

        [MemberDoc("Id of Access Request campaign policy setting", Name = "Policy Id")]
        [BsonElement("p")]
        public Guid PolicyId { get; set; }

        [BsonElement("n")]
        public string Name { get; set; }

        [MemberDoc("This value is not persisted, so delivery action fixures that use it must use workflow memos")]
        [BsonIgnore]
        public string NameLabel { get; set; }

        [BsonElement("ac")]
        public string Action { get; set; }

        [MemberDoc("This value is not persisted, so delivery action fixures that use it must use workflow memos")]
        [BsonIgnore]
        public string ApproveLabel { get; set; }

        [MemberDoc("This value is not persisted, so delivery action fixures that use it must use workflow memos")]
        [BsonIgnore]
        public string RejectLabel { get; set; }

        [BsonIgnore]
        [MemberDoc("Number of requests batched into a single message. This value is not persisted.", Name = "Request Count")]
        public int RequestCount { get; set; }

        [BsonIgnoreIfDefault]
        [BsonElement("tid")]
        public Guid TargetId { get; set; }

        [BsonIgnoreIfNull]
        [BsonElement("tn")]
        public string TargetName { get; set; }

        [JsonIgnore]
        [BsonElement("ob")]
        [MemberDoc("Object context responsible for triggering the request", Name = "Object")]
        public object Object { get; set; }

        [BsonElement("oid")]
        [MemberDoc("Id of context object", Name = "Object Id")]
        public object ObjectId { get; set; }

        [BsonElement("ot")]
        [MemberDoc(".NET Type name of the context object", Name = "Object Type")]
        public string ObjectType { get; set; }

        [BsonElement("fd")]
        [BsonIgnoreIfNull]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public AttributeDictionary<object> FormData { get; set; }

        [BsonElement("rt")]
        [JsonIgnore]
        [MemberDoc(Ignore = true)]
        public string ResponseType { get; set; }

        [BsonElement("rr")]
        [MemberDoc("Access Panel ID of Requestor")]
        public Guid RequestorId { get; set; }

        [BsonElement("rn")]
        [MemberDoc("Display name of requester", Name = "Requestor Name")]
        public string RequestorName { get; set; }

        [BsonElement("ca")]
        [MemberDoc("Category request appears under")]
        public string Category { get; set; }

        [BsonElement("ts")]
        [MemberDoc("Timestamp for request dispatch")]
        public DateTime TimeStamp { get; set; }

        [BsonElement("cs")]
        [MemberDoc("Approval response threads for escalation")]
        public List<AccessRequestChain> Chains { get; set; } = new List<AccessRequestChain>();

        [BsonElement("s")]
        [MemberDoc("Current status of request")]
        [JsonConverter(typeof(StringEnumConverter))]
        public ResponseStatus Status { get; set; }

        [BsonElement("st")]
        [MemberDoc("Timestamp status was last modified", Name = "Status Time")]
        public DateTime StatusTime { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        [BsonIgnoreIfNull]
        [BsonElement("ne")]
        [MemberDoc("Timestamp next action needs to be taken on the request", Name = "Next")]
        public DateTime? Next { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        [BsonElement("re")]
        [MemberDoc("Result of request if completed")]
        public string Result { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        [BsonElement("err")]
        [MemberDoc("Request error details")]
        public string Error { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        [BsonElement("url")]
        [MemberDoc("URL to direct the user to after responding to a request")]
        public string RedirectUrl { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        [BsonIgnore]
        [MemberDoc("Full URL of approval link for current request chain. This value is not persisted, so delivery action fixures that use it must use workflow memos.", "Approve Link")]
        public string ApproveLink { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        [BsonIgnore]
        [MemberDoc("Full URL of rejection link for current request chain. This value is not persisted, so delivery action fixures that use it must use workflow memos.", "Reject Link")]
        public string RejectLink { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        [BsonIgnore]
        [MemberDoc("Path and query parameter of approval links for the request chains. This value is not persisted, so delivery action fixures that use it must use workflow memos.", "Approve Link Path")]
        public string ApproveLinkPath { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        [BsonIgnore]
        [MemberDoc("Path and query parameter of rejection links for the request chains. This value is not persisted, so delivery action fixures that use it must use workflow memos.", "Reject Link Path")]
        public string RejectLinkPath { get; set; }

        [BsonElement("r")]
        [MemberDoc("List of Access Panel IDs of all approval recipients to date")]
        public List<Guid> Recipients { get; set; } = new List<Guid>();

        [BsonElement("w")]
        [MemberDoc("Ids of communication workflows associated with the request")]
        public List<Guid> WorkflowIds { get; set; } = new List<Guid>();
    }

    [ClassDoc("Approval escalation chain associated with a request instance", Name = "Acess Request Chain")]
    public class AccessRequestChain
    {
        [BsonElement("n")]
        public string Name { get; set; }

        [BsonElement("cr")]
        [MemberDoc("Access Panel ID of most recently targeted recipients", Name = "Current Recipient")]
        public List<Guid> CurrentRecipients { get; set; }

        [BsonElement("i")]
        [MemberDoc("Current index into the chain of escalation messages (0-based)", Name = "Message Index")]
        public int MessageIndex { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        [BsonIgnoreIfNull]
        [BsonElement("e")]
        [MemberDoc("Expiration timestamp of the current level of expiration")]
        public DateTime? Expires { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        [BsonIgnoreIfNull]
        [BsonElement("q")]
        [MemberDoc("Timestamp the chain may be queued until prior to sending the first communication")]
        public DateTime? Queue { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        [BsonIgnoreIfNull]
        [BsonElement("re")]
        [MemberDoc("List of timestamps for sending reminders if the request has not been responded to")]
        public List<DateTime> Reminders { get; set; }

        [BsonElement("s")]
        [JsonConverter(typeof(StringEnumConverter))]
        public ResponseStatus Status { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        [BsonElement("rr")]
        [MemberDoc("Response on this request chain")]
        public AccessResponse Response { get; set; }
    }

    [ClassDoc("Response to an access request", Name = "Access Response")]
    public class AccessResponse
    {
        [BsonElement("s")]
        [JsonConverter(typeof(StringEnumConverter))]
        public ResponseStatus Status { get; set; }

        [BsonElement("r")]
        public Guid Responder { get; set; }

        [BsonElement("ts")]
        public DateTime TimeStamp { get; set; }
    }

    public enum ResponseStatus
    {
        Pending,
        Sent,
        Reminded,
        Escalated,
        Approved,
        Rejected,
        Expired,
        Failed
    }
}
