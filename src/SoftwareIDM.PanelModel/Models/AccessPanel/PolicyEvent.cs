﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SoftwareIDM.PanelModel.Attributes;

namespace SoftwareIDM.PanelModel.Models
{
    public enum PolicyAction
    {
        None,
        ActivateEntitlement,
        AddManager,
        AddOwner,
        AddPrincipal,
        AddEntitlement,
        AddRoleAssignment,
        AttributePolicyEvaluation,
        ChangePrincipalSettings,
        DeactivateEntitlement,
        DeactivateUser,
        DeletePrincipal,
        ExclusiveEntitlementViolation,
        ExpireEntitlement,
        ExtendEntitlement,
        EventPolicyEvaluation,
        FlowPolicyEvaluation,
        ReactivateUser,
        RemoveEntitlement,
        RemoveManager,
        RemoveOwner,
        RemoveRoleAssignment,
        RespondAttestation,
        RespondRequest,
        StatePolicyEvaluation,
        SubmitAccessForm,
        SubmitServiceForm,
        SubmitRequest,
        TriggerAttestation
    }

    [ClassDoc("Policy Event Action Enumeration")]
    public class AccessPanelSpecialValues : Rules.ISpecialValues
    {
        public string Name => "Access Policy Event Action";

        public Dictionary<string, object> Values(object callingContext)
        {
            var ret = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
            var names = Enum.GetNames(typeof(PolicyAction));
            for (int i = 0; i < names.Length; i++)
            {
                ret[names[i]] = (long)i;
            }

            return ret;
        }
    }

    [ClassDoc("Access Policy event record for recording historical policy trigger activity", Name = "Access Policy Event", RuleDoc = true)]
    public class AccessPolicyEvent
    {
        [BsonId]
        public ObjectId Id { get; set; }

        [BsonElement("a")]
        [JsonConverter(typeof(StringEnumConverter))]
        public PolicyAction Action { get; set; }

        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        [BsonIgnoreIfDefault]
        [BsonElement("wid")]
        public Guid WorkflowId { get; set; }

        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        [BsonIgnoreIfDefault]
        [BsonElement("pid")]
        public Guid PrincipalId { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        [BsonIgnoreIfNull]
        [BsonElement("pin")]
        public string PrincipalName { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        [BsonIgnoreIfNull]
        [BsonElement("eid")]
        public ObjectId EntitlementId { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        [BsonIgnoreIfNull]
        [BsonElement("aid")]
        public ObjectId AssignmentId { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        [BsonIgnoreIfNull]
        [BsonElement("rqid")]
        public ObjectId RequestId { get; set; }

        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        [BsonIgnoreIfDefault]
        [BsonElement("rid")]
        public Guid RelatedId { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        [BsonIgnoreIfNull]
        [BsonElement("rin")]
        public string RelatedName { get; set; }

        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        [BsonIgnoreIfDefault]
        [BsonElement("roid")]
        public Guid RequestorId { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        [BsonIgnoreIfNull]
        [BsonElement("ron")]
        public string RequestorName { get; set; }

        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        [BsonIgnoreIfDefault]
        [BsonElement("prid")]
        public Guid PolicyRuleId { get; set; }

        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        [BsonIgnoreIfDefault]
        [BsonElement("sfid")]
        public Guid ServiceFormId { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        [BsonIgnoreIfNull]
        [BsonElement("fod")]
        public AttributeDictionary<object> FormData { get; set; }

        [BsonElement("n")]
        public string Name { get; set; }

        [BsonElement("ts")]
        public DateTime TimeStamp { get; set; }
    }
}
