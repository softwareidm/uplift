﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SoftwareIDM.PanelModel.Attributes;

namespace SoftwareIDM.PanelModel.Models
{

    [BsonIgnoreExtraElements]
    [ClassDoc("Base class for access campaign, representing shared settings of various data review campaign types", Name = "Access Campaign", RuleDoc = true)]
    public class AccessCampaign
    {
        public Guid Id { get; set; }

        [BsonElement("n")]
        public string Name { get; set; }

        [BsonElement("d")]
        public string Description { get; set; }

        [MemberDoc("Policy reference for campaign review communications", Name = "Review Policy")]
        [BsonElement("p")]
        public Guid ReviewPolicy { get; set; }

        [MemberDoc("Frequency for campaign recurrence, may be Once, Rolling, Annual, Semiannual, Quarterly, or Monthly")]
        [BsonElement("f")]
        [JsonConverter(typeof(StringEnumConverter))]
        public CampaignFrequency Frequency { get; set; }

        [BsonElement("st")]
        public DateTime Start { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        [BsonIgnoreIfNull]
        [BsonElement("dts")]
        public DateTime? Deleted { get; set; }

        [BsonElement("o")]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public List<Guid> Owners { get; set; }

        // calculated on render so that browser knows how much UI to display for the user
        [BsonIgnore]
        [JsonConverter(typeof(StringEnumConverter))]
        public AccessLevel UserAccess { get; set; }

        [BsonIgnore]
        public List<AccessCampaignInstance> Instances { get; set; }
    }

    [BsonIgnoreExtraElements]
    [ClassDoc("Access review campaign for reviewing entitlement assignments", Name = "Access Entitlement Campaign")]
    [BsonDiscriminator("enc", Required = true)]
    public class EntitlementCampaign : AccessCampaign
    {
        [BsonElement("c")]
        [JsonProperty(ItemConverterType = typeof(StringEnumConverter))]
        [MemberDoc("List of entitlement control settings this campaign applies to")]
        public List<EntitlementControl> Control { get; set; }

        [BsonElement("r")]
        [JsonProperty(ItemConverterType = typeof(StringEnumConverter))]
        [MemberDoc("List of risk levels to include in review (inherited from the associated resources)")]
        public List<RiskLevel> Risk { get; set; }

        [BsonElement("aps")]
        [MemberDoc("List of applications to filter by in review")]
        public List<Guid> Applications { get; set; }

        [BsonElement("fr")]
        [MemberDoc("Filter rule applied to entitlement to determine if in scope. May index into Resource and Principal properties to filter by resource or user settings.")]
        public string FilterRule { get; set; }

        [BsonElement("rr")]
        [MemberDoc("Whether to remove an entitlement rejected in the review. Otherwise review results will simply be reported", Name = "Remove on Reject")]
        public bool RemoveOnReject { get; set; }

        [BsonElement("rt")]
        [MemberDoc("Whether to remove an entitlement if the campaign messages are not responded to. Otherwise review results will simply be reported", Name = "Remove on Timeout")]
        public bool RemoveOnTimeout { get; set; }
    }

    [ClassDoc("Access review campaign for reviewing role assignments", Name = "Access Role Assignment Campaign")]
    [BsonDiscriminator("roac", Required = true)]
    public class RoleAssignmentCampaign : AccessCampaign
    {
        [BsonElement("c")]
        [JsonProperty(ItemConverterType = typeof(StringEnumConverter))]
        [MemberDoc("List of entitlement control settings this campaign applies to")]
        public List<AssignmentControl> Control { get; set; }

        [BsonElement("fr")]
        [MemberDoc("Filter rule applied to assignment to determine if in scope. May index into Role and Principal properties to filter by role or user settings.")]
        public string FilterRule { get; set; }

        [BsonElement("rr")]
        [MemberDoc("Whether to remove an entitlement rejected in the review. Otherwise review results will simply be reported", Name = "Remove on Reject")]
        public bool RemoveOnReject { get; set; }

        [BsonElement("rt")]
        [MemberDoc("Whether to remove an entitlement if the campaign messages are not responded to. Otherwise review results will simply be reported", Name = "Remove on Timeout")]
        public bool RemoveOnTimeout { get; set; }
    }

    [BsonIgnoreExtraElements]
    [ClassDoc("Access review campaign for reviewing resource settings", Name = "Access Resource Campaign")]
    [BsonDiscriminator("rec", Required = true)]
    public class PrincipalCampaign : AccessCampaign
    {
        [BsonElement("fr")]
        [MemberDoc("Filter for determination of whether to include resource in campaign scope")]
        public string FilterRule { get; set; }

        [BsonElement("tb")]
        [MemberDoc("Which tab needs to be reviewed for the attestation")]
        public string Tab { get; set; }

        [BsonElement("pt")]
        [MemberDoc("Type of principal being attested to")]
        public string PrincipalType { get; set; }
    }

    public enum CampaignFrequency
    {
        Once,
        Rolling,
        Annual,
        Semiannual,
        Quarterly,
        Monthly
    }

    public class StatusCounts
    {
        public int Total { get; set; }
        public int Pending { get; set; }
        public int Sent { get; set; }
        public int Escalated { get; set; }
        public int Approved { get; set; }
        public int Rejected { get; set; }
        public int Expired { get; set; }
        public int Failed { get; set; }
    }

    [ClassDoc("Class for tracking the state of an access campaign", Name = "Access Campaign Instance", RuleDoc = true)]
    public class AccessCampaignInstance
    {
        public Guid Id { get; set; }

        public Guid Source { get; set; }

        public string Name { get; set; }

        public List<Guid> Owners { get; set; }

        [BsonIgnore]
        [JsonIgnore]
        [MemberDoc(Ignore = true)] // ignored to prevent loop in rule helper campaign -> settings -> instance...
        public AccessCampaign Settings { get; set; }

        public DateTime Start { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        [BsonIgnoreIfNull]
        public DateTime? End { get; set; }

        public bool Enrolling { get; set; }

        [BsonIgnore]
        public StatusCounts StatusCounts { get; set; }
    }

    [ClassDoc("Class for tracking the state of an access campaign message", Name = "Access Campaign Request", RuleDoc = true)]
    public class AccessCampaignRequest
    {
        public ObjectId Id { get; set; }

        [BsonElement("c")]
        public Guid CampaignId { get; set; }

        [BsonElement("ci")]
        public Guid CampaignInstanceId { get; set; }

        [BsonElement("p")]
        public Guid ReviewPolicyId { get; set; }

        [BsonElement("s")]
        [JsonConverter(typeof(StringEnumConverter))]
        public ResponseStatus Status { get; set; }

        [BsonElement("ca")]
        [BsonIgnoreIfNull]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public AccessCampaign Campaign { get; set; }

        [BsonElement("cai")]
        [BsonIgnoreIfNull]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public AccessCampaignInstance CampaignInstance { get; set; }

        [BsonIgnoreIfDefault]
        [BsonElement("rqid")]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public Guid RequestorId { get; set; }

        [BsonElement("rq")]
        [BsonIgnoreIfNull]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public AccessPrincipal Requestor { get; set; }

        [BsonIgnoreIfDefault]
        [BsonElement("eid")]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public ObjectId EntitlementId { get; set; }

        [BsonElement("e")]
        [BsonIgnoreIfNull]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public AccessEntitlement Entitlement { get; set; }

        [BsonElement("en")]
        [BsonIgnoreIfNull]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string EntitlementName { get; set; }

        [BsonIgnoreIfDefault]
        [BsonElement("amid")]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public ObjectId AssignmentId { get; set; }

        [BsonElement("am")]
        [BsonIgnoreIfNull]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public AccessAssignment Assignment { get; set; }

        [BsonElement("amn")]
        [BsonIgnoreIfNull]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string AssignmentName { get; set; }

        [BsonIgnoreIfDefault]
        [BsonElement("pid")]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public Guid PrincipalId { get; set; }

        [BsonElement("pr")]
        [BsonIgnoreIfNull]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public AccessPrincipal Principal { get; set; }

        [BsonElement("pn")]
        [BsonIgnoreIfNull]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string PrincipalName { get; set; }

        [BsonElement("ts")]
        public DateTime TimeStamp { get; set; }

        [BsonElement("cts")]
        [BsonIgnoreIfNull]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DateTime? Completed { get; set; }

        [BsonElement("rrids")]
        [BsonIgnoreIfNull]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<Guid> ResponderIds { get; set; }

        [BsonElement("res")]
        [BsonIgnoreIfNull]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Result { get; set; }

        [BsonElement("r")]
        [BsonIgnoreIfNull]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ObjectId CommunicationsRequestId { get; set; }

        [BsonElement("wids")]
        [BsonIgnoreIfNull]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<Guid> WorkflowIds { get; set; }
    }
}
