﻿using System;
using System.Collections.Generic;
using System.IO;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SoftwareIDM.PanelModel.Attributes;

namespace SoftwareIDM.PanelModel.Models
{
    public enum RiskLevel
    {
        None,
        Low,
        Medium,
        High,
        Critical
    }

    public enum SelfAction
    {
        None,
        Join,
        Leave,
        RequestJoin,
        RequestLeave
    }

    public enum AccessLevel
    {
        None,
        Read,
        Operate,
        Admin
    }

    public enum EntitlementControl
    {
        Explicit, // explicit membership
        RoleComputed, // from a role assignment
        CriteriaComputed, // from a criteria rule
        ExternalCriteria, // criteria based in owning system, e.g. portal or azure dynamic group
        CriteriaCandidate, // candidate membership derived from criteria
        ExplicitCandidate, // explicit candidate membership
        RoleCandidate, // from a role assignment
        Exclusive, // explicit exclusion to role or criteria based membership
        Inherited // inherited entitlement from membership in resource that's a member
    }

    [BsonDiscriminator("r", Required = true)]
    [BsonIgnoreExtraElements]
    [ClassDoc("Access Panel resource object represents a permissioned entity, e.g. an application role or group.", Name = "Access Resource", RuleDoc = true)]
    public class AccessResource : AccessPrincipal
    {
        internal override void AppendData(MemoryStream dataStream)
        {
            base.AppendData(dataStream);
            dataStream.Write(BitConverter.GetBytes(ManualDefaultExpires.Ticks), 0, 8);
            if (ManualExtensionRequestPolicies == null || ManualExtensionRequestPolicies.Count == 0)
            {
                dataStream.WriteByte(0);
            }
            else
            {
                dataStream.WriteByte((byte)ManualExtensionRequestPolicies.Count);
                foreach (var policy in ManualExtensionRequestPolicies)
                {
                    dataStream.Write(policy.ToByteArray(), 0, 16);
                }
            }

            if (ManualOwners == null || ManualOwners.Count == 0)
            {
                dataStream.WriteByte(0);
            }
            else
            {
                dataStream.WriteByte((byte)ManualOwners.Count);
                foreach (var policy in ManualOwners)
                {
                    dataStream.Write(policy.ToByteArray(), 0, 16);
                }
            }

            if (Applications == null || Applications.Count == 0)
            {
                dataStream.WriteByte(0);
            }
            else
            {
                dataStream.WriteByte((byte)Applications.Count);
                foreach (var app in Applications)
                {
                    dataStream.Write(app.ToByteArray(), 0, 16);
                }
            }

            if (CriteriaRule != null)
            {
                var bson = CriteriaRule.ToBson();
                dataStream.Write(BitConverter.GetBytes((ushort)bson.Length), 0, 2);
                dataStream.Write(bson, 0, bson.Length);
            }
            else
            {
                dataStream.Write(BitConverter.GetBytes((ushort)0), 0, 2);
            }
        }

        internal override void DecodeData(MemoryStream dataStream)
        {
            base.DecodeData(dataStream);
            var buff = new byte[8];
            dataStream.Read(buff, 0, 8);
            ManualDefaultExpires = new TimeSpan(BitConverter.ToInt64(buff, 0));
            if (ManualDefaultExpires != default)
            {
                DefaultExpires = ManualDefaultExpires;
                AssignExpiration = true;
            }

            var extCount = dataStream.ReadByte();
            if (extCount > 0)
            {
                ManualExtensionRequestPolicies = new List<Guid>();
                for (int i = 0; i < extCount; i++)
                {
                    buff = new byte[16];
                    dataStream.Read(buff, 0, 16);
                    ManualExtensionRequestPolicies.Add(new Guid(buff));
                }
            }

            var ownCount = dataStream.ReadByte();
            if (ownCount > 0)
            {
                ManualOwners = new List<Guid>();
                for (int i = 0; i < ownCount; i++)
                {
                    buff = new byte[16];
                    dataStream.Read(buff, 0, 16);
                    ManualOwners.Add(new Guid(buff));
                }
            }

            var appCount = dataStream.ReadByte();
            if (appCount > 0)
            {
                Applications = new List<Guid>();
                for (int i = 0; i < appCount; i++)
                {
                    buff = new byte[16];
                    dataStream.Read(buff, 0, 16);
                    Applications.Add(new Guid(buff));
                }
            }

            buff = new byte[2];
            dataStream.Read(buff, 0, 2);
            var length = BitConverter.ToUInt16(buff, 0);
            if (length > 0)
            {
                buff = new byte[length];
                dataStream.Read(buff, 0, length);
                CriteriaRule = buff.FromBson<AccessCriteriaRule>(new PortableAttributeMapper(new PortableNameMap()));
            }
        }

        public override void CopyForward(object other)
        {
            base.CopyForward(other);
            var r = other as AccessResource;
            ManualDescription = r.ManualDescription;
            if (!string.IsNullOrEmpty(ManualDescription)) { Description = ManualDescription; }
            ManualRisk = r.ManualRisk;
            if (ManualRisk > RiskLevel.None) { Risk = ManualRisk; }
            ManualResourceRequestPolicy = r.ManualResourceRequestPolicy;
            if (ManualResourceRequestPolicy != default) { ResourceRequestPolicy = ManualResourceRequestPolicy; }
            ManualActivationRequestPolicy = r.ManualActivationRequestPolicy;
            if (ManualActivationRequestPolicy != default) { ActivationRequestPolicy = ManualActivationRequestPolicy; }
            ManualActivationCandidateOnly = r.ManualActivationCandidateOnly;
            if (ManualActivationCandidateOnly.HasValue) { ActivationCandidateOnly = ManualActivationCandidateOnly.Value; }

            Applications = r.Applications;
            Owners = r.Owners;
            InheritedOwners = r.InheritedOwners;
        }

        [BsonElement("d")]
        public string Description { get; set; }

        [BsonElement("e")]
        public string Email { get; set; }

        [BsonElement("md")]
        [BsonIgnoreIfDefault]
        public string ManualDescription { get; set; }

        [BsonElement("r")]
        [JsonConverter(typeof(StringEnumConverter))]
        public RiskLevel Risk { get; set; }

        [BsonElement("mr")]
        [BsonIgnoreIfDefault]
        [JsonConverter(typeof(StringEnumConverter))]
        public RiskLevel ManualRisk { get; set; }

        [BsonElement("ec")]
        [BsonIgnoreIfDefault]
        public bool ExternalCriteria { get; set; }

        [BsonElement("ae")]
        [BsonIgnoreIfDefault]
        public bool AssignExpiration { get; set; }

        [BsonElement("kda")]
        [BsonIgnoreIfDefault]
        public bool RetainDeactivated { get; set; }

        [BsonElement("de")]
        [BsonIgnoreIfDefault]
        public TimeSpan DefaultExpires { get; set; }

        [BsonIgnore]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public TimeSpan MaxExpires { get; set; }

        [BsonIgnore]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public TimeSpan MinExpires { get; set; }

        [BsonElement("mde")]
        [BsonIgnoreIfDefault]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public TimeSpan ManualDefaultExpires { get; set; }

        [BsonElement("rrp")]
        [BsonIgnoreIfDefault]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public Guid ResourceRequestPolicy { get; set; }

        [BsonElement("mrrp")]
        [BsonIgnoreIfDefault]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public Guid ManualResourceRequestPolicy { get; set; }

        [BsonElement("erp")]
        [BsonIgnoreIfDefault]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public List<Guid> ExtensionRequestPolicies { get; set; }

        [BsonElement("merp")]
        [BsonIgnoreIfDefault]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public List<Guid> ManualExtensionRequestPolicies { get; set; }

        [BsonElement("arp")]
        [BsonIgnoreIfDefault]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public Guid ActivationRequestPolicy { get; set; }

        [BsonElement("marp")]
        [BsonIgnoreIfDefault]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public Guid ManualActivationRequestPolicy { get; set; }

        [BsonElement("aco")]
        [BsonIgnoreIfDefault]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public bool ActivationCandidateOnly { get; set; }

        [BsonElement("maco")]
        [BsonIgnoreIfDefault]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public bool? ManualActivationCandidateOnly { get; set; }

        [BsonElement("cr")]
        [BsonIgnoreIfNull]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public AccessCriteriaRule CriteriaRule { get; set; }

        [BsonElement("ai")]
        [BsonIgnoreIfDefault]
        public List<Guid> Applications { get; set; }

        [BsonElement("o")]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public List<Guid> Owners { get; set; }

        [BsonElement("mo")]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public List<Guid> ManualOwners { get; set; }

        [BsonElement("io")]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public List<Guid> InheritedOwners { get; set; }

        [BsonElement("ho")]
        [BsonIgnoreIfDefault]
        public bool HierarchicalOwners { get; set; }

        [BsonIgnore]
        [JsonConverter(typeof(StringEnumConverter))]
        public SelfAction SelfAction { get; set; }
    }

    [ClassDoc("Criteria Rule for Resource")]
    public class AccessCriteriaRule
    {
        [BsonElement("cc")]
        public bool Candidate { get; set; }

        [BsonElement("pe")]
        public bool PermitExplicit { get; set; }

        [BsonElement("ro")]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public List<Guid> Roles { get; set; }

        [BsonElement("ero")]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public List<Guid> ExclusiveRoles { get; set; }

        [BsonElement("r")]
        public string Rule { get; set; }

        [BsonElement("d")]
        public string Description { get; set; }

        [BsonElement("rt")]
        public int RemovalThreshold { get; set; }

        [BsonIgnore]
        public Rules.Rule RuleInstance
        {
            get
            {
                if (_rule == null)
                {
                    _rule = Rules.Rule.Build(Rule);
                }

                return _rule;
            }
        }

        Rules.Rule _rule;
    }

    [ClassDoc("Extension request history for entitlement with expiry", Name = "Extension Request")]
    public class ExtensionRequest
    {
        [BsonElement("p")]
        public Guid Policy { get; set; }

        [BsonElement("r")]
        public DateTime Requested { get; set; }

        [BsonElement("e")]
        public DateTime Expires { get; set; }
    }

    [BsonIgnoreExtraElements]
    [ClassDoc("Access Panel entitlement represents the link between a principal and a resource", Name = "Access Entitlement", RuleDoc = true)]
    public class AccessEntitlement
    {
        [JsonConverter(typeof(ObjectIdConverter))]
        public ObjectId Id { get; set; }

        [BsonElement("s")]
        public Guid Source { get; set; }

        [BsonElement("p")]
        public Guid PrincipalId { get; set; }

        [BsonElement("r")]
        public Guid ResourceId { get; set; }

        [BsonElement("i")]
        [BsonIgnoreIfNull]
        public Guid? Inherited { get; set; }

        [BsonElement("pn")]
        public string PrincipalName { get; set; }

        [BsonElement("rn")]
        public string ResourceName { get; set; }

        [BsonElement("in")]
        [BsonIgnoreIfNull]
        public string InheritName { get; set; }

        [BsonElement("ft")]
        [BsonIgnoreIfDefault]
        public bool Future { get; set; }

        [BsonElement("a")]
        public DateTime Added { get; set; }

        [BsonElement("dn")]
        [BsonIgnoreIfNull]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DateTime? Deleting { get; set; }

        [BsonElement("dr")]
        [BsonIgnoreIfNull]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DateTime? DeleteRequested { get; set; }

        [BsonElement("e")]
        [BsonIgnoreIfNull]
        public DateTime? Expires { get; set; }

        [BsonElement("ae")]
        [BsonIgnoreIfNull]
        public DateTime? ActivationExpires { get; set; }

        [BsonElement("c")]
        [JsonConverter(typeof(StringEnumConverter))]
        public EntitlementControl Control { get; set; }

        [BsonElement("er")]
        [BsonIgnoreIfNull]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public List<ExtensionRequest> ExtensionRequests { get; set; }

        // mapped to bson element so it transmits through workflow queue
        [BsonElement("pr")]
        [BsonIgnoreIfNull]
        public AccessPrincipal Principal { get; set; }

        // mapped to bson element so it transmits through workflow queue
        [BsonElement("re")]
        [BsonIgnoreIfNull]
        public AccessResource Resource { get; set; }

        [BsonElement("inh")]
        [BsonIgnoreIfNull]
        public AccessResource Inherit { get; set; }

        // calculated on render so that browser knows how much UI to display for the user
        [BsonIgnore]
        [JsonConverter(typeof(StringEnumConverter))]
        public AccessLevel UserAccess { get; set; }

        [BsonIgnore]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public TimeSpan? MinActivation { get; set; }

        [BsonIgnore]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public TimeSpan? MaxActivation { get; set; }

        [BsonElement("apa")]
        [BsonIgnoreIfNull]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<ObjectId> ApproveAttestations { get; set; }

        [BsonElement("rea")]
        [BsonIgnoreIfNull]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<ObjectId> RejectAttestations { get; set; }

        [BsonElement("exa")]
        [BsonIgnoreIfNull]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<ObjectId> ExpireAttestations { get; set; }
    }
    public enum EntitlementAction
    {
        Add,
        Change,
        Delete,
        Activate,
        Deactivate
    }

    [ClassDoc("Set of changes to make to entitlements", RuleDoc = true, Name = "Entitlement Change")]
    public class EntitlementChange
    {
        [JsonConverter(typeof(ObjectIdConverter))]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ObjectId Id { get; set; }

        [BsonElement("ts")]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public DateTime TimeStamp { get; set; }

        [BsonElement("e")]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(ObjectIdConverter))]
        public ObjectId EntitlementId { get; set; }

        [BsonElement("ent")]
        [BsonIgnoreIfNull]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public AccessEntitlement Entitlement { get; set; }

        [BsonElement("rr")]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public Guid RequestorId { get; set; }

        [BsonElement("rn")]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string RequestorName { get; set; }

        [BsonElement("r")]
        public Guid ResourceId { get; set; }

        [BsonElement("p")]
        public Guid PrincipalId { get; set; }

        [BsonIgnoreIfDefault]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        [BsonElement("irq")]
        public bool IsRequest { get; set; }

        [BsonElement("rrn")]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string ResourceName { get; set; }

        [BsonElement("pn")]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string PrincipalName { get; set; }

        [BsonElement("c")]
        [JsonConverter(typeof(StringEnumConverter))]
        public EntitlementControl Control { get; set; }

        [BsonElement("a")]
        [JsonConverter(typeof(StringEnumConverter))]
        public EntitlementAction Action { get; set; }

        [BsonElement("fod")]
        [BsonIgnoreIfNull]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public AttributeDictionary<object> FormData { get; set; }

        [BsonElement("st")]
        [BsonIgnoreIfNull]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DateTime? Start { get; set; }

        [BsonElement("ex")]
        [BsonIgnoreIfNull]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DateTime? Expires { get; set; }

        [BsonElement("act")]
        [BsonIgnoreIfNull]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public TimeSpan? Activate { get; set; }

        [BsonElement("res")]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Result { get; set; }

        [BsonElement("wid")]
        [BsonIgnoreIfDefault]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public Guid WorkflowId { get; set; }
    }
}
