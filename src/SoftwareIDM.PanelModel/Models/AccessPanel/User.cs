﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using SoftwareIDM.PanelModel.Attributes;
using SoftwareIDM.PanelModel.Rules;

namespace SoftwareIDM.PanelModel.Models
{

    public class DisplayDetails
    {
        public string DisplayName { get; set; }

        public bool DetailLink { get; set; }

        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public bool Reference { get; set; }

        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public bool BreakAll { get; set; }

        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public bool ShowIfNull { get; set; }

        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public bool AllowHtml { get; set; }

        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public int PageSize { get; set; }

        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string ReferenceMode { get; set; }

        public string Tooltip { get; set; }

        public DisplayDetail Before { get; set; }

        public DisplayDetail Replace { get; set; }

        public DisplayDetail After { get; set; }
    }

    public class DisplayDetail
    {
        public List<string> ViewModes { get; set; }

        public string Label { get; set; }

        public string Url { get; set; }

        public string UrlTarget { get; set; }

        public string Photo { get; set; }

        public string PhotoSize { get; set; }

        public string Icon { get; set; }

        public string IconColor { get; set; }
    }

    [BsonDiscriminator("p", Required = true)]
    [ClassDoc("Access Principal representing user, resource, or application in Access Panel", Name = "Access Principal", RuleDoc = true)]
    public class AccessPrincipal
    {
        public AccessPrincipal() { }

        public Guid Id { get; set; }

        [BsonElement("sids")]
        [MemberDoc("List of Object Ids that feed into this user object")]
        public List<Guid> SourceIds { get; set; }

        [BsonElement("sls")]
        [MemberDoc("List of object ids by silo")]
        public Dictionary<string, Guid> BySilo { get; set; }

        [BsonElement("prts")]
        [MemberDoc("Timestamp of policy rule application")]
        public Dictionary<string, DateTime> PolicyRuleTimestamps { get; set; }

        [BsonElement("src")]
        [MemberDoc("User silo definition used to create this user")]
        public Guid Source { get; set; }

        [BsonElement("n")]
        public string DisplayName { get; set; }

        [BsonElement("ct")]
        public DateTime Created { get; set; }

        [BsonElement("mt")]
        public DateTime Modified { get; set; }

        [BsonElement("a")]
        public AttributeDictionary<object> Attributes { get; set; } = new AttributeDictionary<object>();

        [BsonElement("dt")]
        public AttributeDictionary<DisplayDetails> Details { get; set; } = new AttributeDictionary<DisplayDetails>();

        [BsonElement("dfp")]
        [BsonIgnoreIfDefault]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public Guid DataFormPolicy { get; set; }

        [BsonElement("mdfp")]
        [BsonIgnoreIfDefault]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public Guid ManualDataFormPolicy { get; set; }

        [BsonElement("fd")]
        [BsonIgnoreIfDefault]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public AttributeDictionary<object> Data { get; set; }

        // calculated on render so that browser knows how much UI to display for the user
        [BsonIgnore]
        [JsonConverter(typeof(Newtonsoft.Json.Converters.StringEnumConverter))]
        public AccessLevel UserAccess { get; set; }

        [BsonElement("au")]
        [BsonIgnoreIfNull]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public List<string> AudienceValues { get; set; }

        [BsonElement("pd")]
        [JsonIgnore]
        public byte[] ProcessingData { get; set; }

        [BsonIgnore]
        [JsonIgnore]
        public byte[] DataHash => _dataHash;
        byte[] _dataHash;

        [BsonElement("rh")]
        [JsonIgnore]
        public byte[] ReferenceHash { get; set; }

        [BsonIgnore]
        [JsonProperty]
        public string Type => GetType().Name;

        public virtual void DecodeProcessingData()
        {
            using var dataStream = new MemoryStream(ProcessingData);
            DecodeData(dataStream);
        }

        public void EncodeProcessingData()
        {
            using var dataStream = new MemoryStream();
            AppendData(dataStream);
            ProcessingData = dataStream.ToArray();
        }

        internal virtual void AppendData(MemoryStream dataStream)
        {
            dataStream.Write(Id.ToByteArray(), 0, 16);
            using var hashStream = new MemoryStream();
            AppendHash(hashStream);
            _dataHash = Helpers.Hash.Get(hashStream.ToArray());
            dataStream.Write(_dataHash, 0, 16);
        }

        internal virtual void DecodeData(MemoryStream dataStream)
        {
            var buffer = new byte[16];
            dataStream.Read(buffer, 0, 16);
            Id = new Guid(buffer);
            _dataHash = new byte[16];
            dataStream.Read(_dataHash, 0, 16);
        }

        internal virtual void AppendHash(MemoryStream hashStream)
        {
            hashStream.Write(Id.ToByteArray(), 0, 16);
            hashStream.Write(Source.ToByteArray(), 0, 16);
            foreach (var src in SourceIds)
            {
                hashStream.Write(src.ToByteArray(), 0, 16);
            }

            var attr = Attributes.ToBson();
            hashStream.Write(attr, 0, attr.Length);
            var det = Details.ToBson();
            hashStream.Write(det, 0, attr.Length);
            foreach (var v in AudienceValues)
            {
                var a = Encoding.UTF8.GetBytes(v);
                hashStream.Write(a, 0, a.Length);
            }
        }

        public virtual void CopyForward(object other)
        {
            var acp = other as AccessPrincipal;
            ManualDataFormPolicy = acp.ManualDataFormPolicy;
            Created = acp.Created;
            PolicyRuleTimestamps = acp.PolicyRuleTimestamps;
        }
    }

    [BsonIgnoreExtraElements]
    [BsonDiscriminator("u")]
    [ClassDoc("Access Panel user object", Name = "Access User", RuleDoc = true)]
    public class AccessUser : AccessPrincipal
    {
        internal override void AppendHash(MemoryStream hashStream)
        {
            base.AppendHash(hashStream);
            var cv = Encoding.UTF8.GetBytes(ClaimValue ?? "");
            hashStream.Write(cv, 0, cv.Length);
        }

        internal override void AppendData(MemoryStream dataStream)
        {
            base.AppendData(dataStream);
            // need manual recovery account
            dataStream.WriteByte((ManualRecoveryAccount.HasValue && ManualRecoveryAccount.Value) ? (byte)1 : (byte)0);
            // need deactivated date
            dataStream.WriteByte(Deactivated.HasValue ? (byte)1 : (byte)0);
            if (Deactivated.HasValue)
            {
                dataStream.Write(BitConverter.GetBytes(Deactivated.Value.ToBinary()), 0, 8);
            }
        }

        internal override void DecodeData(MemoryStream dataStream)
        {
            base.DecodeData(dataStream);
            if (dataStream.ReadByte() > 0)
            {
                ManualRecoveryAccount = true;
            }

            if (dataStream.ReadByte() > 0)
            {
                var buffer = new byte[8];
                dataStream.Read(buffer, 0, 8);
                Deactivated = DateTime.FromBinary(BitConverter.ToInt64(buffer, 0));
            }
        }

        public override void CopyForward(object other)
        {
            base.CopyForward(other);
            var u = other as AccessUser;
            ManualPreferredLanguage = u.ManualPreferredLanguage;
            if (!string.IsNullOrEmpty(ManualPreferredLanguage)) { PreferredLanguage = ManualPreferredLanguage; }
            ManualRecoveryAccount = u.ManualRecoveryAccount;
            if (ManualRecoveryAccount.HasValue) { RecoveryAccount = ManualRecoveryAccount.Value; }
            ManualSecondaryAccount = u.ManualSecondaryAccount;
            if (ManualSecondaryAccount.HasValue) { SecondaryAccount = ManualSecondaryAccount.Value; }

            Manager = u.Manager;
            InheritedManagers = u.InheritedManagers;
            DirectReports = u.DirectReports;
            DirectOwnerOf = u.DirectOwnerOf;
        }

        [BsonElement("c")]
        [BsonIgnoreIfNull]
        public string ClaimValue { get; set; }

        [BsonIgnore]
        [JsonIgnore]
        public bool SelfOperate { get; set; }

        [BsonElement("t")]
        public string Title { get; set; }

        [BsonElement("mo")]
        public string Mobile { get; set; }

        [BsonElement("e")]
        public string Email { get; set; }

        [BsonElement("l")]
        public string PreferredLanguage { get; set; }

        [BsonElement("mpl")]
        public string ManualPreferredLanguage { get; set; }

        [BsonIgnoreIfDefault]
        [BsonElement("ra")]
        public bool RecoveryAccount { get; set; }

        [BsonIgnoreIfNull]
        [BsonElement("mra")]
        public bool? ManualRecoveryAccount { get; set; }

        [BsonIgnoreIfDefault]
        [BsonElement("sa")]
        public bool SecondaryAccount { get; set; }

        [BsonIgnoreIfNull]
        [BsonElement("msa")]
        public bool? ManualSecondaryAccount { get; set; }

        // simplifies audience calculation if manager and owner have the same property
        [BsonElement("o")]
        [BsonIgnoreIfDefault]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public Guid Manager { get; set; }

        [BsonElement("io")]
        [BsonIgnoreIfDefault]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public List<Guid> InheritedManagers { get; set; }

        [BsonElement("dr")]
        [BsonIgnoreIfDefault]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public List<Guid> DirectReports { get; set; }

        [BsonElement("dof")]
        [BsonIgnoreIfDefault]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public List<Guid> DirectOwnerOf { get; set; }

        [BsonElement("tts")]
        [BsonIgnoreIfNull]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public DateTime? Deactivated { get; set; }

        [BsonIgnore]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public bool HasCandidates { get; set; }
    }
}
