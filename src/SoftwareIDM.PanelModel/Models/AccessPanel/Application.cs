﻿using System;
using System.Collections.Generic;
using System.IO;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SoftwareIDM.PanelModel.Attributes;

namespace SoftwareIDM.PanelModel.Models
{
    [BsonIgnoreExtraElements]
    [BsonDiscriminator("a", Required = true)]
    [ClassDoc("Application definition for grouping resources based on how they're used", "Access Application", RuleDoc = true)]
    public class AccessApplication : AccessPrincipal
    {
        [BsonElement("o")]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public List<Guid> Owners { get; set; }

        [BsonElement("mo")]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public List<Guid> ManualOwners { get; set; }

        [BsonElement("io")]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public List<Guid> InheritedOwners { get; set; }

        [BsonElement("re")]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public List<Guid> Resources { get; set; }

        [BsonElement("ho")]
        [BsonIgnoreIfDefault]
        public bool HierarchicalOwners { get; set; }

        [BsonElement("d")]
        public string Description { get; set; }

        [BsonElement("md")]
        [BsonIgnoreIfDefault]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string ManualDescription { get; set; }

        [BsonElement("r")]
        [JsonConverter(typeof(StringEnumConverter))]
        public RiskLevel Risk { get; set; }

        [BsonElement("mr")]
        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public RiskLevel ManualRisk { get; set; }

        internal override void AppendHash(MemoryStream hashStream)
        {
            base.AppendHash(hashStream);
            if (HierarchicalOwners) { hashStream.WriteByte(1); }
            else { hashStream.WriteByte(0); }
        }

        internal override void AppendData(MemoryStream dataStream)
        {
            base.AppendData(dataStream);
            if (ManualOwners == null || ManualOwners.Count == 0)
            {
                dataStream.WriteByte(0);
            }
            else
            {
                dataStream.WriteByte((byte)ManualOwners.Count);
                foreach (var policy in ManualOwners)
                {
                    dataStream.Write(policy.ToByteArray(), 0, 16);
                }
            }
        }

        internal override void DecodeData(MemoryStream dataStream)
        {
            base.DecodeData(dataStream);

            var ownCount = dataStream.ReadByte();
            if (ownCount > 0)
            {
                ManualOwners = new List<Guid>();
                for (int i = 0; i < ownCount; i++)
                {
                    var buff = new byte[16];
                    dataStream.Read(buff, 0, 16);
                    ManualOwners.Add(new Guid(buff));
                }
            }
        }

        public override void CopyForward(object other)
        {
            base.CopyForward(other);
            var a = other as AccessApplication;
            ManualDescription = a.ManualDescription;
            if (!string.IsNullOrEmpty(ManualDescription)) { Description = ManualDescription; }
            Owners = a.Owners;
            InheritedOwners = a.InheritedOwners;
            Resources = a.Resources;
            ManualRisk = a.ManualRisk;
            if (ManualRisk > RiskLevel.None)
            {
                Risk = ManualRisk;
            }
        }
    }
}
