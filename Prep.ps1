$path = @(
	'SoftwareIDM.Uplift',
	'SoftwareIDM.UpliftMA',
	'SoftwareIDM.UpliftEngine',
	'SoftwareIDM.MAExtension',
	'SoftwareIDM.MVExtension',
	'SoftwareIDM.PanelModel'
)

$path | ForEach-Object { Copy-Item '.\src\SharedAssemblyInfo.cs' ".\src\$_\Properties" }